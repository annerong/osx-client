//
//  PermListEntry.m
//  HipChat
//
//  Created by Christopher Rivers on 1/8/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "PermListEntry.h"
#import "User.h"
#import "XMPP.h"

@implementation PermListEntry

+ (PermListEntry *)permListEntryWithUser:(User *)user {
    PermListEntry *ple = [[PermListEntry alloc] init];
    ple.jid = user.jid;
    ple.name = user.name;
    ple.selected = NO;
    ple.enabled = YES;

    return ple;
}

+ (PermListEntry *)permListEntryWithDiscoItem:(NSXMLElement *)item {
    PermListEntry *ple = [[PermListEntry alloc] init];

    XMPPJID *roomJid = [XMPPJID jidWithString:[item attributeStringValueForName:@"jid"]];
    NSXMLElement *mucUser = [item elementForName:@"x" xmlns:@"http://jabber.org/protocol/muc#user"];
    NSXMLElement *roomItem = [mucUser elementForName:@"item"];
    NSString *affiliation = [roomItem attributeStringValueForName:@"affiliation"];

    ple.jid = [XMPPJID jidWithString:[roomItem attributeStringValueForName:@"jid"]];

    // Cannot edit perms on the owner of the room
    ple.enabled = ([affiliation isEqualToString:OWNER_AFFILIATION] ? NO : YES);
    if (ple.enabled) {
        ple.name = roomJid.resource;
    } else {
        ple.name = [NSString stringWithFormat:@"%@ (owner)", roomJid.resource];
    }
    ple.selected = NO;

    return ple;
}

@end
