//
//  PermListEntry.h
//  HipChat
//
//  Created by Christopher Rivers on 1/8/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class User;
@class XMPPJID;

@interface PermListEntry : NSObject

+ (PermListEntry *)permListEntryWithUser:(User *)user;
+ (PermListEntry *)permListEntryWithDiscoItem:(NSXMLElement *)item;

@property BOOL enabled;
@property XMPPJID *jid;
@property NSString *name;
@property BOOL selected;

@end
