//
//  TabCellView.h
//  HipChat
//
//  Created by Christopher Rivers on 10/19/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "TableCellHighlightView.h"

@class HoverButton;

@interface TabCellView : TableCellHighlightView

@property (readwrite, assign) IBOutlet id delegate;
@property IBOutlet HoverButton *closeButton;

@end
