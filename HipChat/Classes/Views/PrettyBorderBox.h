//
//  PrettyBorderBox.h
//  HipChat
//
//  Created by Christopher Rivers on 11/15/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface PrettyBorderBox : NSBox

@property (readwrite, retain) NSColor *topBorderColor;
@property (readwrite, retain) NSColor *topBorderInsetColor;

@property (readwrite, retain) NSColor *rightBorderColor;
@property (readwrite, retain) NSColor *rightBorderInsetColor;

@property (readwrite, retain) NSColor *bottomBorderColor;
@property (readwrite, retain) NSColor *bottomBorderInsetColor;

@property (readwrite, retain) NSColor *leftBorderColor;
@property (readwrite, retain) NSColor *leftBorderInsetColor;

@end
