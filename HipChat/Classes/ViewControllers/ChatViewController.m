//
//  ChatViewController.m
//  HipChat
//
//  Created by Christopher Rivers on 10/18/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "ChatViewController.h"

#import <WebKit/WebKit.h>

#import "Chat.h"
#import "ChatHelpers.h"
#import "ChatStateMessage.h"
#import "HipChatUser.h"
#import "RegexKitLite.h"
#import "Room.h"
#import "RoomOccupant.h"
#import "User.h"
#import "XMPPFramework.h"

#import <MagicKit/MagicKit.h>

#import "AutoCompleteTextView.h"
#import "EmoticonMenuViewController.h"
#import "HipChatOSXApp.h"
#import "HipChatTextView.h"
#import "HipChatWebView.h"
#import "ImagePreviewViewController.h"
#import "MentionItem.h"
#import "OSXHelpers.h"
#import "PrettyBorderBox.h"

@implementation ChatViewController

@dynamic atAllMention;
@dynamic atHereMention;

static const NSInteger INITIAL_HISTORY_REQUEST_COUNT = 60;
static const NSInteger MAX_MESSAGE_HISTORY = 15;
static const NSInteger MINIMUM_FONT_SIZE = 9;
static const NSInteger PREVIOUS_HISTORY_REQUEST_COUNT = 75;

static NSPopover *emoticonPopover;
static EmoticonMenuViewController *emVC;
static WebPreferences *chatPrefs;

static MentionItem *atAllMention;
static MentionItem *atHereMention;

+ (void)initialize {
    NSUserDefaults *userPrefs = [NSUserDefaults standardUserDefaults];
    chatPrefs = [[WebPreferences alloc] initWithIdentifier:@"chatPreferences"];
    NSNumber *defaultFontSize = [userPrefs objectForKey:@"defaultFontSize"];
    [chatPrefs setDefaultFontSize:[defaultFontSize intValue]];
    [chatPrefs setDefaultFixedFontSize:[defaultFontSize intValue]];
    [chatPrefs setMinimumFontSize:MINIMUM_FONT_SIZE];
}

+ (BOOL)isSelectorExcludedFromWebScript:(SEL)aSelector {
    if (aSelector == @selector(chatBodyClicked:) ||
        aSelector == @selector(getNameForMention:) ||
        aSelector == @selector(logJavascriptError:)) return NO;
    return YES;
}

- (void)awakeFromNib {
    [self.webView.mainFrame loadHTMLString:[ChatHelpers initialHTML] baseURL:[[NSBundle mainBundle] resourceURL]];
    self.webView.frameLoadDelegate = self;
    self.webView.resourceLoadDelegate = self;
    self.webView.policyDelegate = self;
    self.webView.editingDelegate = self;
    self.webView.UIDelegate = self;
    self.webView.preferencesIdentifier = @"chatPreferences";
    webScrollView = [[[[self.webView mainFrame] frameView] documentView] enclosingScrollView];
    [[self.webView windowScriptObject] setValue:self forKey:@"osxConnector"];

    [[[NSWorkspace sharedWorkspace] notificationCenter] addObserver:self
                                                           selector:@selector(onAppDeactivate:)
                                                               name:NSWorkspaceDidDeactivateApplicationNotification
                                                             object:nil];

    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self
               selector:@selector(onChatDragDrop:)
                   name:hcWebViewDropNotification
                 object:self.webView];

    // The contentView (ClipView) bounds will change when scrolls happen
    [center addObserver:self
               selector:@selector(webViewBoundsDidChange:)
                   name:NSViewBoundsDidChangeNotification
                 object:[webScrollView contentView]];

    self.textWrapper.cornerRadius = 0;
    self.messageInput.textContainerInset = NSSizeFromCGSize(CGSizeMake(0, 3));
    [self.messageInput setRichText:NO];
    [self.messageInput setFont:[NSFont fontWithName:@"Lucida Grande" size:chatPrefs.defaultFontSize]];
    [[NSFontManager sharedFontManager] setEnabled:NO];

    self.bottomBox.cornerRadius = 0;
    [self.bottomBox addSubview:self.uploadView];

    [self.view addObserver:self forKeyPath:@"isHidden" options:NSKeyValueObservingOptionNew context:nil];
    viewLoaded = YES;
}

- (id)initWithJid:(XMPPJID *)initJid {
    return [self initWithJid:initJid initialMessage:nil];
}

- (id)initWithJid:(XMPPJID *)initJid initialMessage:(XMPPMessage *)initialMessage {
    self = [super initWithNibName:@"ChatView" bundle:[NSBundle mainBundle]];
    if (self) {
        chat = [[Chat alloc] initWithJid:initJid delegate:self];
        if (initialMessage) {
            DDLogInfo(@"Setting initial message for chat (%@): %@", initJid, initialMessage);
            chat.initialMessage = initialMessage;
        }
        self.jid = initJid;

        [HipChatOSXApp.instance addDelegate:self];

        currentMessage = nil;
        currentMessageIndex = -1;
        fullHistoryLoaded = NO;
        executedJavascripts = [NSMutableArray array];
        isChatDividerShown = NO;
        javascripts = [NSMutableArray array];
        outgoingChatState = ACTIVE_CHAT_STATE;
        previousMessages = [NSMutableArray array];
        requiredExtension = nil;
        uploadFile = nil;
        uploadImage = nil;
        webViewLoaded = NO;

        initialized = YES;
        viewLoaded = NO;
    }

    return self;
}

- (void)close {
    [self deinitialize];
    [chat close];
    chat.delegate = nil;
    chat = nil;

    [HipChatOSXApp.instance removeDelegateImmediate:self];

    if (viewLoaded) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] removeObserver:self];
            [[[NSWorkspace sharedWorkspace] notificationCenter] removeObserver:self];
            [self.view removeObserver:self forKeyPath:@"isHidden"];
            self.webView.frameLoadDelegate = nil;
            self.webView.resourceLoadDelegate = nil;
            self.webView.policyDelegate = nil;
            self.webView.editingDelegate = nil;
            [self.webView.mainFrame loadHTMLString:@"" baseURL:nil];
            [self.webView close];
        });
    }
}

- (void)dealloc {
    DDLogInfo(@"Deallocating CVC: %@", self.jid);
}

- (void)deinitialize {
    if (!initialized) {
        return;
    }

    DDLogInfo(@"Deinitializing ChatViewController: %@", self.jid);
    fullHistoryLoaded = NO;
    executedJavascripts = [NSMutableArray array];
    javascripts = [NSMutableArray array];
    webViewLoaded = NO;
    [self hideHistorySpinner];
    [self hideUploadProgress];
    initialized = NO;

    [chat deinitialize];
}

- (MentionItem *)atAllMention {
    if (!atAllMention) {
        atAllMention = [[MentionItem alloc] init];
        atAllMention.label = [[NSMutableAttributedString alloc] initWithString:@"All room members (@all)"];
        atAllMention.mention = @"all";
        atAllMention.icon = nil;
        atAllMention.isFirstNameMatch = NO;
        atAllMention.isInRoom = YES;
        atAllMention.isMentionMatch = NO;
        atAllMention.isSelf = NO;
        atAllMention.isMatch = YES;
    }

    return atAllMention;
}

- (MentionItem *)atHereMention {
    if (!atHereMention) {
        atHereMention = [[MentionItem alloc] init];
        atHereMention.label = [[NSMutableAttributedString alloc] initWithString:@"Available room members (@here)"];
        atHereMention.mention = @"here";
        atHereMention.icon = nil;
        atHereMention.isFirstNameMatch = NO;
        atHereMention.isInRoom = YES;
        atHereMention.isMentionMatch = NO;
        atHereMention.isSelf = NO;
        atHereMention.isMatch = YES;
    }

    return atHereMention;
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark HipChatApp delegate methods
///////////////////////////////////////////////////////////////////////

- (void)hipChatAppFullyLoaded {
    // Set us back to initialized whenever the app is fully loaded
    initialized = YES;
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Chat Delegate Methods
///////////////////////////////////////////////////////////////////////

- (void)chatDidAddText:(Chat *)sender {
//    [self performSelectorOnMainThread:@selector(checkScroll) withObject:nil waitUntilDone:NO];
}

- (void)chatDidUpdateAutocomplete:(Chat *)sender {
//    // Only change UI / reload data on the main thread
//    dispatch_async(dispatch_get_main_queue(), ^{
//        if ([chat.autocompleteMatches count] > 0) {
//            autocompleteView.hidden = NO;
//            [autocompleteView reloadData];
//        } else {
//            autocompleteView.hidden = YES;
//        }
//    });
}

- (void)chatFailedToRequestHistory:(Chat *)sender {
//    // The alert handler will close the chat when the user presses "OK"
//    UIAlertView *alert =
//    [[UIAlertView alloc] initWithTitle:@"Unable to load chat"
//                               message:@"A problem occurred trying to load chat history"
//                              delegate:self
//                     cancelButtonTitle:@"OK"
//                     otherButtonTitles:nil];
//    [alert setTag:kCloseChatAlert];
//    [alert show];
//    
//    [self showHistoryButton];
}

- (void)chatFailedToUploadFile:(Chat *)sender {
    [self hideUploadProgress];
}

- (void)chatFinishedExecutingPreload:(Chat *)sender {
    [self hideHistorySpinner];

    DDLogInfo(@"Chat finished executing preload - try executing javascripts - %@", self.jid);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self executeJavascripts];
    });
}

- (void)chatFinishedLoadingHistory:(Chat *)sender {
    [self hideHistorySpinner];

    if (chat.currentAnchor) {
        [self scrollChatToAnchor:chat.currentAnchor];
    }
}

- (void)chatFinishedUploadingFile:(Chat *)sender {
    [self hideUploadProgress];
}

- (void)chatLoadedFullHistory:(Chat *)sender {
    [self hideHistorySpinner];
    fullHistoryLoaded = YES;
}

- (BOOL)chatShouldLoadHistory:(Chat *)sender {
    return YES;
}

- (void)chatWillLoadHistory:(Chat *)sender {

}

- (void)chatWillRefresh:(Chat *)sender {
    dispatch_block_t block = ^{
        // Clear out the policy delegate first so this HTML load doesn't open in the browser
        self.webView.policyDelegate = nil;
        [self.webView.mainFrame loadHTMLString:[ChatHelpers initialHTML] baseURL:[[NSBundle mainBundle] resourceURL]];
        self.webView.policyDelegate = self;

        [self showHistorySpinner];
    };

    if ([NSThread isMainThread]) {
        block();
    } else {
        dispatch_async(dispatch_get_main_queue(), block);
    }
}

- (void)chat:(Chat *)sender didAddJavascript:(NSString *)javascript {
	if (chat.executingPreload) {
		[javascripts addObject:javascript];
	} else {
      [self handleJavascript:javascript];
	}
}

- (void)chat:(Chat *)sender didReceiveMessage:(XMPPMessage *)message {
    // If this is the current visible chat and the app is active, don't add any dividers
    if (viewLoaded && !self.view.isHidden && [NSApp isActive]) {
        return;
    }

    // Dividers are not added for history messages
    if (message.delayedDeliveryDate ||
        [[message elementForName:@"body"] stringValue].length == 0 ||
        [message attributeStringValueForName:@"id"]) {
        return;
    }

    // Show a chat divider if we haven't shown one already
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if (!isChatDividerShown && [prefs boolForKey:@"showUnreadDivider"]) {
        [chat addChatDivider];
        isChatDividerShown = YES;
    }
}

- (void)chat:(Chat *)sender receivedErrorPresence:(XMPPPresence *)presence {
//	NSXMLElement *errorNode = [[presence elementForName:@"x" xmlns:@"http://jabber.org/protocol/muc"]
//							   elementForName:@"error"];
//	if (errorNode && [errorNode elementForName:@"item-not-found"]) {
//		// We will close the room in the alert handler once the user presses "OK"
//		UIAlertView *alert =
//		[[UIAlertView alloc] initWithTitle:@"Room not found"
//								   message:@"This room no longer exists"
//								  delegate:self
//						 cancelButtonTitle:@"OK"
//						 otherButtonTitles:nil];
//		[alert setTag:kCloseChatAlert];
//		[alert show];
//		[alert release];
//	}
}

- (void)chat:(Chat *)sender wasDestroyedWithNode:(NSXMLElement *)destroyNode {
//	// Display an alert that the room has been deleted
//	// Don't set a delegate, since this chat will automatically close
//	// and become deallocated by the application
//    Room *room = [HipChatApp.instance getRoomInfo:jid];
//	UIAlertView *alert =
//	[[UIAlertView alloc] initWithTitle:@"Room deleted"
//							   message:[NSString stringWithFormat:@"The room '%@' no longer exists: %@",
//										room.name,
//										[[destroyNode elementForName:@"reason"] stringValue]]
//							  delegate:nil
//					 cancelButtonTitle:@"OK"
//					 otherButtonTitles:nil];
//	[alert setTag:kCloseChatAlert];
//	[alert show];
//	[alert release];
//	if ([self isViewLoaded] && self.view.window) {
//		[self.navigationController popViewControllerAnimated:TRUE];
//	}
}

- (NSInteger)numberOfHistoryStanzasToRequestForChat:(Chat *)sender {
    return INITIAL_HISTORY_REQUEST_COUNT;
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark WebView Delegate Methods / Notification Handlers
///////////////////////////////////////////////////////////////////////

// This is called by the javascript via the osxConnector property
- (void)chatBodyClicked:(NSString *)selection {
    if (chatClickTimer) {
        [chatClickTimer invalidate];
        chatClickTimer = nil;
    }
    chatClickTimer = [NSTimer scheduledTimerWithTimeInterval:0.3
                                                      target:self
                                                    selector:@selector(checkChatSelection)
                                                    userInfo:nil
                                                     repeats:NO];
}

- (void)logJavascriptError:(NSString *)errorText {
    DDLogInfo(@"JS ERROR: JID: %@ -- %@", self.jid, errorText);
}

// Potential default menu items in web view
//enum {

//};
- (NSArray *)webView:(WebView *)sender contextMenuItemsForElement:(NSDictionary *)element defaultMenuItems:(NSArray *)defaultMenuItems {
    for (NSMenuItem *mi in defaultMenuItems) {
        BOOL shouldBeDisplayed = YES;
        NSURL *url;
        switch (mi.tag) {
            // Menu options that we'll handle specifically
            case WebMenuItemTagDownloadImageToDisk:
                [mi setAction:@selector(handleContextMenuClick:)];
                [mi setTarget:self];
                break;
            case WebMenuItemTagDownloadLinkToDisk:
                url = [mi.representedObject objectForKey:WebElementLinkURLKey];
                if ([url.absoluteString rangeOfRegex:@"\\.\\w{2,4}$"].location != NSNotFound) {
                    [mi setAction:@selector(handleContextMenuClick:)];
                    [mi setTarget:self];
                }
                break;
            // Menu options that are handled automatically by the WebView
            case WebMenuItemTagCopyLinkToClipboard:
            case WebMenuItemTagCopy:
                break;
            // Menu options that we want to ignore
            case WebMenuItemTagOpenLinkInNewWindow:
            case WebMenuItemTagOpenImageInNewWindow:
            case WebMenuItemTagCopyImageToClipboard:
            case WebMenuItemTagOpenFrameInNewWindow:
            case WebMenuItemTagGoBack:
            case WebMenuItemTagGoForward:
            case WebMenuItemTagStop:
            case WebMenuItemTagReload:
            case WebMenuItemTagCut:
            case WebMenuItemTagPaste:
            case WebMenuItemTagSpellingGuess:
            case WebMenuItemTagNoGuessesFound:
            case WebMenuItemTagIgnoreSpelling:
            case WebMenuItemTagLearnSpelling:
            case WebMenuItemTagOther:
            case WebMenuItemTagSearchInSpotlight:
            case WebMenuItemTagSearchWeb:
            case WebMenuItemTagLookUpInDictionary:
            case WebMenuItemTagOpenWithDefaultApplication:
            case WebMenuItemPDFActualSize:
            case WebMenuItemPDFZoomIn:
            case WebMenuItemPDFZoomOut:
            case WebMenuItemPDFAutoSize:
            case WebMenuItemPDFSinglePage:
            case WebMenuItemPDFFacingPages:
            case WebMenuItemPDFContinuous:
            case WebMenuItemPDFNextPage:
            case WebMenuItemPDFPreviousPage:
            case 0:
                shouldBeDisplayed = NO;
                break;
            default:
                break;
        }
        [mi setHidden:!shouldBeDisplayed];
    }
    return defaultMenuItems;
}

- (void)handleContextMenuClick:(NSMenuItem *)menuItem {
    NSURL *downloadURL = nil;
    NSURL *linkURL = nil;
    NSString *name = nil;

    switch (menuItem.tag) {
        case WebMenuItemTagDownloadImageToDisk:
            downloadURL = [menuItem.representedObject objectForKey:WebElementImageURLKey];
            linkURL = [menuItem.representedObject objectForKey:WebElementLinkURLKey];
            if (linkURL) {
                name = [OSXHelpers getFileNameFromPath:linkURL.path];
            }
            break;
        case WebMenuItemTagDownloadLinkToDisk:
            downloadURL = [menuItem.representedObject objectForKey:WebElementLinkURLKey];
            break;
        default:
            break;
    }

    if (downloadURL) {
        // If name is nil, it will use the URL to generate the name
        [OSXHelpers saveFileToDisk:downloadURL withName:name window:self.view.window];
    }
}

- (void)webView:(WebView *)sender
    resource:(id)identifier
    didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
    fromDataSource:(WebDataSource *)dataSource {
    // Just cancel the attempt at auth
    [[challenge sender] cancelAuthenticationChallenge:challenge];
}

- (void)webView:(WebView *)webView
    decidePolicyForNavigationAction:(NSDictionary *)actionInformation
    request:(NSURLRequest *)request
    frame:(WebFrame *)frame
    decisionListener:(id < WebPolicyDecisionListener >)listener {

	NSString *requestString = [[request URL] absoluteString];
	NSArray *components = [requestString componentsSeparatedByString:@":"];
	if ([requestString rangeOfString:@"action"].location == 0 && [components count] > 1) {
		if ([[components objectAtIndex:1] isEqualToString:@"load_history"]) {
            if (![chat requestPreviousHistoryWithCount:PREVIOUS_HISTORY_REQUEST_COUNT]) {
                return;
            }
            [self showHistorySpinner];
		} else if ([[components objectAtIndex:1] isEqualToString:@"resend"]) {
			if ([components count] < 3) {
				DDLogError(@"Got resend URL click but no message id available");
			} else {
                [chat resendUnconfirmedMessage:[components objectAtIndex:2]];
            }
		} else if ([[components objectAtIndex:1] isEqualToString:@"mention"]) {
			if ([components count] < 3) {
				DDLogError(@"Got mention URL click but no jid available");
			} else {
                User *userInfo = [[HipChatApp instance] getUserInfo:[XMPPJID jidWithString:[components objectAtIndex:2]]];
                if (userInfo.mentionName) {
                    [self.messageInput appendMentionToMessage:userInfo.mentionName];
                } else {
                    DDLogError(@"No mention name available for %@", [components objectAtIndex:2]);
                }
            }
		}
        return;
	}

    [[NSWorkspace sharedWorkspace] openURL:[request URL]];
}

- (void)webViewBoundsDidChange:(NSNotification *)notif {
    // Don't trigger a previous history request before we're loaded
    if (!webViewLoaded || !chat.finishedExecutingPreload) {
        return;
    }

    NSRect docFrame = [[webScrollView documentView] frame];
    NSRect contentBounds = [[webScrollView contentView] bounds];

    NSPoint currentScrollPosition = contentBounds.origin;
    CGFloat yAtBottom = NSMaxY(docFrame) - NSHeight(contentBounds);

    if (currentScrollPosition.y == yAtBottom) {
        isScrollAtBottom = YES;
    } else {
        isScrollAtBottom = NO;
        if (chat.historyLoaded &&
            !fullHistoryLoaded &&
            currentScrollPosition.y == 0 &&
            docFrame.size.height > webScrollView.frame.size.height) {
            if ([chat requestPreviousHistoryWithCount:PREVIOUS_HISTORY_REQUEST_COUNT]) {
                [self showHistorySpinner];
            }
        }
    }
}

- (void)webViewFrameDidChange:(NSNotification *)notif {
    [self checkScroll];
}

- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame *)frame {
    if (frame == self.webView.mainFrame) {
        // Reset the osxConnector object whenever the frame loads
        [[self.webView windowScriptObject] setValue:self forKey:@"osxConnector"];

        // Add this observer after the frame is loaded. It seems that it gets invalidated
        // if we reload the HTML on the main frame (which we do if we get disconnected
        // after viewing a chat) so we can't load it in awakeFromNib
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        // The documentView (content) frame will change when chat html is added
        [center addObserver: self
                   selector: @selector(webViewFrameDidChange:)
                       name: NSViewFrameDidChangeNotification
                     object: [webScrollView documentView]];

        webViewLoaded = YES;
        DDLogInfo(@"Web view finished loading - try executing javascripts - %@", self.jid);
        [self executeJavascripts];
    }
}

- (BOOL)webView:(WebView *)webView doCommandBySelector:(SEL)selector {
    if (selector == @selector(copy:)) {
        [self handleWebViewCopy];
        return YES;
    }
    return NO;
}

- (void)onAppDeactivate:(NSNotification *)notif {
    // We receive this event every  time *any* app loses foreground status
    NSString *activeAppId = [[[notif userInfo] objectForKey:NSWorkspaceApplicationKey] bundleIdentifier];
    if (![activeAppId isEqualToString:[[NSBundle mainBundle] bundleIdentifier]]) {
        // Do nothing if it's not our app being deactivated
        return;
    }

    // Reset the chat divider if this chat is currently being show
    if (isChatDividerShown && !self.view.isHidden) {
        [chat removeChatDivider];
        isChatDividerShown = NO;
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Other methods
///////////////////////////////////////////////////////////////////////

- (void)checkChatSelection {
    NSString *javascript = @"getSelectedHTML()";
    NSString *sel = [self.webView stringByEvaluatingJavaScriptFromString:javascript];
    if (!sel || [sel length] == 0) {
        [self focusMessageInput];
    }

    chatClickTimer = nil;
}

- (void)executeJavascripts {
    if (!webViewLoaded) {
        DDLogInfo(@"Web view hasn't loaded - not executing javascripts - %@", self.jid);
        return;
    }
    if (!chat.finishedExecutingPreload) {
        DDLogInfo(@"Chat has not finished preloading - not executing javascripts - %@", self.jid);
        return;
    }

    DDLogInfo(@"Executing %ld javascript calls - %@", [javascripts count], self.jid);
//    DDLogInfo(@"JAVASCRIPTS: %@", [javascripts componentsJoinedByString:@";"]);
    [self runJavascript:[javascripts componentsJoinedByString:@";"]];
	[javascripts removeAllObjects];
    [chat scrollChat];
}

- (NSMutableArray *)generateMentionList {
    if (![OSXHelpers isRoomJid:self.jid]) {
        return nil;
    }
    HipChatOSXApp *app = HipChatOSXApp.instance;
    NSString *currentUserBareJid = app.currentUser.jid.bare;
    Room *room = [app getRoomInfo:self.jid];
    NSMutableArray *mentionList = [NSMutableArray array];
    NSMutableDictionary *occupants = room.occupants;
    if (room.isPublic) {
        NSDictionary *users = [app getAllUsers];
        for (id key in users) {
            User *user = [users objectForKey:key];
            MentionItem *item = [[MentionItem alloc] initWithUser:user];
            if ([occupants objectForKey:user.jid.bare]) {
                item.isInRoom = YES;
                item.isSelf = [user.jid.bare isEqualToString:currentUserBareJid];
            }
            [mentionList addObject:item];
        }
        // Add guests as well
        for (NSString *key in occupants) {
            RoomOccupant *occupant = [occupants objectForKey:key];
            if (![occupant.role isEqualToString:VISITOR_ROLE]) {
                continue;
            }
            MentionItem *item = [[MentionItem alloc] initWithOccupant:occupant];
            item.isInRoom = YES;
            item.isSelf = [occupant.jid.bare isEqualToString:currentUserBareJid];
            [mentionList addObject:item];
        }
    } else {
        for (NSString *key in occupants) {
            RoomOccupant *occupant = [occupants objectForKey:key];
            MentionItem *item = [[MentionItem alloc] initWithOccupant:occupant];
            item.isInRoom = YES;
            item.isSelf = [occupant.jid.bare isEqualToString:currentUserBareJid];
            [mentionList addObject:item];
        }
    }
    [mentionList addObject:self.atAllMention];
    [mentionList addObject:self.atHereMention];
    return mentionList;
}

- (NSString *)getMessageInputString {
    // Remove the "line separator" unicode character and replace it with a regular newline
    return [self.messageInput.textStorage.string stringByReplacingOccurrencesOfString:@"\u2028" withString:@"\n"];
}

- (NSString *)getNameForMention:(NSString *)mentionString {
    if (mentionString.length < 2 || [mentionString characterAtIndex:0] != '@') {
        return @"";
    }

    NSString *mention = [mentionString substringFromIndex:1];
    NSDictionary *users = [HipChatOSXApp.instance getAllUsers];
    NSSet *userKeys = [users keysOfEntriesPassingTest:^BOOL(id key, id obj, BOOL *stop) {
        if ([[obj mentionName] isEqualToString:mention]) {
            *stop = YES;
            return YES;
        }
        return NO;
    }];
    if (userKeys.count == 0) {
        return @"";
    }
    return [[users objectForKey:[userKeys anyObject]] name];
}

- (void)handleFileNameEndEditing:(NSNotification *)notification {
    NSString *fileName = [self.fileNameInput stringValue];
    NSString *enteredExtension = [OSXHelpers getFileExtension:fileName];
    if ([requiredExtension caseInsensitiveCompare:enteredExtension] != NSOrderedSame) {
        self.uploadFileNameInput = [NSString stringWithFormat:@"%@.%@",
                                    self.fileNameInput.stringValue,
                                    requiredExtension];
    }
    [self updateFileNameText];
    [self.fileNameInput setHidden:YES];
    [self.fileNameButton setHidden:NO];
    [self focusMessageInput:YES];
}

- (void)handleJavascript:(NSString *)javascript {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self runJavascript:javascript];
    });
}

- (void)handleMessageInputEndEditing:(NSNotification *)notification {
    NSInteger code = [[[notification userInfo] objectForKey:@"NSTextMovement"] intValue];
    if (self.messageInput.showingAutocomplete && (code == NSReturnTextMovement || code == NSTabTextMovement)) {
        [self focusMessageInput:YES];
        [self.messageInput hideAutoComplete];
        return;
    }

    // Always hide autocomplete when the text input loses focus for any reason
    [self.messageInput hideAutoComplete];
    if (code == NSReturnTextMovement) {
        [self sendMessage];
    }
}

- (void)handleWebViewCopy {
    // Get markup - replace images with their alt tags if available
    NSString *markup = [[self.webView selectedDOMRange] markupString];
    // Replace emoticons with alt tags (this is probably just emoticons)
    markup = [markup stringByReplacingOccurrencesOfRegex:@"<img[^>]+name=['\"]emoticon['\"][^>]+alt=['\"](\\S+)['\"][^>]*/?>" withString:@"$1"];
    // Replace any images with alt tags with the alt text (afaik this never happens, ignoring for now...)
    //        markup = [markup stringByReplacingOccurrencesOfRegex:@"<img[^>]+alt=['\"](\\S+)['\"][^>]*/?>" withString:@"[image: $1]"];
    // Remove any image used in our chat display (image dropdown arrow, file type icons, etc)
    markup = [markup stringByReplacingOccurrencesOfRegex:@"<img[^>]+src=['\"](\\S+arrow_drawer\\S+)['\"][^>]*/?>" withString:@""];
    markup = [markup stringByReplacingOccurrencesOfRegex:@"<img[^>]+src=['\"](\\S+/img/filetype_icons/\\S+)['\"][^>]*/?>" withString:@""];
    // Replace other image tags with their src urls
    markup = [markup stringByReplacingOccurrencesOfRegex:@"<img[^>]+src=['\"](\\S+)['\"][^>]*/?>" withString:@"[image: $1]"];
    // Show date dividers with an extra newline above and below and bracketed
    markup = [markup stringByReplacingOccurrencesOfRegex:@"<div[^>]+class=['\"]dateDivider['\"][^>]*>([^<]+)</div>" withString:@"<br>[[$1]]<br><br>"];
    markup = [markup stringByReplacingOccurrencesOfRegex:@"<a[^>]+href=['\"](http[^'\"]+)['\"][^>]*>.+?</a>" withString:@"$1"];

    // Create attributed string with HTML and get text version of HTML from that
    NSData *data = [markup dataUsingEncoding:NSUTF8StringEncoding];
    NSNumber *encoding = [NSNumber numberWithUnsignedInteger:NSUTF8StringEncoding];
    NSDictionary *options = [NSDictionary dictionaryWithObject:encoding forKey:NSCharacterEncodingDocumentOption];
    NSAttributedString *as = [[NSAttributedString alloc] initWithHTML:data options:options documentAttributes:NULL];
    NSString *selectedString = [as string];

    // Apparently, getting a plaintext string from an HTML-generated attributed string
    // places a newline character at the end. Remove that character here
    if ([selectedString characterAtIndex:(selectedString.length-1)] == '\n') {
        selectedString = [selectedString substringToIndex:(selectedString.length - 1)];
    }

    // Copy &ensp; as regular spaces and format names and times more nicely
    selectedString = [selectedString stringByReplacingOccurrencesOfString:@"\u2002" withString:@" "];
    // Place timestamps, names, and first message all on the same line with formatting
    NSString *nameTimeRegex = @"\\b(.+)\\n((?:.+ )?\\d+:\\d+(?: PM| AM)?)\\n";
    NSString *replacementRegex = @"[$2] $1: ";
    NSInteger numLines = [Helpers getNumLines:selectedString];
    BOOL hasMatch = ([selectedString rangeOfRegex:nameTimeRegex].location != NSNotFound);
    selectedString = [selectedString stringByReplacingOccurrencesOfRegex:nameTimeRegex withString:replacementRegex];
    // Indent any grouped blocks of chat messages
    // Only do this if we have more than 1 line and actually matched a name/time
    if (numLines > 1 && hasMatch) {
        selectedString = [selectedString stringByReplacingOccurrencesOfRegex:@"^([^\\s\\[])"
                                                                  withString:@"\t\t$1"
                                                                     options:RKLMultiline
                                                                       range:NSMakeRange(0, selectedString.length)
                                                                       error:nil];
    }

    // Actually put the formmated copy onto the clipboard
    NSPasteboard *pasteboard = [NSPasteboard generalPasteboard];
    [pasteboard clearContents];
    NSArray *objectsToCopy = [NSArray arrayWithObject:selectedString];
    [pasteboard writeObjects:objectsToCopy];
}

- (void)runJavascript:(NSString *)javascript {
    if (!webViewLoaded) {
        [javascripts addObject:javascript];
        return;
    }
    [self.webView stringByEvaluatingJavaScriptFromString:javascript];
    // Don't store executed javascripts unless we want to enable detailed logging for users
//    [executedJavascripts addObject:javascript];
}

- (void)logExecutedJavascripts {
    DDLogInfo(@"EXECUTED JAVASCRIPTS FOR %@:", self.jid);
    for (NSString *js in executedJavascripts) {
        DDLogInfo(@"%@", js);
    }
    DDLogInfo(@"END EXECUTED JAVASCRIPTS FOR %@", self.jid);
    [executedJavascripts removeAllObjects];
}

- (void)sendMessage {
    if (uploadImage) {
        // Cache the reduced image
        NSData *imageData = [OSXHelpers getPNGDataFromImage:uploadImage];

        // We need to copy the string, since it will get cleared out before sending otherwise
        [self hideUploadView];
        [self showUploadProgress];
        [chat sendFileUploadWithData:imageData
                         contentType:@"image/png"
                            fileName:self.fileNameInput.stringValue
                                desc:[[self getMessageInputString] copy]];
    } else if (uploadFile) {
        NSData *fileData = [uploadFile regularFileContents];
        GEMagicResult *fileInfo = [GEMagicKit magicForData:fileData];
        NSString *mimeType = [[fileInfo.mimeType componentsSeparatedByString:@";"] objectAtIndex:0];

        // We need to copy the string, since it will get cleared out before sending otherwise

        [self hideUploadView];
        [self showUploadProgress];
        [chat sendFileUploadWithData:fileData
                         contentType:mimeType
                            fileName:self.fileNameInput.stringValue
                                desc:[[self getMessageInputString] copy]];
    } else {
        if ([OSXHelpers isRoomJid:self.jid] && [HipChatOSXApp.instance getRoomInfo:self.jid].isArchived) {
            [HipChatOSXApp.instance showFlash:@"Messages cannot be sent in archived rooms"];
            [self focusMessageInput:YES];
            return;
        }
        [chat sendMessage:[self getMessageInputString]];
    }

    [previousMessages insertObject:[[self getMessageInputString] copy] atIndex:0];
    if ([previousMessages count] > MAX_MESSAGE_HISTORY) {
        [previousMessages removeLastObject];
    }

    // Set outgoing chat state here - [Chat sendMessage:] will set it as part of the message send
    outgoingChatState = ACTIVE_CHAT_STATE;

    [self.messageInput setString:@""];
    // Make sure to clear out the undomanager so it doesn't try going back to previous messages
    [self.messageInput.undoManager removeAllActions];
    [self.messageInput hideAutoComplete];
    [self focusMessageInput:YES];
    [chat scrollChat];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark IBActions - event/menu handlers
///////////////////////////////////////////////////////////////////////

- (IBAction)onAttachmentClick:(id)sender {
    [self showFileSelect];
}

- (void)onChatDragDrop:(NSNotification *)notif {
    id <NSDraggingInfo> dragInfo = [[notif userInfo] objectForKey:@"dragInfo"];
    [NSApp activateIgnoringOtherApps:YES];
    [self.messageInput readSelectionFromPasteboard:[dragInfo draggingPasteboard]];
}

- (IBAction)onCancelUploadClick:(id)sender {
    [self hideUploadView];
    [self focusMessageInput:YES];
}

- (IBAction)onFileNameClick:(id)sender {
    [self focusFileNameInput];
}

- (IBAction)onEmoticonButtonClick:(id)sender {
    [self showEmoticonMenu];
}

- (IBAction)onSendClick:(id)sender {
    [self sendMessage];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark UI control methods
///////////////////////////////////////////////////////////////////////

- (void)appendEmoticonToMessage:(NSString *)shortcut {
    NSUInteger cursorLoc = [[[self.messageInput selectedRanges] objectAtIndex:0] rangeValue].location;
    NSRange textRange = NSMakeRange(0, cursorLoc);
    NSString *textToCursor = [self.messageInput.string substringWithRange:textRange];

    // Check to see if we need to add a space before the cursor
    BOOL endsWithSpace = [textToCursor rangeOfRegex:@"\\s$"].location == NSNotFound;
    NSString *spacer = (endsWithSpace && self.messageInput.string.length > 0 ? @" " : @"");

    // Replace at the cursor position
    NSRange replaceRange = NSMakeRange(cursorLoc, 0);
    NSString *replaceString = [NSString stringWithFormat:@"%@%@ ", spacer, shortcut];
    [self.messageInput replaceCharactersInRange:replaceRange withString:replaceString];

    [emoticonPopover close];
    [self focusMessageInput:YES];
}

- (void)checkScroll {
    if (isScrollAtBottom && webViewLoaded) {
        [chat scrollChat];
    }
}

- (void)didHide {
    [self.view setHidden:YES];
    [chat removeChatDivider];
    isChatDividerShown = NO;
}

- (void)didShow {
    [self updateImagePopoverPosition];
    [self focusMessageInput];
}

- (void)focusFileNameInput {
    if (self.view.window.firstResponder == self.fileNameInput) {
        return;
    }
    if (![self.fileNameInput isHidden]) {
        return;
    }

    [self.fileNameButton setHidden:YES];
    [self.fileNameInput setHidden:NO];
    [self.view.window makeFirstResponder:self.fileNameInput];
}

- (void)focusMessageInput {
    [self focusMessageInput:NO];
}

- (void)focusMessageInput:(BOOL)forceFocus {
    NSResponder *fr = [self.view.window firstResponder];
    if (!forceFocus && ([fr isKindOfClass:[NSText class]] || [fr isKindOfClass:[NSTextField class]])) {
        return;
    }
    [self.view.window makeFirstResponder:self.messageInput];
}

- (void)hideHistorySpinner {
	NSString *javascript = @"document.getElementById('loading_spinner').style.display = 'none';";
	[self handleJavascript:javascript];
}

- (void)hideUploadProgress {
    [self.uploadProgressView setHidden:YES];
    [self.uploadSpinner stopAnimation:self];
}

- (void)hideUploadView {
    if (self.uploadView.isHidden) {
        return;
    }
    NSInteger uploadWidth = self.uploadView.frame.size.width;
    NSRect inputFrame = self.textWrapper.frame;
    self.textWrapper.frame = NSMakeRect(inputFrame.origin.x - uploadWidth,
                                        inputFrame.origin.y,
                                        inputFrame.size.width + uploadWidth,
                                        inputFrame.size.height);

    [imagePopover close];
    imagePopover = nil;
    uploadImage = nil;
    uploadFile = nil;
    [self.uploadView setHidden:YES];
}

- (void)messageHistoryDown {
    if (currentMessageIndex == 0) {
        [self.messageInput setString:currentMessage];
        currentMessageIndex = -1;
    }
    if (currentMessageIndex > 0 && previousMessages.count > currentMessageIndex-1) {
        [self.messageInput setString:[previousMessages objectAtIndex:currentMessageIndex-1]];
        currentMessageIndex--;
    }
}

- (void)messageHistoryUp {
    if (currentMessageIndex == -1) {
        currentMessage = [[self getMessageInputString] copy];
    }
    if (previousMessages.count > currentMessageIndex+1) {
        [self.messageInput setString:[previousMessages objectAtIndex:currentMessageIndex+1]];
        [self.messageInput setSelectedRange:NSMakeRange(0, 0)];
        currentMessageIndex++;
    }
}

- (void)scrollChatToAnchor:(NSString *)anchor {
    dispatch_async(dispatch_get_main_queue(), ^{
        DOMDocument *doc = [[self.webView mainFrame] DOMDocument];
        DOMElement *element = [doc getElementById:anchor];
        [element scrollIntoView:YES];
    });
}

- (void)showEmoticonMenu {
    if (!emVC) {
        emVC = [[EmoticonMenuViewController alloc] initWithNibName:@"EmoticonMenu" bundle:[NSBundle mainBundle]];
    }
    if (!emoticonPopover) {
        emoticonPopover = [[NSPopover alloc] init];
        emoticonPopover.contentViewController = emVC;
        emoticonPopover.animates = NO;
        emoticonPopover.behavior = NSPopoverBehaviorTransient;
    }
    [emoticonPopover showRelativeToRect:self.emoticonButton.bounds ofView:self.emoticonButton preferredEdge:NSMinYEdge];
}

- (void)showFileSelect {
    BOOL canUploadFiles = [HipChatApp.instance.currentUser getPerm:PERM_SHARE_FILES];
    if (!canUploadFiles) {
        [HipChatApp.instance showFlash:@"File uploads have been disabled by a group administrator."];
        return;
    }

    NSOpenPanel* openDialog = [NSOpenPanel openPanel];
    [openDialog setCanChooseFiles:YES];
    
    // Running the dialog in modal mode will "block" here until the
    // user selects a file
    [openDialog beginSheetModalForWindow:self.view.window completionHandler:^(NSInteger result) {
        if (result == NSFileHandlingPanelOKButton) {
            NSArray* urls = [openDialog URLs];
            if (urls.count == 0) {
                DDLogError(@"User selected 0 files to attach but got 'OKButton' result");
                return;
            }
            
            NSURL *fileURL = [urls objectAtIndex:0];
            NSFileWrapper *fileWrapper = [[NSFileWrapper alloc] initWithURL:fileURL options:NSFileWrapperReadingImmediate error:nil];
            [self showUploadWithFile:fileWrapper];
        }
    }];
}

- (void)showHistorySpinner {
	NSString *javascript = @"document.getElementById('loading_spinner').style.display = 'block';";
	[self handleJavascript:javascript];
}

- (void)showImagePopoverWithImage:(NSImage *)image {
    BOOL canUploadFiles = [HipChatApp.instance.currentUser getPerm:PERM_SHARE_FILES];
    if (!canUploadFiles) {
        return;
    }

    if (imagePopover) {
        ((ImagePreviewViewController *)imagePopover.contentViewController).image = image;
    } else {
        ImagePreviewViewController *imagePreviewVC = [[ImagePreviewViewController alloc] initWithNibName:@"ImagePreview" bundle:[NSBundle mainBundle]];
        imagePreviewVC.image = image;

        imagePopover = [[NSPopover alloc] init];
        imagePopover.contentViewController = imagePreviewVC;
        imagePopover.behavior = NSPopoverBehaviorApplicationDefined;
    }

    [self updateImagePopoverPosition];
}

- (void)showUploadProgress {
    BOOL canUploadFiles = [HipChatApp.instance.currentUser getPerm:PERM_SHARE_FILES];
    if (!canUploadFiles) {
        return;
    }
    [self.uploadProgressView setHidden:NO];
    [self.uploadSpinner startAnimation:self];
}

- (void)showUploadView {
    BOOL canUploadFiles = [HipChatApp.instance.currentUser getPerm:PERM_SHARE_FILES];
    if (!canUploadFiles) {
        [HipChatApp.instance showFlash:@"File uploads have been disabled by a group administrator."];
        return;
    }

    if (!self.uploadView.isHidden) {
        return;
    }

    NSInteger uploadWidth = self.uploadView.frame.size.width;
    NSRect inputFrame = self.textWrapper.frame;
    self.textWrapper.frame = NSMakeRect(inputFrame.origin.x + uploadWidth,
                                        inputFrame.origin.y,
                                        inputFrame.size.width - uploadWidth,
                                        inputFrame.size.height);

    [self.uploadView setHidden:NO];
}

- (void)showUploadWithFile:(NSFileWrapper *)fileWrapper {
    BOOL canUploadFiles = [HipChatApp.instance.currentUser getPerm:PERM_SHARE_FILES];
    if (!canUploadFiles) {
        [HipChatApp.instance showFlash:@"File uploads have been disabled by a group administrator."];
        return;
    }
    if (![fileWrapper isRegularFile]) {
        if ([fileWrapper.filename rangeOfRegex:@"\\.(key|app)$"].location != NSNotFound) {
            [HipChatApp.instance showFlash:@"Keynote and App files are actually directories. They must be compressed before upload."];
        } else {
            [HipChatApp.instance showFlash:@"The chosen file isn't valid for uploading. It may be a directory or symbolic link."];
        }
        return;
    }

    NSString *iconFilename = [Helpers getFileTypeIcon:fileWrapper.filename];
    NSString *iconPath = [NSString stringWithFormat:@"http://%@/img/filetype_icons/%@",
                          HipChatOSXApp.instance.webHost,
                          iconFilename];
    self.uploadFileIconUrl = [NSURL URLWithString:iconPath];
    self.uploadFileNameInput = (fileWrapper.filename.length > 0 ? fileWrapper.filename : @"upload.png");
    requiredExtension = [OSXHelpers getFileExtension:fileWrapper.filename];
    [self showUploadView];

    uploadFile = fileWrapper;
    // Show the image popover if we're uploading an image file
    if ([iconFilename isEqualToString:@"image.png"]) {
        NSImage *popoverImage = [[NSImage alloc] initWithData:fileWrapper.regularFileContents];
        [self showImagePopoverWithImage:popoverImage];
    } else {
        [imagePopover close];
        imagePopover = nil;
        uploadImage = nil;
    }

    [self updateFileNameText];
    if (fileWrapper.filename.length > 0) {
        [self focusMessageInput];
    } else {
        [self focusFileNameInput];
    }
}

- (void)updateFileNameText {
    NSInteger fileSize = (uploadFile ?
                          [[uploadFile.fileAttributes objectForKey:NSFileSize] integerValue] :
                          [OSXHelpers getPNGDataFromImage:uploadImage].length);
    self.uploadFileNameText = [NSString stringWithFormat:@"%@ - %@",
                               self.uploadFileNameInput,
                               [Helpers getFileSizeString:fileSize]];
}

- (void)updateImagePopoverPosition {
    if (imagePopover) {
        [imagePopover showRelativeToRect:self.uploadView.bounds ofView:self.uploadView preferredEdge:NSMaxYEdge];
    }
}

- (void)willHide {
    [imagePopover close];
}

- (void)willShow {
    [self.view setHidden:NO];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark NSSplitViewDelegate methods
///////////////////////////////////////////////////////////////////////

- (CGFloat)splitView:(NSSplitView *)splitView constrainMinCoordinate:(CGFloat)proposedMin ofSubviewAt:(NSInteger)dividerIndex {
    return 200; // Coordinate is from the top (leave at least 200 px of chat displayed)
}

- (CGFloat)splitView:(NSSplitView *)splitView constrainMaxCoordinate:(CGFloat)proposedMax ofSubviewAt:(NSInteger)dividerIndex {
    // Transform the coordinate to be "from the bottom" (default is from the top)
    // See http://developer.apple.com/library/mac/#documentation/Cocoa/Conceptual/CocoaDrawingGuide/Transforms/Transforms.html
    NSRect frameRect = [splitView bounds];
    NSAffineTransform* xform = [NSAffineTransform transform];
    [xform translateXBy:0.0 yBy:frameRect.size.height];
    [xform scaleXBy:1.0 yBy:-1.0];

    // Min height of text bottom box = 36px
    return [xform transformPoint:NSMakePoint(0, 36)].y;
}

- (void)splitView:(NSSplitView *)splitView resizeSubviewsWithOldSize:(NSSize)oldSize {
    // Resize subviews so that all size change goes to top view (chat area)
    // Keep bottom view (input area a constant size)
    NSRect svFrame = splitView.frame;
    NSRect svBounds = splitView.bounds;
    CGFloat heightDiff = svFrame.size.height - oldSize.height;

    // Never set x < 0 || x > total split view width
    // Never set y < 0 || y > total split view height
    // Never set width/height greater than total split view width/height
    // Split view itself will always be >= 0 for x,y,width,height
    NSView *topSplit = [splitView.subviews objectAtIndex:0];
    NSRect tvFrame = topSplit.frame;
    NSRect newTvFrame = NSMakeRect(tvFrame.origin.x,
                                   tvFrame.origin.y,
                                   svBounds.size.width,
                                   tvFrame.size.height + heightDiff);
    topSplit.frame = newTvFrame;

    NSView *bottomSplit = [splitView.subviews objectAtIndex:1];
    NSRect bvFrame = bottomSplit.frame;
    bottomSplit.frame = NSMakeRect(bvFrame.origin.x,
                                   newTvFrame.size.height + splitView.dividerThickness,
                                   svBounds.size.width,
                                   bvFrame.size.height);

    [self updateImagePopoverPosition];
    [self.messageInput updateAutoCompletePopoverPosition];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark HipChatTextViewDelegate methods
///////////////////////////////////////////////////////////////////////

- (void)controlTextDidEndEditing:(NSNotification *)notification {
    if (notification.object == self.fileNameInput) {
        [self handleFileNameEndEditing:notification];
    }
}

- (void)textDidEndEditing:(NSNotification *)notification {
    if (notification.object == self.messageInput) {
        [self handleMessageInputEndEditing:notification];
    }
}

// We're preventing the field from losing focus here to avoid the
// autoComplete popup from taking focus when it gets shown
- (BOOL)textShouldEndEditing:(NSText *)textObject {
    if (self.messageInput.showingAutocomplete) {
        return NO;
    }
    return YES;
}

- (void)textDidChange:(NSNotification *)notification {
    if ([OSXHelpers isRoomJid:self.jid]) {
        // We don't do chat state in rooms
        return;
    }

    if (self.messageInput.string.length == 0 &&
        [outgoingChatState isEqualToString:COMPOSING_CHAT_STATE]) {
        // Send active chat state (clear "is typing" message)
        ChatStateMessage *stateMessage = [ChatStateMessage chatStateMessageWithState:ACTIVE_CHAT_STATE];
        [stateMessage addAttributeWithName:@"to" stringValue:self.jid.bare];
        [HipChatOSXApp.instance.conn sendElement:stateMessage];
        outgoingChatState = ACTIVE_CHAT_STATE;
    } else if (self.messageInput.string.length > 0 &&
               [outgoingChatState isEqualToString:ACTIVE_CHAT_STATE]) {
        // Send composing chat state (show "is typing" message)
        ChatStateMessage *stateMessage = [ChatStateMessage chatStateMessageWithState:COMPOSING_CHAT_STATE];
        [stateMessage addAttributeWithName:@"to" stringValue:self.jid.bare];
        [HipChatOSXApp.instance.conn sendElement:stateMessage];
        outgoingChatState = COMPOSING_CHAT_STATE;
    }
}

- (void)textView:(HipChatTextView *)textView didPasteFile:(NSFileWrapper *)fileWrapper {
    [self showUploadWithFile:fileWrapper];
}

- (void)textView:(HipChatTextView *)textView didPasteImage:(NSImage *)image {
    NSString *iconPath = [NSString stringWithFormat:@"http://%@/img/filetype_icons/%@", HipChatOSXApp.instance.webHost, [Helpers getFileTypeIcon:@"upload.png"]];
    self.uploadFileIconUrl = [NSURL URLWithString:iconPath];
    self.uploadFileNameInput = @"upload.png";
    requiredExtension = @"png";
    uploadImage = image;

    [self showUploadView];
    [self showImagePopoverWithImage:image];
    [self focusFileNameInput];
}

- (void)textView:(HipChatTextView *)textView didPasteURL:(NSURL *)fileURL {
    NSFileWrapper *fileWrapper = [[NSFileWrapper alloc] initWithURL:fileURL options:NSFileWrapperReadingImmediate error:nil];
    [self showUploadWithFile:fileWrapper];
}

// This delegate function is used to override various keypresses
// e.g. When a user hits <tab>, this will get called and aSelector will
// be @selector(insertTab:), return will be @selector(insertNewline:)
// See https://developer.apple.com/library/mac/#documentation/Cocoa/Reference/ApplicationKit/Classes/NSResponder_Class/Reference/Reference.html
// under "Responding to Action Messages" for a list of actions
- (BOOL)textView:(NSTextView *)aTextView doCommandBySelector:(SEL)aSelector {
    if (aSelector == @selector(moveToBeginningOfDocument:)) {
        [self messageHistoryUp];
        return YES;
    } else if (aSelector == @selector(moveToEndOfDocument:)) {
        [self messageHistoryDown];
        return YES;
    } else if (aSelector == @selector(insertNewline:)) {
        // By default, shift+enter trigger endEditing (sending the current message)
        // Override this selector to allow shiftEnter to insert a newline
        // insertNewLineIgnoringFieldEditor is the same selector used by ctrl/opt+enter
        if([[[self.view window] currentEvent] modifierFlags] & NSShiftKeyMask) {
            [aTextView insertNewlineIgnoringFieldEditor:self];
            return YES;
        }
    } else if (aSelector == @selector(scrollPageUp:) || aSelector == @selector(scrollPageDown:)) {
        [self.webView doCommandBySelector:aSelector];
        return YES;
    }

    return NO;
}

@end
