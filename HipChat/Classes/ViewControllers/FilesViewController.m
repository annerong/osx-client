//
//  FilesViewController.m
//  HipChat
//
//  Created by Christopher Rivers on 4/1/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "FilesViewController.h"

#import "FileEntry.h"
#import "HipChatOSXApp.h"
#import "OSXHelpers.h"
#import "XMPPJID.h"

@implementation FilesViewController

static NSString *FILES_NS = @"http://hipchat.com/protocol/files";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onFileAdded:)
                                                     name:HipChatFileAddedNotification
                                                   object:HipChatOSXApp.instance];
    }
    return self;
}

- (RightSidebarEntry *)generateEntryWithItem:(NSXMLElement *)item {
    return [[FileEntry alloc] initWithItem:item];
}

- (NSString *)getRequestNamepsace {
    return FILES_NS;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)onFileAdded:(NSNotification *)notif {
    if ([self.view isHidden] || !self.view.superview) {
        return;
    }
    HipChatOSXApp *app = HipChatOSXApp.instance;
    if ([app.currentJid.bare isEqualToString:
         [[[notif userInfo] objectForKey:@"jid"] bare]]) {
        // Delay a half second so the server has a chance to save the data
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_current_queue(), ^(void){
            [self updateWithJid:app.currentJid];
        });
    }
}


@end
