//
//  AppDelegate.m
//  HipChat
//
//  Created by Christopher Rivers on 10/15/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "AppDelegate.h"

#import <Growl/Growl.h>
#import <HockeySDK/HockeySDK.h>
#import <objc/runtime.h>
#import <Sparkle/Sparkle.h>

#import "AttributedStringTooltipTransformer.h"
#import "DDLog.h"
#import "DDTTYLogger.h"
#import "HipChatLogManager.h"
#import "HipChatOSXApp.h"
#import "ImageTransformer.h"
#import "LoginWindowController.h"
#import "MainWindowController.h"
#import "SettingsWindowController.h"
#import "User.h"
#import "XMPPJID.h"

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
    HipChatLogManager *logManager = [[HipChatLogManager alloc] init];
    DDFileLogger *fileLogger = [[DDFileLogger alloc] initWithLogFileManager:logManager];
    fileLogger.rollingFrequency = 60 * 60 * 24; // 24 hour rolling
    fileLogger.logFileManager.maximumNumberOfLogFiles = 15;
    fileLogger.maximumFileSize = 1024 * 1024 * 2; // 2 MB files
    [DDLog addLogger:fileLogger];

    [self setupUserDefaultsDefaults];

    windows = [NSMutableDictionary dictionary];

    // Set a standard "display tooltip after 3 seconds of hover" rule
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:3]
                                              forKey:@"NSInitialToolTipDelay"];

    // Setup hipchat:// url handling (so we can handle stuff like the reconnect link
    NSAppleEventManager *appleEventManager = [NSAppleEventManager sharedAppleEventManager];
    [appleEventManager setEventHandler:self
                           andSelector:@selector(handleGetURLEvent:withReplyEvent:)
                         forEventClass:kInternetEventClass
                            andEventID:kAEGetURL];

    // Register path transformer for images in rosters and privacy in Lobby
    id imageTransformer = [[ImageTransformer alloc] init];
    [NSValueTransformer setValueTransformer:imageTransformer forName:@"ImageTransformer"];
    id tooltipTransformer = [[AttributedStringTooltipTransformer alloc] init];
    [NSValueTransformer setValueTransformer:tooltipTransformer forName:@"AttributedStringTooltipTransformer"];

    [GrowlApplicationBridge setGrowlDelegate:(id <GrowlApplicationBridgeDelegate>)self];

    // Listen for show window events
    // Also, initialize HipChatApp before showing windows, so we make sure we have the proper chatHost
    // (which will change the login information we retrieve from preferences)
    // (BITHockeyManager will trigger the callback to show the windows)
    HipChatOSXApp *app = HipChatOSXApp.instance;
    [app updateUpdateUrlForceMinor:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onShowLoginWindow:) name:HipChatShowLoginWindowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onShowMainWindow:) name:HipChatShowMainWindowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onShowSettingsWindow:) name:HipChatShowSettingsWindowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFocusApp:) name:HipChatFocusAppNotification object:app];
    [[[NSWorkspace sharedWorkspace] notificationCenter] addObserver:self
                                                           selector:@selector(onAppActivate:)
                                                               name:NSWorkspaceDidActivateApplicationNotification
                                                             object:nil];
    [app addDelegate:self];

    // Make sure the window member variable is initialized before we show any windows
    // (otherwise they will be dealloced)
    // Check for crash reports with HockeyApp
    [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:@"ac4719dff655881a5948e870b0ba69a1"
                                                        companyName:@"HipChat"
                                         crashReportManagerDelegate:self];
    [[BITHockeyManager sharedHockeyManager] setLoggingEnabled:YES];
    [[BITHockeyManager sharedHockeyManager] startManager];

    [self.application activateIgnoringOtherApps:YES];
}

- (void)applicationDidBecomeActive:(NSNotification *)notification {
    HipChatOSXApp.instance.appIsFocused = YES;
}

- (void)applicationDidResignActive:(NSNotification *)notification {
    HipChatOSXApp.instance.appIsFocused = NO;
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark HockeyApp delegate functions
///////////////////////////////////////////////////////////////////////

// Required delegate method
- (void)showMainApplicationWindow {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    NSString *email = [app getSignInEmail];
    NSString *password = [app getSignInPassword];
    if (email && password) {
        [self showWindowWithClass:[MainWindowController class] andNib:@"MainWindow" orderFront:YES];
        [app connectWithEmail:email password:password];
    } else {
        [self showWindowWithClass:[LoginWindowController class] andNib:@"LoginWindow" orderFront:YES];
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Window control functions
///////////////////////////////////////////////////////////////////////

- (BOOL)applicationShouldHandleReopen:(NSApplication *)theApplication hasVisibleWindows:(BOOL)flag {
    if ([NSApp keyWindow] == nil) {
        DDLogInfo(@"App was activated with no key window. Showing last activated window...");
        [lastPrimaryWindowController showWindow:self];
        return NO;
    }
    return YES;
}

// Return whether the window was the key window
- (BOOL)hideWindowWithClass:(Class)class {
    NSString *className = [NSString stringWithCString:class_getName(class) encoding:NSASCIIStringEncoding];
    DDLogInfo(@"Hiding window: %@", className);
    NSWindowController *wc = [windows objectForKey:className];
    if (wc) {
        BOOL isKeyWindow = [wc.window isKeyWindow];
        [wc.window orderOut:nil];
        return isKeyWindow;
    }

    return NO;
}

- (void)onAppActivate:(NSNotification *)notif {
    NSString *activeAppId = [[[notif userInfo] objectForKey:NSWorkspaceApplicationKey] bundleIdentifier];
    if ([activeAppId isEqualToString:[[NSBundle mainBundle] bundleIdentifier]] &&
        [NSApp keyWindow] == nil) {
        DDLogInfo(@"App was activated with no key window. Showing last activated window...");
        [lastPrimaryWindowController showWindow:self];
    }
}

- (void)onFocusApp:(NSNotification *)notif {
    [self.application activateIgnoringOtherApps:YES];
}

- (void)onShowLoginWindow:(NSNotification *)notif {
    BOOL shouldOrderFront = [self hideWindowWithClass:[MainWindowController class]];
    LoginWindowController *lwc = (LoginWindowController *)[self showWindowWithClass:[LoginWindowController class]
                                                                             andNib:@"LoginWindow"
                                                                         orderFront:shouldOrderFront];
    [lwc checkForErrors];
    [lwc updateCredentials];
    lastPrimaryWindowController = lwc;
}

- (void)onShowMainWindow:(NSNotification *)notif {
    BOOL shouldOrderFront = [self hideWindowWithClass:[LoginWindowController class]];
    //    [NSException raise:@"Invalid foo value" format:@"foo of %d is invalid", 5];
    lastPrimaryWindowController = [self showWindowWithClass:[MainWindowController class]
                                                     andNib:@"MainWindow"
                                                 orderFront:shouldOrderFront];
}

- (void)onShowSettingsWindow:(NSNotification *)notif {
    NSString *className = [NSString stringWithCString:class_getName([SettingsWindowController class])
                                             encoding:NSASCIIStringEncoding];

    // Don't re-show the settings window if it's already being shown
    NSWindowController *existing = [windows objectForKey:className];
    if (existing && existing.window.isVisible) {
        [existing.window makeKeyAndOrderFront:self];
        return;
    }
    // We want to refresh the settings window every time we open it
    // (so that if dev prefs are enabled or disabled it shows properly)
    // Always set the value in the windows dictionary to a new window
    // (the old window will be deallocated at this point)
    NSWindowController *wc = [[SettingsWindowController alloc] init];
    [windows setValue:wc forKey:className];

    [self showWindowWithClass:[SettingsWindowController class] andNib:nil];
}

- (NSWindowController *)showWindowWithClass:(Class)class andNib:(NSString *)nibName {
    return [self showWindowWithClass:class andNib:nibName orderFront:YES];
}

- (NSWindowController *)showWindowWithClass:(Class)class andNib:(NSString *)nibName orderFront:(BOOL)orderFront {
    NSString *className = [NSString stringWithCString:class_getName(class) encoding:NSASCIIStringEncoding];
    DDLogInfo(@"Showing window: %@ - orderFront: %@", className, (orderFront ? @"YES" : @"NO"));

    NSWindowController *wc = [windows objectForKey:className];
    if (!wc) {
        wc = [[class alloc] initWithWindowNibName:nibName];
        [windows setValue:wc forKey:className];
    }
    if (!orderFront && wc.isWindowLoaded) {
        // Do nothing if the window is already loaded and we don't want to order it in front
    } else {
        [wc showWindow:self];
    }
    return wc;
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark HipChatApp delegate functions
///////////////////////////////////////////////////////////////////////

- (void)hipChatAppUnreadChangedForJid:(XMPPJID *)jid {
	HipChatOSXApp *app = HipChatOSXApp.instance;

	NSInteger totalCount = [app getTotalUnreadCount];
	if (totalCount > 0) {
        [[NSApp dockTile] setBadgeLabel:[NSString stringWithFormat:@"%ld", (long)totalCount]];
	} else {
		[[NSApp dockTile] setBadgeLabel:nil];
	}
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Menu Outlets
///////////////////////////////////////////////////////////////////////

- (IBAction)checkForUpdates:(id)sender {
    // Temporarily change the update URL force a check for minor updates
    [HipChatOSXApp.instance updateUpdateUrlForceMinor:YES];
    [[SUUpdater sharedUpdater] checkForUpdates:sender];
    [HipChatOSXApp.instance updateUpdateUrlForceMinor:NO];
}

- (IBAction)hipchatEmoticons:(id)sender{
    [[NSWorkspace sharedWorkspace] openURL:
     [NSURL URLWithString:
      @"http://hipchat-emoticons.nyh.name/"]];
}

- (IBAction)keyboardShortcuts:(id)sender {
    [[NSWorkspace sharedWorkspace] openURL:
     [NSURL URLWithString:@"http://help.hipchat.com/knowledgebase/articles/64457-what-are-the-keyboard-shortcuts-hotkeys-"]];
}

- (IBAction)logout:(id)sender {
    [HipChatOSXApp.instance signOutUserAndClearCredentials];
}

- (IBAction)preferences:(id)sender {
    [HipChatOSXApp.instance showSettingsWindow];
}

- (IBAction)setStatusAvailable:(id)sender {
    [HipChatOSXApp.instance updatePresenceWithShow:nil status:nil];
}

- (IBAction)setStatusAway:(id)sender {
    [HipChatOSXApp.instance updatePresenceWithShow:@"xa" status:nil];
}

- (IBAction)setStatusDnd:(id)sender {
    [HipChatOSXApp.instance updatePresenceWithShow:@"dnd" status:nil];
}

- (IBAction)submitFeedback:(id)sender {
    NSURL *feedbackURL = [NSURL URLWithString:@"https://www.hipchat.com/help/osx_app_forum"];
    [[NSWorkspace sharedWorkspace] openURL:feedbackURL];
}

- (BOOL)validateUserInterfaceItem:(id < NSValidatedUserInterfaceItem >)anItem {
    if ([anItem action] == @selector(setStatusAvailable:) ||
        [anItem action] == @selector(setStatusAway:) ||
        [anItem action] == @selector(setStatusDnd:)) {
        return [HipChatOSXApp.instance.conn isConnected];
    }
    return YES;
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark GrowlApplicationBridgeDelegate functions
///////////////////////////////////////////////////////////////////////

- (NSString *)applicationNameForGrowl {
    return @"HipChat";
}

- (void)growlNotificationWasClicked:(id)clickContext {
    [self.application activateIgnoringOtherApps:YES];

    NSDictionary *context = (NSDictionary *)clickContext;
    NSString *fromJidBare = [context objectForKey:@"fromJid"];
    if (fromJidBare) {
        [HipChatOSXApp.instance focusChat:[XMPPJID jidWithString:fromJidBare]];
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark URL Handler
///////////////////////////////////////////////////////////////////////

- (void)handleGetURLEvent:(NSAppleEventDescriptor *)event withReplyEvent:(NSAppleEventDescriptor *)replyEvent {
    NSString* url = [[event paramDescriptorForKeyword:keyDirectObject] stringValue];
    DDLogInfo(@"Got hipchat:// URL request: %@", url);

	NSArray *components = [[url substringFromIndex:10] componentsSeparatedByString:@":"];
    if (components.count == 1) {
        NSString *mentionName = [components objectAtIndex:0];
        DDLogInfo(@"Got hipchat:// URL request with mention name: %@", mentionName);

        NSDictionary *userList = [HipChatOSXApp.instance getAllUsers];
        for (NSString *key in userList) {
            User *u = [userList objectForKey:key];
            if ([u.mentionName isEqualToString:mentionName]) {
                DDLogInfo(@"Joining chat from hipchat:// URL request");
                [HipChatOSXApp.instance joinChat:u.jid withFocus:YES];
                return;
            }
        }
    }

    if ([components count] != 2) {
        return;
    }

    NSString *type = [components objectAtIndex:0];
    NSString *arg1 = [components objectAtIndex:1];
	if ([type isEqualToString:@"action"]) {
		if ([arg1 isEqualToString:@"reconnect"]) {
            [HipChatOSXApp.instance reconnect];
        }
    }
}

- (void)setupUserDefaultsDefaults {
#ifdef DEBUG
    NSNumber *detailedLoggingDefault = [NSNumber numberWithBool:YES];
#else
    NSNumber *detailedLoggingDefault = [NSNumber numberWithBool:NO];
#endif

    NSDictionary *userDefaultsDefaults = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [NSNumber numberWithBool:NO], @"automaticSpellingCorrectionEnabled",
                                          [NSNumber numberWithBool:NO], @"checkMinorUpdates",
                                          [NSNumber numberWithBool:YES], @"continuousSpellCheckingEnabled",
                                          detailedLoggingDefault, @"detailedLoggingEnabled",
                                          [NSNumber numberWithBool:NO], @"disableSounds",
                                          [NSNumber numberWithBool:YES], @"enableEmoticons",
                                          [NSNumber numberWithBool:NO], @"hideGifs",
                                          [NSNumber numberWithBool:NO], @"hidePresenceMessages",
                                          [NSNumber numberWithBool:YES], @"notifyDnd",
                                          [NSNumber numberWithBool:YES], @"notifyForTag",
                                          [NSNumber numberWithBool:YES], @"notifyForPrivate",
                                          [NSNumber numberWithBool:YES], @"notifyForPrivateRoom",
                                          [NSNumber numberWithBool:YES], @"notifyForRoom",
                                          [NSNumber numberWithBool:YES], @"showToasters",
                                          [NSNumber numberWithBool:NO], @"showUnreadDivider",
                                          [NSNumber numberWithBool:NO], @"useCustomConnectHost",
                                          [NSNumber numberWithBool:YES], @"visualNotifications",
                                          [NSNumber numberWithInteger:NORMAL_FONT_SIZE], @"defaultFontSize",
                                          [NSDictionary dictionary], @"notifyForSpecificRooms",
                                         nil];
    [[NSUserDefaults standardUserDefaults] registerDefaults:userDefaultsDefaults];
}

@end
