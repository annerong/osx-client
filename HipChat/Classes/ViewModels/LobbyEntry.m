//
//  LobbyEntry.m
//  HipChat
//
//  Created by Christopher Rivers on 12/12/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "LobbyEntry.h"

#import "HipChatOSXApp.h"
#import "HipChatUser.h"
#import "OSXHelpers.h"
#import "Room.h"
#import "XMPPJID.h"


@implementation LobbyEntry

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Init / Deinit methods
///////////////////////////////////////////////////////////////////////

- (void)dealloc {
    [self removeObservers];
}

- (void)deinitialize {
    [self removeObservers];
}

- (LobbyEntry *)initWithRoom:(Room *)room {
    self = [super init];
    if (self) {
        self.room = room;
        self.jid = room.jid;
        self.isHeader = NO;
        self.isArchived = room.isArchived;
        [self updateProperties:room];
        [room addObserver:self forKeyPath:@"name" options:NSKeyValueObservingOptionNew context:nil];
        [room addObserver:self forKeyPath:@"privacy" options:NSKeyValueObservingOptionNew context:nil];
        [room addObserver:self forKeyPath:@"isArchived" options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}

- (LobbyEntry *)initWithHeader:(NSString *)headerText section:(NSInteger)section {
    self = [super init];
    if (self) {
        self.jid = nil;
        self.name = headerText;
        self.icon = nil;
        self.section = section;
        self.isHeader = YES;
        self.isArchived = NO;
    }
    return self;
}

- (void)removeObservers {
    if (self.room) {
        [self.room removeObserver:self forKeyPath:@"name"];
        [self.room removeObserver:self forKeyPath:@"privacy"];
        [self.room removeObserver:self forKeyPath:@"isArchived"];
        self.room = nil;
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Helper methods
///////////////////////////////////////////////////////////////////////

- (BOOL)hideCreateRoom {
    return (!self.isHeader || self.section != 0);
}

- (BOOL)isPrivate {
    if (self.room) {
        return self.room.isPrivate;
    }
    return NO;
}

- (BOOL)showAdmin {
    if (self.isHeader || !self.room) {
        return NO;
    }

    HipChatUser *user = HipChatOSXApp.instance.currentUser;
    return (user.isAdmin || [self.room isAdmin:user.jid]);
}

- (void)updateProperties:(Room *)room {
    self.name = room.name;
    self.icon = [OSXHelpers getIconNameForRoom:room];
    self.isArchived = room.isArchived;
    self.section = (room.isArchived ? 2 : ([room isPrivate] ? 1 : 0));
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    [self updateProperties:object];
}

@end
