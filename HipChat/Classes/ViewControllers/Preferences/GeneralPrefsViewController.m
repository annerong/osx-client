//
//  GeneralPrefsViewController.m
//  HipChat
//
//  Created by Christopher Rivers on 3/8/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "GeneralPrefsViewController.h"

@implementation GeneralPrefsViewController

-(NSString *)identifier{
    return @"General";
}

-(NSImage *)toolbarItemImage{
    return [NSImage imageNamed:NSImageNamePreferencesGeneral];
}

-(NSString *)toolbarItemLabel{
    return @"General";
}

@end
