//
//  SettingsWindowController.h
//  HipChat
//
//  Created by Christopher Rivers on 11/21/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "MASPreferencesWindowController.h"

@interface SettingsWindowController : MASPreferencesWindowController <NSToolbarDelegate, NSWindowDelegate>

@end
