//
//  RosterEntry.h
//  HipChat
//
//  Created by Christopher Rivers on 10/16/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RoomOccupant;
@class User;
@class XMPPJID;

@interface RosterEntry : NSObject {
    NSInteger intShow;

    NSTimer *idleUpdateTimer;
}

- (RosterEntry *)initWithUser:(User *)user;
- (RosterEntry *)initWithOccupant:(RoomOccupant *)occupant;

@property NSString *icon;
@property BOOL isAdmin;
@property XMPPJID *jid;
@property NSString *mentionName;
@property NSString *name;
@property NSString *status;
@property (readonly) NSString *tooltip;

@end
