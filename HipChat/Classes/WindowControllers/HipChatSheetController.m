//
//  HipChatSheetController.m
//  HipChat
//
//  Created by Christopher Rivers on 3/22/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "HipChatSheetController.h"

#import "ChatFilterArrayController.h"

@implementation HipChatSheetController

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Helper functions
///////////////////////////////////////////////////////////////////////

- (void)closeSheet:(NSInteger)returnCode {
    [self.window orderOut:self];
    [NSApp stopModal];

    // Close the window before ending the sheet
    // Otherwise, it might continue to display behind another sheet
    // (e.g. in the case of the Create Room <-> Select Users sheets)
    [NSApp endSheet:self.window returnCode:returnCode];
}

- (BOOL)handleAutocompleteReturn:(id)selectedItem {
    // Subclasses should override this to handle when return is pressed in the autocomplete input
    // Return: NO = continue on to default behavior, YES = don't perform default behavior
    return YES;
}

- (BOOL)handleAutocompleteTab:(id)selectedItem {
    // Subclasses should override this to handle when tab is pressed in the autocomplete input
    // Return: NO = continue on to default behavior, YES = don't perform default behavior
    return YES;
}

- (IBAction)handleButtonClick:(id)sender {
    if (sender == self.cancelButton) {
        [self closeSheet:NSCancelButton];
    } else {
        [self closeSheet:NSOKButton];
    }
}

- (void)showAutoCompleteUnderInput:(NSView *)input {
    if (!self.autoComplete || !self.tableView || !self.arrayController) {
        DDLogError(@"Tried to show autocomplete in sheet without valid display objects available");
        return;
    }

    DDLogInfo(@"Showing sheet-based autocomplete");
    if (!self.autoComplete.parentWindow) {
        [self.window addChildWindow:self.autoComplete ordered:NSWindowAbove];
    }
    NSArray *arrangedObjects = (NSArray *)self.arrayController.arrangedObjects;
    NSInteger displayedRows = MIN(arrangedObjects.count, 5);
    CGFloat cellHeight = (self.tableView.rowHeight + self.tableView.intercellSpacing.height);
    NSSize newSize = NSMakeSize(input.frame.size.width,
                                displayedRows * cellHeight);
    
    NSPoint sheetOrigin = self.window.frame.origin;
    NSPoint inputOrigin = [input convertRect:input.bounds toView:nil].origin;
    NSPoint newOrigin = NSMakePoint(sheetOrigin.x + inputOrigin.x,
                                    sheetOrigin.y + inputOrigin.y - newSize.height);
    NSRect newFrame = NSMakeRect(newOrigin.x, newOrigin.y, newSize.width, newSize.height);
    [self.autoComplete setFrame:newFrame display:YES];
    [self.arrayController setSelectionIndex:0];
}

- (IBAction)onAutocompleteClick:(id)sender {
    NSTableView *table = (NSTableView *)sender;
    if (table.clickedRow < 0) {
        return;
    }
    id selectedItem = [self.arrayController.arrangedObjects
                       objectAtIndex:table.clickedRow];
    if (selectedItem) {
        // If the class doesn't "preventDefault" on autocomplete "return" button
        // then also trigger the closeSheet function
        // Normally, this would automatically trigger from the keyEquivalent on the "okButton"
        if (![self handleAutocompleteReturn:selectedItem]) {
            [self closeSheet:NSOKButton];
        }
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark NSTextFieldDelegate functions
///////////////////////////////////////////////////////////////////////

- (BOOL)control:(NSControl *)control textView:(NSTextView *)fieldEditor doCommandBySelector:(SEL)aSelector {
    if (!self.autoComplete.isVisible) {
        return NO;
    }

    NSArrayController *acController = self.arrayController;
    if (aSelector == @selector(moveUp:)) {
        NSInteger newSelectedIndex = MAX(0, self.tableView.selectedRow-1);
        [acController selectPrevious:self];
        [self.tableView scrollRowToVisible:newSelectedIndex];
        return YES;
    } else if (aSelector == @selector(moveDown:)) {
        NSInteger newSelectedIndex = MIN(self.tableView.numberOfRows-1, self.tableView.selectedRow+1);
        [acController selectNext:self];
        [self.tableView scrollRowToVisible:newSelectedIndex];
        return YES;
    } else if (aSelector == @selector(insertTab:) || aSelector == @selector(insertNewline:)) {
        id selectedItem = [self.arrayController.arrangedObjects
                           objectAtIndex:self.arrayController.selectionIndex];
        if (selectedItem) {
            if (aSelector == @selector(insertTab:)) {
                return [self handleAutocompleteReturn:selectedItem];
            } else {
                return [self handleAutocompleteTab:selectedItem];
            }
        }
    }
    
    return NO;
}

@end
