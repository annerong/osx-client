//
//  PrivateChatSelectUsersWindowController.m
//  HipChat
//
//  Created by Chris Rivers on 10/28/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "ManagePermissionsWindowController.h"

#import "User.h"

@implementation ManagePermissionsWindowController

@dynamic perms;

- (void)windowDidLoad {
    [super windowDidLoad];
    [self initializeUserList];
}

- (void)setPerms:(NSArray *)perms {
    // Do nothing now - this would be used if we supported using this
    // UI to edit private room perms in the future.
    // For now, the state of the UI is automaically saved by keeping the
    // sheet around during the create room flow
}

- (NSArray *)perms {
    NSMutableArray *permArray = [NSMutableArray array];
    for (id item in self.namesInput.objectValue) {
        if ([item isKindOfClass:[User class]]) {
            [permArray addObject:[item jid]];
        }
    }
    return permArray;
}

@end
