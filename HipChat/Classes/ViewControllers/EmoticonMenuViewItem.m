//
//  EmoticonMenuViewItem.m
//  HipChat
//
//  Created by Christopher Rivers on 5/8/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "EmoticonMenuViewItem.h"

#import "EmoticonMenuItem.h"
#import "HipChatOSXApp.h"

@implementation EmoticonMenuViewItem

- (void)awakeFromNib {
    NSTrackingAreaOptions focusTrackingAreaOptions = NSTrackingActiveAlways;
    focusTrackingAreaOptions |= NSTrackingMouseEnteredAndExited;
    focusTrackingAreaOptions |= NSTrackingAssumeInside;
    focusTrackingAreaOptions |= NSTrackingInVisibleRect;

    NSTrackingArea *focusTrackingArea = [[NSTrackingArea alloc] initWithRect:NSZeroRect
                                                                     options:focusTrackingAreaOptions
                                                                       owner:self
                                                                    userInfo:nil];
    [self.view addTrackingArea:focusTrackingArea];
    self.box.cornerRadius = 3.0;
}

- (IBAction)onEmoticonClick:(id)sender {
    EmoticonMenuItem *item = (EmoticonMenuItem *)self.representedObject;
    self.box.borderColor = [NSColor clearColor];
    self.box.fillColor = [NSColor clearColor];
    [HipChatOSXApp.instance appendEmoticonToCurrentMessage:item.shortcut];
}

- (void)mouseEntered:(NSEvent *)theEvent {
    self.box.borderColor = [NSColor grayColor];
    self.box.fillColor = [NSColor grayColor];
    [super mouseEntered:theEvent];
}

- (void)mouseExited:(NSEvent *)theEvent {
    self.box.borderColor = [NSColor clearColor];
    self.box.fillColor = [NSColor clearColor];
    [super mouseExited:theEvent];
}


@end
