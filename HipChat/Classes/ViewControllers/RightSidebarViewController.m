//
//  RightSidebarViewController.m
//  HipChat
//
//  Created by Christopher Rivers on 4/1/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "RightSidebarViewController.h"

#import "FilesViewController.h"
#import "OSXHelpers.h"
#import "HipChatOSXApp.h"
#import "LinksViewController.h"
#import "ProfileViewController.h"
#import "RosterViewController.h"

@implementation RightSidebarViewController

static const NSInteger ROSTER_SEGMENT_INDEX = 0;
static const NSInteger PROFILE_SEGMENT_INDEX = 0;
static const NSInteger FILES_SEGMENT_INDEX = 1;
static const NSInteger LINKS_SEGMENT_INDEX = 2;

- (void)awakeFromNib {
    filesVC = [[FilesViewController alloc] initWithNibName:@"Files" bundle:[NSBundle mainBundle]];
    linksVC = [[LinksViewController alloc] initWithNibName:@"Links" bundle:[NSBundle mainBundle]];
    profileVC = [[ProfileViewController alloc] initWithNibName:@"Profile" bundle:[NSBundle mainBundle]];
    rosterVC = [[RosterViewController alloc] initWithNibName:@"Roster" bundle:[NSBundle mainBundle]];
    [self showLobbySidebar];

    // Since NSSegmentedView (the tabs) doesn't auto resize to fille the width
    // we need to watch for frame changes and resize it ourselves
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self
               selector:@selector(onFrameChange:)
                   name:NSViewFrameDidChangeNotification
                 object:self.view];
    HipChatOSXApp *app = HipChatOSXApp.instance;
    [center addObserver:self selector:@selector(showFiles) name:HipChatShowSidebarFilesNotification object:app];
    [center addObserver:self selector:@selector(showLinks) name:HipChatShowSidebarLinksNotification object:app];
    [center addObserver:self selector:@selector(showPrimaryRightSidebar) name:HipChatShowSidebarPrimaryNotification object:app];
}

- (void)displayView:(NSView *)view {
    NSRect newFrame;
    if (!self.tabs.isHidden) {
        // Adding 3 to the height makes the display flush with the top tabs
        // It's unclear why it's not +1 (which the amount we place tabs above the
        // upper border of the container)
        newFrame = NSMakeRect(0,
                              0,
                              self.view.frame.size.width,
                              self.view.frame.size.height - self.tabs.frame.size.height + 3);
    } else {
        newFrame = NSMakeRect(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
    if (self.displayedView.subviews.count == 0) {
        [self.displayedView addSubview:view];
    } else {
        NSView *firstSubview = [self.displayedView.subviews objectAtIndex:0];
        [self.displayedView replaceSubview:firstSubview with:view];
    }   
    [self.displayedView setFrame:newFrame];
    [view setFrame:newFrame];
}

- (IBAction)handleTabChange:(id)sender {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    if (app.currentJid == app.lobbyJid) {
        // Do nothing
        return;
    }

    if (self.tabs.selectedSegment == FILES_SEGMENT_INDEX) {
        [self displayView:filesVC.view];
        [filesVC updateWithJid:app.currentJid];
    } else if (self.tabs.selectedSegment == LINKS_SEGMENT_INDEX) {
        [self displayView:linksVC.view];
        [linksVC updateWithJid:app.currentJid];
    } else {
        if ([Helpers isRoomJid:app.currentJid]) {
            [self displayView:rosterVC.view];
        } else if ([Helpers isUserJid:app.currentJid]) {
            [self displayView:profileVC.view];
        }
    }
}

- (void)onFrameChange:(NSNotification *)notif {
    CGFloat totalWidth = self.view.frame.size.width;
    CGFloat thirdWidth = floorf(totalWidth / 3.0);
    [self.tabs setWidth:thirdWidth forSegment:0];
    [self.tabs setWidth:thirdWidth forSegment:1];
    [self.tabs setWidth:(totalWidth - thirdWidth - thirdWidth) forSegment:2];
}

- (void)showFiles {
    if (self.tabs.selectedSegment == FILES_SEGMENT_INDEX) {
        return;
    }
    [self.tabs selectSegmentWithTag:FILES_SEGMENT_INDEX];
    [self handleTabChange:self.tabs];
}

- (void)showPrivateChatSidebar {
    [self.tabs setLabel:@"Profile" forSegment:PROFILE_SEGMENT_INDEX];
    [self.tabs selectSegmentWithTag:PROFILE_SEGMENT_INDEX];
    [self.tabs setHidden:NO];
    [self displayView:profileVC.view];
    [filesVC clearEntries];
    [linksVC clearEntries];
}

- (void)showRoomSidebar {
    [self.tabs setLabel:@"People" forSegment:ROSTER_SEGMENT_INDEX];
    [self.tabs selectSegmentWithTag:ROSTER_SEGMENT_INDEX];
    [self.tabs setHidden:NO];
    [self displayView:rosterVC.view];
    [filesVC clearEntries];
    [linksVC clearEntries];
}

- (void)showPrimaryRightSidebar {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    if (app.currentJid == app.lobbyJid) {
        [self showLobbySidebar];
    } else if ([OSXHelpers isRoomJid:app.currentJid]) {
        [self.tabs selectSegmentWithTag:ROSTER_SEGMENT_INDEX];
        [self handleTabChange:self.tabs];
    } else if ([OSXHelpers isUserJid:app.currentJid]) {
        [self.tabs selectSegmentWithTag:PROFILE_SEGMENT_INDEX];
        [self handleTabChange:self.tabs];
    }
}

- (void)showLinks {
    if (self.tabs.selectedSegment == LINKS_SEGMENT_INDEX) {
        return;
    }

    [self.tabs selectSegmentWithTag:LINKS_SEGMENT_INDEX];
    [self handleTabChange:self.tabs];
}

- (void)showLobbySidebar {
    [self.tabs setHidden:YES];
    [self displayView:rosterVC.view];
}


@end
