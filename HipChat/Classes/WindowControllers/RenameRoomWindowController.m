//
//  RenameRoomWindowController.m
//  HipChat
//
//  Created by Christopher Rivers on 1/4/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "RenameRoomWindowController.h"

#import "HipChatOSXApp.h"
#import "OSXHelpers.h"
#import "RegexKitLite.h"
#import "Room.h"
#import "RoomList.h"
#import "XMPPIQ.h"
#import "XMPPJID.h"

@implementation RenameRoomWindowController

- (void)windowDidLoad {
    [super windowDidLoad];

    if (!self.jid) {
        DDLogError(@"No jid was set when removing members from room");
        [self closeSheet:NSCancelButton];
    }

    HipChatOSXApp *app = HipChatOSXApp.instance;
    Room *room = [app getRoomInfo:self.jid];
    if (!room) {
        DDLogError(@"No room data found when showing room remove users sheet. Aborting...");
        [app showFlash:@"Room data was not available. Cannot manage users."];
        [self closeSheet:NSCancelButton];
    }

    self.roomNameField.stringValue = @"";
    self.titleText.stringValue = [NSString stringWithFormat:@"Rename the room '%@':", room.name];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Helper functions
///////////////////////////////////////////////////////////////////////

- (void)closeSheet:(NSInteger)returnCode {
    // If we're not cancelling, make sure something was selected
    // Otherwise, beep
    if (returnCode == NSOKButton) {
        if ([OSXHelpers validateChatName:self.roomNameField.stringValue]) {
            HipChatOSXApp *app = HipChatOSXApp.instance;
            Room *room = [app.roomList roomForJid:self.jid];
            if (!room) {
                [app showFlash:@"Rename failed. The room could not be found"];
            } else {
                IQCallback callback = ^(XMPPIQ *iq) {
                    if ([iq.type isEqualToString:@"error"]) {
                        [HipChatApp.instance showFlash:@"There was an error renaming the room."];
                    } else {
                        [HipChatApp.instance showFlash:@"Rename successful"];
                    }
                };
                [room rename:self.roomNameField.stringValue callback:callback];
            }
        } else {
            NSBeep();
            return;
        }
    }

    [super closeSheet:returnCode];
}

- (void)handleErrorCheckTimer {
    if ([OSXHelpers roomNameExists:self.roomNameField.stringValue]) {
        [self showError:@"That name is already taken."];
        return;
    } else if ([OSXHelpers roomNameHasInvalidCharacters:self.roomNameField.stringValue]) {
        [self showError:@"That room name contains invalid characters."];
        return;
    }
    [self hideError];
}

- (void)hideError {
    if (self.errorText.isHidden) {
        return;
    }

    [self.errorText setHidden:YES];
    NSRect winFrame = self.window.frame;
    NSRect fieldFrame = self.roomNameField.frame;
    NSRect errorFrame = self.errorText.frame;
    CGFloat textHeight = self.errorText.frame.size.height;
    CGFloat spacingHeight = (fieldFrame.origin.y - fieldFrame.size.height - errorFrame.origin.y);
    NSRect newWindowFrame = NSMakeRect(winFrame.origin.x,
                                       winFrame.origin.y,
                                       winFrame.size.width,
                                       winFrame.size.height - textHeight - spacingHeight);
    [self.window setFrame:newWindowFrame display:YES animate:YES];
}

- (void)showError:(NSString *)errorText {
    [self.errorText setStringValue:errorText];
    if (self.errorText.isHidden) {
        [self.errorText setHidden:NO];
        NSRect winFrame = self.window.frame;
        NSRect fieldFrame = self.roomNameField.frame;
        NSRect errorFrame = self.errorText.frame;
        CGFloat textHeight = self.errorText.frame.size.height;
        CGFloat spacingHeight = (fieldFrame.origin.y - fieldFrame.size.height - errorFrame.origin.y);
        NSRect newWindowFrame = NSMakeRect(winFrame.origin.x,
                                           winFrame.origin.y,
                                           winFrame.size.width,
                                           winFrame.size.height + textHeight + spacingHeight);
        [self.window setFrame:newWindowFrame display:YES animate:YES];
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark NSTextFieldDelegate functions
///////////////////////////////////////////////////////////////////////

- (void)controlTextDidChange:(NSNotification *)notification {
    if (errorCheckTimer) {
        [errorCheckTimer invalidate];
    }

    // Since this is a modal loop, we have to add the timer manually
    // using the modal mode. Using scheduledTimerWith... won't work
    errorCheckTimer = [NSTimer timerWithTimeInterval:0.25
                                              target:self
                                            selector:@selector(handleErrorCheckTimer)
                                            userInfo:nil
                                             repeats:NO];
    [[NSRunLoop currentRunLoop] addTimer:errorCheckTimer
                                 forMode:NSModalPanelRunLoopMode];
}

@end
