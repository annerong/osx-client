//
//  FocusOnlyBox.h
//  HipChat
//
//  FocusOnlyBox is an NSBox which only is visible as long as a subview inside it has focus
//  (the application's current firstResponder is one of the subviews)
//
//  Created by Christopher Rivers on 2/13/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface FocusOnlyBox : NSBox {
    NSArray *firstResponders;
    NSTimer *focusTimer;
}

@property id delegate;

@end
