//
//  ErrorMessageViewController.h
//  HipChat
//
//  Created by Christopher Rivers on 6/3/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ErrorMessageViewController : NSViewController

@property IBOutlet NSString *message;

@end
