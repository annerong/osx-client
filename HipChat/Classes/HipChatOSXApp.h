//
//  HipChatOSXApp.h
//  HipChat
//
//  Created by Christopher Rivers on 10/15/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "HipChatApp.h"
#import "DDLog.h"

@class IdleTimer;
@class ChatViewController;

@interface HipChatOSXApp : HipChatApp {
    const XMPPJID *lobbyJid;

    ThreadSafeArray *closedChats;
    IdleTimer *idleTimer;
    NSDate *lastRoomListFetch;
    NSSound *notifySound;
    ThreadSafeDictionary *rosterEntryCache;
    NSTimer *savePrefsTimer;
    ThreadSafeDictionary *unreadMentionChats;
}

///////////////////////////////////////////////////////////////////////
#pragma mark Singleton methods
///////////////////////////////////////////////////////////////////////

+ (HipChatOSXApp *)instance;

///////////////////////////////////////////////////////////////////////
#pragma mark Connection / Disconnection
///////////////////////////////////////////////////////////////////////

- (BOOL)triggerManualReconnect;

///////////////////////////////////////////////////////////////////////
#pragma mark Chat methods
///////////////////////////////////////////////////////////////////////

- (void)addToClosedChats:(XMPPJID *)jid index:(NSInteger)index;

- (void)focusChatAtIndex:(NSInteger)index;

- (void)focusCurrentChatInput;

- (BOOL)getUnreadMentionForJid:(XMPPJID *)jid;

- (void)joinChat:(XMPPJID *)jid;

- (void)joinChat:(XMPPJID *)jid withFocus:(BOOL)doFocus;

- (void)reopenLastChat;

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Helper methods
///////////////////////////////////////////////////////////////////////

- (void)appendEmoticonToCurrentMessage:(NSString *)text;

- (void)decreaseFontSize;

- (void)focusApp;

- (NSMutableArray *)getRosterEntriesForBareJids:(NSArray *)bareJids;

- (void)increaseFontSize;
    
- (void)muteAllMessagesForJid:(XMPPJID *)jid;
    
- (void)muteMessagesExceptMeForJid:(XMPPJID *)jid;
    
- (void)unmute:(XMPPJID *)jid;

- (void)updateMuteMenuStatus:(XMPPJID *)jid;

- (void)returnToNormalFontSize;

- (void)showChangePrivacyForJid:(XMPPJID *)jid;

- (void)showChangeTopic;

- (void)showCreateRoomForJid:(XMPPJID *)jid;

- (void)showJoinChat;

- (void)showManagePermsForJid:(XMPPJID *)jid;

- (void)showReconnectFlashWithMessage:(NSString *)message;

- (void)showRenameRoomForJid:(XMPPJID *)jid;

- (void)showRightSidebarFiles;

- (void)showRightSidebarLinks;

- (void)showRightSidebarPrimary;

- (void)showRoomInviteForJid:(XMPPJID *)jid;

- (void)showRoomRemoveMembersForJid:(XMPPJID *)jid;

- (void)showSettingsWindow;

- (void)toggleDevPrefs;

- (void)toggleLeftSidebar;

- (void)toggleRightSidebar;

- (void)updateUpdateUrlForceMinor:(BOOL)forceMinorCheck;

@property BOOL appIsFocused;

@end