//
//  SplitViewController.m
//  HipChat
//
//  Created by Christopher Rivers on 10/17/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "SplitViewController.h"

#import "HipChatOSXApp.h"
#import "ChatStackViewController.h"
#import "LobbyViewController.h"
#import "OSXHelpers.h"
#import "RightSidebarViewController.h"
#import "TabViewController.h"

CGFloat MIN_CENTER_WIDTH = 150.0;

@implementation SplitViewController

- (void)awakeFromNib {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFocusChat:) name:HipChatFocusChatNotification object:app];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFocusLobby:) name:HipChatFocusLobbyNotification object:app];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onToggleLeftSidebar:) name:HipChatToggleLeftSidebarNotification object:app];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onToggleRightSidebar:) name:HipChatToggleRightSidebarNotification object:app];

    // View we will switch to later
    chatStackVC = [[ChatStackViewController alloc] initWithNibName:@"ChatStack" bundle:[NSBundle mainBundle]];

    // Default views for signed-in mode
    tabVC = [[TabViewController alloc] initWithNibName:@"Tabs" bundle:[NSBundle mainBundle]];
    [self setLeftSidebar:tabVC.view];
    lobbyVC = [[LobbyViewController alloc] initWithNibName:@"Lobby" bundle:[NSBundle mainBundle]];
    [self setMainView:lobbyVC.view];
    sidebarVC = [[RightSidebarViewController alloc] initWithNibName:@"RightSidebar" bundle:[NSBundle mainBundle]];
    [self setRightSidebar:sidebarVC.view];

    // Set the delegate after the initial load
    // We want the split view to resize the views be default initially,
    // but after everything is loaded, we want to handle the resizing ourselves
    // (so that we can resize only the center panel)
    // If we don't do this, sometimes the sidebars may not show up/resize correctly on first load
    NSSplitView *splitView = (NSSplitView *)self.view;
    splitView.delegate = self;
}

- (void)setLeftSidebar:(NSView *)newView {
    NSView *currentLeftView = [[self.view subviews] objectAtIndex:0];
    newView.frame = currentLeftView.frame;
    leftSidebarWidth = currentLeftView.frame.size.width;
    [self.view replaceSubview:currentLeftView with:newView];
}

- (void)setMainView:(NSView *)newView {
    NSView *currentMainView = [[self.view subviews] objectAtIndex:1];
    newView.frame = currentMainView.frame;
    [self.view replaceSubview:currentMainView with:newView];
}

- (void)setRightSidebar:(NSView *)newView {
    NSView *currentRightView = [[self.view subviews] objectAtIndex:2];
    newView.frame = currentRightView.frame;
    rightSidebarWidth = currentRightView.frame.size.width;
    [self.view replaceSubview:currentRightView with:newView];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Notification handlers
///////////////////////////////////////////////////////////////////////

- (void)onFocusChat:(NSNotification *)notif {
    XMPPJID *jid = [[notif userInfo] objectForKey:@"jid"];
    if ([OSXHelpers isUserJid:jid]) {
        [sidebarVC showPrivateChatSidebar];
    } else {
        [sidebarVC showRoomSidebar];
    }
    [self setMainView:chatStackVC.view];
}

- (void)onFocusLobby:(NSNotification *)notif {
    [self setMainView:lobbyVC.view];
    [sidebarVC showLobbySidebar];
}

- (void)onToggleLeftSidebar:(NSNotification *)notif {
    NSSplitView *sv = (NSSplitView *)self.view;
    NSView *leftSidebarView = [sv.subviews objectAtIndex:0];
    if (leftSidebarView.isHidden) {
        [sv setPosition:MAX(leftSidebarWidth,[sv minPossiblePositionOfDividerAtIndex:0]) ofDividerAtIndex:0];
    } else {
        leftSidebarWidth = leftSidebarView.frame.size.width;
        [sv setPosition:0.0 ofDividerAtIndex:0];
    }
}

- (void)onToggleRightSidebar:(NSNotification *)notif {
    NSSplitView *sv = (NSSplitView *)self.view;
    NSView *rightSidebarView = [sv.subviews objectAtIndex:2];
    if (rightSidebarView.isHidden) {
        CGFloat dividerPosition = sv.frame.size.width - rightSidebarWidth;
        CGFloat minPosition = [sv minPossiblePositionOfDividerAtIndex:1];
        CGFloat defaultPosition = MAX(minPosition, sv.frame.size.width - 200);
        if (dividerPosition == sv.frame.size.width) {
            dividerPosition = defaultPosition;
        }
        if (dividerPosition < minPosition) {
            dividerPosition = minPosition;
        }
        [sv setPosition:dividerPosition ofDividerAtIndex:1];
    } else {
        rightSidebarWidth = rightSidebarView.frame.size.width;
        [sv setPosition:sv.frame.size.width ofDividerAtIndex:1];
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark NSSplitViewDelegate methods
///////////////////////////////////////////////////////////////////////

- (void)splitView:(NSSplitView *)splitView resizeSubviewsWithOldSize:(NSSize)oldSize {
    // Resize subviews so that all size change goes to top view (chat area)
    // Keep bottom view (input area a constant size)
    NSRect svFrame = splitView.frame;
    NSRect svBounds = splitView.bounds;
    CGFloat widthDiff = svFrame.size.width - oldSize.width;

    // Never set x < 0 || x > total split view width
    // Never set y < 0 || y > total split view height
    // Never set width/height greater than total split view width/height
    // Split view itself will always be >= 0 for x,y,width,height
    NSView *leftSidebarView = [splitView.subviews objectAtIndex:0];
    NSView *centerView = [splitView.subviews objectAtIndex:1];
    NSView *rightSidebarView = [splitView.subviews objectAtIndex:2];

    // The right and left sidebars retain their width/height if they're collapsed
    // If they're not being shown, make the center view expand to fill their space
    NSRect lsFrame = leftSidebarView.frame;
    CGFloat lsWidth = (leftSidebarView.isHidden ? 0 : lsFrame.size.width);
    NSRect centerFrame = centerView.frame;
    NSRect rsFrame = rightSidebarView.frame;
    CGFloat rsWidth = (rightSidebarView.isHidden ? 0 : rsFrame.size.width);

    CGFloat centerWidth = svBounds.size.width - rsWidth - lsWidth - (splitView.dividerThickness * 2);
    centerWidth = MAX(centerWidth, 150.0);
    NSRect newCenterFrame = NSMakeRect(centerFrame.origin.x,
                                       centerFrame.origin.y,
                                       centerWidth > 0 ? centerWidth : 0,
                                       svBounds.size.height);
    centerView.frame = newCenterFrame;

    rightSidebarView.frame = NSMakeRect(rsFrame.origin.x + widthDiff,
                                        rsFrame.origin.y,
                                        rsFrame.size.width,
                                        svBounds.size.height);
    leftSidebarView.frame = NSMakeRect(lsFrame.origin.x,
                                       lsFrame.origin.y,
                                       lsFrame.size.width,
                                       svBounds.size.height);
}

//- (BOOL)splitView:(NSSplitView *)splitView shouldAdjustSizeOfSubview:(NSView *)subview {
//    NSView *leftSidebarView = [splitView.subviews objectAtIndex:0];
//    NSView *centerView = [splitView.subviews objectAtIndex:1];
//    NSView *rightSidebarView = [splitView.subviews objectAtIndex:2];
//    BOOL centerAtMinWidth = (centerView.frame.size.width <= 150.0);
//    if (subview == leftSidebarView && !centerAtMinWidth) {
//        return NO;
//    } else if (subview == rightSidebarView && !centerAtMinWidth) {
//        return NO;
//    } else if (subview == centerView && centerAtMinWidth) {
//        return NO;
//    }
//    
//    return YES;
//}

- (CGFloat)splitView:(NSSplitView *)splitView constrainMaxCoordinate:(CGFloat)proposedMin ofSubviewAt:(NSInteger)dividerIndex {
    NSView *rightSidebarView = [splitView.subviews objectAtIndex:2];

    if (dividerIndex == 0) {
        return splitView.frame.size.width - rightSidebarView.frame.size.width - MIN_CENTER_WIDTH;
    } else if (dividerIndex == 1) {
        return splitView.frame.size.width - 80;
    }

    return NSIntegerMax;
}

- (CGFloat)splitView:(NSSplitView *)splitView constrainMinCoordinate:(CGFloat)proposedMin ofSubviewAt:(NSInteger)dividerIndex {
    NSView *leftSidebarView = [splitView.subviews objectAtIndex:0];
    if (dividerIndex == 0) {
        return 80;
    } else if (dividerIndex == 1) {
        return leftSidebarView.frame.size.width + MIN_CENTER_WIDTH;
    }

    return NSIntegerMax;
}

- (BOOL)splitView:(NSSplitView *)splitView canCollapseSubview:(NSView *)subview {
    NSView *leftSidebarView = [splitView.subviews objectAtIndex:0];
    NSView *rightSidebarView = [splitView.subviews objectAtIndex:2];

    if (subview == leftSidebarView || subview == rightSidebarView) {
        return YES;
    }

    return NO;
}

- (BOOL)splitView:(NSSplitView *)splitView shouldCollapseSubview:(NSView *)subview forDoubleClickOnDividerAtIndex:(NSInteger)dividerIndex {
    NSView *leftSidebarView = [splitView.subviews objectAtIndex:0];
    NSView *rightSidebarView = [splitView.subviews objectAtIndex:2];
    if (dividerIndex == 0 && subview == leftSidebarView) {
        return YES;
    } else if (dividerIndex == 1 && subview == rightSidebarView) {
        return YES;
    }

    return NO;
}

@end
