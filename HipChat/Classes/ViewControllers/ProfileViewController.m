//
//  ProfileViewController.m
//  HipChat
//
//  Created by Christopher Rivers on 12/3/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "ProfileViewController.h"

#import "XMPPJID.h"
#import "Profile.h"
#import "User.h"

#import "HipChatOSXApp.h"
#import "OSXHelpers.h"

@implementation ProfileViewController

static NSTimeInterval IDLE_UPDATE_INTERVAL = 60.0;
static NSMutableDictionary *photoCache;

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Static Methods
///////////////////////////////////////////////////////////////////////

+ (void)clearPhotoCache {
    [photoCache removeAllObjects];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Init methods
///////////////////////////////////////////////////////////////////////

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialize profile cache
        photoCache = [NSMutableDictionary dictionary];

        // Listen for focus chat notifications
        HipChatOSXApp *app = HipChatOSXApp.instance;
        [app addDelegate:self];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFocusChat:) name:HipChatFocusChatNotification object:app];
        idleUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:IDLE_UPDATE_INTERVAL
                                                           target:self
                                                         selector:@selector(updatePresence)
                                                         userInfo:nil
                                                          repeats:YES];
    }

    return self;
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Helper methods
///////////////////////////////////////////////////////////////////////

- (IBAction)handleEmailClick:(id)sender {
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"mailto:%@", self.email]]];
}

- (void)setDataWithProfile:(Profile *)profileData {
    dispatch_block_t block = ^{
        HipChatOSXApp *app = HipChatOSXApp.instance;
        if (![app.currentJid.bare isEqualToString:profileData.jid.bare]) {
            // This may happen if we make a request for profile data, then change tabs
            // before the request has a chance to make it back
            DDLogInfo(@"Profile jid does not match current jid (switched tabs?)");
            return;
        }

        if (!profileData) {
            DDLogError(@"Profile data was nil - cannot display profile");
            return;
        }

        DDLogInfo(@"Updating profile display with data: %@", profileData);

        // Make sure we clear out any old observation so it doesn't leak memory
        if (user) {
            [user removeObserver:self forKeyPath:@"lastPresence"];
        }

        // Observe any changes in presence
        user = [HipChatOSXApp.instance getUserInfo:profileData.jid];
        [user addObserver:self forKeyPath:@"lastPresence" options:NSKeyValueObservingOptionNew context:nil];

        self.emailButton.attributedTitle = [OSXHelpers linkStyleWithText:profileData.email
                                                                    font:[NSFont fontWithName:@"Lucida Grande" size:13.0]];
        self.email = profileData.email;
        self.jobTitle = profileData.title;
        self.name = profileData.name;

        self.photoUrl = [NSURL URLWithString:profileData.photoURLLarge];
        NSImage *photoImage = [photoCache objectForKey:profileData.photoURLLarge];
        if (!photoImage) {
            photoImage = [[NSImage alloc] initByReferencingURL:self.photoUrl];
        }
        if (photoImage) {
            [photoCache setObject:photoImage forKey:profileData.photoURLLarge];
            [self.photo setImage:photoImage];
        }
        self.presenceIcon = [OSXHelpers getStatusIconNameForUser:user];
        self.presenceText = (user.lastPresence.status ?
                             user.lastPresence.status :
                             [OSXHelpers getFriendlyStatusForUser:user includeIdle:YES]);
        self.isHiddenNum = [NSNumber numberWithBool:NO];
    };

    if (dispatch_get_current_queue() == dispatch_get_main_queue())
		block();
	else
		dispatch_async(dispatch_get_main_queue(), block);
}

- (void)updatePresence {
    if ([self.isHiddenNum boolValue]) {
        return;
    }

    self.presenceIcon = [OSXHelpers getStatusIconNameForUser:user];
    self.presenceText = [OSXHelpers getFriendlyStatusForUser:user includeIdle:YES];
}

- (void)updateProfileForJid:(XMPPJID *)jid {
    // Set hidden before updating
    self.isHiddenNum = [NSNumber numberWithBool:YES];

    // If we're currently displaying a room, there is no profile
    if ([OSXHelpers isRoomJid:jid]) {
        return;
    }

    HipChatOSXApp *app = HipChatOSXApp.instance;
    Profile *cachedData = [app getProfileData:jid];
    if (!cachedData) {
        [app requestProfileForJid:jid callback:^(XMPPIQ *iq) {
            [app handleProfileResponse:iq];
            [self setDataWithProfile:[app getProfileData:jid]];
        }];
    } else {
        [self setDataWithProfile:cachedData];
    }
    DDLogInfo(@"Updating profile display for jid %@", jid);
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Notification handlers
///////////////////////////////////////////////////////////////////////

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    // Trigger property updates on the main queue, since they will trigger UI updates
    if (dispatch_get_current_queue() == dispatch_get_main_queue()) {
        [self updatePresence];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updatePresence];
        });
    }
}

- (void)onFocusChat:(NSNotification *)notif {
    XMPPJID *jid = [[notif userInfo] objectForKey:@"jid"];
    [self updateProfileForJid:jid];
}


@end
