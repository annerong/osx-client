//
//  RightSidebarEntry.h
//  HipChat
//
//  Created by Christopher Rivers on 4/8/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RightSidebarEntry : NSObject

@property NSString *entryId;
@property (readonly) NSString *timeAgo;
@property NSDate *uploadDate;

@end
