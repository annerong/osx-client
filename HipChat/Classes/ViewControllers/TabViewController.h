//
//  TabViewController.h
//  HipChat
//
//  Created by Christopher Rivers on 10/19/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class PrettyBorderBox;
@class StatusBox;
@class User;

@interface TabViewController : NSViewController <NSTableViewDelegate> {
    NSInteger currentSelectedIndex;
    
    BOOL initialized;
    BOOL changingContent;
    User *currentUser;
}

- (IBAction)handleFeedbackClick:(id)sender;
- (IBAction)handlePhotoClick:(id)sender;

@property IBOutlet PrettyBorderBox *helpBox;
@property IBOutlet NSIndexSet *selectedIndex;
@property IBOutlet StatusBox *statusBox;
@property IBOutlet NSTableView *tableView;
@property IBOutlet NSArrayController *tabArrayController;
@property IBOutlet NSMutableArray *tabs;

@end
