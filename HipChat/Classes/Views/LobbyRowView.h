//
//  LobbyRowView.h
//  HipChat
//
//  Created by Christopher Rivers on 10/23/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "TableRowHighlightView.h"

@interface LobbyRowView : TableRowHighlightView {
    NSTrackingArea *trackingArea;
}

@end
