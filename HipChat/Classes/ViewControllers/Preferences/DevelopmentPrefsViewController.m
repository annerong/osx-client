//
//  DevelopmentPrefsViewController.m
//  HipChat
//
//  Created by Christopher Rivers on 4/12/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "DevelopmentPrefsViewController.h"

#import "HipChatOSXApp.h"
#import "OSXHelpers.h"
#import "XMPPStream.h"

@implementation DevelopmentPrefsViewController

const NSInteger CUSTOM_SERVER_TAG = 0;
const NSInteger DEFAULT_SERVER_TAG = 1;

- (void)awakeFromNib {
    // Remove all the old, manually set preferences
    // These values will now get returned by the server during auth
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs removeObjectForKey:@"apiHost"];
    [prefs removeObjectForKey:@"chatHost"];
    [prefs removeObjectForKey:@"mucHost"];
    [prefs removeObjectForKey:@"webHost"];
    [prefs synchronize];

    self.connectRadios.target = self;
    self.connectRadios.action = @selector(onRadioChange:);
    BOOL useCustomHost = [[NSUserDefaults standardUserDefaults] boolForKey:@"useCustomConnectHost"];
    if (useCustomHost) {
        [self.connectRadios selectCellWithTag:CUSTOM_SERVER_TAG];
    } else {
        [self.connectRadios selectCellWithTag:DEFAULT_SERVER_TAG];
    }
    [self onRadioChange:self.connectRadios];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark MASPreferencesViewController methods
///////////////////////////////////////////////////////////////////////

-(NSString *)identifier{
    return @"Connection";
}

-(NSImage *)toolbarItemImage{
    return [NSImage imageNamed:NSImageNameNetwork];
}

-(NSString *)toolbarItemLabel{
    return @"Connection";
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark UI Listeners
///////////////////////////////////////////////////////////////////////

- (IBAction)onRadioChange:(id)sender {
    NSButtonCell *selectedCell = [sender selectedCell];
    if (selectedCell.tag == CUSTOM_SERVER_TAG) {
        [self.connectField setEnabled:YES];
        [self.validateCertCheck setEnabled:YES];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"useCustomConnectHost"];
    } else {
        [self.connectField setEnabled:NO];
        [self.validateCertCheck setEnabled:NO];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"useCustomConnectHost"];
    }
}

@end
