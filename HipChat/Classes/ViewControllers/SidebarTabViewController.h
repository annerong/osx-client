//
//  SidebarTabViewController.h
//  HipChat
//
//  Created by Christopher Rivers on 4/8/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class XMPPIQ;
@class XMPPJID;
@class RightSidebarEntry;

@interface SidebarTabViewController : NSViewController {
    BOOL allEntriesLoaded;
    BOOL requestInProgress;
    NSDate *oldestDate;
    NSDate *newestDate;
}

- (void)clearEntries;
- (RightSidebarEntry *)generateEntryWithItem:(NSXMLElement *)item;
- (NSInteger)getFetchCount;
- (NSString *)getRequestNamepsace;
- (void)handleEntryClick:(id)entry;
- (void)updateWithJid:(XMPPJID *)jid;

@property IBOutlet NSArrayController *entriesArrayController;
@property IBOutlet NSMutableArray *entries;
@property IBOutlet NSTableView *tableView;
@property IBOutlet NSScrollView *scrollView;
@property IBOutlet NSView *noEntriesView;

@end
