//
//  EmoticonMenuViewController.m
//  HipChat
//
//  Created by Christopher Rivers on 5/7/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "EmoticonMenuViewController.h"

#import "EmoticonMenuItem.h"
#import "Linkify.h"

@implementation EmoticonMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        NSArray *defaultEmoticons = [Linkify getDefaultEmoticons];
        self.emoticons = [NSMutableArray arrayWithCapacity:defaultEmoticons.count];
        for (NSDictionary *eInfo in defaultEmoticons) {
            // Don't include the .png ending... we'll just look up the file using the [NSImage imageNamed:] method
            NSString *icon = [[eInfo objectForKey:@"image"] stringByReplacingOccurrencesOfString:@".png" withString:@""];
            NSString *shortcut = [eInfo objectForKey:@"shortcut"];
            if ([shortcut isEqualToString:@";p"] || [shortcut isEqualToString:@":')"]) {
                continue;
            }
            [self.emoticons addObject:[EmoticonMenuItem emoticonMenuItemWithIcon:icon shortcut:shortcut]];
        }
    }

    return self;
}

@end
