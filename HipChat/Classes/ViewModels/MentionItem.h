//
//  MentionItem.h
//  HipChat
//
//  Created by Christopher Rivers on 11/28/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RoomOccupant;
@class User;

@interface MentionItem : NSObject

- (id)initWithOccupant:(RoomOccupant *)occupant;
- (id)initWithUser:(User *)user;
- (void)checkMatchAndHighlight:(NSString *)matchStr;

@property IBOutlet NSString *icon;
@property IBOutlet NSMutableAttributedString *label;
@property IBOutlet NSString *mention;

@property (readwrite, assign) BOOL isFirstNameMatch;
@property (readwrite, assign) BOOL isInRoom;
@property (readwrite, assign) BOOL isMatch;
@property (readwrite, assign) BOOL isMentionMatch;
@property (readwrite, assign) BOOL isSelf;

@property (readonly) NSInteger matchRating;

@end
