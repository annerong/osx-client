//
//  HipChatOSXConstants.h
//  HipChat
//
//  Created by Christopher Rivers on 11/21/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

FOUNDATION_EXPORT NSString *const HipChatFocusAppNotification;
FOUNDATION_EXPORT NSString *const HipChatFocusChatAtIndexNotification;
FOUNDATION_EXPORT NSString *const HipChatShowChangePrivacyNotification;
FOUNDATION_EXPORT NSString *const HipChatShowChangeTopicNotification;
FOUNDATION_EXPORT NSString *const HipChatShowCreateRoomNotification;
FOUNDATION_EXPORT NSString *const HipChatFocusChatAtIndexNotification;
FOUNDATION_EXPORT NSString *const HipChatShowJoinChatNotification;
FOUNDATION_EXPORT NSString *const HipChatShowLoginWindowNotification;
FOUNDATION_EXPORT NSString *const HipChatShowMainWindowNotification;
FOUNDATION_EXPORT NSString *const HipChatShowManagePermsNotification;
FOUNDATION_EXPORT NSString *const HipChatShowRenameRoomNotification;
FOUNDATION_EXPORT NSString *const HipChatShowRoomInviteNotification;
FOUNDATION_EXPORT NSString *const HipChatShowRoomRemoveMembersNotification;
FOUNDATION_EXPORT NSString *const HipChatShowSearchNotification;
FOUNDATION_EXPORT NSString *const HipChatShowSettingsWindowNotification;
FOUNDATION_EXPORT NSString *const HipChatShowSidebarFilesNotification;
FOUNDATION_EXPORT NSString *const HipChatShowSidebarLinksNotification;
FOUNDATION_EXPORT NSString *const HipChatShowSidebarPrimaryNotification;
FOUNDATION_EXPORT NSString *const HipChatToggleLeftSidebarNotification;
FOUNDATION_EXPORT NSString *const HipChatToggleRightSidebarNotification;

FOUNDATION_EXPORT NSInteger const NORMAL_FONT_SIZE;

typedef enum {
    kCloseWindowTag = 1,
    kCloseTabTag = 2
} FileMenuItemTag;

typedef enum {
    kHipChatMenuTag = 1,
    kFileMenuTag = 2,
    kEditMenuTag = 3,
    kViewMenuTag = 4,
    kRoomMenuTag = 5,
    kWindowMenuTag = 6,
    kHelpMenuTag = 7
} MainMenuItemTag;

typedef enum {
    kMuteMenuTag = 1
} RoomMenuItemTag;

typedef enum {
    kMuteRoomAllTag = 1,
    kMuteRoomExceptMeTag = 2
} MuteMenuItemTag;

enum ActiveChatNotificationSetting {
    ActiveChatNotificationOn,
    ActiveChatNotificationMuteAll,
    ActiveChatNotificationMuteAllExceptMe,
};
typedef enum ActiveChatNotificationSetting ActiveChatNotificationSetting;
