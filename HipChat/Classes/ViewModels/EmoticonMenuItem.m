//
//  EmoticonMenuItem.m
//  HipChat
//
//  Created by Christopher Rivers on 5/7/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "EmoticonMenuItem.h"

@implementation EmoticonMenuItem

+ (EmoticonMenuItem *)emoticonMenuItemWithIcon:(NSString *)icon shortcut:(NSString *)shortcut {
    EmoticonMenuItem *emi = [[EmoticonMenuItem alloc] init];
    emi.icon = icon;
    emi.shortcut = shortcut;
    return emi;
}

@end
