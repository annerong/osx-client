//
//  ProfileViewController.h
//  HipChat
//
//  Created by Christopher Rivers on 12/3/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class User;

@interface ProfileViewController : NSViewController {
    NSTimer *idleUpdateTimer;
    User *user;
}

+ (void)clearPhotoCache;

- (IBAction)handleEmailClick:(id)sender;

@property IBOutlet NSString *email;
@property IBOutlet NSButton *emailButton;
@property NSString *skypeName;
@property (strong) IBOutlet NSButton *skypeButton;
@property IBOutlet NSNumber *isHiddenNum;
@property IBOutlet NSString *jobTitle;
@property IBOutlet NSString *name;
@property IBOutlet NSURL *photoUrl;
@property IBOutlet NSImageView *photo;
@property IBOutlet NSString *presenceIcon;
@property IBOutlet NSString *presenceText;

@end
