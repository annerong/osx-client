//
//  TabViewController.m
//  HipChat
//
//  Created by Christopher Rivers on 10/19/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "TabViewController.h"

#import "HipChatUser.h"
#import "User.h"
#import "XMPPJID.h"

#import "HipChatOSXApp.h"
#import "OSXHelpers.h"
#import "StatusBox.h"
#import "TabData.h"
#import "TabRowView.h"

@implementation TabViewController

@synthesize tabs;

static const TabData *lobbyTabData;

static NSString *TAB_DRAG_TYPE = @"TabDragType";

- (void)awakeFromNib {
    if (initialized) {
        return;
    }

    [self.tableView registerForDraggedTypes:[NSArray arrayWithObjects:TAB_DRAG_TYPE, nil]];
    self.statusBox.topBorderColor = RGB(37.0, 44.0, 57.0);
    self.statusBox.topBorderInsetColor = RGB(81.0, 88.0, 105.0);
    self.helpBox.topBorderColor = RGB(37.0, 44.0, 57.0);
    self.helpBox.topBorderInsetColor = RGB(81.0, 88.0, 105.0);
    self.helpBox.leftBorderColor = RGB(36.0, 43.0, 55.0);
    self.helpBox.leftBorderInsetColor = RGB(81.0, 88.0, 105.0);
    self.statusBox.rightBorderInsetColor = RGB(81.0, 88.0, 105.0);

    initialized = YES;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        HipChatOSXApp *app = HipChatOSXApp.instance;
        [app addDelegate:self];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFocusChat:) name:HipChatFocusChatNotification object:app];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFocusChat:) name:HipChatFocusChatAtIndexNotification object:app];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFocusLobby:) name:HipChatFocusLobbyNotification object:app];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFocusNextChat:) name:HipChatFocusNextChatNotification object:app];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFocusPreviousChat:) name:HipChatFocusPreviousChatNotification object:app];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onJoinChat:) name:HipChatJoinChatNotification object:app];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onLeaveChat:) name:HipChatLeaveChatNotification object:app];

        lobbyTabData = [[TabData alloc] initWithJid:app.lobbyJid];
        self.tabs = [NSMutableArray arrayWithObject:lobbyTabData];
        currentUser = nil;
        currentSelectedIndex = 0;
        [self selectIndex:0];
//        self.selectedIndex = [NSIndexSet indexSetWithIndex:0];
    }
    
    return self;
}

- (void)dealloc {
    [currentUser removeObserver:self forKeyPath:@"lastPresence"];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Action handlers
///////////////////////////////////////////////////////////////////////

- (IBAction)handleFeedbackClick:(id)sender {
    NSURL *feedbackURL = [NSURL URLWithString:@"https://www.hipchat.com/help/osx_app_forum"];
    [[NSWorkspace sharedWorkspace] openURL:feedbackURL];
}

- (IBAction)handlePhotoClick:(id)sender {
    NSURL *accountURL = [NSURL URLWithString:
                          [NSString stringWithFormat:@"https://%@/account", HipChatOSXApp.instance.webHost]];
    [[NSWorkspace sharedWorkspace] openURL:accountURL];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark KVO compliance methods
///////////////////////////////////////////////////////////////////////

- (void)insertObject:(TabData *)tabData inTabsAtIndex:(NSUInteger)index {
    [tabs insertObject:tabData atIndex:index];
}

- (void)removeObjectFromTabsAtIndex:(NSUInteger)index {
    [tabs removeObjectAtIndex:index];
}

- (void)replaceObjectInTabsAtIndex:(NSUInteger)index withObject:(id)object {
    [tabs replaceObjectAtIndex:index withObject:object];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark hipchatAppDelegate methods
///////////////////////////////////////////////////////////////////////

- (void)hipChatAppUnreadChangedForJid:(XMPPJID *)jid {
    [self updateUnreadCountForJid:jid];
}

- (void)hipChatAppFullyLoaded {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    if (currentUser) {
        // This happens when we reconnect
        [currentUser removeObserver:self forKeyPath:@"lastPresence"];
    }
    currentUser = [app getUserInfo:app.currentUser.jid];
    [currentUser addObserver:self forKeyPath:@"lastPresence" options:NSKeyValueObservingOptionNew context:nil];

    [self refreshTabs];
    [self updateStatusWithUser:currentUser];

    [self.statusBox.photo setImage:[[NSImage alloc] initWithContentsOfURL:app.currentUser.photoURLSmall]];
}

- (void)hipChatAppWillManuallyDisconnect {
    [self.tabArrayController removeObjects:[self.tabs subarrayWithRange:NSMakeRange(1, [self.tabs count] - 1)]];
    [self.tableView reloadData];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark NSTableViewDelegate
///////////////////////////////////////////////////////////////////////

- (NSTableRowView *)tableView:(NSTableView *)tableView rowViewForRow:(NSInteger)row {
    return [[TabRowView alloc] init];
}

// Tell the table to update the row above the one that's changing
// since we need to redraw its bottom border as part of the selection style
- (BOOL)selectionShouldChangeInTableView:(NSTableView *)table {
    if (changingContent) {
        return NO;
    }

    return YES;
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification {
    NSTableView *table = (NSTableView *)[notification object];
    if (table.selectedRow != currentSelectedIndex) {
        [table scrollRowToVisible:table.selectedRow];
        if (currentSelectedIndex > 0) {
            [table setNeedsDisplayInRect:[table rectOfRow:(currentSelectedIndex - 1)]];
            [table setNeedsDisplayInRect:[table rectOfRow:currentSelectedIndex]];
        }

        currentSelectedIndex = table.selectedRow;

        NSArray *arrangedTabs = self.tabArrayController.arrangedObjects;
        if (currentSelectedIndex > arrangedTabs.count - 1 || currentSelectedIndex < 0) {
            DDLogError(@"Got bad index from tabs table: %ld", (long)currentSelectedIndex);
            return;
        }
        TabData *tabData = [arrangedTabs objectAtIndex:currentSelectedIndex];
        DDLogInfo(@"Tab selection changed -- new index: %ld, new jid: %@", currentSelectedIndex, tabData.jid);

        HipChatOSXApp *app = HipChatOSXApp.instance;
        [app focusChat:tabData.jid];
    }
}

- (BOOL)tableView:(NSTableView *)aTableView
writeRowsWithIndexes:(NSIndexSet *)rowIndexes
     toPasteboard:(NSPasteboard *)pboard {
    if (![self.view.window isKeyWindow]) {
        return NO;
    }

    if (rowIndexes.firstIndex == 0) {
        return NO;
    }

	NSArray *typesArray = [NSArray arrayWithObjects:TAB_DRAG_TYPE, nil];
	[pboard declareTypes:typesArray owner:self];

    TabData *tabData = [self.tabArrayController.arrangedObjects objectAtIndex:rowIndexes.firstIndex];
	[pboard setString:tabData.jid.bare forType:TAB_DRAG_TYPE];

	return YES;
}

- (NSDragOperation)tableView:(NSTableView *)table
                validateDrop:(id <NSDraggingInfo>)info
                 proposedRow:(int)row
       proposedDropOperation:(NSTableViewDropOperation)op {
	// Only support internal drags (i.e. moves)
	if ([info draggingSource] != self.tableView)
		return NSDragOperationNone;

    if (row == 0) {
        return NSDragOperationNone;
    }

	[table setDropRow:row dropOperation:NSTableViewDropAbove];
	return NSDragOperationMove;
}

- (BOOL)tableView:(NSTableView *)table
       acceptDrop:(id <NSDraggingInfo>)info
              row:(int)row
    dropOperation:(NSTableViewDropOperation)op {
    // Don't allow draggng of the Lobby tab (Tab 0)
    // Rows below index 0 is undefined (should never happen)
	if (row <= 0) {
        return NO;
    }

	// Only support internal drags (i.e. moves)
	if ([info draggingSource] != self.tableView) {
		return NO;
    }

	NSString *bareJid = [[info draggingPasteboard] stringForType:TAB_DRAG_TYPE];
    TabData *draggedTabData = nil;
    TabData *tabData = nil;
    NSUInteger tabIdx = 0;
    NSArray *arrangedObjects = self.tabArrayController.arrangedObjects;
    for (tabIdx = 0; tabIdx < arrangedObjects.count; tabIdx++) {
        tabData = [arrangedObjects objectAtIndex:tabIdx];
        if ([tabData.jid.bare isEqualToString:bareJid]) {
            draggedTabData = tabData;
            break;
        }
    }
    if (tabIdx == row) {
        // If we're dropping at the same location, we don't need to do anything
        return YES;
    }

    NSInteger currentIndex = self.tableView.selectedRow;
    NSInteger newSelectedIndex = currentIndex;
    if (row <= currentIndex && tabIdx > currentIndex) {
        // If we drag a tab to a row above the currently selected row, we will need
        // to increase the current selected index (as there's an additional tab above it now)
        newSelectedIndex += 1;
    } else if (row > currentIndex && tabIdx < currentIndex) {
        // If we drag a tab to a row below the currently selected row, we will need
        // to decrease the current selected index
        newSelectedIndex -= 1;
    }

    if (row > tabIdx) {
        // If the row we dropped at was greater than the dragged tab then decrement, 
        // when we remove the tab, all the rows at a higher index will be
        // decreased by 1
        row--;
    }
    if (tabIdx == currentIndex) {
        // If we drag the selected tab, just set the new selection to the dropped row
        newSelectedIndex = row;
    }

    changingContent = YES;
    if (draggedTabData) {
        [self.tabArrayController removeObject:draggedTabData];
        [self.tabArrayController insertObject:draggedTabData atArrangedObjectIndex:row];
        [self selectIndex:newSelectedIndex];
    }
    changingContent = NO;

    NSArray *newArrangedObjects = self.tabArrayController.arrangedObjects;
    NSMutableArray *newAutoJoin = [NSMutableArray arrayWithCapacity:[newArrangedObjects count]];
    for (tabIdx = 1; tabIdx < newArrangedObjects.count; tabIdx++) {
        tabData = [arrangedObjects objectAtIndex:tabIdx];
        [newAutoJoin addObject:tabData.jid.bare];
    }
    HipChatOSXApp *app = HipChatOSXApp.instance;
    [app setAutoJoin:newAutoJoin];
    [app savePreferences];

	return YES;
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Helper methods
///////////////////////////////////////////////////////////////////////

- (BOOL)focusChatForIndex:(NSInteger)index {
    // Negative indexes indicate "<index> tabs from the end"
    // We should only ever really get -1 for this unless we change the shortcut keys
    if (index < 0) {
        index = ([self.tabArrayController.arrangedObjects count] + index);
    }
    if (index >= [self.tabArrayController.arrangedObjects count]) {
        // Might get this if a user tries to use cmd-# with # > chat count
        return NO;
    }

    [self selectIndex:index];
    return YES;
}

// Return whether we were able to focus a tab for the given jid
- (BOOL)focusChatForJid:(XMPPJID *)jid {
    NSInteger tabIdx = [self getTabIndexForJid:jid];
    if (tabIdx != NSNotFound && self.tableView.selectedRow != tabIdx) {
        [self selectIndex:tabIdx];
        return YES;
    }

    return NO;
}

- (NSInteger)getTabIndexForJid:(XMPPJID *)jid {
    return [self.tabs indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        if ([[jid bare] isEqualToString:[[obj jid] bare]]) {
            *stop = YES;
            return YES;
        }
        return NO;
    }];
}

- (void)refreshTabs { @synchronized(HipChatApp.instance) {
    DDLogInfo(@"Refreshing tab list");
    NSArray *activeChatJids = [HipChatOSXApp.instance activeChatJids];
    NSMutableArray *tabDatas = [NSMutableArray arrayWithCapacity:(activeChatJids.count+1)];
    [tabDatas addObject:lobbyTabData];
    for (XMPPJID *jid in activeChatJids) {
        TabData *tabData = [[TabData alloc] initWithJid:jid];
        [tabDatas addObject:tabData];
    }

    // Wrap this with "changingContent" to avoid the tableView resetting to index 0
    // Also refresh unread counts whenever we refresh tabs
    changingContent = YES;
    self.tabs = tabDatas;

    // For some reason, despite changingContent, this causes the table to
    // deselect the currently selected tab. (Maybe because the content briefly
    // becomes empty, instead of editing an exiting array?)
    // Either way, just re-select it after updating the content array
    [self selectIndex:currentSelectedIndex];
    [self updateUnreadCountForJid:nil];
    changingContent = NO;
}}

- (void)selectIndex:(NSInteger)index {
    [self.tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:index] byExtendingSelection:NO];
}

- (void)updateStatusWithUser:(User *)user {
    NSString *iconName = [OSXHelpers getStatusIconNameForUser:user];
    [self.statusBox.statusIcon setImage:[NSImage imageNamed:iconName]];
    NSString *statusMessage = [OSXHelpers getFriendlyStatusForUser:user includeIdle:NO];
    [self.statusBox.statusMessage setStringValue:statusMessage];
    [self.statusBox.statusMessage setToolTip:statusMessage];
}

- (void)updateUnreadCountForJid:(XMPPJID *)jid {
	HipChatOSXApp *app = HipChatOSXApp.instance;

    for (TabData *tabData in self.tabs) {
        // If the jid is nil, we will be clearing all unread counts (match on all tabs)
        if (jid == nil || [tabData.jid.bare isEqualToString:jid.bare]) {
            // Setting color to nil will return it to its default color
            BOOL hasUnreadMention = [app getUnreadMentionForJid:tabData.jid];
            if (hasUnreadMention) {
                tabData.color = RGB(255.0, 255.0, 119.0);
            } else {
                BOOL hasUnread = [app getUnreadForJid:tabData.jid];
                tabData.color = (hasUnread ? RGB(255.0, 119.0, 119.0) : nil);
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Notification handlers
///////////////////////////////////////////////////////////////////////

- (void)onCloseChatClick:(id)sender {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    TabData *data = (TabData *)[sender objectValue];
    DDLogInfo(@"Clicked close chat for JID: %@", data.jid);
    [app closeChat:data.jid isManual:YES];
}

- (void)onFocusChat:(NSNotification *)notif {
    // JID-based focus chat
    XMPPJID *jid = [[notif userInfo] objectForKey:@"jid"];
    if (jid) {
        [self focusChatForJid:jid];
    } else {
        // Index-based focus chat
        NSInteger index = [[[notif userInfo] objectForKey:@"index"] intValue];
        [self focusChatForIndex:index];
    }
}

- (void)onFocusLobby:(NSNotification *)notif {
    if (self.tableView.selectedRow != 0) {
        [self selectIndex:0];
    }
}

- (void)onFocusNextChat:(NSNotification *)notif {
    NSInteger selectedRowNumber = [self.tableView selectedRow];
    if (selectedRowNumber > 0) {
        [self.tableView setNeedsDisplayInRect:[self.tableView rectOfRow:(selectedRowNumber - 1)]];
        [self.tableView setNeedsDisplayInRect:[self.tableView rectOfRow:selectedRowNumber]];
    }

    if (self.tableView.selectedRow < self.tableView.numberOfRows - 1) {
        [self selectIndex:self.tableView.selectedRow+1];
    } else {
        [self selectIndex:0];
    }
}

- (void)onFocusPreviousChat:(NSNotification *)notif {
    if (self.tableView.selectedRow > 0 && self.tableView.selectedRow-1 < self.tableView.numberOfRows-1) {
        [self selectIndex:self.tableView.selectedRow-1];
    } else {
        [self selectIndex:[self.tabArrayController.arrangedObjects count]-1];
    }
}

- (void)onJoinChat:(NSNotification *)notif {
    NSDictionary *args = [notif userInfo];
    XMPPJID *jid = [args objectForKey:@"jid"];
    TabData *tabData = [[TabData alloc] initWithJid:jid];

    // Wrap this with "changingContent" to avoid the tableView resetting to index 0
    changingContent = YES;
    NSNumber *indexNum = [args objectForKey:@"index"];
    if (indexNum == nil) {
        [self.tabArrayController addObject:tabData];
    } else {
        NSInteger index = [indexNum integerValue];
        if (index >= 0) {
            // Make sure we never insert beyond the end of our current set of tabs
            index = MIN(index, [self.tabArrayController.arrangedObjects count]);
            [self.tabArrayController insertObject:tabData atArrangedObjectIndex:index];
        } else {
            [self.tabArrayController addObject:tabData];
        }
    }
    changingContent = NO;

    if ([[[notif userInfo] objectForKey:@"doFocus"] boolValue]) {
        [self focusChatForJid:jid];
    }
}

- (void)onLeaveChat:(NSNotification *)notif {
    NSDictionary *args = [notif userInfo] ;
    XMPPJID *jid = [args objectForKey:@"jid"];
    NSUInteger tabIdx = [self.tabs indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        if ([[jid bare] isEqualToString:[[obj jid] bare]]) {
            *stop = YES;
            return YES;
        }
        return NO;
    }];

    HipChatOSXApp *app = HipChatOSXApp.instance;
    if (tabIdx != NSNotFound) {
        changingContent = YES;

        // Add to closed chats on manual leaves so we can reopen later
        if ([[args objectForKey:@"isManual"] boolValue]) {
            [app addToClosedChats:jid index:tabIdx];
        }
        [self.tabArrayController removeObject:[self.tabs objectAtIndex:tabIdx]];

        // If we're closing the current chat, focus the tabs on the same index as previous
        // If that index isn't available, focus on the last tab
        if ([jid.bare isEqualToString:app.currentJid.bare]) {
            NSInteger numTabs = [self.tabArrayController.arrangedObjects count];
            TabData *newTabData = (numTabs > currentSelectedIndex ?
                                   [self.tabArrayController.arrangedObjects objectAtIndex:currentSelectedIndex] :
                                   [self.tabArrayController.arrangedObjects objectAtIndex:(numTabs-1)]);
            [app focusChat:newTabData.jid];
        }
        changingContent = NO;
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark KVO
///////////////////////////////////////////////////////////////////////

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    [self updateStatusWithUser:object];
}

@end
