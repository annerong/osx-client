//
//  AppDelegate.h
//  HipChat
//
//  Created by Christopher Rivers on 10/15/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Growl/GrowlApplicationBridge.h>
#import <HockeySDK/BITCrashReportManagerDelegate.h>

@class LoginWindowController;
@class MainWindowController;
@class SettingsWindowController;

@interface AppDelegate : NSObject <NSApplicationDelegate, BITCrashReportManagerDelegate> {
    NSDictionary *windows;
    NSWindowController *lastPrimaryWindowController;
}

- (IBAction)checkForUpdates:(id)sender;
- (IBAction)logout:(id)sender;
- (IBAction)preferences:(id)sender;
- (IBAction)setStatusAvailable:(id)sender;
- (IBAction)setStatusAway:(id)sender;
- (IBAction)setStatusDnd:(id)sender;
- (IBAction)submitFeedback:(id)sender;

@property (nonatomic, assign) IBOutlet NSApplication *application;

@end
