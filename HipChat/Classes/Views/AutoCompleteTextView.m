//
//  AutoCompleteTextView.m
//  HipChat
//
//  Created by Christopher Rivers on 2/25/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "AutoCompleteTextView.h"

#import "RegexKitLite.h"

#import "AutoCompleteViewController.h"
#import "MentionItem.h"

@implementation AutoCompleteTextView

- (void)awakeFromNib {
    self.trigger = @"@";
}

- (void)appendMentionToMessage:(NSString *)mentionString {
    NSUInteger cursorLoc = [[[self selectedRanges] objectAtIndex:0] rangeValue].location;
    NSRange textRange = NSMakeRange(0, cursorLoc);
    NSString *textToCursor = [self.string substringWithRange:textRange];

    // Append the mention from the last space or @-symbol before the cursor
    NSUInteger triggerLoc = [textToCursor rangeOfString:self.trigger options:NSBackwardsSearch].location;
    if (triggerLoc == NSNotFound) {
        triggerLoc = 0;
    }
    NSUInteger spaceLoc = [textToCursor rangeOfString:@" " options:NSBackwardsSearch].location;
    if (spaceLoc == NSNotFound) {
        spaceLoc = 0;
    } else {
        // Add 1 to indicate the index AFTER the space, not before it
        spaceLoc += 1;
    }

    if (spaceLoc > triggerLoc) {
        triggerLoc = spaceLoc;
    }
    NSRange mentionRange = NSMakeRange(triggerLoc, textToCursor.length-triggerLoc);
    NSString *replaceString = [NSString stringWithFormat:@"@%@ ", mentionString];
    [self.textStorage replaceCharactersInRange:mentionRange withString:replaceString];
}

- (void)autocompleteSelectedItem:(MentionItem *)selection {
    [self appendMentionToMessage:selection.mention];
    [self.window makeFirstResponder:self];
}

- (void)didChangeText {
    [super didChangeText];

    NSInteger cursorLoc = [[[self selectedRanges] objectAtIndex:0] rangeValue].location;
    NSRange searchRange = NSMakeRange(0, cursorLoc);
    NSString *searchString = [self.string substringWithRange:searchRange];
    NSString *regex = [NSString stringWithFormat:@"(?:^|\\W)%@([\\w]*)$", self.trigger];
    NSArray *matches = [searchString captureComponentsMatchedByRegex:regex range:searchRange];
    if (matches.count > 1) {
        acSearch = [matches objectAtIndex:1];
        if (!acPopover) {
            [self showAutoCompletePopover];
        } else {
            [acController updateMentionListWithSearch:acSearch];
        }

        // Only show autocomplete when there are actually matches
        if ([acController getMatchCount] == 0) {
            [self hideAutoComplete];
        } else {
            [self updateAutoCompletePopoverSize];
            [self updateAutoCompletePopoverPosition];
        }
    } else {
        [self hideAutoComplete];
    }
}

- (void)doCommandBySelector:(SEL)aSelector {
    if (acPopover) {
        if (aSelector == @selector(moveUp:)) {
            NSInteger newSelectedIndex = MAX(0, acController.tableView.selectedRow-1);
            [acController.mentionListArrayController selectPrevious:self];
            [acController.tableView scrollRowToVisible:newSelectedIndex];
            return;
        } else if (aSelector == @selector(moveDown:)) {
            NSInteger newSelectedIndex = MIN(acController.tableView.numberOfRows-1, acController.tableView.selectedRow+1);
            [acController.mentionListArrayController selectNext:self];
            [acController.tableView scrollRowToVisible:newSelectedIndex];
            return;
        } else if (aSelector == @selector(insertTab:) || aSelector == @selector(insertNewline:)) {
            NSUInteger selectedIndex = acController.mentionListArrayController.selectionIndex;
            MentionItem *selection = [acController.mentionListArrayController.arrangedObjects objectAtIndex:selectedIndex];
            [self appendMentionToMessage:selection.mention];
            [self hideAutoComplete];
            return;
        } else if (aSelector == @selector(complete:)) {
            [self hideAutoComplete];
            return;
        }
    }

    [super doCommandBySelector:aSelector];
}

- (void)hideAutoComplete {
    [acPopover close];
    acPopover = nil;
}

- (void)showAutoCompletePopover {
    if (!self.delegate || ![self.delegate respondsToSelector:@selector(generateMentionList)]) {
        return;
    }

    if (!acPopover) {
        acController = [[AutoCompleteViewController alloc]
                        initWithNibName:@"AutoCompletePopup" bundle:[NSBundle mainBundle]];
        acController.delegate = self;
        acPopover = [[NSPopover alloc] init];
        acPopover.contentViewController = acController;
        acPopover.behavior = NSPopoverBehaviorApplicationDefined;
    }
    if (!acPopover.isShown) {
        [acController setMentionList:[self.delegate performSelector:@selector(generateMentionList)]];
        [acController updateMentionListWithSearch:acSearch];
    } else {
        [acController updateMentionListWithSearch:acSearch];
    }

    if ([acController getMatchCount] == 0) {
        // Only show autocomplete when there are actually matches
        [self hideAutoComplete];
    } else {
        // showingAutocomplete is a transient variable that is set
        // only as we're in the process of showing the popup. It doesn't
        // indicate that the popup is currently visible
        self.showingAutocomplete = YES;
        [self updateAutoCompletePopoverSize];
        [self updateAutoCompletePopoverPosition];
        self.showingAutocomplete = NO;
    }
}

- (void)updateAutoCompletePopoverPosition {
    if (acPopover) {
        NSRect positioningRect = self.bounds;
        positioningRect.size.width = MIN(200, positioningRect.size.width);
        [acPopover showRelativeToRect:positioningRect ofView:self preferredEdge:NSMaxYEdge];
    }
}

- (void)updateAutoCompletePopoverSize {
    if (acPopover) {
        acPopover.contentSize = acController.view.frame.size;
    }
}


@end
