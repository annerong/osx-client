//
//  RoomInviteViewController.m
//  HipChat
//
//  Created by Christopher Rivers on 2/25/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "RoomInviteWindowController.h"

#import "Room.h"
#import "User.h"
#import "XMPPJID.h"

#import "ChatFilterArrayController.h"
#import "HipChatOSXApp.h"
#import "NS(Attributed)String+Geometrics.h"
#import "OSXHelpers.h"

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Controller class
///////////////////////////////////////////////////////////////////////

@implementation RoomInviteWindowController

- (void)windowDidLoad {
    [super windowDidLoad];

    if (!self.jid) {
        DDLogError(@"No jid was set when inviting to room");
        [self closeSheet:NSCancelButton];
    }

    HipChatOSXApp *app = HipChatOSXApp.instance;
    Room *room = [app getRoomInfo:self.jid];
    if (!room) {
        DDLogError(@"No room data found when showing room invite sheet. Aborting...");
        [app showFlash:@"Room data was not available. Cannot invite users."];
        [self closeSheet:NSCancelButton];
    }
    [self initializeUserList];
}

- (void)closeSheet:(NSInteger)returnCode {
    if (returnCode == NSOKButton) {
        NSString *message = self.messageInput.stringValue;
        for (id item in self.namesInput.objectValue) {
            if ([item isKindOfClass:[User class]]) {
                Room *room = [HipChatOSXApp.instance getRoomInfo:self.jid];
                if (!room) {
                    DDLogError(@"Unable to find room data for jid: %@ -- cannot invite members", self.jid);
                    break;
                }
                [room invite:[item jid] reason:message];
            }
        }
    }

    [self hideAutoComplete];
    [super closeSheet:returnCode];
}

@end
