//
//  StatusBox.h
//  HipChat
//
//  Created by Christopher Rivers on 11/16/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "PrettyBorderBox.h"

@interface StatusBox : PrettyBorderBox <NSMenuDelegate> {
    NSTrackingArea *trackingArea;
    NSString *currentShow;

    BOOL isHovered;
}

- (void)unhighlight;
- (void)showStatusMessageMenuWithShow:(NSString *)show;

- (IBAction)handleMenuClick:(id)sender;
- (IBAction)handleStatusMessageSet:(id)sender;

@property IBOutlet NSImageView *dropdownArrow;
@property IBOutlet NSPopUpButton *menuButton;
@property IBOutlet NSMenu *settingsMenu;
@property IBOutlet NSTextField *statusMessageInput;
@property IBOutlet NSMenu *statusMessageMenu;
@property IBOutlet NSImageView *statusMessageIcon;
@property IBOutlet NSImageView *statusIcon;
@property IBOutlet NSTextField *statusMessage;
@property IBOutlet NSImageView *photo;

@end
