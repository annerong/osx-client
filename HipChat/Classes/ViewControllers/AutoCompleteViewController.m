//
//  AutoCompleteViewController.m
//  HipChat
//
//  Created by Christopher Rivers on 11/26/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "AutoCompleteViewController.h"

#import "MentionItem.h"


@implementation AutoCompleteViewController

@dynamic mentionList;

- (void)setMentionList:(NSMutableArray *)newMentionList {
    [self willChangeValueForKey:@"mentionList"];
    mentionList = newMentionList;
    [self didChangeValueForKey:@"mentionList"];

    [self updateViewHeight];
}

- (NSMutableArray *)mentionList {
    return mentionList;
}

- (NSInteger)getMatchCount {
    return [[self.mentionListArrayController arrangedObjects] count];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        NSSortDescriptor *matchRatingDescriptor =
            [[NSSortDescriptor alloc] initWithKey:@"matchRating"
                                        ascending:NO
                                         selector:nil];
        NSSortDescriptor *nameSortDescriptor =
            [[NSSortDescriptor alloc] initWithKey:@"label.string"
                                        ascending:YES
                                         selector:@selector(localizedCaseInsensitiveCompare:)];
        self.sortDescriptors = [[NSArray alloc] initWithObjects:matchRatingDescriptor, nameSortDescriptor, nil];
        self.filterPredicate = [NSPredicate predicateWithFormat:@"isMatch == YES"];
        self.mentionListArrayController.automaticallyRearrangesObjects = NO;
        self.mentionListArrayController.avoidsEmptySelection = YES;
    }

    return self;
}

- (IBAction)onAutocompleteClick:(id)sender {
    NSTableView *table = (NSTableView *)sender;
    if (table.clickedRow < 0) {
        return;
    }
    id selectedItem = [self.mentionListArrayController.arrangedObjects
                       objectAtIndex:table.clickedRow];
    if (selectedItem) {
        if ([self.delegate respondsToSelector:@selector(autocompleteSelectedItem:)]) {
            [self.delegate performSelector:@selector(autocompleteSelectedItem:) withObject:selectedItem];
        }
    }
}

- (void)updateMentionListWithSearch:(NSString *)searchStr {
    MentionItem *item = nil;
    for (item in self.mentionList) {
        [item checkMatchAndHighlight:searchStr];
    }

    [self.mentionListArrayController rearrangeObjects];
    self.mentionListArrayController.selectionIndex = 0;
    [self updateViewHeight];
}

- (void)updateViewHeight {
    NSInteger height = (self.tableView.rowHeight + self.tableView.intercellSpacing.height) * MIN(MAX(self.tableView.numberOfRows, 1), 5);
    NSRect frame = self.view.frame;
    frame.size.height = height;
    self.view.frame = frame;
}


@end
