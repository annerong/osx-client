//
//  CustomSecureTextFieldCell.h
//  HipChat
//
//  Created by Christopher Rivers on 5/15/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface CustomSecureTextFieldCell : NSSecureTextFieldCell

@end
