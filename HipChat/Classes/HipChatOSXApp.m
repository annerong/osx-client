//
//  HipChatApp.m
//  hipchat
//
//  Created by Christopher Rivers on 9/22/10.
//  Copyright 2010 HipChat. All rights reserved.
//

#import "HipChatOSXApp.h"

#import <Growl/Growl.h>
#import "HipChatUser.h"
#import "RegexKitLite.h"
#import "Room.h"
#import "RoomList.h"
#import "Roster.h"
#import "SBJson.h"
#import <Sparkle/Sparkle.h>
#import "User.h"
#import "XMPPFramework.h"
#import <WebKit/WebKit.h>

#import "AutoCompleteTextView.h"
#import "ChatViewController.h"
#import "IdleTimer.h"
#import "MainWindow.h"
#import "OSXHelpers.h"
#import "ProfileViewController.h"
#import "RosterEntry.h"
#import "SSKeychain.h"

// Default idle time in seconds
static const NSInteger DEFAULT_IDLE_SECONDS = 300;

// Default delay before reconnection for non-idle users
static const NSInteger NON_IDLE_RECONNECT_DELAY = 5;

// Default volume % for notification sound
static const CGFloat NOTIFY_VOLUME_DEFAULT = 0.6;

///////////////////////////////////////////////////////////////////////
#pragma mark -
///////////////////////////////////////////////////////////////////////

@implementation HipChatOSXApp

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Singleton methods
///////////////////////////////////////////////////////////////////////

+ (HipChatOSXApp *)instance {
    HipChatOSXApp *instance = (HipChatOSXApp *) [HipChatApp getSharedInstance];
    if (instance) {
        return instance;
    }

    @synchronized (self) {
        if (instance == nil) {
            instance = [[self alloc] init];
            [HipChatApp setSharedInstance:instance];
        }
    }

    return instance;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Initialization / De-Initialization
///////////////////////////////////////////////////////////////////////

- (HipChatOSXApp *)init {
    if (self = [super init]) {
        // Start with default hosts - we'll override these values once we initiate a connection
        apiHost = API_HOST;
        mucHost = MUC_HOST;
        chatHost = CHAT_HOST;
        webHost = WEB_HOST;

        // Set reconnect delay to default (non-idle) value
        // Set the interval to a random value between 15 and 45 seconds
        [self setReconnectDelay:NON_IDLE_RECONNECT_DELAY andInterval:((arc4random() % 30) + 15.0)];

        closedChats = [[ThreadSafeArray alloc] initWithQueueName:"com.hipchat.closedChats"];
        idleTimer = [[IdleTimer alloc] initWithIdleSeconds:DEFAULT_IDLE_SECONDS];
        lastRoomListFetch = [NSDate date];
        lobbyJid = [XMPPJID jidWithString:[NSString stringWithFormat:@"lobby@%@", chatHost]];
        notifySound = [NSSound soundNamed:@"notify"];
        rosterEntryCache = [[ThreadSafeDictionary alloc] initWithQueueName:"com.hipchat.rosterEntryCache"];
        unreadMentionChats = [[ThreadSafeDictionary alloc] initWithQueueName:"com.hipchat.unreadMentionChats"];


        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onUserIdle:)
                                                     name:kIdleTimerInactiveNotification
                                                   object:idleTimer];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onUserActive:)
                                                     name:kIdleTimerActiveNotification
                                                   object:idleTimer];

        NSNotificationCenter *center = [[NSWorkspace sharedWorkspace] notificationCenter];
        [center addObserver:self
                   selector:@selector(onAppSleep:)
                       name:NSWorkspaceWillSleepNotification
                     object:nil];
        [center addObserver:self
                   selector:@selector(onAppWake:)
                       name:NSWorkspaceDidWakeNotification
                     object:nil];
    }

    [self checkOneTimeVersionUpdates];

    return self;
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Connection / Disconnection
///////////////////////////////////////////////////////////////////////

- (void)clearUserData {
    [ProfileViewController clearPhotoCache];
    [rosterEntryCache removeAllObjects];
    [super clearUserData];
}

- (void)connect {
    resource = @"osx";
    connectHost = [self getConnectHost];
    self.conn.hostName = connectHost;

    DDLogInfo(@"Set connectHost to %@", self.conn.hostName);
    self.conn.myJID = [XMPPJID jidWithString:[NSString stringWithFormat:@"user@%@", chatHost]];
    [super connect];
}

- (void)connectWithEmail:(NSString *)email password:(NSString *)password {
    [self storeSignInEmail:email andPassword:password];
    [super connectWithEmail:email password:password];
}

- (void)onDisconnect {
    [rosterEntryCache removeAllObjects];
    [super onDisconnect];
}

- (BOOL)reconnect {
    [reconnector stop];
    [reconnector manualStart];
    return YES;
}

- (NSError *)sendConnectFailed:(NSString *)errMsg code:(NSInteger)code {
    NSError *err = [super sendConnectFailed:errMsg code:code];

    // If we're not going to reconnect after a connect error, show the login screen
    if (!doReconnect) {
        [self signOutUser];
    }
    return err;
}

- (void)signOutUser {
    [super signOutUser];

    [self postNotifWithName:HipChatShowLoginWindowNotification args:nil];
}

- (BOOL)triggerManualReconnect {
    isManualReconnect = YES;
    return [self reconnect];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark XMPPStreamDelegate methods
///////////////////////////////////////////////////////////////////////

- (void)xmppStreamDidAuthenticate:(XMPPStream *)sender {
    [self postNotifWithName:HipChatShowMainWindowNotification args:nil];

    [super xmppStreamDidAuthenticate:sender];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Roster delegate methods
///////////////////////////////////////////////////////////////////////

- (void)roster:(Roster *)sender removedUser:(XMPPJID *)jid {
    // Deinit so we make sure to remove observers before losing the reference
    dispatch_barrier_async(rosterEntryCache.collectionQueue, ^{
        [[rosterEntryCache.internalDictionary objectForKey:jid.bare] deinitialize];
        [rosterEntryCache.internalDictionary removeObjectForKey:jid.bare];
    });
    [self removeFromAutoJoin:jid];
    if ([self getActiveChat:jid]) {
        [self closeChat:jid];
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Presence methods
///////////////////////////////////////////////////////////////////////

- (void)sendInitialPresence {
    // Send up presence right away
    NSXMLElement *capsNode = [NSXMLElement elementWithName:@"c" xmlns:@"http://jabber.org/protocol/caps"];

    [capsNode addAttributeWithName:@"node" stringValue:@"http://hipchat.com/client/mac"];
    if (floor(NSAppKitVersionNumber) <= NSAppKitVersionNumber10_6) {
        [capsNode addAttributeWithName:@"os_ver" stringValue:@"10.6"];
    } else if (floor(NSAppKitVersionNumber) <= NSAppKitVersionNumber10_7) {
        [capsNode addAttributeWithName:@"os_ver" stringValue:@"10.7"];
    } else {
        [capsNode addAttributeWithName:@"os_ver" stringValue:@"10.8"];
    }

    NSString *versionNumber = [[NSBundle mainBundle]
            objectForInfoDictionaryKey:@"CFBundleVersion"];
    [capsNode addAttributeWithName:@"ver" stringValue:versionNumber];
    [[NSUserDefaults standardUserDefaults] setObject:versionNumber forKey:@"app_version"];

    // Tell the server not to send group presences in response to this
    // roster_presences="false"
    NSDictionary *presenceArgs = [NSDictionary dictionaryWithObjectsAndKeys:
            [NSDictionary dictionaryWithObjectsAndKeys:@"false", @"roster_presences", nil], @"attributes",
            [NSArray arrayWithObject:capsNode], @"elements",
            nil];
    [self sendPresenceWithArgs:presenceArgs];
}


///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Chat methods
///////////////////////////////////////////////////////////////////////

- (void)addToClosedChats:(XMPPJID *)jid index:(NSInteger)index {
    [self removeFromClosedChats:jid];

    DDLogInfo(@"Adding chat to closed chats: %@ (index: %ld)", jid, (long) index);
    [closedChats push:[NSDictionary dictionaryWithObjectsAndKeys:
            jid, @"jid",
            [NSNumber numberWithInteger:index], @"index",
            nil]];
}

- (void)appendEmoticonToCurrentMessage:(NSString *)shortcut {
    ChatViewController *currentChat = [self getActiveChat:self.currentJid];
    if (currentChat) {
        DDLogInfo(@"Focusing current chat input");
        [currentChat appendEmoticonToMessage:shortcut];
    }
}

- (void)clearUnreadCount:(XMPPJID *)jid {
    if (jid) {
        [unreadMentionChats removeObjectForKey:jid.bare];
    } else {
        [unreadMentionChats removeAllObjects];
    }
    [super clearUnreadCount:jid];
}

- (void)doJoinChat:(XMPPJID *)jid withFocus:(BOOL)doFocus {
    [self doJoinChat:jid withFocus:doFocus atIndex:-1 withMessage:nil];
}

// Synchronize to avoid active chats being iterated and modified at the same time
- (void)doJoinChat:(XMPPJID *)jid
         withFocus:(BOOL)doFocus
           atIndex:(NSInteger)index
       withMessage:(XMPPMessage *)initialMessage {
    @synchronized (self) {
        DDLogInfo(@"Joining chat: %@", jid.bare);

        if ([Helpers isRoomJid:jid] && ![self getRoomInfo:jid]) {
            DDLogError(@"Attempted to join chat which was not available: %@ - ignoring...", jid);
            return;
        } else if ([Helpers isUserJid:jid]) {
            if (![self getUserInfo:jid]) {
                DDLogError(@"Attempted to join chat which was not available: %@ - ignoring...", jid);
                return;
            } else if (![self.currentUser getPerm:PERM_PRIVATE_CHAT]) {
                DDLogError(@"Attempted to join 1:1 chat when 1:1 chats are disabled: %@ - ignoring...", jid);
                return;
            }

        }

        // Check existing active chats first to see if we're already in this chat
        // If so, just check to see if we should focus that chat instead
        if ([self isActiveChat:jid]) {
            if (doFocus) {
                [self focusChat:jid];
            }
            return;
        }

        [self removeFromClosedChats:jid];

        ChatViewController *cvc = [[ChatViewController alloc] initWithJid:jid initialMessage:initialMessage];
        [self setActiveChat:cvc forJid:jid];

        // Store the jid in activeChatJids so we can remember the order for display in ActiveChats VC
        // (NSMutableDictionary doesn't have a way to remember insert order)
        [self enqueueActiveChatJid:jid];
        [self addToAutoJoin:jid atIndex:index];

        [self postDelegateBlock:^{
            [multicastDelegate hipChatAppChatsChanged];
        }];

        NSMutableDictionary *args = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                jid, @"jid",
                cvc, @"chatViewController",
                [NSNumber numberWithBool:doFocus], @"doFocus",
                nil];
        if (index >= 0) {
            [args setObject:[NSNumber numberWithInteger:index] forKey:@"index"];
        }
        [self postNotifWithName:HipChatJoinChatNotification args:args];
    }
}

- (void)focusChatAtIndex:(NSInteger)index {
    NSDictionary *args = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithLong:index], @"index", nil];
    [self postNotifWithName:HipChatFocusChatAtIndexNotification args:args];
}

- (void)focusCurrentChatInput {
    ChatViewController *currentChat = [self getActiveChat:self.currentJid];
    if (currentChat) {
        DDLogInfo(@"Focusing current chat input");
        [currentChat focusMessageInput:NO];
    }
}

- (void)focusLobby {
    NSTimeInterval diff = [[NSDate date] timeIntervalSinceDate:lastRoomListFetch];
    if (diff > 10) {
        [self.roomList fetchItems:self.mucHost];
        lastRoomListFetch = [NSDate date];
    }
    [super focusLobby];
}

- (BOOL)getUnreadMentionForJid:(XMPPJID *)jid {
    id b = [unreadMentionChats objectForKey:jid.bare];
    if (b) {
        return [b boolValue];
    } else {
        return NO;
    }
}

- (void)joinChat:(XMPPJID *)jid {
    return [self joinChat:jid withFocus:NO];
}

- (void)joinChat:(XMPPJID *)jid withFocus:(BOOL)doFocus {
    return [self joinChat:jid withFocus:doFocus atIndex:-1 withMessage:nil];
}

- (void)joinChat:(XMPPJID *)jid
       withFocus:(BOOL)doFocus
         atIndex:(NSInteger)index
     withMessage:(XMPPMessage *)initialMessage {

    if (!conn.isConnected) {
        DDLogError(@"Connection isn't yet established - cannot join chat");
        return;
    }

    if (dispatch_get_current_queue() == dispatch_get_main_queue()) {
        [self doJoinChat:jid withFocus:doFocus atIndex:index withMessage:initialMessage];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self doJoinChat:jid withFocus:doFocus atIndex:index withMessage:initialMessage];
        });
    }
}

- (void)joinChat:(XMPPJID *)jid withMessage:(XMPPMessage *)initialMessage {
    return [self joinChat:jid withFocus:NO atIndex:-1 withMessage:initialMessage];
}

- (BOOL)notifyUserForMessage:(XMPPMessage *)message {
    XMPPJID *fromJid = message.from;
    if (self.appIsFocused && [self.currentJid.bare isEqualToString:fromJid.bare]) {
        DDLogInfo(@"App is focused and on this chat - not notifying for message");
        return NO;
    }

    DDLogInfo(@"Attempting to notify for message...");

    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    BOOL doNotify = YES;

    // Check to see if we actually want to notify for this message at all
    // Not done in superclass since it check desktop app prefs (not found in iOS)
    NSString *body = [[message elementForName:@"body"] stringValue];

    // Test for system messages (HTML)
    NSXMLElement *mucNode = [message elementForName:@"x" xmlns:@"http://hipchat.com/protocol/muc#room"];
    NSString *typeValue = [[mucNode elementForName:@"type"] stringValue];
    if ([typeValue isEqualToString:@"system"] || [typeValue isEqualToString:@"announcement"]) {
        NSString *messageFormat = [[mucNode elementForName:@"message_format"] stringValue];
        if (![messageFormat isEqualToString:@"text"]) {
            // If the message_format isn't "text", it is default HTML
            // Generate an attributed string with the HTML and get the base text from it
            // Use UTF16 encoding - UTF8 apparently mucks up special characters like the ">" in Bamboo messages
            body = [[[NSAttributedString alloc]
                    initWithHTML:[body dataUsingEncoding:NSUTF16StringEncoding] options:nil documentAttributes:nil] string];
        }
    }

    // Avoid missing name regex crash
    BOOL allMentionMatch = ([body rangeOfRegex:@"@all\\b"].location != NSNotFound);
    BOOL hereMentionMatch = ([body rangeOfRegex:@"@here\\b"].location != NSNotFound);
    BOOL specificMentionMatch = (currentUser.specificMentionRegex ?
                         [body rangeOfRegex:currentUser.specificMentionRegex].location != NSNotFound :
                         NO);

    // We have a mention match if the message was @<this users mention>, @all, or (@here AND we're available status)
    // Available presence means show == nil
    BOOL mentionMatch = (specificMentionMatch || allMentionMatch || (currentPresenceShow == nil && hereMentionMatch));
    BOOL otoMessage = [OSXHelpers isUserJid:fromJid];

    NSString *notificationName = @"MessageNotification";
    if (mentionMatch) {
        doNotify = (doNotify && [prefs boolForKey:@"notifyForTag"]);
        [unreadMentionChats setObject:[NSNumber numberWithBool:YES] forKey:fromJid.bare];
        notificationName = @"MentionNotification";
    } else if (otoMessage) {
        doNotify = (doNotify && [prefs boolForKey:@"notifyForPrivate"]);
        notificationName = @"OneToOneNotification";
    } else if ([OSXHelpers isRoomJid:fromJid]) {
        Room *room = [self getRoomInfo:fromJid];
        if ([room isPrivate]) {
            notificationName = @"PrivateRoomNotification";
            doNotify = (doNotify && [prefs boolForKey:@"notifyForPrivateRoom"]);
        } else {
            notificationName = @"OpenRoomNotification";
            doNotify = (doNotify && [prefs boolForKey:@"notifyForRoom"]);
        }
    }

    // If the user doesn't actually want notifications about this type of message
    // we're done - just cut out here
    if (!doNotify) {
        DDLogInfo(@"User preferences prevented notification - not notifying");
        return NO;
    }

    // Don't trigger "invasive" notifications (sound/toaster/bounce) if the user
    // is DND, but still return "YES" that we notified the user (so unread count will go up)
    if (currentPresenceShow && [currentPresenceShow caseInsensitiveCompare:@"dnd"] == NSOrderedSame) {
        if ([prefs boolForKey:@"notifyDnd"] && (mentionMatch || [OSXHelpers isUserJid:fromJid])) {
            // User wants to be notified for 1:1/@mention messages while DND
            // Don't return here
        } else {
            DDLogInfo(@"User is DND status - not notifying user of message");
            return YES;
        }
    }
    
    ActiveChatNotificationSetting setting = [self getActiveChatNotificationSetting:fromJid];
    if (setting == ActiveChatNotificationMuteAllExceptMe && !mentionMatch) {
        DDLogInfo(@"Chat muted and user not mentioned - not notifying user of message");
        return YES;
    } else if (setting == ActiveChatNotificationMuteAll) {
        DDLogInfo(@"Chat muted - not notifying user of message");
        return YES;
    }
    
    // Check for Growl/toaster notifications
    if ([prefs boolForKey:@"showToasters"]) {
        // Send Growl notification is available
        NSString *title = nil;
        XMPPJID *fromUser = ([OSXHelpers isRoomJid:fromJid] ? [self jidForNickname:fromJid.resource] : fromJid);
        User *user = [self getUserInfo:fromUser];
        if ([OSXHelpers isRoomJid:fromJid]) {
            Room *room = [self getRoomInfo:fromJid];
            if (room) {
                title = [NSString stringWithFormat:@"%@ - %@", (user ? user.name : fromJid.resource), room.name];
            }
        } else if ([OSXHelpers isUserJid:fromJid]) {
            title = [NSString stringWithFormat:@"%@", user.name];
        }

        // Make sure we have everything required to show a toaster notification
        if (!title || !body || !fromJid) {
            DDLogError(@"Failed to retrieve required info for notification. "
                    @"TITLE: %@ -- BODY: %@ -- SENDER: %@ -- FROMJID: %@",
            title, body, fromUser, fromJid);
        } else {
            // Growl will fall back to Notification Center if users don't have it installed
            NSDictionary *context = [NSDictionary dictionaryWithObjectsAndKeys:
                    fromJid.bare, @"fromJid",
                    nil];
            [GrowlApplicationBridge notifyWithTitle:title
                                        description:body
                                   notificationName:notificationName
                                           iconData:nil
                                           priority:0
                                           isSticky:NO
                                       clickContext:context];
        }
    }

    // Check for dock bounce notification
    if ([prefs boolForKey:@"visualNotifications"]) {
        if (otoMessage || mentionMatch) {
            [NSApp requestUserAttention:NSCriticalRequest];
        } else {
            [NSApp requestUserAttention:NSInformationalRequest];
        }
    }

    // Check for sound notification
    if (![prefs boolForKey:@"disableSounds"]) {
        CGFloat notifyVolume = [[NSUserDefaults standardUserDefaults] floatForKey:@"notificationVolume"];
        if (!notifyVolume) {
            [[NSUserDefaults standardUserDefaults] setFloat:(NOTIFY_VOLUME_DEFAULT * 100) forKey:@"notificationVolume"];
            notifyVolume = NOTIFY_VOLUME_DEFAULT;
        } else {
            notifyVolume /= 100.0;
        }
        [notifySound setVolume:notifyVolume];
        [notifySound play];
    }
    [super notifyUserForMessage:message];
    return YES;
}

- (void)removeFromClosedChats:(XMPPJID *)jid {
    DDLogInfo(@"Removing chat from closed chats: %@", jid);
    dispatch_barrier_async(closedChats.collectionQueue, ^{
        NSUInteger index = [closedChats.internalArray indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            XMPPJID *curJid = [obj objectForKey:@"jid"];
            if ([curJid.bare isEqualToString:jid.bare]) {
                *stop = YES;
                return YES;
            }
            return NO;
        }];
        if (index != NSNotFound) {
            [closedChats.internalArray removeObjectAtIndex:index];
        }
    });
}

- (void)reopenLastChat {
    if (closedChats.count == 0) {
        DDLogInfo(@"No chats left to reopen");
        return;
    }

    NSDictionary *chatInfo = [closedChats pop];

    DDLogInfo(@"Reopening chat: %@", [chatInfo objectForKey:@"jid"]);
    [self joinChat:[chatInfo objectForKey:@"jid"]
         withFocus:YES
           atIndex:[[chatInfo objectForKey:@"index"] integerValue]
       withMessage:nil];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Stored sign in information
///////////////////////////////////////////////////////////////////////

- (void)storeSignInEmail:(NSString *)email andPassword:(NSString *)password {
    DDLogInfo(@"Storing sign in email and password");

    if ([self getRememberLogin]) {
        NSString *host = [self getConnectHost];
        DDLogInfo(@"Storing password for email: %@ and service: %@", email, host);
        if ([host isEqualToString:@"chat-main.hipchat.com"]) {
            // Special check so we don't end up clearing out people's PW on an upgrade
            // Previously we used self.chatHost to store the PW, but now chatHost
            // gets returned during connect (we only know connectHost before connecting)
            // Set the pw on both chat.hipchat.com and chat-main.hipchat.com until we
            // get all the clients switched over to the new style
            [SSKeychain setPassword:password forService:@"chat.hipchat.com" account:email];
        }
        [SSKeychain setPassword:password forService:host account:email];
    }

    // store email in NSUserDefaults
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:email forKey:[self getEmailPrefsKey]];
    [prefs synchronize];
}

- (NSString *)getEmailPrefsKey {
    NSString *host = [self getConnectHost];
    if ([host isEqualToString:CONNECT_HOST]) {
        return @"signInEmail";
    }
    return [NSString stringWithFormat:@"signInEmail-%@", host];
}

- (NSString *)getSignInEmail {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    return [prefs stringForKey:[self getEmailPrefsKey]];
}

- (NSString *)getSignInPassword {
    return [self getSignInPasswordForEmail:[self getSignInEmail]];
}

- (NSString *)getSignInPasswordForEmail:(NSString *)email {
    NSString *host = [self getConnectHost];
    if ([host isEqualToString:@"chat-main.hipchat.com"]) {
        // Special check so we don't end up clearing out people's PW on an upgrade
        // Previously we used self.chatHost to store the PW, but now chatHost
        // gets returned during connect (we only know connectHost before connecting)
        host = @"chat.hipchat.com";
    }
    DDLogInfo(@"Retrieving password for email: %@ and service: %@", email, host);
    return [SSKeychain passwordForService:host account:email];
}

- (void)clearSignInPassword {
    NSString *host = [self getConnectHost];
    DDLogInfo(@"Clearing stored sign in password for host: %@ and email: %@", host, [self getSignInEmail]);
    BOOL success = [SSKeychain deletePasswordForService:host account:[self getSignInEmail]];
    if (!success) {
        DDLogInfo(@"Failed to delete password from keychain");
    } else {
        DDLogInfo(@"Password cleared successfully");
    }
    self.loginPassword = nil;
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Notification / Event Listeners
///////////////////////////////////////////////////////////////////////

- (void)onAppSleep:(NSNotification *)notif {
    if ([self.conn isConnected]) {
        [self disconnect];
    }
    if (!disconnectedAt) {
        disconnectedAt = [NSDate date];
    }
}

- (void)onAppWake:(NSNotification *)notif {
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        if ([self reconnect]) {
            [self showReconnectFlashWithMessage:nil];
        }
    });
}

- (void)onUserIdle:(NSNotification *)notif {
    // Set the reconnection delay to 10-30 seconds, interval to 100-300 seconds
    [self setReconnectDelay:((arc4random() % 20) + 10.0) andInterval:((arc4random() % 200) + 100.0)];
    NSString *newShow = ([currentPresenceShow isEqualToString:@"dnd"] ? @"dnd" : @"away");
    [self updatePresenceWithShow:newShow status:currentPresenceStatus idle:[idleTimer getIdleSeconds]];
}

- (void)onUserActive:(NSNotification *)notif {
    [self setReconnectDelay:NON_IDLE_RECONNECT_DELAY andInterval:(arc4random() % 30) + 15.0];
    [self updatePresenceWithShow:previousPresenceShow status:previousPresenceStatus];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Helper methods
///////////////////////////////////////////////////////////////////////

- (void)decreaseFontSize {
    if (self.currentJid == self.lobbyJid) {
        return;
    }
    WebPreferences *prefs = [[WebPreferences alloc] initWithIdentifier:@"chatPreferences"];
    int newFontSize = [prefs defaultFontSize] - 1;

    DDLogInfo(@"Decreasing font size from %d to %d", [prefs defaultFontSize], newFontSize);
    [prefs setDefaultFontSize:newFontSize];
    [prefs setDefaultFixedFontSize:newFontSize];

    NSUserDefaults *userPrefs = [NSUserDefaults standardUserDefaults];
    [userPrefs setObject:[NSNumber numberWithInt:newFontSize] forKey:@"defaultFontSize"];
    [userPrefs synchronize];

    for (ChatViewController *cvc in [self.activeChats allValues]) {
        [cvc.messageInput setFont:[NSFont fontWithName:@"Lucida Grande" size:newFontSize]];
    }
}

- (void)focusApp {
    [self postNotifWithName:HipChatFocusAppNotification args:nil];
}

- (NSMutableArray *)getRosterEntriesForBareJids:(NSArray *)bareJids {
    if (!bareJids) {
        DDLogError(@"Failed to get user list for roster update");
        return [NSMutableArray array];
    }

    NSMutableArray *entries = [NSMutableArray array];
    for (NSString *bareJid in bareJids) {
        RosterEntry *re = [rosterEntryCache objectForKey:bareJid];
        if (re) {
            [entries addObject:re];
        } else {
            User *user = [roster userForBareJid:bareJid];
            if (!user) {
                DDLogError(@"Tried to generate roster entry for non-existent user: %@", bareJid);
                continue;
            }
            RosterEntry *entry = [[RosterEntry alloc] initWithUser:user];
            [entries addObject:entry];
            [rosterEntryCache setObject:entry forKey:bareJid];
        }
    }
    return entries;
}

- (void)handleErrorStanza:(NSXMLElement *)stanza {
    NSXMLElement *errorNode = [stanza elementForName:@"error"];
    if (errorNode && [errorNode elementForName:@"not-acceptable" xmlns:@"urn:ietf:params:xml:ns:xmpp-stanzas"]) {
        XMPPJID *from = [XMPPJID jidWithString:[stanza attributeStringValueForName:@"from"]];
        [self closeChat:from.bareJID isManual:NO];

        // Add a slight delay so we can display any error messages via the flash UI first
        // Showing the create room popup freezes the display for anything outside the modal dialog
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            [self showCreateRoomForJid:from.bareJID];
        });
    }
    [super handleErrorStanza:stanza];
}

- (void)handleRosterPush:(XMPPIQ *)iq {
    // Clear out any roster entry cache we have for this user
    NSXMLElement *rosterQuery = [iq elementForName:@"query" xmlns:@"jabber:iq:roster"];
    NSXMLElement *item = [rosterQuery elementForName:@"item"];
    XMPPJID *jid = [XMPPJID jidWithString:[item attributeStringValueForName:@"jid"]];
    [rosterEntryCache removeObjectForKey:jid.bare];

    [super handleRosterPush:iq];
}

- (void)handleSavePrefsTimer {
    // Set all boolean prefs (checkboxes from settings window)
    NSUserDefaults *localPrefs = [NSUserDefaults standardUserDefaults];
    NSArray *boolPrefs = [NSArray arrayWithObjects:@"notifyForTag", @"notifyForPrivate", @"notifyForPrivateRoom",
                                                   @"notifyForRoom", @"showToasters", @"visualNotifications", @"disableSounds",
                                                   @"timeFormat24Hour", @"hidePresenceMessages", @"checkMinorUpdates", @"enableEmoticons", nil];
    NSString *prefName = nil;
    NSMutableDictionary *prefDict = [NSMutableDictionary dictionaryWithCapacity:boolPrefs.count];
    for (prefName in boolPrefs) {
        BOOL prefValue = [localPrefs boolForKey:prefName];
        [prefDict setObject:(prefValue ? @"true" : @"false") forKey:prefName];
    }

    // Set autoJoin prefs as well
    NSMutableArray *autoJoinArg = [self generateAutoJoinArg];
    [prefDict setObject:autoJoinArg forKey:@"autoJoin"];

    // Send prefs up as JSON
    NSString *jsonArg = [prefDict JSONRepresentation];
    NSMutableDictionary *args = [NSMutableDictionary dictionaryWithObjectsAndKeys:jsonArg, @"json", nil];
    [self makeAPIRequest:@"save_preferences"
                withArgs:args
           tokenRequired:YES
       completionHandler:nil];
}

- (void)increaseFontSize {
    if (self.currentJid == self.lobbyJid) {
        return;
    }
    WebPreferences *prefs = [[WebPreferences alloc] initWithIdentifier:@"chatPreferences"];
    int newFontSize = MIN([prefs defaultFontSize] + 1, 18);

    DDLogInfo(@"Increasing font size from %d to %d", [prefs defaultFontSize], newFontSize);
    [prefs setDefaultFontSize:newFontSize];
    [prefs setDefaultFixedFontSize:newFontSize];

    NSUserDefaults *userPrefs = [NSUserDefaults standardUserDefaults];
    [userPrefs setObject:[NSNumber numberWithInt:newFontSize] forKey:@"defaultFontSize"];
    [userPrefs synchronize];

    for (ChatViewController *cvc in [self.activeChats allValues]) {
        [cvc.messageInput setFont:[NSFont fontWithName:@"Lucida Grande" size:newFontSize]];
    }
}

- (BOOL)isAppFocused {
    return [[NSApp keyWindow] isKindOfClass:[MainWindow class]];
}
    
- (void)muteAllMessagesForJid:(XMPPJID *)jid {
    [self setActiveChatNotificationSetting:ActiveChatNotificationMuteAll forJid:jid];
    [self updateMuteMenuStatus:jid];
}
    
- (void)muteMessagesExceptMeForJid:(XMPPJID *)jid {
    [self setActiveChatNotificationSetting:ActiveChatNotificationMuteAllExceptMe forJid:jid];
    [self updateMuteMenuStatus:jid];
}
    
- (void)unmute:(XMPPJID *)jid {
    [self setActiveChatNotificationSetting:ActiveChatNotificationOn forJid:jid];
    [self updateMuteMenuStatus:jid];
}

- (void)updateMuteMenuStatus:(XMPPJID *)jid{
    ActiveChatNotificationSetting setting = [self getActiveChatNotificationSetting:jid];
    NSMenu *mute = [[[[[NSApp mainMenu] itemWithTag:kRoomMenuTag] submenu] itemWithTag:kMuteMenuTag] submenu];
    if (setting == ActiveChatNotificationMuteAll) {
        [[mute itemWithTag:kMuteRoomAllTag] setState:(NSOnState)];
        [[mute itemWithTag:kMuteRoomExceptMeTag] setState:(NSOffState)];
    } else if (setting == ActiveChatNotificationMuteAllExceptMe) {
        [[mute itemWithTag:kMuteRoomAllTag] setState:(NSOffState)];
        [[mute itemWithTag:kMuteRoomExceptMeTag] setState:(NSOnState)];
    } else {
        [[mute itemWithTag:kMuteRoomAllTag] setState:(NSOffState)];
        [[mute itemWithTag:kMuteRoomExceptMeTag] setState:(NSOffState)];
    }
}

- (void)returnToNormalFontSize {
    if (self.currentJid == self.lobbyJid) {
        return;
    }
    DDLogInfo(@"Returning font size to normal: %d", (int) NORMAL_FONT_SIZE);
    WebPreferences *prefs = [[WebPreferences alloc] initWithIdentifier:@"chatPreferences"];
    [prefs setDefaultFontSize:(int) NORMAL_FONT_SIZE];
    [prefs setDefaultFixedFontSize:(int) NORMAL_FONT_SIZE];

    NSUserDefaults *userPrefs = [NSUserDefaults standardUserDefaults];
    [userPrefs setObject:[NSNumber numberWithInt:(int) NORMAL_FONT_SIZE] forKey:@"defaultFontSize"];
    [userPrefs synchronize];

    for (ChatViewController *cvc in [self.activeChats allValues]) {
        [cvc.messageInput setFont:[NSFont fontWithName:@"Lucida Grande" size:NORMAL_FONT_SIZE]];
    }
}

- (void)savePreferences {
    [self savePreferences:NO];
}

- (void)savePreferences:(BOOL)immediately {
    // Don't save prefs more than once every 10 seconds
    if (savePrefsTimer) {
        [savePrefsTimer invalidate];
        savePrefsTimer = nil;
    }
    NSTimeInterval delay = (immediately ? 0 : 10);
    savePrefsTimer = [NSTimer scheduledTimerWithTimeInterval:delay
                                                      target:self
                                                    selector:@selector(handleSavePrefsTimer)
                                                    userInfo:nil
                                                     repeats:NO];
}

- (void)showChangeTopic {
    [self postNotifWithName:HipChatShowChangeTopicNotification args:nil];
}

- (void)showChangePrivacyForJid:(XMPPJID *)jid {
    NSDictionary *args = (jid ? [NSDictionary dictionaryWithObject:jid forKey:@"jid"] : nil);
    [self postNotifWithName:HipChatShowChangePrivacyNotification args:args];
}

// Call this will a nil JID in order to create a new room
- (void)showCreateRoomForJid:(XMPPJID *)jid {
    NSDictionary *args = (jid ? [NSDictionary dictionaryWithObject:jid forKey:@"jid"] : nil);
    [self postNotifWithName:HipChatShowCreateRoomNotification args:args];
}

- (void)showJoinChat {
    [self postNotifWithName:HipChatShowJoinChatNotification args:nil];
}

// Call this with a nil JID in order to manage permissions for a room not yet created
- (void)showManagePermsForJid:(XMPPJID *)jid {
    NSDictionary *args = (jid ? [NSDictionary dictionaryWithObject:jid forKey:@"jid"] : nil);
    [self postNotifWithName:HipChatShowCreateRoomNotification args:args];
}

- (void)showReconnectFlashWithMessage:(NSString *)message {
    if (self.conn.changingHosts) {
        return;
    }

    if (!message || message.length == 0) {
        message = @"Connection lost.";
    }
    NSString *flashMessage = [NSString stringWithFormat:@"<div>%@ <a href='action:reconnect'>Reconnect now...</a></div>", message];
    [self showFlash:flashMessage duration:0];
}

- (void)showRenameRoomForJid:(XMPPJID *)jid {
    DDLogInfo(@"Show rename room for %@", jid);
    NSDictionary *args = (jid ? [NSDictionary dictionaryWithObject:jid forKey:@"jid"] : nil);
    [self postNotifWithName:HipChatShowRenameRoomNotification args:args];
}

- (void)showRightSidebarFiles; {
    [self postNotifWithName:HipChatShowSidebarFilesNotification args:nil];
}

- (void)showRightSidebarLinks {
    [self postNotifWithName:HipChatShowSidebarLinksNotification args:nil];
}

- (void)showRightSidebarPrimary {
    [self postNotifWithName:HipChatShowSidebarPrimaryNotification args:nil];
}

- (void)showRoomInviteForJid:(XMPPJID *)jid {
    [self postNotifWithName:HipChatShowRoomInviteNotification
                       args:[NSDictionary dictionaryWithObject:jid forKey:@"jid"]];
}

- (void)showRoomRemoveMembersForJid:(XMPPJID *)jid {
    [self postNotifWithName:HipChatShowRoomRemoveMembersNotification
                       args:[NSDictionary dictionaryWithObject:jid forKey:@"jid"]];
}

- (void)showSettingsWindow {
    [self postNotifWithName:HipChatShowSettingsWindowNotification args:nil];
}

- (void)toggleDevPrefs {
    // Disable this toggle for now. "Dev prefs" are now Connection prefs and shown by default
//    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//    BOOL devPrefsEnabled = [prefs boolForKey:@"devPrefsEnabled"];
//    devPrefsEnabled = !devPrefsEnabled;
//    [prefs setBool:devPrefsEnabled forKey:@"devPrefsEnabled"];
//    [prefs synchronize];
//
//    DDLogInfo(@"Set development prefs: %@", (devPrefsEnabled ? @"enabled" : @"disabled"));
//    [self showFlash:[NSString stringWithFormat:
//                     @"Development prefs %@", (devPrefsEnabled ? @"enabled" : @"disabled")]];
}

- (void)toggleLeftSidebar {
    [self postNotifWithName:HipChatToggleLeftSidebarNotification args:nil];
}

- (void)toggleRightSidebar {
    [self postNotifWithName:HipChatToggleRightSidebarNotification args:nil];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Helper methods
///////////////////////////////////////////////////////////////////////

// Perform any one-time logic when users update to a new version
- (void)checkOneTimeVersionUpdates {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *storedVersion = [prefs stringForKey:@"appVersion"];
    if (!storedVersion) {
        storedVersion = @"0.0";
    }

    NSString *currentVersion = [[NSBundle mainBundle]
            objectForInfoDictionaryKey:@"CFBundleVersion"];

    // Apparently NSNumericSearch properly handles version numbers
    // see: http://stackoverflow.com/questions/5160216/comparing-version-numbers
    // (Mike Weller's answer)
    NSInteger result = [storedVersion compare:currentVersion options:NSNumericSearch];
    if (result == NSOrderedSame) {
        // storedVersion == currentVersion
    } else if (result == NSOrderedAscending) {
        // storedVersion < currentVersion (upgrade case)
        DDLogInfo(@"Detected upgrade - checking for upgrade actions");
        if ([storedVersion compare:@"0.35" options:NSNumericSearch] == NSOrderedAscending) {
            // Going from pre-0.35 to 0.35 or above
            // Remove emoticon/roster version - we changed the cache file in this version
            DDLogInfo(@"Previous version was <0.35 - clearing emoticon/roster cache keys");
            [prefs removeObjectForKey:@"emoticonsVersion"];
            [prefs removeObjectForKey:@"rosterVersion"];
        }
    } else {
        DDLogInfo(@"Detected downgrade - this is unusual - perhaps the app was rolled back?");
        // storedVersion > currentVersion (downgrade case)
    }
    [prefs setObject:currentVersion forKey:@"appVersion"];
    [prefs synchronize];
}

- (void)updateUpdateUrlForceMinor:(BOOL)forceMinorCheck {
    // Send up the bundle-ID, version, and minorUpdate pref when checking for updates (SUFeedURL)
    NSString *updateURL = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"SUFeedURL"];
    NSString *bundleId = [[NSBundle mainBundle] bundleIdentifier];
    NSString *versionNumber = [[NSBundle mainBundle]
            objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSInteger checkMinorUpdates = (forceMinorCheck || [[NSUserDefaults standardUserDefaults] boolForKey:@"checkMinorUpdates"] ? 1 : 0);

    NSURL *newURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@?version=%@&bundle=%@&include_minor=%ld",
                                                                    updateURL, versionNumber, bundleId, checkMinorUpdates]];
    [[SUUpdater sharedUpdater] setFeedURL:newURL];
}

@end
