//
//  RowHighlightTableView.h
//  HipChat
//
//  Created by Christopher Rivers on 10/23/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface RowHighlightTableView : NSTableView {
    NSTrackingArea *trackingArea;
    BOOL mouseOverView;
    NSInteger mouseOverRow;
    NSInteger lastOverRow;
}

@end
