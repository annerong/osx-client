//
//  JoinChatWindowController.h
//  HipChat
//
//  Created by Christopher Rivers on 1/4/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "HipChatSheetController.h"

@class ChatFilterArrayController;
@class XMPPJID;

@interface JoinChatWindowController : HipChatSheetController

- (XMPPJID *)selectedJid;

@property NSMutableArray *chatList;
@property IBOutlet NSPanel *joinChatPanel;
@property IBOutlet NSTextField *searchField;

@end
