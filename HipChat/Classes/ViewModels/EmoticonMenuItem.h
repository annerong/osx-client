//
//  EmoticonMenuItem.h
//  HipChat
//
//  Created by Christopher Rivers on 5/7/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EmoticonMenuItem : NSObject

+ (EmoticonMenuItem *)emoticonMenuItemWithIcon:(NSString *)icon shortcut:(NSString *)shortcut;

@property NSString *icon;
@property NSString *shortcut;

@end
