//
//  ChatFilterArrayController.h
//  HipChat
//
//  Created by Christopher Rivers on 12/31/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ChatFilterArrayController : NSArrayController {
    NSString *searchString;
}

- (void)search:(NSString *)searchString;

@property NSDictionary *selectedJids;

@end
