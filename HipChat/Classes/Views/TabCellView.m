//
//  TabCellView.m
//  HipChat
//
//  Created by Christopher Rivers on 10/19/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "TabCellView.h"

#import "HoverButton.h"

@implementation TabCellView

- (BOOL)acceptsFirstMouse:(NSEvent *)theEvent {
    return YES;
}

- (void)otherMouseDown:(NSEvent *)theEvent {
    // Middle-mouse click closes tabs
    [self.delegate performSelector:@selector(onCloseChatClick:) withObject:self];
}

- (void)onCloseChatClick {
    [self.delegate performSelector:@selector(onCloseChatClick:) withObject:self];
}

- (void)drawRect:(NSRect)dirtyRect {
    NSTableRowView *parent = (NSTableRowView *)self.superview;
    if (parent.selected) {
        self.textField.textColor = [NSColor whiteColor];
    } else {
        NSColor *manualColor = [[self objectValue] color];
        self.textField.textColor = (manualColor ?
                                    manualColor :
                                    (self.isHiglighted ? [NSColor whiteColor] : RGB(177, 179, 182)));
    }

    [super drawRect:dirtyRect];
}

- (void)viewWillMoveToSuperview:(NSView *)newSuperview {
    if (newSuperview) {
        self.isHiglighted = NO;
        [self.closeButton resetImage];
    }
}

@end
