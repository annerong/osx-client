//
//  PathTransformer.m
//  HipChat
//
//  Created by Christopher Rivers on 10/16/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "ImageTransformer.h"

@implementation ImageTransformer

+ (Class)transformedValueClass
{
    return [NSString self];
}

+ (BOOL)allowsReverseTransformation
{
    return NO;
}

// Return a path string for the given image name
- (id)transformedValue:(id)beforeObject
{
    if (beforeObject == nil) return nil;
    return [NSImage imageNamed:beforeObject];
}

@end
