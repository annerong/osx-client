//
//  TabCellView.m
//  HipChat
//
//  Created by Christopher Rivers on 10/19/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "TabRowView.h"

@implementation TabRowView

- (void)drawSeparatorInRect:(NSRect)dirtyRect {
    NSColor *darkBorderColor = RGB(36, 43, 55);
    NSColor *darkSelectedBorderColor = RGB(26, 31, 40);
    NSColor *lightBorderColor = RGB(65, 72, 87);

    // Get the table view and info on row index numbers
    NSTableView *tableView = (NSTableView*)[self superview]; // The table view the row is part of
    NSInteger selectedRowNumber = [tableView selectedRow];
    NSInteger ownRowNumber = [tableView rowForView:self];
    BOOL isBeforeSelected = (selectedRowNumber == ownRowNumber+1);
    
    [[NSGraphicsContext currentContext] setShouldAntialias:NO];
    if (!self.isSelected && !isBeforeSelected) {
        [darkBorderColor set];
        NSBezierPath *topBorderPath = [NSBezierPath bezierPath];
        [topBorderPath moveToPoint:NSMakePoint(0, self.bounds.size.height-1)];
        [topBorderPath lineToPoint:NSMakePoint(self.bounds.size.width, self.bounds.size.height-1)];
        [topBorderPath stroke];
    }

    if (isBeforeSelected) {
        [darkSelectedBorderColor set];
    } else {
        [lightBorderColor set];
    }
    NSBezierPath *bottomBorderPath = [NSBezierPath bezierPath];
    [bottomBorderPath moveToPoint:NSMakePoint(0, self.bounds.size.height)];
    [bottomBorderPath lineToPoint:NSMakePoint(self.bounds.size.width, self.bounds.size.height)];
    [bottomBorderPath stroke];

    // If the row is selected, tell the preceding row to redraw its bottom separator line (which is also the top line of the selected row)
    if (([self isSelected]) && (selectedRowNumber > 0)) {
        [tableView setNeedsDisplayInRect:[tableView rectOfRow:selectedRowNumber-1]];
    }
}

- (void)drawSelectionInRect:(NSRect)dirtyRect {
    NSColor *bgColor = RGB(36, 43, 55);

    [bgColor set];
    [NSBezierPath fillRect:self.bounds];
}

@end
