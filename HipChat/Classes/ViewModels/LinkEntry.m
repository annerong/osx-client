//
//  FileEntry.m
//  HipChat
//
//  Created by Christopher Rivers on 4/1/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "LinkEntry.h"

#import "HipChatOSXApp.h"
#import "OSXHelpers.h"
#import "RegexKitLite.h"
#import "User.h"
#import "XMPP.h"

@implementation LinkEntry

//    Response format for links:
//    <iq from="1_1@chat.hipchat.com" id="1" type="result">
//        <query xmlns="http://hipchat.com/protocol/files">
//            <item>
//                <id>2</id>
//                <url>http://www.hipchat.com/history</url>
//                <group_id>10803</group_id>
//                <user_id>29833</user_id>
//                <date>120957092892</date>
//            </item>
//            <item>...</item>
//            <item>...</item>
//            <item>...</item>
//        </query>
//    </iq>
- (id)initWithItem:(NSXMLElement *)item {
    self = [super init];
    if (self) {
        self.entryId = [[item elementForName:@"id"] stringValue];
        NSString *urlValue = [[item elementForName:@"url"] stringValue];
        self.url = [NSURL URLWithString:urlValue];
        self.urlString = [OSXHelpers linkStyleWithText:[urlValue stringByReplacingOccurrencesOfRegex:@"^https?://" withString:@""]
                                                  font:nil
                                             alignment:nil
                                                   url:[NSURL URLWithString:urlValue]];

        XMPPJID *userJid = [OSXHelpers jidFromUserId:[[item elementForName:@"user_id"] stringValue]];
        User *user = [HipChatOSXApp.instance getUserInfo:userJid];
        self.userName = (user ? user.name : @"Unknown");
        self.uploadDate = [OSXHelpers parseDate:[[item elementForName:@"date"] stringValue]];
    }
    return self;
}

@end
