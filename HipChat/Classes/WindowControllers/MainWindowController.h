//
//  MainWindowController.h
//  HipChat
//
//  Created by Christopher Rivers on 11/19/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class WebView;
@class XMPPJID;

@interface MainWindowController : NSWindowController <NSWindowDelegate> {
    NSTimer *flashTimer;
    XMPPJID *inviteJid;
    NSMutableDictionary *sheets;
}

// Menu outlets
- (IBAction)changeTopic:(id)sender;
- (IBAction)closeCurrentChat:(id)sender;
- (IBAction)decreaseFontSize:(id)sender;
- (IBAction)increaseFontSize:(id)sender;
- (IBAction)joinChat:(id)sender;
- (IBAction)logAdditionalData:(id)sender;
- (IBAction)muteRoomAll:(id)sender;
- (IBAction)muteRoomExceptMe:(id)sender;
- (IBAction)nextTab:(id)sender;
- (IBAction)previousTab:(id)sender;
- (IBAction)releaseNotes:(id)sender;
// Not currently used
//- (IBAction)keyboardShortcuts:(id)sender;
//- (IBAction)hipchatEmoticons:(id)sender;
- (IBAction)reopenLastChat:(id)sender;
- (IBAction)returnToNormalFontSize:(id)sender;
- (IBAction)roomArchive:(id)sender;
- (IBAction)roomChangePrivacy:(id)sender;
- (IBAction)roomDelete:(id)sender;
- (IBAction)roomInvite:(id)sender;
- (IBAction)roomRemoveUsers:(id)sender;
- (IBAction)roomRename:(id)sender;
- (IBAction)roomUnarchive:(id)sender;
- (IBAction)showFilesSidebar:(id)sender;
- (IBAction)showLinksSidebar:(id)sender;
- (IBAction)showPrimaryRightSidebar:(id)sender;
- (IBAction)showSearch:(id)sender;
- (IBAction)toggleLeftSidebar:(id)sender;
- (IBAction)toggleRightSidebar:(id)sender;
- (IBAction)unmute:(id)sender;

@property IBOutlet NSView *contentView;
@property IBOutlet NSBox *noticeBox;
@property IBOutlet WebView *noticeWebView;

@end
