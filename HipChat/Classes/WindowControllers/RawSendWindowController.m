//
//  RawSendWindowController.m
//  HipChat
//
//  Created by Christopher Rivers on 4/29/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "RawSendWindowController.h"

#import "HipChatOSXApp.h"
#import "OSXHelpers.h"
#import "XMPP.h"

@implementation RawSendWindowController

- (void)windowDidLoad {
    [self.inputField setTextContainerInset:NSMakeSize(2.0, 5.0)];
    [self.inputField setFont:[NSFont fontWithName:@"Monaco" size:12.0]];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Helper functions
///////////////////////////////////////////////////////////////////////

- (void)closeSheet:(NSInteger)returnCode {
    // If we're not cancelling, make sure something was selected
    // Otherwise, beep
    if (returnCode == NSOKButton) {
        NSError *error = nil;
        NSXMLElement *elem = [[NSXMLElement alloc] initWithXMLString:self.inputField.string error:&error];
        if (elem) {
            [HipChatOSXApp.instance.conn sendElement:elem];
        } else {
            if (error) {
                DDLogWarn(@"Error creating XML element from string: %@", [error localizedDescription]);
            }
            NSBeep();
            return;
        }
    }
    [super closeSheet:returnCode];
}

@end
