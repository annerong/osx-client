//
//  HipChatLogManager.h
//  HipChat
//
//  Created by Christopher Rivers on 2/4/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "DDFileLogger.h"

@interface HipChatLogManager : DDLogFileManagerDefault

@end
