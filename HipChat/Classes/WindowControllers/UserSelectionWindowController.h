//
//  UserSelectionWindowController.h
//  HipChat
//
//  Created by Chris Rivers on 10/28/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "HipChatSheetController.h"

@class ChatFilterArrayController;
@class XMPPJID;
@class User;

@interface UserSelectionWindowController : HipChatSheetController <NSTextViewDelegate> {
    NSMutableArray *selectedUsers;
}

- (void)hideAutoComplete;
- (void)initializeUserList;

@property NSMutableArray *userList;
@property IBOutlet NSTokenField *namesInput;

@end
