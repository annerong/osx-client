//
//  LoginWindow.m
//  HipChat
//
//  Created by Christopher Rivers on 11/19/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Carbon/Carbon.h>

#import "LoginWindow.h"

#import "HipChatOSXApp.h"

@implementation LoginWindow

- (BOOL)canBecomeKeyWindow {
    return YES;
}

- (BOOL)performKeyEquivalent:(NSEvent *)theEvent {
    // Supersets must come first in the if statement
    // e.g. CMD+ALT must come before just CMD or just ALT
    NSUInteger flags = [theEvent modifierFlags] & NSDeviceIndependentModifierFlagsMask;
    if ((flags & NSCommandKeyMask) && (flags & NSControlKeyMask) && (flags & NSShiftKeyMask)) {
        if ([theEvent keyCode] == kVK_F3) {
            [HipChatOSXApp.instance toggleDevPrefs];
        }
        return YES;
    }

    return NO;
}

@end
