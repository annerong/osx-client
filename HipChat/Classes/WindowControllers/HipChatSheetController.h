//
//  HipChatSheetController.h
//  HipChat
//
//  Created by Christopher Rivers on 3/22/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class ChatFilterArrayController;

@interface HipChatSheetController : NSWindowController <NSTableViewDelegate, NSTextViewDelegate>

- (void)closeSheet:(NSInteger)returnCode;
- (void)showAutoCompleteUnderInput:(NSView *)input;

- (IBAction)handleButtonClick:(id)sender;
- (IBAction)onAutocompleteClick:(id)sender;

@property IBOutlet ChatFilterArrayController *arrayController;
@property IBOutlet NSPanel *autoComplete;
@property IBOutlet NSButton *cancelButton;
@property IBOutlet NSButton *okButton;
@property IBOutlet NSTableView *tableView;

@end
