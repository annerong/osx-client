//
//  DevelopmentPrefsViewController.h
//  HipChat
//
//  Created by Christopher Rivers on 4/12/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface DevelopmentPrefsViewController : NSViewController

//- (IBAction)restoreAlternateHosts:(id)sender;
//- (IBAction)restoreDefaults:(id)sender;

@property IBOutlet NSMatrix *connectRadios;
@property IBOutlet NSTextField *connectField;
@property IBOutlet NSButton *validateCertCheck;


@end
