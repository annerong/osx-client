//
//  RoomRemoveMembersWindowController.m
//  HipChat
//
//  Created by Christopher Rivers on 4/19/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "RoomRemoveMembersWindowController.h"

#import "HipChatOSXApp.h"
#import "OSXHelpers.h"
#import "PermListEntry.h"
#import "Room.h"
#import "User.h"
#import "XMPP.h"

@implementation RoomRemoveMembersWindowController

- (void)windowDidLoad {
    [super windowDidLoad];

    if (!self.jid) {
        DDLogError(@"No jid was set when removing members from room");
        [self closeSheet:NSCancelButton];
    }
    
    HipChatOSXApp *app = HipChatOSXApp.instance;
    Room *room = [app getRoomInfo:self.jid];
    if (!room) {
        DDLogError(@"No room data found when showing room remove users sheet. Aborting...");
        [app showFlash:@"Room data was not available. Cannot manage users."];
        [self closeSheet:NSCancelButton];
    }

    IQCallback callback = ^(XMPPIQ *iq) {
        NSXMLElement *query = [iq elementForName:@"query" xmlns:@"http://jabber.org/protocol/disco#items"];
        NSArray *items = [query elementsForName:@"item"];
        NSMutableArray *entryList = [NSMutableArray arrayWithCapacity:items.count];
        for (NSXMLElement *item in items) {
            PermListEntry *entry = [PermListEntry permListEntryWithDiscoItem:item];
            [entryList addObject:entry];
            [userDict setObject:entry forKey:entry.jid.bare];
        }
        [entryList sortUsingDescriptors:
         [NSArray arrayWithObject:
          [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]];
        self.userList = entryList;
        [self.permissionsTable reloadData];
    };
    [room fetchMembersWithCallback:callback];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Helper functions
///////////////////////////////////////////////////////////////////////

- (void)closeSheet:(NSInteger)returnCode {
    // If we're not cancelling, make sure something was selected
    // Otherwise, beep
    if (returnCode == NSOKButton) {
        HipChatOSXApp *app = HipChatOSXApp.instance;
        Room *room = [app getRoomInfo:self.jid];
        if (!room) {
            DDLogError(@"No room data found when closing room remove users sheet. Aborting...");
            [app showFlash:@"Room data was not available. Cannot manage users."];
        } else {
            IQCallback callback = ^(XMPPIQ *iq) {
                if ([iq.type isEqualToString:@"error"]) {
                    [HipChatOSXApp.instance showFlash:@"There was an error trying to remove users from the room."];
                } else {
                    [HipChatOSXApp.instance showFlash:@"Successfully removed users."];
                }
            };

            [room removeMultipleUsers:self.selectedJids callback:callback];
        }
    }
    
    [super closeSheet:returnCode];
}

- (NSArray *)selectedJids {
    NSMutableArray *selectedUsers = [NSMutableArray array];
    for (PermListEntry *perm in self.userList) {
        if (perm.selected) {
            [selectedUsers addObject:perm.jid];
        }
    }
    return selectedUsers;
}

- (void)setDisabledUsers:(NSArray *)users {
    for (XMPPJID *jid in users) {
        PermListEntry *entry = (PermListEntry *)[userDict objectForKey:jid.bare];
        entry.enabled = NO;
        // TODO: We're assuming disable users are always selected here
        // Maybe not true in the future? (e.g. if we have admin settings to prevent
        // users from joining rooms)
        entry.selected = YES;
    }
}

@end
