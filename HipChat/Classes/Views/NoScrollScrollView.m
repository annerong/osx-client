//
//  NoScrollScrollView.m
//  HipChat
//
//  Created by Christopher Rivers on 11/12/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "NoScrollScrollView.h"

@implementation NoScrollScrollView

// Ignore scroll wheel events - this scroll view will be sized to always accomodate its content
// Pass it directly to nextResponder since we don't want our parent (NSScrollView) to eat the event
- (void)scrollWheel:(NSEvent *)theEvent {
    [[self nextResponder] scrollWheel:theEvent];
}

@end
