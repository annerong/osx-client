//
//  ChatStackViewController.h
//  HipChat
//
//  Created by Christopher Rivers on 10/23/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class ChatViewController;
@class FocusOnlyBox;
@class PrettyBorderBox;
@class Room;
@class WebView;
@class XMPPJID;

@protocol HipChatTextViewDelegate;

@interface ChatStackViewController : NSViewController <NSTabViewDelegate> {
    XMPPJID *chatToFocus;
    ChatViewController *currentChat;
    Room *currentRoom;
    NSTimer *popupDisplayTimer;
}

- (IBAction)handleCancelTopic:(id)sender;
- (IBAction)handleSearch:(id)sender;
- (IBAction)handleSetTopic:(id)sender;

@property IBOutlet NSButton *cancelTopicButton;
@property IBOutlet NSBox *chatView;
@property IBOutlet FocusOnlyBox *searchBox;
@property IBOutlet NSButton *searchButton;
@property IBOutlet NSTextField *searchInput;
@property IBOutlet NSButton *searchToggle;
@property IBOutlet NSButton *setTopicButton;
@property IBOutlet FocusOnlyBox *topicBox;
@property IBOutlet NSTextField *topicInput;
@property IBOutlet WebView *topicWebView;
@property IBOutlet PrettyBorderBox *topBox;


@end
