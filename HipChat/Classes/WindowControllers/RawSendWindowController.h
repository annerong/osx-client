//
//  RawSendWindowController.h
//  HipChat
//
//  Created by Christopher Rivers on 4/29/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "HipChatSheetController.h"

@interface RawSendWindowController : HipChatSheetController

@property IBOutlet NSTextView *inputField;

@end
