//
//  ChangePrivacyWindowController.h
//  HipChat
//
//  Created by Christopher Rivers on 4/18/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "HipChatSheetController.h"

@class XMPPJID;

@interface ChangePrivacyWindowController : HipChatSheetController {
    NSString *initialPrivacy;
}

- (IBAction)onSelectOpen:(id)sender;
- (IBAction)onSelectPrivate:(id)sender;

@property XMPPJID *jid;
@property IBOutlet NSTextField *permText;
@property IBOutlet NSMatrix *radioOpen;
@property IBOutlet NSMatrix *radioPrivate;

@end
