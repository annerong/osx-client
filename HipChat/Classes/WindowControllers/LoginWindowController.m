//
//  LoginWindowController.m
//  HipChat
//
//  Created by Christopher Rivers on 11/19/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "LoginWindowController.h"

#import "HipChatOSXApp.h"
#import "NS(Attributed)String+Geometrics.h"
#import "OSXHelpers.h"

@implementation LoginWindowController

static NSString *htmlHead =
@"<style>"
" *, div, p { margin: 0; padding; 0 } "
" * { line-height: 20px; font-size: 12px; font-family: \"Lucida Grande\",sans-serif; }"
" a { color: #6666cc; text-decoration: underline; cursor: pointer; }"
"</style>";

static NSInteger FLASH_DISPLAY_SECONDS = 3;

static CGFloat INITIAL_WINDOW_HEIGHT;

- (void)windowDidLoad {
    // Set colors for the "links" on the sign in window
    NSColor *txtColor = RGB(190.0, 190.0, 190.0);
    [OSXHelpers setTextColor:txtColor forButton:self.helpButton];
    [OSXHelpers setTextColor:txtColor forButton:self.signUpButton];
    [OSXHelpers setTextColor:txtColor forButton:self.rememberMeCheck];
    [self.helpButton.cell setBackgroundStyle:NSBackgroundStyleDark];
    [self.signUpButton.cell setBackgroundStyle:NSBackgroundStyleDark];
    [self.rememberMeCheck.cell setBackgroundStyle:NSBackgroundStyleDark];

    [OSXHelpers setTextColor:RGB(69.0, 69.0, 69.0) forButton:self.signInButton];
    [self.signInButton.cell setBackgroundStyle:NSBackgroundStyleDark];

    // CMD-W should close the window, not attempt to close tabs
    [OSXHelpers setCommandWClosesTabs:NO];

    HipChatOSXApp *app = HipChatOSXApp.instance;
    [self.rememberMeCheck setState:([app getRememberLogin] ? NSOnState : NSOffState)];
    [self updateCredentials];

    [app addDelegate:self];

    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(handleFlash:) name:HipChatFlashNotification object:nil];

    // Set the logo and background image for the window
    [self.appLogo setImage:[NSImage imageNamed:@"login_logo"]];
    [self.window setBackgroundColor:[NSColor colorWithPatternImage:[NSImage imageNamed:@"login_bg"]]];

    // Save the initial window height so we can resize appropriately when displaying errors
    INITIAL_WINDOW_HEIGHT = self.window.frame.size.height;

    // Setup the flash message box. Attempting to create the WebView within IB
    // always results in headaches and improper displays. Create it here instead
    NSSize boxSize = self.noticeBox.frame.size;
    NSRect noticeFrame = NSMakeRect(10.0, 10.0, (boxSize.width-20.0), (boxSize.height-20.0));
    self.noticeWebView = [[WebView alloc] initWithFrame:noticeFrame];
    self.noticeWebView.drawsBackground = NO;
    self.noticeWebView.frameLoadDelegate = self;
    self.noticeWebView.policyDelegate = self;
    [self.noticeBox addSubview:self.noticeWebView];
    self.noticeBox.cornerRadius = 4.0;
}

- (void)doSignIn {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    [app connectWithEmail:[self.emailField stringValue] password:[self.passwordField stringValue]];
}

///////////////////////////////////////////////////////////////////////
#pragma mark Event listeners
///////////////////////////////////////////////////////////////////////

- (void)handleFlash:(NSNotification *)notif {
    NSString *message = [[notif userInfo] objectForKey:@"message"];
    if (!message) {
        DDLogError(@"Received flash notification with no message. WTF Mate?");
        return;
    }
    NSNumber *duration = [[notif userInfo] objectForKey:@"duration"];
    if (duration) {
        [self showFlashMessage:message duration:[duration floatValue]];
    } else {
        [self showFlashMessage:message];
    }
}

- (void)handleFlashTimer:(NSTimer *)timer {
    flashTimer = nil;
    [self hideFlashMessage];
}

- (IBAction)onHelpClicked:(id)sender {
    NSURL *feedbackURL = [NSURL URLWithString:@"http://help.hipchat.com/knowledgebase/articles/64363-can-t-sign-in-to-desktop-client"];
    [[NSWorkspace sharedWorkspace] openURL:feedbackURL];
}

- (IBAction)onRememberMeChange:(id)sender {
    BOOL isChecked = (self.rememberMeCheck.state == NSOnState);
    [HipChatOSXApp.instance setRememberLogin:isChecked];
}

- (IBAction)onSignupClicked:(id)sender {
    NSURL *feedbackURL = [NSURL URLWithString:@"https://www.hipchat.com/sign_up"];
    [[NSWorkspace sharedWorkspace] openURL:feedbackURL];
}

- (IBAction)submitLogin:(id)sender {
    [self hideErrorBox];
    [self doSignIn];
}

///////////////////////////////////////////////////////////////////////
#pragma mark HipChatOSXApp Delegate methods
///////////////////////////////////////////////////////////////////////

- (void)hipChatAppConnectFailed:(NSError *)err {
    if (!self.window.isVisible) {
        return;
    }

    if (self.errorBox.isHidden) {
        [self showError:err];
    }
}

- (void)hipChatAppConnectSucceeded {
    [self.passwordField setStringValue:@""];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark WebView Delegate functions
///////////////////////////////////////////////////////////////////////

- (void)webView:(WebView *)webView
decidePolicyForNavigationAction:(NSDictionary *)actionInformation
        request:(NSURLRequest *)request
          frame:(WebFrame *)frame
decisionListener:(id < WebPolicyDecisionListener >)listener {

	NSString *requestString = [[request URL] absoluteString];
	NSArray *components = [requestString componentsSeparatedByString:@":"];
	if ([requestString rangeOfString:@"action"].location == 0 && [components count] > 1) {
		if ([[components objectAtIndex:1] isEqualToString:@"reconnect"]) {
            [HipChatOSXApp.instance triggerManualReconnect];
		}
        return;
	}

    [[NSWorkspace sharedWorkspace] openURL:[request URL]];
    return;
}

- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame *)webFrame {
    if([webFrame isEqual:[self.noticeWebView mainFrame]]) {
        // Get size of webview content
        NSRect webFrameRect = [[[webFrame frameView] documentView] frame];

        // Set the box to the correct height
        [self.noticeBox setFrameSize:NSMakeSize(NSWidth(self.noticeBox.frame),
                                                NSHeight(webFrameRect)+14.0)];

        // Set the webview to the correct height and position
        // (for some reason autoresizing doesn't like to keep the webview properly positioned)
        NSRect boxFrame = self.noticeBox.frame;
        [self.noticeWebView setFrame:NSMakeRect(7.0, 7.0, boxFrame.size.width-14.0, boxFrame.size.height-14.0)];
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark Helper functions
///////////////////////////////////////////////////////////////////////

- (void)checkForErrors {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    if (app.lastConnectError) {
        [self showError:app.lastConnectError];
    }
}

- (void)hideFlashMessage {
    [[self.noticeBox animator] setAlphaValue:0.0];

    // Delay actually setting the hidden value of the box to allow the animation to complete
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self.noticeBox setHidden:YES];
    });
}

- (void)showError:(NSError *)err {
	DDLogInfo(@"Connect failure: %@", err);

	// Set a title for the alert based on the code (default to "Signin failed")
	NSString *title = err.localizedDescription;
    if (!title) {
        switch ([err code]) {
            case HipChatErrorBadEmailPassword:
                title = @"Sign in failed - bad email / password";
                break;
            case HipChatErrorBadResponse:
                title = @"We received an invalid response from the server. Please try again in a few moments.";
                break;
            case HipChatErrorLoginConflict:
                title = @"This account was disconnected because it signed in from somewhere else.";
                break;
            case HipChatErrorNetworkUnavailable:
                break;
            case HipChatErrorNoSessionsAvailable:
                title = @"The are too many users connected to your HipChat account. Please have an administrator upgrade the account.";
                break;
            case HipChatErrorPlanChange:
                title = @"An administrator is changing plans. Please wait a moment and try connecting again.";
                break;
            case HipChatErrorServerHostNotFound:
                title = @"Failed to connect - HipChat Server host not found.";
                break;
            case HipChatErrorServerUnreachable:
            case HipChatErrorServiceUnavailable:
            case HipChatErrorSystemShutdown:
            case HipChatErrorUnknown:
                title = @"Failed to connect - a server error occurred.";
                break;
            default:
                title = @"Could not connect to the HipChat servers.";
                break;
        }
    }

	DDLogInfo(@"Showing login error message: %@", title);
//    [self.errorMessage.textStorage setAttributedString:[[NSAttributedString alloc] initWithHTML:[title dataUsingEncoding:NSUTF16StringEncoding] baseURL:nil documentAttributes:nil]];
    [self.errorMessage setString:title];
    [self showErrorBox];
}

- (void)showFlashMessage:(NSString *)message {
    [self showFlashMessage:message duration:FLASH_DISPLAY_SECONDS];
}

- (void)showFlashMessage:(NSString *)message duration:(NSTimeInterval)duration {
    // Need to include style block so we don't show in Times New Roman (dumb)
    NSString *html = [NSString stringWithFormat:@"<html><head>%@</head><body>%@</body></html>",
                      htmlHead,
                      message];
    self.noticeWebView.policyDelegate = nil;
    [self.noticeWebView.mainFrame loadHTMLString:html baseURL:[NSURL URLWithString:@"http://www.hipchat.com"]];
    self.noticeWebView.policyDelegate = self;

    // Fade in animation
    [self.noticeBox setAlphaValue:0.0];
    [self.noticeBox setHidden:NO];
    [[self.noticeBox animator] setAlphaValue:1.0];

    // Set a timer to hide the flash after hideDelay
    // Don't autohide if delay <= 0
    [flashTimer invalidate];
    if (duration > 0) {
        flashTimer = [NSTimer scheduledTimerWithTimeInterval:duration
                                                      target:self
                                                    selector:@selector(handleFlashTimer:)
                                                    userInfo:nil
                                                     repeats:NO];
    }
}


- (void)showErrorBox {
    // Calculate the height required to display the text
    CGFloat textHeight = [self.errorMessage.attributedString heightForWidth:self.errorMessage.frame.size.width];

    NSRect formFrame = [self.formBox frame];
    NSRect errorFrame = [self.errorBox frame];
    errorFrame.size.height = textHeight;
    errorFrame.origin.y = formFrame.origin.y + formFrame.size.height;

    // Set the window height and position to accomodate the resized error display
    NSRect windowFrame = [self.window frame];
    CGFloat oldHeight = windowFrame.size.height;
    CGFloat newHeight = INITIAL_WINDOW_HEIGHT + errorFrame.size.height;
    windowFrame.size.height = newHeight;
    windowFrame.origin.y += (oldHeight - newHeight);
    [self.window setFrame:windowFrame display:YES animate:NO];

    // Show the error box
    [self.errorBox setHidden:NO];
    [self.errorBox setFrame:errorFrame];
}

- (void)hideErrorBox {
    if (self.errorBox.isHidden) {
        return;
    }

    NSRect errorFrame = [self.errorBox frame];
    CGFloat errorHeight = errorFrame.size.height;
    [self.errorBox setHidden:YES];

    NSRect windowFrame = [self.window frame];
    windowFrame.size.height -= errorHeight;
    windowFrame.origin.y += errorHeight;
    [self.window setFrame:windowFrame display:YES animate:NO];
}

- (void)updateCredentials {
    DDLogInfo(@"Updating login form credentials to current stored values");
    HipChatOSXApp *app = HipChatOSXApp.instance;
    NSString *signinEmail = [app getSignInEmail];
    if (signinEmail) {
        [self.emailField setStringValue:signinEmail];
    } else {
        [self.emailField setStringValue:@""];
    }

    NSString *signinPassword = [app getSignInPassword];
    if (signinPassword && [app getRememberLogin]) {
        [self.passwordField setStringValue:signinPassword];
    } else {
        [self.passwordField setStringValue:@""];
    }
}

@end
