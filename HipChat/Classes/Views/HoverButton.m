//
//  HoverButton.m
//  HipChat
//
//  Created by Christopher Rivers on 11/9/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "HoverButton.h"

@implementation HoverButton

- (void)awakeFromNib {
    NSTrackingArea *tracking = [[NSTrackingArea alloc] initWithRect:self.frame
                                                            options:(NSTrackingMouseEnteredAndExited | NSTrackingActiveInKeyWindow | NSTrackingInVisibleRect)
                                                              owner:self
                                                           userInfo:nil];
    [self addTrackingArea:tracking];
    originalImage = self.image;
}

- (void)mouseEntered:(NSEvent *)theEvent {
    self.image = self.alternateImage;
}

- (void)mouseExited:(NSEvent *)theEvent {
    if (originalImage) {
        self.image = originalImage;
    }
}

- (void)resetImage {
    if (originalImage) {
        self.image = originalImage;
        [self setNeedsDisplay];
    }
}

@end
