#/bin/sh

rm HipChat.zip

# Store the app on S3
zip -ry HipChat.zip HipChat.app
VERSTR=`cat HipChat.app/Contents/Info.plist | grep -A 1 CFBundleShortVersionString | tail -1 | sed "s#</string>##g" | sed "s#<string>##g" | perl -p -i -n -e "s/\s+//g"`
BUNDLE=`grep "com.hipchat.HipChat" HipChat.app/Contents/Info.plist | cut -d ">" -f2- | cut -d "<" -f1-1 | sed 's/com.hipchat.HipChat\(\w*\)/\1/'`
FILENAME=HipChat$BUNDLE-$VERSTR.zip
DMG_FILENAME=HipChat$BUNDLE-$VERSTR.dmg

./dmgcreate.sh

scp HipChat.zip dev-crivers1.hipchat.com:~/mac-beta/$FILENAME
scp HipChat.dmg dev-crivers1.hipchat.com:~/mac-beta/$DMG_FILENAME
ssh dev-crivers1.hipchat.com s3cmd put --acl-public /home/chris/mac-beta/$FILENAME s3://downloads.hipchat.com/osx/
ssh dev-crivers1.hipchat.com s3cmd cp --acl-public s3://downloads.hipchat.com/osx/$FILENAME s3://downloads.hipchat.com/osx/HipChat$BUNDLE.zip
ssh dev-crivers1.hipchat.com s3cmd put --acl-public /home/chris/mac-beta/$DMG_FILENAME s3://downloads.hipchat.com/osx/
ssh dev-crivers1.hipchat.com s3cmd cp --acl-public s3://downloads.hipchat.com/osx/$DMG_FILENAME s3://downloads.hipchat.com/osx/HipChat$BUNDLE.dmg

