//
//  CursorButton.h
//  HipChat
//
//  Created by Christopher Rivers on 12/27/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface CursorButton : NSButton <NSMenuDelegate> {
    NSTimer *tooltipTimer;
}

@end
