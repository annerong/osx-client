//
//  EmoticonMenuViewController.h
//  HipChat
//
//  Created by Christopher Rivers on 5/7/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface EmoticonMenuViewController : NSViewController

@property NSMutableArray *emoticons;
@property IBOutlet NSArrayController *emoticonsArrayController;

@end
