//
//  LobbyEntry.h
//  HipChat
//
//  Created by Christopher Rivers on 12/12/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Room;
@class XMPPJID;

@interface LobbyEntry : NSObject

- (void)deinitialize;
- (LobbyEntry *)initWithRoom:(Room *)room;
- (LobbyEntry *)initWithHeader:(NSString *)headerText section:(NSInteger)section;

@property NSString *icon;
@property BOOL isArchived;
@property BOOL isHeader;
@property (readonly) BOOL isPrivate;
@property XMPPJID *jid;
@property NSString *name;
@property Room *room;
@property NSInteger section;
@property (readonly) BOOL showAdmin;

@end
