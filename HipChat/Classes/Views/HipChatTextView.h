//
//  HipChatTextView.h
//  HipChat
//
//  Created by Christopher Rivers on 11/6/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@protocol HipChatTextViewDelegate;

@interface HipChatTextView : NSTextView

@property (nonatomic, assign) id <HipChatTextViewDelegate> delegate;

@end



@protocol HipChatTextViewDelegate <NSTextViewDelegate>

@optional

- (void)textView:(HipChatTextView *)textView hitAutoCompleteTrigger:(NSString *)keyCode;
- (void)textView:(HipChatTextView *)textView didPasteImage:(NSImage *)image;
- (void)textView:(HipChatTextView *)textView didPasteURL:(NSURL *)fileURL;
- (void)textView:(HipChatTextView *)textView didPasteFile:(NSFileWrapper *)fileWrapper;

@end
