//
//  NotificationPrefsViewController.m
//  HipChat
//
//  Created by Christopher Rivers on 3/8/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "NotificationPrefsViewController.h"

@implementation NotificationPrefsViewController

-(NSString *)identifier{
    return @"Notifications";
}

-(NSImage *)toolbarItemImage{
    return [NSImage imageNamed:NSImageNameAdvanced];
}

-(NSString *)toolbarItemLabel{
    return @"Notifications";
}
@end
