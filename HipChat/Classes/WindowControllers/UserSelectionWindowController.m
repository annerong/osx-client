//
//  UserSelectionWindowController.m
//  HipChat
//
//  Created by Chris Rivers on 10/28/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "UserSelectionWindowController.h"

#import "Room.h"
#import "User.h"
#import "XMPPJID.h"

#import "ChatFilterArrayController.h"
#import "HipChatOSXApp.h"
#import "NS(Attributed)String+Geometrics.h"
#import "OSXHelpers.h"

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Controller class
///////////////////////////////////////////////////////////////////////

@implementation UserSelectionWindowController

static CGFloat inputBaseHeight;
static CGFloat baseExtraHeight;

- (void)initializeUserList {
    HipChatOSXApp *app = [HipChatOSXApp instance];
    NSDictionary *userList = [app getAllUsers];
    self.userList = [NSMutableArray arrayWithCapacity:(userList.count)];
    for (id key in userList) {
        User *user = [userList objectForKey:key];
        [self.userList addObject:user];
    }
    [self.userList sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSString *name1 = [obj1 performSelector:@selector(name)];
        NSString *name2 = [obj2 performSelector:@selector(name)];
        return [name1 compare:name2];
    }];

    selectedUsers = [NSMutableArray array];

    // Store the initial height of the invite names input area
    // We never want to reduce the input height below its initial height
    inputBaseHeight = self.namesInput.frame.size.height;

    // The baseExtraHeight will be used to determine the additional height we
    // should add to the content height when resizing the names input
    baseExtraHeight = 0.0;
}

- (NSDictionary *)generateSelectedJidsDictionary {
    NSMutableDictionary *selectedDict = [NSMutableDictionary dictionaryWithCapacity:selectedUsers.count];
    for (User *user in selectedUsers) {
        [selectedDict setObject:[NSNumber numberWithBool:YES] forKey:user.jid.bare];
    }
    return selectedDict;
}

// This gets called by the parent class whenever someone hits return with an item selected from autocomplete
// Return: NO = continue on to default behavior, YES = don't perform default behavior
- (BOOL)handleAutocompleteReturn:(id)selectedItem {
    [selectedUsers addObject:selectedItem];
    self.arrayController.selectedJids = [self generateSelectedJidsDictionary];
    [self.namesInput setObjectValue:selectedUsers];
    [self updateNamesInputSize];
    [self hideAutoComplete];
    return YES;
}

// Return: NO = continue on to default behavior, YES = don't perform default behavior
- (BOOL)handleAutocompleteTab:(id)selectedItem {
    [self handleAutocompleteReturn:selectedItem];
    return YES;
}

- (void)hideAutoComplete {
    [self.autoComplete close];
}

- (void)updateNamesInputSize {
    NSRect currentFrame = self.namesInput.frame;
    CGFloat contentHeight = [self.namesInput.attributedStringValue heightForWidth:self.namesInput.frame.size.width];
    if (contentHeight < inputBaseHeight) {
        baseExtraHeight = inputBaseHeight - contentHeight;
    }
    contentHeight += baseExtraHeight;
    contentHeight = MAX(inputBaseHeight, contentHeight);
    CGFloat heightDiff = (contentHeight - NSHeight(currentFrame));
    [self.namesInput setFrame:NSMakeRect(currentFrame.origin.x,
                                         currentFrame.origin.y - heightDiff,
                                         NSWidth(currentFrame),
                                         contentHeight)];

    NSRect winFrame = self.window.frame;
    NSRect newWindowFrame = NSMakeRect(winFrame.origin.x,
                                       winFrame.origin.y,
                                       winFrame.size.width,
                                       winFrame.size.height + heightDiff);
    [self.window setFrame:newWindowFrame display:YES animate:YES];
}

- (void)updateSelectedUsersWithNames:(NSArray *)names {
    // This is something of a hack. If two users have the same name, we will simply
    // choose the first one with that name and add them to the list of invitees.
    // This only gets called if someone deletes a previous entry.
    // If they add users via autocomplete and DON'T delete, we will just
    // use the selected objects from the autocomplete menu
    DDLogInfo(@"Updating selected users array with current names: %@", names);
    NSMutableArray *newSelectedUsers = [NSMutableArray arrayWithCapacity:names.count];
    for (NSString *name in names) {
        for (User *user in self.userList) {
            if ([user.name isEqualToString:name]) {
                [newSelectedUsers addObject:user];
                break;
            }
        }
    }
    selectedUsers = newSelectedUsers;
    self.arrayController.selectedJids = [self generateSelectedJidsDictionary];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark NSTextFieldDelegate functions
///////////////////////////////////////////////////////////////////////

- (void)controlTextDidChange:(NSNotification *)aNotification {
    // The stringValue is the full string value of the input, with tokenized values
    // separated by commas
    NSArray *names = [self.namesInput.stringValue componentsSeparatedByString:@","];
    if (selectedUsers.count > names.count ||
        (names.count == 1 && [[names objectAtIndex:0] length] == 0)) {
        [self updateSelectedUsersWithNames:names];
    }

    // The "currentEditor" string is the non-tokenized search string
    NSString *searchStr = self.namesInput.currentEditor.string;
    // For some reason, the editor adds the "Object Replacement Character" at the
    // beginning of the string it provides (Unicode 0xfffc). Strip it out.
    NSCharacterSet *objRepChar = [NSCharacterSet characterSetWithCharactersInString:@"\ufffc"];
    searchStr = [searchStr stringByTrimmingCharactersInSet:objRepChar];
    if (!searchStr || searchStr.length == 0) {
        [self hideAutoComplete];
    } else {
        [self.arrayController search:searchStr];
        if (((NSArray *)self.arrayController.arrangedObjects).count == 0) {
            [self hideAutoComplete];
        } else {
            [self showAutoCompleteUnderInput:self.namesInput];
        }
    }
    [self updateNamesInputSize];
    
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark NSTokenFieldDelegate functions
///////////////////////////////////////////////////////////////////////

- (NSString *)tokenField:(NSTokenField *)tokenField displayStringForRepresentedObject:(id)representedObject {
    return ([representedObject isKindOfClass:[User class]] ? [((User *)representedObject) name] : representedObject);
}

- (NSString *)tokenField:(NSTokenField *)tokenField editingStringForRepresentedObject:(id)representedObject {
    return ([representedObject isKindOfClass:[User class]] ? [((User *)representedObject) mentionName] : representedObject);
}

- (NSTokenStyle)tokenField:(NSTokenField *)tokenField styleForRepresentedObject:(id)representedObject {
    if ([representedObject isKindOfClass:[User class]]) {
        return NSDefaultTokenStyle;
    }
    return NSPlainTextTokenStyle;
}

@end
