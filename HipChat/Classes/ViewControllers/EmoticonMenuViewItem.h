//
//  EmoticonMenuViewItem.h
//  HipChat
//
//  Created by Christopher Rivers on 5/8/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface EmoticonMenuViewItem : NSCollectionViewItem

- (IBAction)onEmoticonClick:(id)sender;

@property IBOutlet NSBox *box;

@end
