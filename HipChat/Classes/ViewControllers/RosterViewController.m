//
//  RosterViewController.m
//  HipChat
//
//  Created by Christopher Rivers on 10/17/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "RosterViewController.h"

#import "HipChatOSXApp.h"
#import "OSXHelpers.h"
#import "PrettyBorderBox.h"
#import "RosterEntry.h"

#import "HipChatUser.h"
#import "Room.h"
#import "RoomOccupant.h"
#import "Roster.h"
#import "User.h"
#import "XMPPIQ.h"

@implementation RosterViewController

static NSString *SORT_BY_STATUS = @"sortByStatus";
static NSString *SORT_BY_NAME = @"sortByName";

@dynamic entries;

- (void)awakeFromNib {
    [self.tableView setTarget:self];
    [self.tableView setDoubleAction:@selector(onRosterDoubleClick)];

    self.guestAccessToggleBox.topBorderColor = RGB(194.0, 194.0, 194.0);
    self.guestAccessToggleBox.topBorderInsetColor = RGB(239.0, 239.0, 239.0);
    self.guestAccessToggleCell.onOffSwitchControlColors = OnOffSwitchControlBlueGreyColors;
    self.guestAccessToggleCell.onSwitchLabel = @"On";
    self.guestAccessToggleCell.offSwitchLabel = @"Off";
    self.guestAccessToggleCell.showsOnOffLabels = YES;
    self.guestAccessToggleCell.labelFontSize = 11.0;
    self.guestAccessURLBox.topBorderColor = RGB(225.0, 200.0, 156.0);

    NSString *sortOption = [[NSUserDefaults standardUserDefaults] objectForKey:@"rosterSort"];
    [self setSort:sortOption];
    [self.sortButton selectItemAtIndex:([sortOption isEqualToString:SORT_BY_NAME] ? 0 : 1)];
    self.sortBox.topBorderColor = RGB(194.0, 194.0, 194.0);
    self.sortBox.topBorderInsetColor = RGB(239.0, 239.0, 239.0);

    self.searchBox.bottomBorderColor = RGB(194.0, 194.0, 194.0);

    // Don't trigger rearrange on every single RosterEntry update - we'll handle it ourselves
    // See the observeValueForKeyPath function
    self.arrayController.automaticallyRearrangesObjects = NO;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        HipChatOSXApp *app = HipChatOSXApp.instance;
        [app.roster addDelegate:self delegateQueue:dispatch_get_main_queue()];
        [app addDelegate:self];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFocusChat:) name:HipChatFocusChatNotification object:app];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFocusLobby:) name:HipChatFocusLobbyNotification object:app];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onShowSearch:) name:HipChatShowSearchNotification object:app];

        self.entries = [NSMutableArray array];
        isPresenceLoaded = NO;
    }
    
    return self;
}

- (void)dealloc {
    DDLogError(@"Unexpected de-alloc of RosterViewController!");
    HipChatOSXApp *app = HipChatOSXApp.instance;
    [app.roster removeDelegate:self];
    [app removeDelegateImmediate:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark NSAlert delegate methods
///////////////////////////////////////////////////////////////////////

- (void)alertDidEnd:(NSAlert *)alert returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo {
    if (returnCode == NSAlertFirstButtonReturn) {
        HipChatOSXApp *app = HipChatOSXApp.instance;
        XMPPJID *currentJid = app.currentJid;
        Room *room = [app getRoomInfo:currentJid];
        if (!room) {
            return;
        }
        IQCallback callback = ^(XMPPIQ *iq) {
            if ([iq.type isEqualToString:@"error"]) {
                [app showFlash:@"There was an error disabling guess access."];
            } else {
                [app showFlash:@"Guest access disabled"];
            }
        };
        [room setGuestAccess:NO callback:callback];
    } else {
        // Do nothing - they cancelled their action
        [self.guestAccessToggle setState:NSOnState];
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark KVO compliance methods
///////////////////////////////////////////////////////////////////////

- (void)setEntries:(NSMutableArray *)newEntries {
    [self willChangeValueForKey:@"entries"];
    [self removeObservers];
    entries = newEntries;
    [self addObservers];
    [self didChangeValueForKey:@"entires"];
}

- (NSMutableArray *)entries {
    return entries;
}

- (void)insertObject:(RosterEntry *)entry inEntiresAtIndex:(NSUInteger)index {
    [entries insertObject:entry atIndex:index];
}

- (void)removeObjectFromEntriesAtIndex:(NSUInteger)index {
    [entries removeObjectAtIndex:index];
}

- (void)replaceObjectInEntriesAtIndex:(NSUInteger)index withObject:(id)object {
    [entries replaceObjectAtIndex:index withObject:object];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark NSSearchField / NSTextField delegate
///////////////////////////////////////////////////////////////////////

- (BOOL)control:(NSControl *)control textView:(NSTextView *)textView doCommandBySelector:(SEL)commandSelector {
    if (commandSelector == @selector(insertNewline:)) {
        if (self.tableView.selectedRow == -1) {
            return NO;
        }

        RosterEntry *entry = [self.arrayController.arrangedObjects objectAtIndex:self.tableView.selectedRow];
        HipChatOSXApp *app = HipChatOSXApp.instance;
        [app joinChat:entry.jid withFocus:YES];
    } else if (commandSelector == @selector(moveDown:)) {
        NSInteger selectedIndex = self.tableView.selectedRow;
        if (self.tableView.numberOfRows > selectedIndex+1) {
            [self.tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:selectedIndex+1] byExtendingSelection:NO];
        } else if (self.tableView.numberOfRows > 0) {
            [self.tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:0] byExtendingSelection:NO];
        }
    } else if (commandSelector == @selector(moveUp:)) {
        NSInteger selectedIndex = self.tableView.selectedRow;
        if (selectedIndex-1 > -1 && self.tableView.numberOfRows > 0) {
            [self.tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:selectedIndex-1] byExtendingSelection:NO];
        } else if (self.tableView.numberOfRows > 0) {
            [self.tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:self.tableView.numberOfRows-1] byExtendingSelection:NO];
        }
    }

    return NO;
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark NSTableView Delegate methods
///////////////////////////////////////////////////////////////////////

- (CGFloat)tableView:(NSTableView *)tableView heightOfRow:(NSInteger)row {
    RosterEntry *entry = [self.arrayController.arrangedObjects objectAtIndex:row];
    if (entry.status.length > 0) {
        return 33;
    }
    return 23;
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark RosterDelegate
///////////////////////////////////////////////////////////////////////

//- (void)rosterFinishedLoading:(Roster *)sender {
//	[self updateRosterEntriesForJid:[HipChatOSXApp.instance currentJid]];
//}

- (void)presenceFinishedLoading:(Roster *)sender {
    isPresenceLoaded = YES;
	[self updateRosterEntriesForJid:[HipChatOSXApp.instance currentJid]];
}

- (void)roster:(Roster *)sender removedUser:(XMPPJID *)jid {
	[self updateRosterEntriesForJid:[HipChatOSXApp.instance currentJid]];
}

- (void)roster:(Roster *)sender addedUser:(User *)jid {
	[self updateRosterEntriesForJid:[HipChatOSXApp.instance currentJid]];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark RoomDelegate methods
///////////////////////////////////////////////////////////////////////

- (void)room:(Room *)sender occupantAdded:(XMPPJID *)jid {
	[self updateRosterEntriesForJid:currentRoom.jid];
}

- (void)room:(Room *)sender occupantRemoved:(XMPPJID *)jid {
	[self updateRosterEntriesForJid:currentRoom.jid];
}

- (void)roomRosterLoaded:(Room *)sender {
	[self updateRosterEntriesForJid:currentRoom.jid];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark hipchatAppDelegate methods
///////////////////////////////////////////////////////////////////////

- (void)hipChatAppWillManuallyDisconnect {
    [self.arrayController removeObjects:self.entries];
    [self.entries removeAllObjects];
    [self.tableView reloadData];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark UI Handlers
///////////////////////////////////////////////////////////////////////

- (IBAction)onAddUsersClick:(id)sender {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://%@/admin/user", app.webHost]]];
}

- (IBAction)onGuestAccessToggle:(id)sender {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    XMPPJID *currentJid = app.currentJid;
    if (![OSXHelpers isRoomJid:currentJid]) {
        DDLogError(@"Toggled guest access when current jid was not a room. Ignoring.");
        return;
    }

    Room *room = [app getRoomInfo:currentJid];
    if (!room) {
        DDLogError(@"Couldn't load room info for %@ when toggling guest access", currentJid);
        return;
    }

    HipChatUser *currentUser = app.currentUser;
    NSInteger oppositeState = (self.guestAccessToggle.state == NSOnState ? NSOffState : NSOnState);
    if (![room isAdmin:currentUser.jid] && !currentUser.isAdmin) {
        [app showFlash:@"Only room owners or group admins can enable guest access."];
        [self.guestAccessToggle setState:oppositeState];
        return;
    }
    if (![currentUser getPerm:PERM_GUEST_ACCESS]) {
        [app showFlash:@"The guest access feature has been disabled by a group administrator."];
        [self.guestAccessToggle setState:oppositeState];
        return;
    }

    if (self.guestAccessToggle.state == NSOnState) {
        IQCallback callback = ^(XMPPIQ *iq) {
            if (![iq.type isEqualToString:@"error"]) {
                [app showFlash:@"Guest access enabled"];
            } else if (![[iq elementForName:@"error"] elementForName:@"text" xmlns:@"urn:ietf:params:xml:ns:xmpp-stanzas"]) {
                // Only show this error if there was no other text error provided with the response
                // Server-provided text errors are handled generically
                [app showFlash:@"There was an error enabling guess access."];
            }
        };
        [room setGuestAccess:YES callback:callback];
    } else {
        [self showAlertWithTitle:@"Are you sure you want to disable guest access?"
                            text:@"The room URL will be disabled and current guests will be kicked from the room"
                     contextInfo:nil];
    }
}

- (IBAction)onGuestAccessURLClick:(id)sender {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    XMPPJID *currentJid = app.currentJid;
    if (![OSXHelpers isRoomJid:currentJid]) {
        DDLogError(@"Clicked on guest access URL when not in a room? This is odd...");
        return;
    }

    Room *room = [app getRoomInfo:currentJid];
    if (!room) {
        DDLogError(@"Couldn't load room info for %@ when clicking guest access URL", currentJid);
        return;
    }

    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:room.guestUrl]];
}

- (IBAction)onRosterDoubleClick {
    NSInteger clickedRow = [self.tableView clickedRow];
    if (clickedRow > [self.arrayController.arrangedObjects count]) {
        return;
    }

    RosterEntry *entry = [self.arrayController.arrangedObjects objectAtIndex:[self.tableView clickedRow]];
    HipChatOSXApp *app = HipChatOSXApp.instance;

    BOOL canPrivateChat = [app.currentUser getPerm:PERM_PRIVATE_CHAT];
    if (!canPrivateChat) {
        [app showFlash:@"One-to-one chats have been disabled by a group administrator."];
        return;
    }

    [app joinChat:entry.jid withFocus:YES];
}

- (IBAction)onSearchRoster:(id)sender {
    // Always select the first result when searching
    [self selectFirstUser];
}

- (void)onShowSearch:(NSNotification *)notif {
    if (self.searchBox.isHidden) {
        return;
    }

    [self.view.window makeFirstResponder:self.searchField];
}

- (IBAction)onSortByName:(id)sender {
    [self setSort:SORT_BY_NAME];
    [[NSUserDefaults standardUserDefaults] setObject:SORT_BY_NAME forKey:@"rosterSort"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.arrayController rearrangeObjects];
}

- (IBAction)onSortByStatus:(id)sender {
    [self setSort:SORT_BY_STATUS];
    [[NSUserDefaults standardUserDefaults] setObject:SORT_BY_STATUS forKey:@"rosterSort"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.arrayController rearrangeObjects];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Helper methods
///////////////////////////////////////////////////////////////////////

// Use member variable here, not the property
// This function gets called in the property setter
- (void)addObservers {
    RosterEntry *re;
    for (re in entries) {
        [re addObserver:self forKeyPath:@"intShow" options:NSKeyValueObservingOptionNew context:nil];
    }
}

- (NSMutableArray *)generateRosterEntriesWithOccupants:(NSDictionary *)occupants {
    if (!occupants) {
        DDLogError(@"Failed to get occupant list for roster update");
        return nil;
    }

    NSMutableArray *updatedEntries = [NSMutableArray array];
    NSString *key;
    @synchronized(occupants) {
        for (key in occupants) {
            RoomOccupant *occupant = [occupants objectForKey:key];
            RosterEntry *entry = [[RosterEntry alloc] initWithOccupant:occupant];
            [updatedEntries addObject:entry];
        }
    }
    return updatedEntries;
}

- (void)rearrangeContent {
    if (self.searchField.stringValue.length > 0) {
        // Don't rearrange content when we're searching
        return;
    }
    NSInteger currentSelection = self.tableView.selectedRow;
    [self.arrayController rearrangeObjects];
    if (currentSelection >= 0) {
        NSInteger indexToSelect = MIN([self.arrayController.arrangedObjects count]-1, currentSelection);
        [self.tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:indexToSelect] byExtendingSelection:NO];
    }
}

// Use member variable here, not the property
// This function gets called in the property setter
- (void)removeObservers {
    if (!entries) {
        return;
    }
    RosterEntry *re;
    for (re in entries) {
        [re removeObserver:self forKeyPath:@"intShow"];
    }
}

- (void)selectFirstUser {
    if (self.tableView.numberOfRows > 0) {
        [self.tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:0] byExtendingSelection:NO];
    }
}

- (void)setSort:(NSString *)sortOption {
    NSSortDescriptor *showSorter = nil;
    showSorter = [[NSSortDescriptor alloc] initWithKey:@"intShow"
                                             ascending:NO
                                              selector:nil];
    NSSortDescriptor *nameSorter = nil;
    nameSorter = [[NSSortDescriptor alloc] initWithKey:@"name"
                                             ascending:YES
                                              selector:@selector(localizedCaseInsensitiveCompare:)];

    if ([sortOption isEqualToString:SORT_BY_NAME]) {
        self.sortDescriptors = [NSArray arrayWithObjects:nameSorter, nil];
    } else if ([sortOption isEqualToString:SORT_BY_STATUS]) {
        self.sortDescriptors = [NSArray arrayWithObjects:showSorter, nameSorter, nil];
    } else {
        // Default to sort by status
        self.sortDescriptors = [NSArray arrayWithObjects:showSorter, nameSorter, nil];
    }

    self.arrayController.sortDescriptors = self.sortDescriptors;
}

- (void)showAlertWithTitle:(NSString *)title text:(NSString *)text contextInfo:(NSDictionary *)contextInfo {
    NSAlert *alert = [[NSAlert alloc] init];
    [alert addButtonWithTitle:@"OK"];
    [alert addButtonWithTitle:@"Cancel"];
    [alert setMessageText:title];
    [alert setInformativeText:text];
    [alert setAlertStyle:NSInformationalAlertStyle];
    [alert beginSheetModalForWindow:[self.view window]
                      modalDelegate:self
                     didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:)
                        contextInfo:(__bridge void *)contextInfo];
}

- (void)updateRosterEntriesForJid:(XMPPJID *)jid {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    if (!isPresenceLoaded) {
        return;
    }

    if (!jid) {
        jid = app.lobbyJid;
    }

    // If we're currently displaying a 1:1 chat, there is no roster
    if ([OSXHelpers isUserJid:jid]) {
        self.entries = [NSMutableArray array];
        return;
    }

    DDLogInfo(@"Updating roster entries for jid %@", jid);

    NSMutableArray *updatedEntries;
    if (jid == app.lobbyJid) {
        updatedEntries = [app getRosterEntriesForBareJids:[app getAllUsers].allKeys];
        if (currentRoom) {
            [currentRoom removeDelegate:self];
            [currentRoom removeObserver:self forKeyPath:@"guestUrl"];
            currentRoom = nil;
        }
    } else {
        Room *room = [app getRoomInfo:jid];
        if (currentRoom) {
            [currentRoom removeDelegate:self];
            [currentRoom removeObserver:self forKeyPath:@"guestUrl"];
        }
        currentRoom = room;
        [currentRoom addDelegate:self];
        [currentRoom addObserver:self forKeyPath:@"guestUrl" options:NSKeyValueObservingOptionNew context:nil];
        updatedEntries = [self generateRosterEntriesWithOccupants:currentRoom.occupants];
    }

    if (!updatedEntries) {
        DDLogError(@"Failed to generate list of RosterEntries for updated roster. JID: %@", jid);
        return;
    }

    self.entries = updatedEntries;
}

- (void)updateWithGuestURL:(NSString *)guestUrl {
    if (guestUrl && guestUrl.length > 0) {
        if (guestUrl.length <= 7) {
            DDLogError(@"Cannot update guest URL with such a short string: %@", guestUrl);
            return;
        }

        [self.guestAccessToggle setState:NSOnState];
        self.guestAccessUrl.attributedTitle = [OSXHelpers linkStyleWithText:[guestUrl substringFromIndex:7]
                                                                       font:[NSFont fontWithName:@"Helvetica-Bold" size:11.0]
                                                                  alignment:NSCenterTextAlignment
                                                                        url:[NSURL URLWithString:guestUrl]];

        [self.guestAccessURLBox setHidden:NO];
        [self.guestAccessToggle setTitle:@"Guest access is on"];
    } else {
        [self.guestAccessToggle setState:NSOffState];
        [self.guestAccessURLBox setHidden:YES];
        [self.guestAccessToggle setTitle:@"Guest access is off"];
    }

    CGFloat tBoxHeight = self.guestAccessToggleBox.frame.size.height;
    CGFloat uBoxHeight = (self.guestAccessURLBox.isHidden ? 0.0 : self.guestAccessURLBox.frame.size.height);
    NSRect viewFrame = self.view.frame;
    [self.scrollView setFrame:NSMakeRect(0,
                                         tBoxHeight + uBoxHeight,
                                         viewFrame.size.width,
                                         viewFrame.size.height - tBoxHeight - uBoxHeight)];
    [self.guestAccessToggleBox setHidden:NO];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Notification handlers
///////////////////////////////////////////////////////////////////////

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"intShow"]) {
        if (rearrangeTimer) {
            [rearrangeTimer invalidate];
        }

        // Only trigger resorting once every 250ms
        // This avoids getting blasted with presences (esp. on startup)
        // and resorts causing the main thread to mini-freeze
        rearrangeTimer = [NSTimer scheduledTimerWithTimeInterval:0.25
                                                          target:self
                                                        selector:@selector(rearrangeContent)
                                                        userInfo:nil
                                                         repeats:NO];
    } else if ([keyPath isEqualToString:@"guestUrl"]) {
        [self updateWithGuestURL:[object guestUrl]];
    }
}

- (void)onFocusChat:(NSNotification *)notif {
    [self.sortBox setHidden:YES];
    [self.searchBox setHidden:YES];
        [self.searchField setStringValue:@""];
    [self.arrayController setFilterPredicate:nil];

    XMPPJID *jid = [[notif userInfo] objectForKey:@"jid"];
    if ([OSXHelpers isUserJid:jid]) {
        return;
    }

    [self updateRosterEntriesForJid:jid];

    // Set the toggle to the correct state
    Room *room = [HipChatOSXApp.instance getRoomInfo:jid];

    [self updateWithGuestURL:room.guestUrl];

    // Disable the checkbox if the user doesn't have admin privileges
    HipChatUser *currentUser = HipChatOSXApp.instance.currentUser;
    if (![room isAdmin:currentUser.jid] && !currentUser.isAdmin) {
        [self.guestAccessToggle setEnabled:NO];
        self.guestAccessToggleLabel.textColor = [NSColor grayColor];
    } else {
        [self.guestAccessToggle setEnabled:YES];
        self.guestAccessToggleLabel.textColor = [NSColor blackColor];
    }

}

- (void)onFocusLobby:(NSNotification *)notif {
    [self updateRosterEntriesForJid:[HipChatOSXApp.instance lobbyJid]];

    [self.guestAccessToggleBox setHidden:YES];
    [self.guestAccessURLBox setHidden:YES];
    [self.sortBox setHidden:NO];
    [self.searchBox setHidden:NO];
    NSRect viewFrame = self.view.frame;
    CGFloat sortBoxHeight = NSHeight(self.sortBox.frame);
    CGFloat searchBoxHeight = NSHeight(self.searchBox.frame);
    [self.scrollView setFrame:NSMakeRect(0,
                                         sortBoxHeight,
                                         viewFrame.size.width,
                                         viewFrame.size.height - sortBoxHeight - searchBoxHeight)];

    [self.searchField setStringValue:@""];
    [self.arrayController setFilterPredicate:nil];
    [self selectFirstUser];
    [self.view.window makeFirstResponder:self.searchField];
}

@end
