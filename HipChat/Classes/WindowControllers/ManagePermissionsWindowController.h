//
//  PrivateChatSelectUsersWindowController.h
//  HipChat
//
//  Created by Chris Rivers on 10/28/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "UserSelectionWindowController.h"

@interface ManagePermissionsWindowController : UserSelectionWindowController

@property IBOutlet NSButton *invitesCheckbox;

@property (readwrite) NSArray *perms;

@end
