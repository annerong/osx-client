//
//  IdleTimer.m
//  HipChat
//
//  Created by Christopher Rivers on 12/10/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "IdleTimer.h"

#include <IOKit/IOKitLib.h>

NSString *const kIdleTimerActiveNotification = @"idleTimerActiveNotification";
NSString *const kIdleTimerInactiveNotification = @"idleTimerInactiveNotification";

@implementation IdleTimer

- (id)initWithIdleSeconds:(NSTimeInterval)idleSeconds {
    self = [super init];
    if (self) {
        self.isIdle = NO;
        self.secondsBeforeIdle = idleSeconds;
        lastEventTime = [[NSDate date] timeIntervalSince1970];
        [NSTimer scheduledTimerWithTimeInterval:1.0
                                         target:self
                                       selector:@selector(checkIdleTime:)
                                       userInfo:nil
                                        repeats:YES];
    }

    [NSEvent addGlobalMonitorForEventsMatchingMask:(NSKeyDownMask | NSMouseMovedMask) handler:^(NSEvent *event) {
        lastEventTime = [[NSDate date] timeIntervalSince1970];
    }];
    [NSEvent addLocalMonitorForEventsMatchingMask:(NSKeyDownMask | NSMouseMovedMask) handler:^NSEvent *(NSEvent *event) {
        lastEventTime = [[NSDate date] timeIntervalSince1970];
        return event;
    }];
    return self;
}

- (void)checkIdleTime:(NSTimer *)timer {
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    NSInteger diff = (currentTime - lastEventTime);
    if (!self.isIdle && diff > self.secondsBeforeIdle) {
        // Apparently some apps prevent us from getting the global keydown event, so
        // double check idleness by using the old-style using IOService calls
        // If the difference between the two is > 10, reset the "lastEventTime" using the new value
        NSTimeInterval systemIdleSeconds = [self getSystemIdle];
        if ((diff - systemIdleSeconds) > 10) {
            lastEventTime = ([[NSDate date] timeIntervalSince1970] - systemIdleSeconds);
            return;
        }

        self.isIdle = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:kIdleTimerInactiveNotification object:self];
    } else if (self.isIdle && diff < self.secondsBeforeIdle) {
        self.isIdle = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:kIdleTimerActiveNotification object:self];
    }
}

- (NSTimeInterval)getIdleSeconds {
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    return (currentTime - lastEventTime);
}

- (int64_t)getSystemIdle {
    int64_t idlesecs = -1;
    io_iterator_t iter = 0;
    if (IOServiceGetMatchingServices(kIOMasterPortDefault, IOServiceMatching("IOHIDSystem"), &iter) == KERN_SUCCESS) {
        io_registry_entry_t entry = IOIteratorNext(iter);
        if (entry) {
            CFMutableDictionaryRef dict = NULL;
            if (IORegistryEntryCreateCFProperties(entry, &dict, kCFAllocatorDefault, 0) == KERN_SUCCESS) {
                CFNumberRef obj = CFDictionaryGetValue(dict, CFSTR("HIDIdleTime"));
                if (obj) {
                    int64_t nanoseconds = 0;
                    if (CFNumberGetValue(obj, kCFNumberSInt64Type, &nanoseconds)) {
                        idlesecs = (nanoseconds >> 30); // Divide by 10^9 to convert from nanoseconds to seconds.
                    }
                }
                CFRelease(dict);
            }
            IOObjectRelease(entry);
        }
        IOObjectRelease(iter);
    }
    return idlesecs;
}

@end
