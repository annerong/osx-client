//
//  FocusOnlyBox.m
//  HipChat
//
//  Created by Christopher Rivers on 2/13/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "FocusOnlyBox.h"
#import "NSView+SubviewIteration.h"

@implementation FocusOnlyBox

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Static functions
///////////////////////////////////////////////////////////////////////

+ (BOOL)isResponder:(NSView *)view {
    return ([view acceptsFirstResponder] ||
            [view isKindOfClass:[NSTextField class]] ||
            [view isKindOfClass:[NSTextView class]]);
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Member functions
///////////////////////////////////////////////////////////////////////

- (void)addSubview:(NSView *)aView {
    [super addSubview:aView];
    if ([FocusOnlyBox isResponder:aView]) {
        NSMutableArray *newFirstResponders = [NSMutableArray arrayWithObject:aView];
        [newFirstResponders addObjectsFromArray:firstResponders];
        firstResponders = [NSArray arrayWithArray:newFirstResponders];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];

    NSMutableArray *responders = [NSMutableArray array];
    for (NSView *sv in self.allSubviews) {
        if ([FocusOnlyBox isResponder:sv]) {
            [responders addObject:sv];
        }
    }
    firstResponders = [NSArray arrayWithArray:responders];

    if (![self isHidden]) {
        [self startFocusTimer];
    }
}

- (void)handleFocusTimer {
    id firstResponder = [[NSApp keyWindow] firstResponder];
    
    // For NSTextView, the delegate of the responder is the actual view
    // see: https://developer.apple.com/library/mac/#documentation/cocoa/Conceptual/EventOverview/EventHandlingBasics/EventHandlingBasics.html
    // under "Determining First-Responder Status"
    if ([firstResponder isKindOfClass:[NSTextView class]]) {
        firstResponder = [(NSTextView *)firstResponder delegate];
    }

    for (NSView *v in firstResponders) {
        if (firstResponder == v) {
            return;
        }
    }

    [self setHidden:YES];
    if ([self.delegate respondsToSelector:@selector(focusBoxHidden:)]) {
        [self.delegate performSelector:@selector(focusBoxHidden:) withObject:self];
    }
}

- (void)setHidden:(BOOL)flag {
    [super setHidden:flag];

    if (flag == NO) {
        [self startFocusTimer];
    } else {
        [focusTimer invalidate];
        focusTimer = nil;
    }
}

- (void)startFocusTimer {
    [focusTimer invalidate];
    focusTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                  target:self
                                                selector:@selector(handleFocusTimer)
                                                userInfo:nil
                                                 repeats:YES];
}

@end
