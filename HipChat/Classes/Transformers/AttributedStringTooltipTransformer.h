//
//  AttributedStringTooltipTransformer.h
//  HipChat
//
//  Created by Christopher Rivers on 4/11/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AttributedStringTooltipTransformer : NSValueTransformer

@end
