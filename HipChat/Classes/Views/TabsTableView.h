//
//  TabsTableView.h
//  HipChat
//
//  Created by Christopher Rivers on 2/8/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface TabsTableView : NSTableView

@end
