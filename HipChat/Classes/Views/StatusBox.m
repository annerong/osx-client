//
//  StatusBox.m
//  HipChat
//
//  Created by Christopher Rivers on 11/16/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "StatusBox.h"

#import "HipChatOSXApp.h"

@implementation StatusBox

NSImage *dropdownArrow;
NSImage *dropdownArrowHover;

static const NSInteger menuTagAvailable = 1;
static const NSInteger menuTagAway = 2;
static const NSInteger menuTagDND = 3;

static const NSInteger menuTagSettings = 10;
static const NSInteger menuTagSignOut = 11;

- (void)awakeFromNib {
    // Adding NSTrackingInVisibleRect to option makes the tracking area auto-update
    trackingArea = [[NSTrackingArea alloc] initWithRect:self.menuButton.frame
                                                    options:(NSTrackingMouseEnteredAndExited | NSTrackingActiveInKeyWindow | NSTrackingInVisibleRect)
                                                      owner:self
                                                   userInfo:nil];
    [self addTrackingArea:trackingArea];

    dropdownArrow = [NSImage imageNamed:@"settings_arrow"];
    dropdownArrowHover = [NSImage imageNamed:@"settings_arrow_hover"];
}

- (void)drawRect:(NSRect)dirtyRect {
    if (isHovered) {
        self.statusMessage.textColor = [NSColor whiteColor];
    } else {
        self.statusMessage.textColor = RGB(177, 179, 182);
    }
    
    [super drawRect:dirtyRect];
}

- (void)focusStatusInput {
    [self.statusMessageInput.window makeFirstResponder:self.statusMessageInput];
}

- (id)initWithFrame:(NSRect)frameRect {
    self = [super initWithFrame:frameRect];
	if (self == nil) return nil;

    return self;
}

- (void)mouseEntered:(NSEvent *)theEvent {
    if (theEvent.trackingArea == trackingArea) {
        isHovered = YES;
        self.statusMessage.textColor = [NSColor whiteColor];
        [self.dropdownArrow setImage:dropdownArrowHover];
    }
}

- (void)mouseExited:(NSEvent *)theEvent {
    if (theEvent.trackingArea == trackingArea) {
        isHovered = NO;
        self.statusMessage.textColor = RGB(177, 179, 182);
        [self.dropdownArrow setImage:dropdownArrow];
    }
}

- (void)showStatusMessageMenuWithShow:(NSString *)show {
    NSString *iconName = @"icon_available";
    if ([show isEqualToString:@"dnd"]) {
        iconName = @"icon_dnd";
    } else if ([show isEqualToString:@"xa"] || [show isEqualToString:@"away"]) {
        iconName = @"icon_away";
    }
    [self.statusMessageIcon setImage:[NSImage imageNamed:iconName]];
    [self.menuButton setMenu:self.statusMessageMenu];
    [self.menuButton performClick:self];
}

- (void)unhighlight {
    isHovered = NO;
    [self setNeedsDisplay:YES];
    
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark NSMenu Delegate methods
///////////////////////////////////////////////////////////////////////

- (void)menuWillOpen:(NSMenu *)menu {
    if (menu == self.statusMessageMenu) {
        // Need to delay this focus so that the menu is open when it happens
        [self performSelector:@selector(focusStatusInput) withObject:nil afterDelay:0.0 inModes:[NSArray arrayWithObject:NSRunLoopCommonModes]];
    }
}

- (void)menuDidClose:(NSMenu *)menu {
    if (menu == self.statusMessageMenu) {
        [self.menuButton setMenu:self.settingsMenu];
        [self.statusMessageInput setStringValue:@""];
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Received actions
///////////////////////////////////////////////////////////////////////

- (IBAction)handleMenuClick:(id)sender {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    
    NSMenuItem *menuItem = (NSMenuItem *)sender;
    switch ([menuItem tag]) {
        case menuTagAvailable:
            [self performSelector:@selector(showStatusMessageMenuWithShow:) withObject:nil afterDelay:0];
            [app updatePresenceWithShow:nil status:nil];
            break;
        case menuTagAway:
            [self performSelector:@selector(showStatusMessageMenuWithShow:) withObject:@"xa" afterDelay:0];
            [app updatePresenceWithShow:@"xa" status:nil];
            break;
        case menuTagDND:
            [self performSelector:@selector(showStatusMessageMenuWithShow:) withObject:@"dnd" afterDelay:0];
            [app updatePresenceWithShow:@"dnd" status:nil];
            break;
        case menuTagSettings:
            [app showSettingsWindow];
            break;
        case menuTagSignOut:
            [app signOutUserAndClearCredentials];
            break;
        default:
            break;
    }
    [self unhighlight];
}

- (IBAction)handleStatusMessageSet:(id)sender {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    [app updatePresenceWithStatus:self.statusMessageInput.stringValue];
    [self.statusMessageMenu cancelTracking];
}


@end
