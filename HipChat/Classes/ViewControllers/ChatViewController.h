//
//  ChatViewController.h
//  HipChat
//
//  Created by Christopher Rivers on 10/18/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Chat.h"

@class AutoCompleteTextView;
@class Chat;
@class MentionItem;
@class PrettyBorderBox;
@class WebView;
@class XMPPJID;
@class XMPPMessage;

@protocol HipChatTextViewDelegate;

@interface ChatViewController : NSViewController <NSTextViewDelegate, NSSplitViewDelegate, ChatDelegate, NSURLDownloadDelegate> {
    NSTrackingArea *cancelUploadTrackingArea;
    Chat *chat;
    NSTimer *chatClickTimer;
    NSString *currentMessage;
    NSInteger currentMessageIndex;
    NSMutableArray *executedJavascripts;
    NSPopover *imagePopover;
    NSMutableArray *javascripts;
    NSString *outgoingChatState;
    NSMutableArray *previousMessages;
    NSString *requiredExtension;
    NSImage *uploadImage;
    NSFileWrapper *uploadFile;
    NSScrollView *webScrollView;

    BOOL fullHistoryLoaded;
    BOOL initialized;
    BOOL isChatDividerShown;
    BOOL isScrollAtBottom;
    BOOL viewLoaded;
    BOOL webViewLoaded;
}

- (id)initWithJid:(XMPPJID *)initJid;
- (id)initWithJid:(XMPPJID *)initJid initialMessage:(XMPPMessage *)initialMessage;
- (void)deinitialize;

- (void)appendEmoticonToMessage:(NSString *)shortcut;
- (void)focusMessageInput;
- (void)focusMessageInput:(BOOL)forceFocus;

- (IBAction)onAttachmentClick:(id)sender;
- (IBAction)onCancelUploadClick:(id)sender;
- (IBAction)onEmoticonButtonClick:(id)sender;
- (IBAction)onFileNameClick:(id)sender;
- (IBAction)onSendClick:(id)sender;

@property IBOutlet NSBox *bottomBox;
@property IBOutlet NSButton *emoticonButton;
@property IBOutlet NSTextField *fileNameInput;
@property IBOutlet NSButton *fileNameButton;
@property IBOutlet AutoCompleteTextView *messageInput;
@property IBOutlet NSButton *sendButton;
@property IBOutlet NSSplitView *splitView;
@property IBOutlet NSBox *textWrapper;
@property IBOutlet PrettyBorderBox *topBox;
@property IBOutlet NSBox *uploadView;
@property IBOutlet NSBox *uploadProgressView;
@property IBOutlet NSProgressIndicator *uploadSpinner;
@property IBOutlet WebView *webView;

@property (readonly) MentionItem *atAllMention;
@property (readonly) MentionItem *atHereMention;
@property (readwrite, copy) XMPPJID *jid;
@property (nonatomic, assign) NSURL *uploadFileIconUrl;
@property (nonatomic, assign) NSString *uploadFileNameInput;
@property (nonatomic, assign) NSString *uploadFileNameText;

@end
