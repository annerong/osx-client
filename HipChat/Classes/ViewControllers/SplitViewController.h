//
//  SplitViewController.h
//  HipChat
//
//  Created by Christopher Rivers on 10/17/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class ChatStackViewController;
@class LobbyViewController;
@class RightSidebarViewController;
@class TabViewController;

@interface SplitViewController : NSViewController <NSSplitViewDelegate> {
    ChatStackViewController *chatStackVC;
    LobbyViewController *lobbyVC;
    RightSidebarViewController *sidebarVC;
    TabViewController *tabVC;

    CGFloat leftSidebarWidth;
    CGFloat rightSidebarWidth;
}

@end
