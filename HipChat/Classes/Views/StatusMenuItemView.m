//
//  StatusMenuItemView.m
//  HipChat
//
//  Created by Christopher Rivers on 11/16/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "StatusMenuItemView.h"

@implementation StatusMenuItemView

- (void)drawRect:(NSRect) rect {
    BOOL isHighlighted = [[self enclosingMenuItem] isHighlighted];
    if (isHighlighted) {
        [RGB(224.0, 224.0, 224.0) set];
        [NSBezierPath fillRect:rect];
    } else {
        [super drawRect:rect];
    }
}

- (BOOL)acceptsFirstMouse:(NSEvent *)theEvent {
    return YES;
}

- (void)mouseUp:(NSEvent*) event {
    NSMenu *menu = self.enclosingMenuItem.menu;
    [menu cancelTracking];
    [menu performActionForItemAtIndex:[menu indexOfItem:self.enclosingMenuItem]];

    NSMenuItem *menuItem = [self enclosingMenuItem];
    NSInteger i = [menu indexOfItem:menuItem];
    [menu removeItemAtIndex:i];
    [menu insertItem:menuItem atIndex:i];
}

@end
