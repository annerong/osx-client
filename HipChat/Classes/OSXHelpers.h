//
//  OSXHelpers.h
//  HipChat
//
//  Created by Christopher Rivers on 10/15/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helpers.h"

@interface OSXHelpers : Helpers

+ (NSData *)getPNGDataFromImage:(NSImage *)image;
+ (BOOL)isImageURL:(NSString *)imageUrl;
+ (NSMutableAttributedString *)linkStyleWithText:(NSString *)text;
+ (NSMutableAttributedString *)linkStyleWithText:(NSString *)text font:(NSFont *)font;
+ (NSMutableAttributedString *)linkStyleWithText:(NSString *)text font:(NSFont *)font alignment:(NSTextAlignment)align;
+ (NSMutableAttributedString *)linkStyleWithText:(NSString *)text
                                            font:(NSFont *)font
                                       alignment:(NSTextAlignment)align
                                             url:(NSURL *)url;
+ (BOOL)roomNameExists:(NSString *)roomName;
+ (BOOL)roomNameHasInvalidCharacters:(NSString *)roomName;
+ (void)saveFileToDisk:(NSURL *)fileUrl withName:(NSString *)name window:(NSWindow *)window;
+ (void)setCommandWClosesTabs:(BOOL)closesTabs;
+ (void)setTextColor:(NSColor *)txtColor forButton:(NSButton *)button;
+ (BOOL)validateChatName:(NSString *)chatName;

@end
