//
//  HipChatOSXConstants.m
//  HipChat
//
//  Created by Christopher Rivers on 11/21/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "HipChatOSXConstants.h"

NSString *const HipChatFocusAppNotification = @"focusApp";
NSString *const HipChatFocusChatAtIndexNotification = @"focusChatIndex";
NSString *const HipChatShowChangePrivacyNotification = @"showChangePrivacy";
NSString *const HipChatShowChangeTopicNotification = @"showChangeTopic";
NSString *const HipChatShowCreateRoomNotification = @"showCreateRoom";
NSString *const HipChatShowJoinChatNotification = @"showJoinChat";
NSString *const HipChatShowLoginWindowNotification = @"showLoginWindow";
NSString *const HipChatShowMainWindowNotification = @"showMainWindow";
NSString *const HipChatShowManagePermsNotification = @"showManagePerms";
NSString *const HipChatShowRenameRoomNotification = @"showRenameRoom";
NSString *const HipChatShowRoomInviteNotification = @"showRoomInvite";
NSString *const HipChatShowRoomRemoveMembersNotification = @"showRoomRemoveMembers";
NSString *const HipChatShowSearchNotification = @"showSearch";
NSString *const HipChatShowSettingsWindowNotification = @"showSettingsWindow";
NSString *const HipChatShowSidebarFilesNotification = @"showSidebarFiles";
NSString *const HipChatShowSidebarLinksNotification = @"showSidebarLinks";
NSString *const HipChatShowSidebarPrimaryNotification = @"showSidebarPrimary";
NSString *const HipChatToggleLeftSidebarNotification = @"toggleLeftSidebar";
NSString *const HipChatToggleRightSidebarNotification = @"toggleRightSidebar";

NSInteger const NORMAL_FONT_SIZE = 12; // Default font size before any increases or decreases made by the user
