//
//  OSXHelpers.m
//  HipChat
//
//  Created by Christopher Rivers on 10/15/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "HipChatOSXApp.h"
#import "OSXHelpers.h"
#import "RegexKitLite.h"
#import "RoomList.h"

@implementation OSXHelpers

static NSFont *DEFAULT_LINK_FONT;

+ (void)initialize {
    DEFAULT_LINK_FONT = [NSFont fontWithName:@"Lucida Grande" size:11.0];
    [super initialize];
}

+ (NSData *)getPNGDataFromImage:(NSImage *)image {
    NSData *imageData = [image TIFFRepresentation];
    NSBitmapImageRep *imageRep = [NSBitmapImageRep imageRepWithData:imageData];
    NSDictionary *imageProps = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:1.0] forKey:NSImageCompressionFactor];
    imageData = [imageRep representationUsingType:NSPNGFileType properties:imageProps];
    return imageData;
}

+ (BOOL)isImageURL:(NSString *)imageUrl {
    return ([imageUrl rangeOfRegex:@"(?i:png|gif|jpg|jpeg|tiff|bmp)$"].location != NSNotFound);
}

+ (NSMutableAttributedString *)linkStyleWithText:(NSString *)text {
    return [OSXHelpers linkStyleWithText:text font:nil];
}

+ (NSMutableAttributedString *)linkStyleWithText:(NSString *)text font:(NSFont *)font {
    return [OSXHelpers linkStyleWithText:text font:font alignment:NSLeftTextAlignment];
}

+ (NSMutableAttributedString *)linkStyleWithText:(NSString *)text font:(NSFont *)font alignment:(NSTextAlignment)align {
    return [OSXHelpers linkStyleWithText:text font:font alignment:align url:nil];
}

+ (NSMutableAttributedString *)linkStyleWithText:(NSString *)text font:(NSFont *)font alignment:(NSTextAlignment)align url:(NSURL *)url {
    if (!text) {
        return nil;
    }

    if (!font) {
        font = DEFAULT_LINK_FONT;
    }

    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setLineBreakMode:NSLineBreakByTruncatingTail];
    [style setAlignment:align];
    NSMutableDictionary *attrs =
        [NSMutableDictionary dictionaryWithObjectsAndKeys:
         style, NSParagraphStyleAttributeName,
         font, NSFontAttributeName,
         RGB(25.0, 95.0, 180.0), NSForegroundColorAttributeName,
//         [NSNumber numberWithInteger:NSUnderlineStyleSingle], NSUnderlineStyleAttributeName,
         nil];

    if (url) {
        [attrs setObject:url forKey:NSLinkAttributeName];
    }
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attrs];
}

+ (BOOL)roomNameExists:(NSString *)roomName {
    Room *room = [HipChatOSXApp.instance.roomList roomForName:roomName];
    return (room != nil);
}

+ (BOOL)roomNameHasInvalidCharacters:(NSString *)roomName {
    return ([OSXHelpers sanitizeXML:roomName].length != roomName.length);
}

+ (void)saveFileToDisk:(NSURL *)fileUrl withName:(NSString *)name window:(NSWindow *)window {
    NSSavePanel* panel = [NSSavePanel savePanel];
    if (name) {
        [panel setNameFieldStringValue:name];
    } else {
        [panel setNameFieldStringValue:[OSXHelpers getFileNameFromPath:fileUrl.path]];
    }
    [panel beginSheetModalForWindow:window completionHandler:^(NSInteger result){
        if (result == NSFileHandlingPanelOKButton) {
            NSURLRequest *request = [NSURLRequest requestWithURL:fileUrl];
            NSURLDownload  *download = [[NSURLDownload alloc] initWithRequest:request
                                                                     delegate:(id<NSURLDownloadDelegate>)window];
            if (download) {
                [download setDestination:panel.URL.path allowOverwrite:YES];
            } else {
                [HipChatOSXApp.instance showFlash:@"There was an error trying to save the file to disk."];
            }
        }
    }];
}

+ (void)setCommandWClosesTabs:(BOOL)closesTabs {
    if (closesTabs) {
        NSMenu *fileMenu = [[[NSApp mainMenu] itemWithTag:kFileMenuTag] submenu];
        NSMenuItem *closeTabItem = [fileMenu itemWithTag:kCloseTabTag];
        if (closeTabItem) {
            [closeTabItem setKeyEquivalent:@"w"];
        }
        NSMenuItem *closeWindowItem = [fileMenu itemWithTag:kCloseWindowTag];
        if (closeWindowItem) {
            [closeWindowItem setKeyEquivalent:@"W"];
        }
    } else {
        NSMenuItem *closeTabItem = [[[[NSApp mainMenu] itemWithTag:kFileMenuTag] submenu] itemWithTag:kCloseTabTag];
        if (closeTabItem) {
            [closeTabItem setKeyEquivalent:@""];
        }
        NSMenuItem *closeWindowItem = [[[[NSApp mainMenu] itemWithTag:kFileMenuTag] submenu] itemWithTag:kCloseWindowTag];
        if (closeWindowItem) {
            [closeWindowItem setKeyEquivalent:@"w"];
            [closeWindowItem setKeyEquivalentModifierMask:NSCommandKeyMask];
        }
    }
}

+ (void)setTextColor:(NSColor *)txtColor forButton:(NSButton *)button {
    NSMutableAttributedString *colorTitle =
    [[NSMutableAttributedString alloc] initWithAttributedString:[button attributedTitle]];
    NSRange titleRange = NSMakeRange(0, [colorTitle length]);
    [colorTitle addAttribute:NSForegroundColorAttributeName
                       value:txtColor
                       range:titleRange];
    [button setAttributedTitle:colorTitle];
}

+ (BOOL)validateChatName:(NSString *)chatName {
    if ([OSXHelpers roomNameExists:chatName] ||
        [OSXHelpers roomNameHasInvalidCharacters:chatName]) {
        return NO;
    }
    return YES;
}

@end
