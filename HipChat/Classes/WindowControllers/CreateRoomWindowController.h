//
//  CreateRoomWindowController.h
//  HipChat
//
//  Created by Christopher Rivers on 1/4/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

static const NSInteger SHOW_PERMS_RETURN_CODE = 10;

@class ManagePermissionsWindowController;
@class XMPPJID;

@interface CreateRoomWindowController : NSWindowController {
    NSPopover *errorPopover;
    XMPPJID *jid;
    NSArray *membersToAdd;
    NSString *previousPrivacy;
    BOOL sendInvites;
}

- (IBAction)closeCreateRoom:(id)sender;
- (void)handleManagePermsClose:(ManagePermissionsWindowController *)mpWc returnCode:(NSInteger)returnCode;
- (IBAction)onSelectOpen:(id)sender;
- (IBAction)onSelectPrivate:(id)sender;

@property IBOutlet NSButton *cancelButton;
@property IBOutlet NSTextField *nameField;
@property IBOutlet NSButton *okButton;
@property IBOutlet NSTextField *topicField;
@property IBOutlet NSMatrix *radioOpen;
@property IBOutlet NSMatrix *radioPrivate;
@property IBOutlet NSTextField *permissionsText;

@property (readwrite) XMPPJID *jid;
@property (readwrite) NSArray *perms;

@end
