//
//  HipChatWebView.h
//  HipChat
//
//  Created by Christopher Rivers on 2/8/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <WebKit/WebKit.h>

static NSString *hcWebViewDropNotification = @"hipchatWebViewDrop";

@interface HipChatWebView : WebView

@end
