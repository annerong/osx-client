//
//  CursorButton.m
//  HipChat
//
//  Created by Christopher Rivers on 12/27/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "CursorButton.h"

#import "ImagePreviewViewController.h"
#import "OSXHelpers.h"
#import "RegexKitLite.h"

@implementation CursorButton

static ImagePreviewViewController *tooltipVC;
static NSPopover *tooltipPopover;
static NSString *currentImageUrl;

- (void)awakeFromNib {
    // Need to add tracking area for mouse enter / exit events (to show image tooltips)
    NSTrackingAreaOptions focusTrackingAreaOptions = NSTrackingActiveInActiveApp;
    focusTrackingAreaOptions |= NSTrackingMouseEnteredAndExited;
    focusTrackingAreaOptions |= NSTrackingAssumeInside;
    focusTrackingAreaOptions |= NSTrackingInVisibleRect;

    NSTrackingArea *focusTrackingArea = [[NSTrackingArea alloc] initWithRect:NSZeroRect
                                                                     options:focusTrackingAreaOptions
                                                                       owner:self
                                                                    userInfo:nil];
    [self addTrackingArea:focusTrackingArea];
}

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];

    // Stop tooltip timer when we redraw
    // Primarily, this will happen on scroll, which prevents us from
    // accidentally showing tooltips on button not actually being hoevered over
    // (since scrolling doesn't trigger mouseExited events until the mouse moves)
    if (tooltipTimer) {
        [tooltipTimer invalidate];
        tooltipTimer = nil;
    }
}

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addCursorRect:self.bounds cursor:[NSCursor pointingHandCursor]];
    }

    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)copyLink:(id)sender {
    // Handle link copy action
    NSURL *urlValue = [self.attributedTitle attribute:NSLinkAttributeName atIndex:0 effectiveRange:nil];
    if (urlValue) {
        NSPasteboard *pasteboard = [NSPasteboard generalPasteboard];
        [pasteboard clearContents];
        NSArray *objectsToCopy = [NSArray arrayWithObject:urlValue];
        [pasteboard writeObjects:objectsToCopy];
    }
}

- (void)downloadFile:(id)sender {
    NSURL *urlValue = [self.attributedTitle attribute:NSLinkAttributeName atIndex:0 effectiveRange:nil];
    if (urlValue) {
        NSString *name = [OSXHelpers getFileNameFromPath:urlValue.path];
        [OSXHelpers saveFileToDisk:urlValue withName:name window:self.window];
    }
}

- (ImagePreviewViewController *)getTooltipVC {
    // Only ever show one tooltip
    if (!tooltipVC) {
        tooltipVC = [[ImagePreviewViewController alloc] initWithNibName:@"ImagePreview" bundle:[NSBundle mainBundle]];
    }
    return tooltipVC;
}

- (NSPopover *)getTooltipPopover {
    // Only ever show one popover tooltip
    if (!tooltipPopover) {
        tooltipPopover = [[NSPopover alloc] init];
        tooltipPopover.behavior = NSPopoverBehaviorApplicationDefined;
    }
    return tooltipPopover;
}

- (void)loadImage:(NSURL *)imageUrl {
    // Load the contents of the tooltip image in the background,
    // so large images don't freeze the main thread
    NSImage *image = [[NSImage alloc] initWithContentsOfURL:imageUrl];
    dispatch_async(dispatch_get_main_queue(), ^{
        // Don't upodate the tooltip unless we're still looking at the same URL
        // (We may have moves the mouse away)
        if ([currentImageUrl isEqualToString:[imageUrl absoluteString]]) {
            ImagePreviewViewController *controller = [self getTooltipVC];
            controller.image = image;
            [controller.spinner stopAnimation:nil];
        }
    });
}

- (void)mouseEntered:(NSEvent *)theEvent {
    // Set a timer to show a tooltip
    if (tooltipTimer) {
        [tooltipTimer invalidate];
    }
    tooltipTimer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(showTooltip) userInfo:nil repeats:NO];
    [super mouseEntered:theEvent];
}

- (void)mouseExited:(NSEvent *)theEvent {
    if (tooltipTimer) {
        [tooltipTimer invalidate];
        tooltipTimer = nil;
    }
    if ([tooltipPopover isShown]) {
        [tooltipPopover close];
    }
    [super mouseExited:theEvent];
}

- (void)resetCursorRects {
    // Always add the hand cursor whenever we reset cursor rects
    [self addCursorRect:self.bounds cursor:[NSCursor pointingHandCursor]];
}

- (void)rightMouseDown:(NSEvent *)theEvent {
    // Enable a context menu to copy the link URL if we're display a link-style button
    NSURL *urlValue = [self.attributedTitle attribute:NSLinkAttributeName atIndex:0 effectiveRange:nil];
    if (urlValue) {
        NSMenu *contextMenu = [[NSMenu alloc] init];
        [contextMenu addItem:[[NSMenuItem alloc] initWithTitle:@"Copy Link" action:@selector(copyLink:) keyEquivalent:@""]];
        if ([urlValue.path rangeOfRegex:@"\\.\\w{1,4}"].location != NSNotFound) {
            [contextMenu addItem:[[NSMenuItem alloc] initWithTitle:@"Download file..." action:@selector(downloadFile:) keyEquivalent:@""]];
        }
        contextMenu.delegate = self;
        [NSMenu popUpContextMenu:contextMenu withEvent:theEvent forView:self];
    }
}

- (void)showTooltip {
    if (!self.attributedTitle || self.attributedTitle.length == 0) {
        return;
    }

    NSURL *urlValue = [self.attributedTitle attribute:NSLinkAttributeName atIndex:0 effectiveRange:nil];
    if (!urlValue) {
        return;
    }
    if (![OSXHelpers isImageURL:[urlValue absoluteString]]) {
        return;
    }

    NSPopover *popover = [self getTooltipPopover];
    ImagePreviewViewController *controller = [self getTooltipVC];
    controller.image = nil;
    popover.contentViewController = controller;

    currentImageUrl = [urlValue absoluteString];
    [self performSelectorInBackground:@selector(loadImage:) withObject:urlValue];
    [popover showRelativeToRect:self.bounds ofView:self preferredEdge:NSMinYEdge];
    [controller.spinner startAnimation:nil];
}

@end
