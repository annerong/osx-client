//
//  SettingsWindowController.m
//  HipChat
//
//  Created by Christopher Rivers on 11/21/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "SettingsWindowController.h"

#import "DevelopmentPrefsViewController.h"
#import "GeneralPrefsViewController.h"
#import "HipChatOSXApp.h"
#import "NotificationPrefsViewController.h"
#import "OSXHelpers.h"

@implementation SettingsWindowController

- (id)init {
    NSMutableArray *vcs = [NSMutableArray arrayWithObjects:
                           [[GeneralPrefsViewController alloc] initWithNibName:@"GeneralPrefs" bundle:[NSBundle mainBundle]],
                           [[NotificationPrefsViewController alloc] initWithNibName:@"NotificationPrefs" bundle:[NSBundle mainBundle]],
                           [[DevelopmentPrefsViewController alloc] initWithNibName:@"DevelopmentPrefs" bundle:[NSBundle mainBundle]],
                           nil];

    return [self initWithViewControllers:vcs];
}

- (void)windowDidBecomeKey:(NSNotification *)notification {
}

- (void)windowWillClose:(NSNotification *)notification {
    [HipChatOSXApp.instance savePreferences:YES];
}

@end
