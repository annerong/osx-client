//
//  DraggingView.h
//  HipChat
//
//  Created by Christopher Rivers on 11/19/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface DraggingView : NSView {
    NSPoint initialLocation;
}

@end
