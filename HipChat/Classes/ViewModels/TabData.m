//
//  TabData.m
//  HipChat
//
//  Created by Christopher Rivers on 10/19/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "TabData.h"

#import "Room.h"
#import "User.h"
#import "XMPPJID.h"

#import "HipChatOSXApp.h"
#import "OSXHelpers.h"

@interface TabData()

@property User *user;
@property Room *room;

@end

@implementation TabData

@synthesize color;
@synthesize hideClose;
@synthesize icon;
@synthesize jid;
@synthesize name;

- (TabData *)initWithJid:(XMPPJID *)initJid {
    self = [super init];
    if (self) {
        self.jid = initJid;
        HipChatOSXApp *app = HipChatOSXApp.instance;
        if (initJid == app.lobbyJid) {
            // Don't show close for the Lobby tab
            self.hideClose = YES;
            self.icon = @"icon_lobby";
            self.name = @"Lobby";
        } else if ([OSXHelpers isUserJid:initJid]) {
            self.user = [app getUserInfo:initJid];
            self.hideClose = NO;
            self.icon = [OSXHelpers getStatusIconNameForUser:self.user];
            self.name = self.user.name;
            
            [self.user addObserver:self forKeyPath:@"lastPresence" options:NSKeyValueObservingOptionNew context:nil];
        } else {
            self.room = [app getRoomInfo:initJid];
            self.hideClose = NO;
            self.icon = [OSXHelpers getIconNameForRoom:self.room lightVersion:YES];
            self.name = self.room.name;
            [self.room addObserver:self forKeyPath:@"name" options:NSKeyValueObservingOptionNew context:nil];
            [self.room addObserver:self forKeyPath:@"privacy" options:NSKeyValueObservingOptionNew context:nil];
        }
    }

    return self;
}

- (void)dealloc {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    if (self.jid == app.lobbyJid) {
        // Do nothing - we added no observers
    } else if ([OSXHelpers isUserJid:self.jid]) {
        [self.user removeObserver:self forKeyPath:@"lastPresence"];
    } else if ([OSXHelpers isRoomJid:self.jid]) {
        [self.room removeObserver:self forKeyPath:@"name"];
        [self.room removeObserver:self forKeyPath:@"privacy"];
    }
}

- (NSString *)description {
    return [NSString stringWithFormat:@"{jid: %@, name: %@, color: %@, hideClose: %d, icon: %@",
            jid, name, color, hideClose, icon];
}

- (BOOL)isEqual:(id)object {
    return [jid isEqualToJID:[object jid]];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([OSXHelpers isUserJid:self.jid]) {
        self.icon = [OSXHelpers getStatusIconNameForUser:self.user];
        self.name = self.user.name;
    } else {
        self.icon = [OSXHelpers getIconNameForRoom:self.room lightVersion:YES];
        self.name = self.room.name;
    }
}

@end
