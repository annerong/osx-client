//
//  AutoCompleteTextView.h
//  HipChat
//
//  Created by Christopher Rivers on 2/25/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "HipChatTextView.h"

@class AutoCompleteViewController;

@interface AutoCompleteTextView : HipChatTextView {
    AutoCompleteViewController *acController;
    NSPopover *acPopover;
    NSString *acSearch;
}

- (void)appendMentionToMessage:(NSString *)mentionString;
- (void)hideAutoComplete;
- (void)updateAutoCompletePopoverPosition;

@property BOOL showingAutocomplete;
@property NSString *trigger;

@end
