//
//  RightSidebarEntry.m
//  HipChat
//
//  Created by Christopher Rivers on 4/8/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "RightSidebarEntry.h"

#import "OSXHelpers.h"

@implementation RightSidebarEntry

// Use a getter function for timeAgo so that it updates on-display
- (NSString *)timeAgo {
    return [OSXHelpers formatTime:self.uploadDate];
}

@end
