//
//  HipChatTextView.m
//  HipChat
//
//  Created by Christopher Rivers on 11/6/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <WebKit/WebKit.h>

#import "HipChatTextView.h"
#import "HipChatOSXApp.h"

@implementation HipChatTextView

@dynamic delegate;

// Update the spelling / autocorrect options whenever the text box gets shown
- (void)viewDidMoveToWindow {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [self setContinuousSpellCheckingEnabled:[prefs boolForKey:@"continuousSpellCheckingEnabled"]];
    [self setAutomaticSpellingCorrectionEnabled:[prefs boolForKey:@"automaticSpellingCorrectionEnabled"]];
}

- (void)concludeDragOperation:(id<NSDraggingInfo>)sender {
    [super concludeDragOperation:sender];
    [HipChatOSXApp.instance focusApp];
}

- (NSString *)preferredPasteboardTypeFromArray:(NSArray *)availableTypes restrictedToTypesFromArray:(NSArray *)allowedTypes {
    if ([availableTypes containsObject:NSFilenamesPboardType]) {
        return NSFilenamesPboardType;
    }
    // We prefer dropping of image data (handle dragging images directly from browsers)
    if ([availableTypes containsObject:NSPasteboardTypePNG]) {
        return NSPasteboardTypePNG;
    } else if ([availableTypes containsObject:NSPasteboardTypeTIFF]) {
        return NSPasteboardTypeTIFF;
    }

    return [super preferredPasteboardTypeFromArray:availableTypes restrictedToTypesFromArray:allowedTypes];
}

- (NSArray *)readablePasteboardTypes {
    NSMutableArray *types = [NSMutableArray arrayWithArray:[super readablePasteboardTypes]];
    [types addObject:NSPasteboardTypePNG];
    [types addObject:NSPasteboardTypeTIFF];
    return types;
}

- (BOOL)readSelectionFromPasteboard:(NSPasteboard *)pboard type:(NSString *)type {
    if (type == NSPasteboardTypePNG || type == NSPasteboardTypeTIFF) {
        NSArray *images = [pboard readObjectsForClasses:[NSArray arrayWithObjects:[NSImage class], nil] options:nil];
        if (!images || [images count] == 0) {
            return NO;
        }
        if ([self.delegate respondsToSelector:@selector(textView:didPasteImage:)]) {
            [self.delegate textView:self didPasteImage:[images objectAtIndex:0]];
        }
    } else if (type == NSFilenamesPboardType) {
        NSArray *files = [pboard propertyListForType:NSFilenamesPboardType];
        if (!files || [files count] == 0) {
            return NO;
        }
        if ([self.delegate respondsToSelector:@selector(textView:didPasteFile:)]) {
            NSString *filePath = [files objectAtIndex:0];
            NSFileWrapper *fileWrapper = [[NSFileWrapper alloc] initWithPath:filePath];
            [self.delegate textView:self didPasteFile:fileWrapper];
        }
    } else if (type == NSURLPboardType) {
        if ([self.delegate respondsToSelector:@selector(textView:didPasteURL:)]) {
            NSArray *classes = [NSArray arrayWithObject:[NSURL class]];
            NSDictionary *options = [NSDictionary dictionaryWithObject:
                                     [NSNumber numberWithBool:YES] forKey:NSPasteboardURLReadingFileURLsOnlyKey];

            NSArray *fileURLs = [pboard readObjectsForClasses:classes options:options];
            if ([fileURLs count] != 1) {
                return NO;
            }
            [self.delegate textView:self didPasteURL:[fileURLs objectAtIndex:0]];
        }
    } else {
        return [super readSelectionFromPasteboard:pboard type:type];
    }
    return YES;
}

// Store auto-correct as a permanent preference so it can be remembered between sessions and chats
- (void)setAutomaticSpellingCorrectionEnabled:(BOOL)flag {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setBool:flag forKey:@"automaticSpellingCorrectionEnabled"];
    [prefs synchronize];
    [super setAutomaticSpellingCorrectionEnabled:flag];
}

// Store spell checking as a permanent preference so it can be remembered between sessions and chats
- (void)setContinuousSpellCheckingEnabled:(BOOL)flag {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setBool:flag forKey:@"continuousSpellCheckingEnabled"];
    [prefs synchronize];
    [super setContinuousSpellCheckingEnabled:flag];
}

@end
