//
//  RoomActionsController.m
//  HipChat
//
//  Created by Christopher Rivers on 5/1/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "RoomActionsController.h"

#import "HipChatOSXApp.h"
#import "HipChatUser.h"
#import "Room.h"
#import "RoomList.h"
#import "XMPPIQ.h"

@implementation RoomActionsController

static NSString *ALERT_TYPE_ARCHIVE = @"archive";
static NSString *ALERT_TYPE_DELETE = @"delete";
static NSString *ALERT_TYPE_UNARCHIVE = @"unarchive";

static NSDictionary *alertArgs;

+ (void)handleArchiveRoom:(XMPPJID *)jid window:(NSWindow *)window {
    HipChatOSXApp *app = HipChatOSXApp.instance;

    // Make sure room exists
    Room *room = [app getRoomInfo:jid];
    if (!room) {
        [app showFlash:@"Failed to archive room. No room data found."];
        return;
    }
    // Check for admin privileges
    HipChatUser *currentUser = app.currentUser;
    if (!currentUser.isAdmin && ![room isAdmin:currentUser.jid]) {
        [app showFlash:@"Permission denied. You don't have admin privileges on this room."];
        return;
    }

    // Confirm action with an alert popup
    NSString *confirmMessage = [NSString stringWithFormat:@"Are you sure you want to archive the '%@' room?", room.name];
    [self showRoomActionAlertWithJid:jid
                                type:ALERT_TYPE_ARCHIVE
                             message:confirmMessage
                            infoText:@"Archiving will prevent any users from chatting in the room."
                              window:window];

}

+ (void)handleDeleteRoom:(XMPPJID *)jid window:(NSWindow *)window {
    HipChatOSXApp *app = HipChatOSXApp.instance;

    // Make sure room exists
    Room *room = [app getRoomInfo:jid];
    if (!room) {
        [app showFlash:@"Failed to delete room. No room data found."];
        return;
    }
    // Check for admin privileges
    HipChatUser *currentUser = app.currentUser;
    if (!currentUser.isAdmin && ![room isAdmin:currentUser.jid]) {
        [app showFlash:@"Permission denied. You don't have admin privileges on this room."];
        return;
    }

    // Confirm action with an alert popup
    NSString *confirmMessage = [NSString stringWithFormat:@"Are you sure you want to delete the '%@' room?", room.name];
    [self showRoomActionAlertWithJid:jid
                                type:ALERT_TYPE_DELETE
                             message:confirmMessage
                            infoText:@"All users will be removed from the room and it will no longer be available for viewing"
                              window:window];
}

+ (void)handleUnarchiveRoom:(XMPPJID *)jid window:(NSWindow *)window {
    HipChatOSXApp *app = HipChatOSXApp.instance;

    // Make sure room exists
    Room *room = [app getRoomInfo:jid];
    if (!room) {
        [app showFlash:@"Failed to unarchive room. No room data found."];
        return;
    }
    // Check for admin privileges
    HipChatUser *currentUser = app.currentUser;
    if (!currentUser.isAdmin && ![room isAdmin:currentUser.jid]) {
        [app showFlash:@"Permission denied. You don't have admin privileges on this room."];
        return;
    }

    // Confirm action with an alert popup
    NSString *confirmMessage = [NSString stringWithFormat:@"Are you sure you want to unarchive the '%@' room?", room.name];
    [self showRoomActionAlertWithJid:jid
                                type:ALERT_TYPE_UNARCHIVE
                             message:confirmMessage
                            infoText:nil
                              window:window];
}

+ (void)showRoomActionAlertWithJid:(XMPPJID *)jid
                              type:(NSString *)type
                           message:(NSString *)message
                          infoText:(NSString *)infoText
                            window:(NSWindow *)window {
    NSAlert *alert = [NSAlert alertWithMessageText:message
                                     defaultButton:@"OK"
                                   alternateButton:@"Cancel"
                                       otherButton:nil
                         informativeTextWithFormat:@"%@", (infoText ? infoText : @"")];

    alertArgs = [NSDictionary dictionaryWithObjectsAndKeys:
                 type, @"type",
                 jid, @"jid",
                 nil];
    [alert beginSheetModalForWindow:window
                      modalDelegate:self
                     didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:)
                        contextInfo:(void *)alertArgs];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark NSAlert Delegate functions
///////////////////////////////////////////////////////////////////////

+ (void)alertDidEnd:(NSAlert *)alert returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo {
    if (returnCode != NSAlertDefaultReturn) {
        // Do nothing
        return;
    }

    if (!contextInfo) {
        DDLogError(@"Not context info found for alert handler");
        return;
    }
    NSDictionary *args = (__bridge NSDictionary *)contextInfo;
    HipChatOSXApp *app = HipChatOSXApp.instance;
    XMPPJID *jid = [args objectForKey:@"jid"];
    Room *room = [app getRoomInfo:jid];
    if (!room) {
        [app showFlash:@"Failed to archive room. No room data found."];
        return;
    }

    if ([[args objectForKey:@"type"] isEqualToString:ALERT_TYPE_ARCHIVE]) {
        IQCallback callback = ^(XMPPIQ *iq) {
            if ([iq.type isEqualToString:@"error"]) {
                [HipChatApp.instance showFlash:@"There was an error archiving the room."];
            } else {
                [HipChatApp.instance showFlash:[NSString stringWithFormat:
                                                @"Archived room: '%@'", room.name]];
            }
        };
        [room setArchived:YES callback:callback];
    } else if ([[args objectForKey:@"type"] isEqualToString:ALERT_TYPE_DELETE]) {
        IQCallback callback = ^(XMPPIQ *iq) {
            if ([iq.type isEqualToString:@"error"]) {
                [HipChatApp.instance showFlash:@"There was an error deleting the room."];
            } else {
                [HipChatApp.instance showFlash:[NSString stringWithFormat:@"Deleted the room '%@'", room.name]];
            }
        };
        [app.roomList destroyRoom:jid withReason:@"Admin is deleting the room" callback:callback];
    } else if ([[args objectForKey:@"type"] isEqualToString:ALERT_TYPE_UNARCHIVE]) {
        IQCallback callback = ^(XMPPIQ *iq) {
            if ([iq.type isEqualToString:@"error"]) {
                [HipChatApp.instance showFlash:@"There was an error unarchiving the room."];
            } else {
                [HipChatApp.instance showFlash:[NSString stringWithFormat:
                                                @"Unarchived room: '%@'", room.name]];
            }
        };
        [room setArchived:NO callback:callback];
    }
}

@end
