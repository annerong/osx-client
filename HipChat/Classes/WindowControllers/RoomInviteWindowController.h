//
//  RoomInviteViewController.h
//  HipChat
//
//  Created by Christopher Rivers on 2/25/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "UserSelectionWindowController.h"

@class ChatFilterArrayController;
@class XMPPJID;
@class User;

@interface RoomInviteWindowController : UserSelectionWindowController

@property XMPPJID *jid;

@property IBOutlet NSButton *cancelButton;
@property IBOutlet NSButton *inviteButton;
@property IBOutlet NSTextField *messageInput;

@end
