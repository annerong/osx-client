//
//  NSView+SubviewIteration.h
//  HipChat
//
//  Created by Christopher Rivers on 2/13/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSView (SubviewIteration)

- (NSArray *)allSubviews;

@end
