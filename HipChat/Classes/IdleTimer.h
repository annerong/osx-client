//
//  IdleTimer.h
//  HipChat
//
//  Created by Christopher Rivers on 12/10/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const kIdleTimerActiveNotification;
FOUNDATION_EXPORT NSString *const kIdleTimerInactiveNotification;

@interface IdleTimer : NSObject {
    NSTimeInterval lastEventTime;
}

- (id)initWithIdleSeconds:(NSTimeInterval)idleSeconds;

- (NSTimeInterval)getIdleSeconds;

@property (nonatomic, assign) NSTimeInterval secondsBeforeIdle;
@property (nonatomic, assign) BOOL isIdle;

@end
