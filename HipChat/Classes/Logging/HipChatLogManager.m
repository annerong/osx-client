//
//  HipChatLogManager.m
//  HipChat
//
//  Created by Christopher Rivers on 2/4/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "RegexKitLite.h"

#import "HipChatLogManager.h"

@implementation HipChatLogManager

/**
 * Generates a new unique log file path, and creates the corresponding log file.
 **/
- (NSString *)createNewLogFile {
    NSDate *now = [NSDate date];
    NSDateFormatter *dFormatter = [[NSDateFormatter alloc] init];
    [dFormatter setDateFormat:@"yyyy-MM-dd"];

	// Generate a random log file name, and create the file (if there isn't a collision)
    NSInteger logNum = 1;
	NSString *logsDirectory = [self logsDirectory];
	do {
		NSString *fileName = [NSString stringWithFormat:@"log-%@-%ld.txt", [dFormatter stringFromDate:now], (long)logNum];
		NSString *filePath = [logsDirectory stringByAppendingPathComponent:fileName];

		if (![[NSFileManager defaultManager] fileExistsAtPath:filePath])
		{
			DDLogInfo(@"HipChatFileLogger: Creating new log file: %@", fileName);

			[[NSFileManager defaultManager] createFileAtPath:filePath contents:nil attributes:nil];

			// Since we just created a new log file, we may need to delete some old log files
            if ([self respondsToSelector:@selector(deleteOldLogFiles)]) {
                [self performSelector:@selector(deleteOldLogFiles)];
            }

			return filePath;
		}
        logNum++;
	} while(YES);
}

- (BOOL)isLogFile:(NSString *)fileName {
    // Extend isLogFile to match our new log file format: log-<date>-<#>.txt
	BOOL hasProperPrefix = [fileName hasPrefix:@"log-"];
	BOOL hasProperLength = [fileName length] >= 10;

	if (hasProperPrefix && hasProperLength)	{
        if ([fileName rangeOfRegex:@"log-\\d+-\\d+-\\d+"].location != NSNotFound) {
            return YES;
        }
	}

    return NO;
}

@end
