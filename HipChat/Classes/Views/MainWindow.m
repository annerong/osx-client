//
//  MainWindow.m
//  HipChat
//
//  Created by Christopher Rivers on 11/26/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Carbon/Carbon.h>

#import "MainWindow.h"

#import "HipChatOSXApp.h"
#import "HipChatUser.h"
#import "RawSendWindowController.h"
#import "RegexKitLite.h"
#import "Room.h"

@implementation MainWindow

// We may want to allow users to drop files anywhere in the window at some point
//- (id)initWithContentRect:(NSRect)contentRect styleMask:(NSUInteger)aStyle backing:(NSBackingStoreType)bufferingType defer:(BOOL)flag {
//    self = [super initWithContentRect:contentRect styleMask:aStyle backing:bufferingType defer:flag];
//    if (self) {
//        [self registerForDraggedTypes:[NSArray arrayWithObjects:
//                                       NSStringPboardType, NSURLPboardType, NSFilenamesPboardType, nil]];
//    }
//    return self;
//}

- (void)becomeKeyWindow {
    [super becomeKeyWindow];
    [HipChatOSXApp.instance focusCurrentChatInput];
}

- (BOOL)performKeyEquivalent:(NSEvent *)theEvent {
    // Supersets must come first in the if statement
    // e.g. CMD+ALT must come before just CMD or just ALT
    NSUInteger flags = [theEvent modifierFlags] & NSDeviceIndependentModifierFlagsMask;
    if ((flags & NSCommandKeyMask) && (flags & NSControlKeyMask) && (flags & NSShiftKeyMask)) {
        if ([theEvent keyCode] == kVK_F3) {
            [HipChatOSXApp.instance toggleDevPrefs];
            return YES;
        } else if ([theEvent keyCode] == kVK_F5) {
            if ([HipChatOSXApp.instance.currentUser.email
                 rangeOfRegex:@"@(hipchat.com|atlassian.com)"].location == NSNotFound) {
                return NO;
            }
            NSWindowController *wc = [[RawSendWindowController alloc] initWithWindowNibName:@"RawSendSheet"];
            [NSApp beginSheet:wc.window
               modalForWindow:self
                modalDelegate:nil
               didEndSelector:nil
                  contextInfo:nil];
            [wc.window setPreventsApplicationTerminationWhenModal:NO];
            [NSApp runModalForWindow:wc.window];
            return YES;
        }
    } else if ((flags & NSCommandKeyMask) && (flags & NSShiftKeyMask)) {
        // COMMAND + SHIFT hotkeys
        if ([theEvent keyCode] == kVK_ANSI_RightBracket) {
            [HipChatOSXApp.instance focusNextChat];
            return YES;
        } else if ([theEvent keyCode] == kVK_ANSI_LeftBracket) {
            [HipChatOSXApp.instance focusPreviousChat];
            return YES;
        }
    } else if ((flags & NSCommandKeyMask) && (flags & NSAlternateKeyMask)) {
        // COMMAND + OPTION (ALT) hotkeys
        if ([theEvent keyCode] == kVK_LeftArrow) {
            [HipChatOSXApp.instance focusPreviousChat];
            return YES;
        } else if ([theEvent keyCode] == kVK_RightArrow) {
            [HipChatOSXApp.instance focusNextChat];
            return YES;
        } else if ([theEvent keyCode] == kVK_DownArrow) {
            [HipChatOSXApp.instance focusNextChat];
            return YES;
        } else if ([theEvent keyCode] == kVK_UpArrow) {
            [HipChatOSXApp.instance focusPreviousChat];
            return YES;
        }
    } else if ((flags & NSShiftKeyMask) && (flags & NSAlternateKeyMask)) {
        // OPTION (ALT) + SHIFT hotkeys
        // None right now
    } else if (flags & NSControlKeyMask) {
        // CONTROL only hotkeys
        if ([theEvent keyCode] == kVK_PageUp) {
            [HipChatOSXApp.instance focusPreviousChat];
            return YES;
        } else if ([theEvent keyCode] == kVK_PageDown) {
            [HipChatOSXApp.instance focusNextChat];
            return YES;
        }
    } else if (flags & NSCommandKeyMask) {
        // COMMAND only hotkeys
        if ([theEvent keyCode] == kVK_ANSI_1) {
            [HipChatOSXApp.instance focusChatAtIndex:0];
            return YES;
        } else if ([theEvent keyCode] == kVK_ANSI_2) {
            [HipChatOSXApp.instance focusChatAtIndex:1];
            return YES;
        } else if ([theEvent keyCode] == kVK_ANSI_3) {
            [HipChatOSXApp.instance focusChatAtIndex:2];
            return YES;
        } else if ([theEvent keyCode] == kVK_ANSI_4) {
            [HipChatOSXApp.instance focusChatAtIndex:3];
            return YES;
        } else if ([theEvent keyCode] == kVK_ANSI_5) {
            [HipChatOSXApp.instance focusChatAtIndex:4];
            return YES;
        } else if ([theEvent keyCode] == kVK_ANSI_6) {
            [HipChatOSXApp.instance focusChatAtIndex:5];
            return YES;
        } else if ([theEvent keyCode] == kVK_ANSI_7) {
            [HipChatOSXApp.instance focusChatAtIndex:6];
            return YES;
        } else if ([theEvent keyCode] == kVK_ANSI_8) {
            [HipChatOSXApp.instance focusChatAtIndex:7];
            return YES;
        } else if ([theEvent keyCode] == kVK_ANSI_9) {
            [HipChatOSXApp.instance focusChatAtIndex:-1];
            return YES;
        // Use [theEvent characters] for letter-based shortcuts
        // Otherwise, it won't work properly on non-qwerty layouts (like dvorak)
        } else if ([[theEvent characters] isEqualToString:@"n"]) {
            [HipChatOSXApp.instance showJoinChat];
            return YES;
        }
    }

    return NO;
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark File download methods
///////////////////////////////////////////////////////////////////////

- (void)download:(NSURLDownload *)download didFailWithError:(NSError *)error {
    // Inform the user.
    DDLogInfo(@"Save file to disk failed. File: %@ -- Error: %@ %@",
              download.request.URL.absoluteString,
              [error localizedDescription],
              [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    [HipChatOSXApp.instance showFlash:[NSString stringWithFormat:@"Failed to save the file to disk. Error %ld", error.code]];
}

- (void)downloadDidFinish:(NSURLDownload *)download {
    // Do something with the data.
    DDLogInfo(@"Finished downloading file from link: %@", download.request.URL.absoluteString);
    [HipChatOSXApp.instance showFlash:@"Saved file successfully."];
}



@end
