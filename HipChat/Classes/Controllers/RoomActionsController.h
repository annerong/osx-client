//
//  RoomActionsController.h
//  HipChat
//
//  Created by Christopher Rivers on 5/1/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class XMPPJID;

@interface RoomActionsController : NSObject

+ (void)handleArchiveRoom:(XMPPJID *)jid window:(NSWindow *)window;
+ (void)handleDeleteRoom:(XMPPJID *)jid window:(NSWindow *)window;
+ (void)handleUnarchiveRoom:(XMPPJID *)jid window:(NSWindow *)window;

@end
