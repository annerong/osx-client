//
//  TabData.h
//  HipChat
//
//  Created by Christopher Rivers on 10/19/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Room;
@class User;
@class XMPPJID;

@interface TabData : NSObject

@property (readwrite, strong) IBOutlet NSColor *color;
@property (readwrite, assign) BOOL hideClose;
@property (readwrite, strong) IBOutlet NSString *icon;
@property (readwrite, strong) XMPPJID *jid;
@property (readwrite, strong) IBOutlet NSString *name;

- (TabData *)initWithJid:(XMPPJID *)initJid;

@end
