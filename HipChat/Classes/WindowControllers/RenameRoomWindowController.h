//
//  RenameRoomWindowController.h
//  HipChat
//
//  Created by Christopher Rivers on 1/4/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "HipChatSheetController.h"

@class XMPPJID;

@interface RenameRoomWindowController : HipChatSheetController {
    NSTimer *errorCheckTimer;
}

@property XMPPJID *jid;
@property IBOutlet NSTextField *errorText;
@property IBOutlet NSTextField *roomNameField;
@property IBOutlet NSTextField *titleText;

@end
