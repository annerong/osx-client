//
//  ChatFilterArrayController.m
//  HipChat
//
//  Created by Christopher Rivers on 12/31/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "ChatFilterArrayController.h"

#import "RegexKitLite.h"
#import "User.h"
#import "XMPPJID.h"

@implementation ChatFilterArrayController

static NSDictionary *accentReplacements;

+ (void)initialize {
    accentReplacements = [NSDictionary dictionaryWithObjectsAndKeys:
                          @"[aÂÃÄÀÁÅàáâãäå]", @"[Aa]",
                          @"[cÇç]", @"[Cc]",
                          @"[eÈÉÊËèéêë]", @"[Ee]",
                          @"[iÌÍÎÏìíîï]", @"[Ii]",
                          @"[nÑñ]", @"[Nn]",
                          @"[oÒÓÔÕÖØðòóôõöø]", @"[Oo]",
                          @"[uÙÚÛÜùúûü]", @"[Uu]",
                          @"[yÝýÿ]", @"[Yy]",
                          nil];
}

- (void)awakeFromNib {
    self.selectedJids = [NSDictionary dictionary];
}

- (NSArray *)arrangeObjects:(NSArray *)objects {
    if (searchString == nil) {
        return [super arrangeObjects:objects];
    }

    for (NSString *key in accentReplacements) {
        searchString = [searchString stringByReplacingOccurrencesOfRegex:key withString:[accentReplacements objectForKey:key]];
    }

    NSString *regexStr = [NSString stringWithFormat:@"\\b%@", searchString];
    NSMutableDictionary *firstWordMatches = [NSMutableDictionary dictionary];
    NSMutableArray *filteredObjects = [NSMutableArray arrayWithCapacity:[objects count]];
    NSEnumerator *objectsEnumerator = [objects objectEnumerator];
    id item;
    while (item = [objectsEnumerator nextObject]) {
        NSString *name = [item valueForKeyPath:@"name"];
        XMPPJID *jid = [item valueForKeyPath:@"jid"];
        if (!name || !jid) {
            DDLogError(@"Encountered item with no name for chat filter: %@", item);
            continue;
        }
        if ([self.selectedJids objectForKey:jid.bare]) {
            continue;
        }

        NSRange matchRange = [name rangeOfRegex:regexStr
                                        options:RKLCaseless
                                        inRange:NSMakeRange(0, name.length)
                                        capture:0
                                          error:nil];
        if (matchRange.location != NSNotFound) {
            [filteredObjects addObject:item];
        }
        if (matchRange.location == 0) {
            [firstWordMatches setObject:[NSNumber numberWithBool:YES] forKey:name];
        }
    }

    // Sort first-word-matches first
    [filteredObjects sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSNumber *obj1Matches = [firstWordMatches objectForKey:[obj1 valueForKeyPath:@"name"]];
        NSNumber *obj2Matches = [firstWordMatches objectForKey:[obj2 valueForKeyPath:@"name"]];
        if ([obj1Matches boolValue] && [obj2Matches boolValue]) {
            return NSOrderedSame;
        } else if ([obj1Matches boolValue]) {
            return NSOrderedAscending;
        } else if ([obj2Matches boolValue]) {
            return NSOrderedDescending;
        }
        return NSOrderedSame;
    }];
    return [super arrangeObjects:filteredObjects];
}

- (void)search:(NSString *)newSearch {
    searchString = newSearch;
    [self rearrangeObjects];
}

- (NSArray *)names {
    __block NSMutableArray *namesArray = [NSMutableArray arrayWithCapacity:[self.arrangedObjects count]];
    [self.arrangedObjects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [namesArray addObject:((User *)obj).name];
    }];
    return namesArray;
}

@end
