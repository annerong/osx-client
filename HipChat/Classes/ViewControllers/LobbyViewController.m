//
//  LobbyViewController.m
//  HipChat
//
//  Created by Christopher Rivers on 10/18/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "LobbyViewController.h"

#import <WebKit/WebKit.h>

#import "HipChatOSXApp.h"
#import "HipChatUser.h"
#import "LobbyRowView.h"
#import "LobbyEntry.h"
#import "PrettyBorderBox.h"
#import "Room.h"
#import "RoomActionsController.h"
#import "RoomList.h"
#import "XMPPIQ.h"
#import "XMPPJID.h"

@implementation LobbyViewController

@dynamic entries;

- (void)awakeFromNib {
    // Use height-1 so we can see the top border of the box
    NSRect webFrame = NSMakeRect(0, 0, self.bottomBox.frame.size.width, self.bottomBox.frame.size.height-1);
    latestNewsWebView = [[WebView alloc] initWithFrame:webFrame];
    [self.bottomBox addSubview:latestNewsWebView];
    latestNewsWebView.drawsBackground = NO;
    latestNewsWebView.autoresizingMask = (NSViewMinXMargin |
                                          NSViewWidthSizable |
                                          NSViewMaxXMargin |
                                          NSViewMinYMargin |
                                          NSViewHeightSizable |
                                          NSViewMaxYMargin);
    [latestNewsWebView.mainFrame.frameView setAllowsScrolling:NO];
    latestNewsWebView.frameLoadDelegate = self;
    [latestNewsWebView.mainFrame loadRequest:
     [NSURLRequest requestWithURL:
      [NSURL URLWithString:@"http://downloads.hipchat.com/blog_info.html"]]];

    NSSortDescriptor *sectionSort = nil;
	sectionSort = [[NSSortDescriptor alloc] initWithKey:@"section"
                                              ascending:YES
                                               selector:nil];
    NSSortDescriptor *headerSort = nil;
	headerSort = [[NSSortDescriptor alloc] initWithKey:@"isHeader"
                                             ascending:NO
                                              selector:nil];
    NSSortDescriptor *nameSort = nil;
	nameSort = [[NSSortDescriptor alloc] initWithKey:@"name"
                                           ascending:YES
                                            selector:@selector(localizedCaseInsensitiveCompare:)];
	self.lobbyArrayController.sortDescriptors = [NSArray arrayWithObjects:sectionSort, headerSort, nameSort, nil];
    self.lobbyArrayController.automaticallyRearrangesObjects = NO;

    [self.bottomBox setTopBorderColor:RGB(210.0, 210.0, 210.0)];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        HipChatOSXApp *app = HipChatOSXApp.instance;
        [app.roomList addDelegate:self delegateQueue:dispatch_get_main_queue()];
    }

    self.entries = [NSMutableArray array];

    return self;
}

- (void)setEntries:(NSMutableArray *)newEntries {
    [self willChangeValueForKey:@"entries"];
    [self removeObservers];
    entries = newEntries;
    [self addObservers];
    [self didChangeValueForKey:@"entires"];
}

- (NSMutableArray *)entries {
    return entries;
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark HipChatAppDelegate
///////////////////////////////////////////////////////////////////////

- (void)roomListLoaded:(RoomList *)sender {
	[self updateRoomEntries];
}

- (void)roomList:(RoomList *)sender addedRoom:(Room *)room {
	[self.lobbyArrayController addObject:[[LobbyEntry alloc] initWithRoom:room]];
    [self.lobbyArrayController rearrangeObjects];
}

- (void)roomList:(RoomList *)sender deletedRoom:(XMPPJID *)roomJid {
    NSUInteger roomIdx = [self.lobbyArrayController.arrangedObjects indexOfObjectPassingTest:
                         ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                            LobbyEntry *entry = (LobbyEntry *)obj;
                            if ([entry.jid.bare isEqualToString:roomJid.bare]) {
                                *stop = YES;
                                return YES;
                            }
                            return NO;
                        }];
    if (roomIdx != NSNotFound) {
        // Deinit LobbyEntry before removing, so we know we remove observers successfully
        LobbyEntry *entry = (LobbyEntry *)[[self.lobbyArrayController arrangedObjects] objectAtIndex:roomIdx];
        [entry deinitialize];
        [self.lobbyArrayController removeObjectAtArrangedObjectIndex:roomIdx];
        [self.lobbyArrayController rearrangeObjects];
    }
}

- (void)hipChatAppWillManuallyDisconnect {
    // Deinitialize before clearing the entries array
    for (LobbyEntry *entry in self.entries) {
        [entry deinitialize];
    }
    [self.lobbyArrayController removeObjects:self.entries];
    [self.tableView reloadData];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark WebView Delegate functions
///////////////////////////////////////////////////////////////////////

- (void)webView:(WebView *)webView
decidePolicyForNavigationAction:(NSDictionary *)actionInformation
        request:(NSURLRequest *)request
          frame:(WebFrame *)frame
decisionListener:(id < WebPolicyDecisionListener >)listener {
    [[NSWorkspace sharedWorkspace] openURL:[request URL]];
}

- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame *)frame {
    if (frame == latestNewsWebView.mainFrame) {
        latestNewsWebView.policyDelegate = self;
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark NSTableViewDelegate
///////////////////////////////////////////////////////////////////////

- (NSIndexSet *)tableView:(NSTableView *)tableView
selectionIndexesForProposedSelection:(NSIndexSet *)proposedSelectionIndexes {
    return NO;
}

- (NSTableRowView *)tableView:(NSTableView *)tableView rowViewForRow:(NSInteger)row {
    return [[LobbyRowView alloc] init];
}

- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    if ([self tableView:tableView isGroupRow:row]) {
        return [tableView makeViewWithIdentifier:@"header" owner:self];
    }
    return [tableView makeViewWithIdentifier:@"room_entry" owner:self];
}

- (BOOL)tableView:(NSTableView *)tableView isGroupRow:(NSInteger)row {
    LobbyEntry *entry = [self.lobbyArrayController.arrangedObjects objectAtIndex:row];
    if (entry.isHeader) {
        return YES;
    }
    return NO;
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification {
    NSTableView *table = (NSTableView *)[notification object];
    LobbyEntry *entry = [self.entries objectAtIndex:table.selectedRow];

    if (entry.jid) {
        HipChatOSXApp *app = HipChatOSXApp.instance;
        [app joinChat:entry.jid withFocus:YES];
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark UI Listeners
///////////////////////////////////////////////////////////////////////

- (IBAction)handleArchiveRoom:(XMPPJID *)jid {
    [RoomActionsController handleArchiveRoom:jid window:self.view.window];
}

- (IBAction)handleDeleteRoom:(XMPPJID *)jid {
    [RoomActionsController handleDeleteRoom:jid window:self.view.window];
}

- (IBAction)handleChangePrivacy:(XMPPJID *)jid {
    [HipChatOSXApp.instance showChangePrivacyForJid:jid];
}

- (IBAction)handleCreateRoom {
    [HipChatOSXApp.instance showCreateRoomForJid:nil];
}

- (IBAction)handleInviteUsers:(XMPPJID *)jid {
    [HipChatOSXApp.instance showRoomInviteForJid:jid];
}

- (IBAction)handleRemoveUsers:(XMPPJID *)jid {
    [HipChatOSXApp.instance showRoomRemoveMembersForJid:jid];
}

- (IBAction)handleRenameRoom:(XMPPJID *)jid {
    [HipChatOSXApp.instance showRenameRoomForJid:jid];
}

- (IBAction)handleUnarchiveRoom:(XMPPJID *)jid {
    [RoomActionsController handleUnarchiveRoom:jid window:self.view.window];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Notification handlers
///////////////////////////////////////////////////////////////////////

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    // Make sure the timer is scheduled on the main thread
    dispatch_async(dispatch_get_main_queue(), ^{
        if (rearrangeTimer) {
            [rearrangeTimer invalidate];
        }

        // Only trigger resorting once every 250ms
        // This avoids getting blasted with presences (esp. on startup)
        // and resorts causing the main thread to mini-freeze
        rearrangeTimer = [NSTimer scheduledTimerWithTimeInterval:0.25
                                                          target:self
                                                        selector:@selector(rearrangeContent)
                                                        userInfo:nil
                                                         repeats:NO];
    });
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Helper functions
///////////////////////////////////////////////////////////////////////

// Use member variable here, not the property
// This function gets called in the property setter
- (void)addObservers {
    LobbyEntry *le;
    for (le in entries) {
        [le addObserver:self forKeyPath:@"section" options:NSKeyValueObservingOptionNew context:nil];
    }
}

- (IBAction)lobbyEntryClickedForJid:(XMPPJID *)jid {
    [HipChatOSXApp.instance joinChat:jid withFocus:YES];
}

- (void)rearrangeContent {
    [self.lobbyArrayController rearrangeObjects];
}

// Use member variable here, not the property
// This function gets called in the property setter
- (void)removeObservers {
    if (!entries) {
        return;
    }
    LobbyEntry *le;
    for (le in entries) {
        [le removeObserver:self forKeyPath:@"section"];
    }
}

- (void)updateRoomEntries {
    DDLogInfo(@"Updating room entries");
    HipChatOSXApp *app = HipChatOSXApp.instance;
    NSDictionary *allRooms = [app getAllRooms];
    NSMutableArray *allEntries = [NSMutableArray arrayWithCapacity:(allRooms.count + 3)];

    for (id key in allRooms) {
        Room *room = [allRooms objectForKey:key];
        [allEntries addObject:[[LobbyEntry alloc] initWithRoom:room]];
    }
    [allEntries addObject:[[LobbyEntry alloc] initWithHeader:@"Open Rooms" section:0]];
    [allEntries addObject:[[LobbyEntry alloc] initWithHeader:@"Private Rooms" section:1]];
    [allEntries addObject:[[LobbyEntry alloc] initWithHeader:@"Archived Rooms" section:2]];
    
    self.entries = allEntries;
}


@end
