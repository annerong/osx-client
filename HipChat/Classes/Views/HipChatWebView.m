//
//  HipChatWebView.m
//  HipChat
//
//  Created by Christopher Rivers on 2/8/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "HipChatWebView.h"

@implementation HipChatWebView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSMutableArray *types = [NSMutableArray array];
        [types addObject:NSPasteboardTypePNG];
        [types addObject:NSPasteboardTypeTIFF];
        [self registerForDraggedTypes:types];
    }
    
    return self;
}

- (BOOL)performDragOperation:(id < NSDraggingInfo >)sender {
    // Forward file/image drag & drop operations to the message input via notifications
    NSPasteboard *pboard = [sender draggingPasteboard];
    if ([pboard.types containsObject:NSPasteboardTypePNG] ||
        [pboard.types containsObject:NSPasteboardTypeTIFF] ||
        [pboard.types containsObject:NSURLPboardType] ||
        [pboard.types containsObject:NSFilenamesPboardType]) {
        NSDictionary *args = [NSDictionary dictionaryWithObject:sender forKey:@"dragInfo"];
        [[NSNotificationCenter defaultCenter] postNotificationName:hcWebViewDropNotification
                                                            object:self
                                                          userInfo:args];
        return YES;
    }
    return NO;
}

@end
