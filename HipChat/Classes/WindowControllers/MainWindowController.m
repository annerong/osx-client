//
//  MainWindowController.m
//  HipChat
//
//  Created by Christopher Rivers on 11/19/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "MainWindowController.h"

#import <WebKit/WebKit.h>
#import <objc/runtime.h>

#import "HipChatUser.h"
#import "RoomList.h"
#import "Room.h"
#import "User.h"
#import "XMPPJID.h"

#import "ChangePrivacyWindowController.h"
#import "CreateRoomWindowController.h"
#import "HipChatOSXApp.h"
#import "JoinChatWindowController.h"
#import "ManagePermissionsWindowController.h"
#import "OSXHelpers.h"
#import "RoomActionsController.h"
#import "RoomInviteWindowController.h"
#import "RoomRemoveMembersWindowController.h"
#import "RenameRoomWindowController.h"

@implementation MainWindowController

static NSString *htmlHead =
    @"<style>"
    " *, div, p { margin: 0; padding; 0 } "
    " * { line-height: 20px; font-size: 12px; font-family: \"Lucida Grande\",sans-serif; }"
    " a { color: #6666cc; text-decoration: underline; cursor: pointer; }"
    "</style>";

static NSInteger FLASH_DISPLAY_SECONDS = 5;

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Initialization
///////////////////////////////////////////////////////////////////////

- (void)windowDidLoad {
    [super windowDidLoad];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(onFocusChat:) name:HipChatFocusChatNotification object:nil];
    [center addObserver:self selector:@selector(onFocusLobby:) name:HipChatFocusLobbyNotification object:nil];
    [center addObserver:self selector:@selector(handleInvite:) name:HipChatInviteNotification object:nil];
    [center addObserver:self selector:@selector(handleFlash:) name:HipChatFlashNotification object:nil];
    [center addObserver:self selector:@selector(onShowChangePrivacy:) name:HipChatShowChangePrivacyNotification object:nil];
    [center addObserver:self selector:@selector(onShowCreateRoom:) name:HipChatShowCreateRoomNotification object:nil];
    [center addObserver:self selector:@selector(onShowJoinChat:) name:HipChatShowJoinChatNotification object:nil];
    [center addObserver:self selector:@selector(onShowManagePerms:) name:HipChatShowManagePermsNotification object:nil];
    [center addObserver:self selector:@selector(onShowRenameRoom:) name:HipChatShowRenameRoomNotification object:nil];
    [center addObserver:self selector:@selector(onShowRoomInvite:) name:HipChatShowRoomInviteNotification object:nil];
    [center addObserver:self selector:@selector(onShowRoomRemoveMembers:) name:HipChatShowRoomRemoveMembersNotification object:nil];
    [center addObserver:self
               selector:@selector(onAppBecomeActive:)
                   name:NSApplicationDidBecomeActiveNotification
                 object:[NSApplication sharedApplication]];

    sheets = [NSMutableDictionary dictionary];
    NSSize boxSize = self.noticeBox.frame.size;
    NSRect noticeFrame = NSMakeRect(10.0, 10.0, (boxSize.width-20.0), (boxSize.height-20.0));
    
    // This makes absolutely no sense, but if this webview is setup in the MainWindow NIB,
    // sometimes it causes the chat views to either 1) not load or 2) not load images
    // Workaround: manually initialize the web view and arrange it here (dumb)
    self.noticeWebView = [[WebView alloc] initWithFrame:noticeFrame];
    self.noticeWebView.drawsBackground = NO;
    self.noticeWebView.frameLoadDelegate = self;
    [self.noticeBox addSubview:self.noticeWebView];
    self.noticeBox.cornerRadius = 4.0;

    // Resizing the window too small causes the Lobby button to disappear
    // Avoid this with a minimum window size
    self.window.minSize = NSMakeSize(350.0, 250.0);

    [HipChatApp.instance addDelegate:self];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Menu outlets
///////////////////////////////////////////////////////////////////////

- (IBAction)closeCurrentChat:(id)sender {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    if (app.currentJid == app.lobbyJid) {
        [self.window performClose:self];
        return;
    }
    [app closeChat:app.currentJid isManual:YES];
}

- (IBAction)createRoom:(id)sender {
    [HipChatOSXApp.instance showCreateRoomForJid:nil];
}

- (IBAction)changeTopic:(id)sender {
    [HipChatOSXApp.instance showChangeTopic];
}

- (IBAction)decreaseFontSize:(id)sender {
    [HipChatOSXApp.instance decreaseFontSize];
}

- (IBAction)increaseFontSize:(id)sender {
    [HipChatOSXApp.instance increaseFontSize];
}

- (IBAction)logAdditionalData:(id)sender {
    // Disabled for now - enable if we want to log javascripts in chat
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    BOOL detailedLoggingEnabled = [prefs boolForKey:@"detailedLoggingEnabled"];
    detailedLoggingEnabled = !detailedLoggingEnabled;
    DDLogInfo(@"Detailed logging: %@", (detailedLoggingEnabled ? @"Enabled" : @"Disabled"));
    [XMPPStream setDetailedLogging:detailedLoggingEnabled];

//    HipChatOSXApp *app = HipChatOSXApp.instance;
//    id cvc = [app.activeChats objectForKey:app.currentJid.bare];
//    if (cvc && [cvc respondsToSelector:@selector(logExecutedJavascripts)]) {
//        [cvc performSelector:@selector(logExecutedJavascripts)];
//    }
}

- (IBAction)joinChat:(id)sender {
    [HipChatOSXApp.instance showJoinChat];
}

- (IBAction)muteRoomAll:(id)sender{
    [HipChatOSXApp.instance muteAllMessagesForJid:[HipChatOSXApp.instance currentJid]];
}
    
- (IBAction)muteRoomExceptMe:(id)sender {
    [HipChatOSXApp.instance muteMessagesExceptMeForJid:[HipChatOSXApp.instance currentJid]];
}

- (IBAction)nextTab:(id)sender {
    [HipChatOSXApp.instance focusNextChat];
}

- (IBAction)previousTab:(id)sender {
    [HipChatOSXApp.instance focusPreviousChat];
}

- (IBAction)releaseNotes:(id)sender {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    [[NSWorkspace sharedWorkspace] openURL:
     [NSURL URLWithString:
      [NSString stringWithFormat:@"https://%@/release_notes/mac", app.webHost]]];
}

- (IBAction)reopenLastChat:(id)sender {
    [HipChatOSXApp.instance reopenLastChat];
}

- (IBAction)returnToNormalFontSize:(id)sender {
    [HipChatOSXApp.instance returnToNormalFontSize];
}

- (IBAction)roomArchive:(id)sender {
    [RoomActionsController handleArchiveRoom:HipChatOSXApp.instance.currentJid window:self.window];
}

- (IBAction)roomChangePrivacy:(id)sender {
    [HipChatOSXApp.instance showChangePrivacyForJid:[HipChatOSXApp.instance currentJid]];
}

- (IBAction)roomDelete:(id)sender {
    [RoomActionsController handleDeleteRoom:HipChatOSXApp.instance.currentJid window:self.window];
}

- (IBAction)roomInvite:(id)sender {
    [HipChatOSXApp.instance showRoomInviteForJid:[HipChatOSXApp.instance currentJid]];
}

- (IBAction)roomRemoveUsers:(id)sender {
    [HipChatOSXApp.instance showRoomRemoveMembersForJid:[HipChatOSXApp.instance currentJid]];
}

- (IBAction)roomRename:(id)sender {
    [HipChatOSXApp.instance showRenameRoomForJid:[HipChatOSXApp.instance currentJid]];
}

- (IBAction)roomUnarchive:(id)sender {
    [RoomActionsController handleUnarchiveRoom:HipChatOSXApp.instance.currentJid window:self.window];
}

- (IBAction)showFilesSidebar:(id)sender {
    [HipChatOSXApp.instance showRightSidebarFiles];
}

- (IBAction)showLinksSidebar:(id)sender {
    [HipChatOSXApp.instance showRightSidebarLinks];
}

- (IBAction)showPrimaryRightSidebar:(id)sender {
    [HipChatOSXApp.instance showRightSidebarPrimary];
}

- (IBAction)showSearch:(id)sender {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    [[NSNotificationCenter defaultCenter] postNotificationName:HipChatShowSearchNotification object:app];
}

- (IBAction)toggleLeftSidebar:(id)sender {
    [HipChatOSXApp.instance toggleLeftSidebar];
}

- (IBAction)toggleRightSidebar:(id)sender {
    [HipChatOSXApp.instance toggleRightSidebar];
}
    
- (IBAction)unmute:(id)sender {
    [HipChatOSXApp.instance unmute:[HipChatOSXApp.instance currentJid]];
}

- (BOOL)validateUserInterfaceItem:(id < NSValidatedUserInterfaceItem >)anItem {
    if ([anItem action] == @selector(decreaseFontSize:) ||
        [anItem action] == @selector(increaseFontSize:) ||
        [anItem action] == @selector(showFilesSidebar:) ||
        [anItem action] == @selector(showLinksSidebar:) ||
        [anItem action] == @selector(showPrimaryRightSidebar:) ||
        [anItem action] == @selector(returnToNormalFontSize:)) {
        // Menu actions that only apply when you're viewing a chat (1:1 or room)
        HipChatOSXApp *app = HipChatOSXApp.instance;
        return (app.lobbyJid != app.currentJid);
    } else if ([anItem action] == @selector(changeTopic:)) {
        // Menu actions that only apply when you're in a room
        HipChatOSXApp *app = HipChatOSXApp.instance;
        return (app.currentJid != app.lobbyJid && [OSXHelpers isRoomJid:app.currentJid]);
    } else if ([anItem action] == @selector(roomInvite:)) {
        // Requires public room or private room and admin privileges
        HipChatOSXApp *app = HipChatOSXApp.instance;
        Room *room = [app getRoomInfo:app.currentJid];
        if (!room) {
            return NO;
        }
        if (room.isPublic) {
            return YES;
        }
        return ([room isAdmin:app.currentUser.jid] || app.currentUser.isAdmin);
    } else if ([anItem action] == @selector(roomArchive:) ||
               [anItem action] == @selector(roomChangePrivacy:) ||
               [anItem action] == @selector(roomDelete:) ||
               [anItem action] == @selector(roomRename:) ||
               [anItem action] == @selector(roomRemoveUsers:) ||
               [anItem action] == @selector(roomUnarchive:)) {
        // Menu actions that only apply when you're admin or private room admin
        HipChatOSXApp *app = HipChatOSXApp.instance;
        Room *room = [app getRoomInfo:app.currentJid];
        if (!room) {
            return NO;
        }
        // Remove users doesn't apply for public rooms
        if (room.isPublic && [anItem action] == @selector(roomRemoveUsers:)) {
            return NO;
        }

        // Check specifically for archive/unarchive
        if ([anItem action] == @selector(roomArchive:) && room.isArchived) {
            return NO;
        } else if ([anItem action] == @selector(roomUnarchive:) && !room.isArchived) {
            return NO;
        }

        return ([room isAdmin:app.currentUser.jid] || app.currentUser.isAdmin);
    }
    return YES;
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Sheet functions
///////////////////////////////////////////////////////////////////////

- (void)didEndSheet:(NSWindow *)sheet returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo {
    NSWindowController *cpWc = [self getSheetControllerForClass:[ChangePrivacyWindowController class]];
    NSWindowController *crWc = [self getSheetControllerForClass:[CreateRoomWindowController class]];
    NSWindowController *jcWc = [self getSheetControllerForClass:[JoinChatWindowController class]];
    NSWindowController *mpWc = [self getSheetControllerForClass:[ManagePermissionsWindowController class]];
    NSWindowController *rrWc = [self getSheetControllerForClass:[RenameRoomWindowController class]];
    NSWindowController *riWc = [self getSheetControllerForClass:[RoomInviteWindowController class]];
    NSWindowController *rrmWc = [self getSheetControllerForClass:[RoomRemoveMembersWindowController class]];

    if (sheet == jcWc.window) {
        [self handleJoinChatClose:(JoinChatWindowController *)jcWc returnCode:returnCode];
    } else if (sheet == crWc.window) {
        [self handleCreateRoomClose:(CreateRoomWindowController *)crWc returnCode:returnCode];
    } else if (sheet == mpWc.window) {
        [self handleManagePermsClose:(ManagePermissionsWindowController *)mpWc returnCode:returnCode];
    } else if (sheet == riWc.window) {
        [sheets removeObjectForKey:
         [NSString stringWithCString:class_getName([RoomInviteWindowController class])
                            encoding:NSASCIIStringEncoding]];
    } else if (sheet == cpWc.window) {
        [sheets removeObjectForKey:
         [NSString stringWithCString:class_getName([ChangePrivacyWindowController class])
                            encoding:NSASCIIStringEncoding]];
    } else if (sheet == rrWc.window) {
        [sheets removeObjectForKey:
         [NSString stringWithCString:class_getName([RenameRoomWindowController class])
                            encoding:NSASCIIStringEncoding]];
    } else if (sheet == rrmWc.window) {
        [sheets removeObjectForKey:
         [NSString stringWithCString:class_getName([RoomRemoveMembersWindowController class])
                            encoding:NSASCIIStringEncoding]];
    }
}

// Shortcut function for when we don't care about creating the controller
// (used to determine whether the controller currently exists)
- (NSWindowController *)getSheetControllerForClass:(Class)class {
    return [self getSheetControllerForClass:class andNib:nil create:NO];
}

- (NSWindowController *)getSheetControllerForClass:(Class)class andNib:(NSString *)nibName create:(BOOL)create {
    NSString *className = [NSString stringWithCString:class_getName(class) encoding:NSASCIIStringEncoding];
    NSWindowController *wc = [sheets objectForKey:className];
    if (!wc && create) {
        wc = [[class alloc] initWithWindowNibName:nibName];
        [sheets setValue:wc forKey:className];
    }
    return wc;
}

- (void)handleCreateRoomClose:(CreateRoomWindowController *)wc returnCode:(NSInteger)returnCode {
    if (returnCode == SHOW_PERMS_RETURN_CODE) {
        // If the sheet is ending due to selecting private-room perms, show the
        // manage perms sheet (we'll re-start create room after manage perms closes)
        // Delay a short moment so that the create room sheet has a chance to close
        int64_t delayInSeconds = 0.2;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            ManagePermissionsWindowController *mpWc = (ManagePermissionsWindowController *)
            [self getSheetControllerForClass:[ManagePermissionsWindowController class]
                                      andNib:@"ManagePermissionsSheet"
                                      create:YES];
            [mpWc setPerms:wc.perms];
            [self showSheetWithClass:[ManagePermissionsWindowController class] andNib:@"ManagePermissionsSheet"];
        });
    } else {
        // Otherwise, remove the reference to the create room and manage perms sheets
        [sheets removeObjectForKey:
         [NSString stringWithCString:class_getName([CreateRoomWindowController class])
                            encoding:NSASCIIStringEncoding]];
        [sheets removeObjectForKey:
         [NSString stringWithCString:class_getName([ManagePermissionsWindowController class])
                            encoding:NSASCIIStringEncoding]];
    }
}

- (void)handleJoinChatClose:(JoinChatWindowController *)wc returnCode:(NSInteger)returnCode {
    [sheets removeObjectForKey:
     [NSString stringWithCString:class_getName([JoinChatWindowController class])
                        encoding:NSASCIIStringEncoding]];
}

- (void)handleManagePermsClose:(ManagePermissionsWindowController *)wc returnCode:(NSInteger)returnCode {
    CreateRoomWindowController *crWc = (CreateRoomWindowController *)
        [self getSheetControllerForClass:[CreateRoomWindowController class]];
    if (crWc) {
        // If we still have the create room sheet, restart the sheet and update the perms on it
        [crWc handleManagePermsClose:wc returnCode:returnCode];
        [self showSheetWithClass:[CreateRoomWindowController class] andNib:@"CreateRoomSheet"];
    } else {
        // Otherwise, just clear out our reference to the manage perms controller
        [sheets removeObjectForKey:
         [NSString stringWithCString:class_getName([ManagePermissionsWindowController class])
                            encoding:NSASCIIStringEncoding]];
    }
}

- (NSWindowController *)showSheetWithClass:(Class)class andNib:(NSString *)nibName {
    NSWindowController *wc = [self getSheetControllerForClass:class andNib:nibName create:YES];

    [NSApp beginSheet:wc.window
       modalForWindow:self.window
        modalDelegate:self
       didEndSelector:@selector(didEndSheet:returnCode:contextInfo:)
          contextInfo:nil];
    [wc.window setPreventsApplicationTerminationWhenModal:NO];
    [NSApp runModalForWindow:wc.window];

    return wc;
}

- (BOOL)hideSheetWithClass:(Class)class {
    NSString *className = [NSString stringWithCString:class_getName(class) encoding:NSASCIIStringEncoding];
    NSWindowController *wc = [sheets objectForKey:className];
    if (wc) {
        [wc.window orderOut:nil];
        return YES;
    }

    return NO;
}

- (void)onFocusChat:(NSNotification *)notif {
    XMPPJID *jid = [[notif userInfo] objectForKey:@"jid"];
    if (!jid) {
        return;
    }

    HipChatOSXApp *app = HipChatOSXApp.instance;
    if ([OSXHelpers isRoomJid:jid]) {
        Room *room = [app getRoomInfo:jid];
        if (!room) {
            return;
        }
        [self.window setTitle:[NSString stringWithFormat:@"HipChat - %@", room.name]];
    } else if ([OSXHelpers isUserJid:jid]) {
        User *user = [app getUserInfo:jid];
        if (!user) {
            return;
        }

        [self.window setTitle:[NSString stringWithFormat:@"HipChat - %@", user.name]];
    }
}

- (void)onFocusLobby:(NSNotification *)notif {
    [self.window setTitle:@"HipChat - Lobby"];
}

- (void)onShowChangePrivacy:(NSNotification *)notif {
    ChangePrivacyWindowController *crWc = (ChangePrivacyWindowController *)
    [self getSheetControllerForClass:[ChangePrivacyWindowController class]
                              andNib:@"ChangePrivacySheet"
                              create:YES];

    XMPPJID *jid = [notif.userInfo objectForKey:@"jid"];
    [crWc setJid:jid];

    [self showSheetWithClass:[ChangePrivacyWindowController class] andNib:@"ChangePrivacySheet"];
}

- (void)onShowCreateRoom:(NSNotification *)notif {
    CreateRoomWindowController *crWc = (CreateRoomWindowController *)
        [self getSheetControllerForClass:[CreateRoomWindowController class]
                                  andNib:@"CreateRoomSheet"
                                  create:YES];

    XMPPJID *jid = [notif.userInfo objectForKey:@"jid"];
    if (jid) {
        [crWc setJid:jid];
    }

    [self showSheetWithClass:[CreateRoomWindowController class] andNib:@"CreateRoomSheet"];
}

- (void)onShowJoinChat:(NSNotification *)notif {
    [self showSheetWithClass:[JoinChatWindowController class] andNib:@"JoinChatSheet"];
}

- (void)onShowManagePerms:(NSNotification *)notif {
    ManagePermissionsWindowController *mpWc = (ManagePermissionsWindowController *)
        [self getSheetControllerForClass:[ManagePermissionsWindowController class]
                                  andNib:@"ManagePermissionsSheet"
                                  create:YES];

    CreateRoomWindowController *crWc = (CreateRoomWindowController *)
        [self getSheetControllerForClass:[CreateRoomWindowController class]];
    if (crWc) {
        [mpWc setPerms:crWc.perms];
    }
    
    [self showSheetWithClass:[ManagePermissionsWindowController class] andNib:@"ManagePermissionsSheet"];
}

- (void)onShowRenameRoom:(NSNotification *)notif {
    RenameRoomWindowController *rrWc = (RenameRoomWindowController *)
    [self getSheetControllerForClass:[RenameRoomWindowController class]
                              andNib:@"RoomRenameSheet"
                              create:YES];

    XMPPJID *jid = [notif.userInfo objectForKey:@"jid"];
    [rrWc setJid:jid];

    [self showSheetWithClass:[RenameRoomWindowController class] andNib:@"RoomRenameSheet"];
}

- (void)onShowRoomInvite:(NSNotification *)notif {
    RoomInviteWindowController *riWc = (RoomInviteWindowController *)
        [self getSheetControllerForClass:[RoomInviteWindowController class]
                                  andNib:@"RoomInviteSheet"
                                  create:YES];

    XMPPJID *jid = [notif.userInfo objectForKey:@"jid"];
    if (jid) {
        [riWc setJid:jid];
    }

    [self showSheetWithClass:[RoomInviteWindowController class] andNib:@"RoomInviteSheet"];
}

- (void)onShowRoomRemoveMembers:(NSNotification *)notif {
    RoomRemoveMembersWindowController *rrmWc = (RoomRemoveMembersWindowController *)
    [self getSheetControllerForClass:[RoomRemoveMembersWindowController class]
                              andNib:@"RoomRemoveMembersSheet"
                              create:YES];

    XMPPJID *jid = [notif.userInfo objectForKey:@"jid"];
    if (jid) {
        [rrmWc setJid:jid];
    }

    [self showSheetWithClass:[RoomRemoveMembersWindowController class] andNib:@"RoomRemoveMembersSheet"];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark NSWindowDelegate functions
///////////////////////////////////////////////////////////////////////

- (void)windowDidBecomeKey:(NSNotification *)notification {
    // Only have cmd-w close tabs when the main window is focused
    [OSXHelpers setCommandWClosesTabs:YES];

    HipChatOSXApp *app = HipChatOSXApp.instance;
    if (app.currentJid && app.currentJid != app.lobbyJid) {
        [app clearUnreadCount:app.currentJid];
    }
}

- (void)windowDidResignKey:(NSNotification *)notification {
    // Only have cmd-w close tabs when the main window is focused
    [OSXHelpers setCommandWClosesTabs:NO];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark WebView Delegate functions
///////////////////////////////////////////////////////////////////////

- (void)webView:(WebView *)webView
decidePolicyForNavigationAction:(NSDictionary *)actionInformation
        request:(NSURLRequest *)request
          frame:(WebFrame *)frame
decisionListener:(id < WebPolicyDecisionListener >)listener {

	NSString *requestString = [[request URL] absoluteString];
	NSArray *components = [requestString componentsSeparatedByString:@":"];
	if ([requestString rangeOfString:@"action"].location == 0 && [components count] > 1) {
		if ([[components objectAtIndex:1] isEqualToString:@"reconnect"]) {
            [HipChatOSXApp.instance triggerManualReconnect];
		}
        return;
	}
    return;
}

- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame *)webFrame {
    if([webFrame isEqual:[self.noticeWebView mainFrame]]) {
        // Get size of webview content
        NSRect webFrameRect = [[[webFrame frameView] documentView] frame];

        // Set the box to the correct height
        [self.noticeBox setFrameSize:NSMakeSize(NSWidth(self.noticeBox.frame),
                                                NSHeight(webFrameRect)+14.0)];

        // Set the webview to the correct height and position
        // (for some reason autoresizing doesn't like to keep the webview properly positioned)
        NSRect boxFrame = self.noticeBox.frame;
        [self.noticeWebView setFrame:NSMakeRect(7.0, 7.0, boxFrame.size.width-14.0, boxFrame.size.height-14.0)];
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark NSAlert Delegate functions
///////////////////////////////////////////////////////////////////////

- (void)alertDidEnd:(NSAlert *)alert returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo {
    if (returnCode == NSAlertFirstButtonReturn) {
        [HipChatOSXApp.instance joinChat:inviteJid withFocus:YES];
    } else {
        // Do nothing
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Notification handlers
///////////////////////////////////////////////////////////////////////

- (void)handleFlash:(NSNotification *)notif {
    NSString *message = [[notif userInfo] objectForKey:@"message"];
    if (!message) {
        DDLogError(@"Received flash notification with no message. WTF Mate?");
        return;
    }
    NSNumber *duration = [[notif userInfo] objectForKey:@"duration"];
    if (duration) {
        [self showFlashMessage:message duration:[duration floatValue]];
    } else {
        [self showFlashMessage:message];
    }
}

- (void)handleFlashTimer:(NSTimer *)timer {
    flashTimer = nil;
    [self hideFlashMessage];
}

- (void)handleInvite:(NSNotification *)notif {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    NSDictionary *args = [notif userInfo];
    XMPPJID *roomJid = [args objectForKey:@"roomJid"];
    XMPPJID *sender = [args objectForKey:@"sender"];
    NSString *reason = [args objectForKey:@"reason"];
    NSString *senderName = [app getDisplayName:sender abbreviated:NO];
    Room *room = [app getRoomInfo:roomJid];
    if (!room) {
        return;
    }

    inviteJid = roomJid;
    NSString *title = [NSString stringWithFormat:@"%@ has invited you to join the room %@",
                       senderName, room.name];
    NSAlert *alert = [[NSAlert alloc] init];
    [alert addButtonWithTitle:@"Join"];
    [alert addButtonWithTitle:@"Ignore"];
    [alert setMessageText:title];
    if (reason && reason.length > 0) {
        [alert setInformativeText:[NSString stringWithFormat:@"Reason: %@", reason]];
    }
    [alert setAlertStyle:NSInformationalAlertStyle];

    [alert beginSheetModalForWindow:[self window]
                      modalDelegate:self
                     didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:)
                        contextInfo:nil];
}

- (void)hipChatAppConnectFailed:(NSError *)err {
    if (!HipChatOSXApp.instance.doReconnect) {
        // Don't show disconnect popup if we're not going to try reconnecting
		DDLogInfo(@"Connect failed - not trying to reconnect - not showing disconnect notice");
        return;
    }

    if (self.window && self.window.isVisible) {
        [HipChatOSXApp.instance showReconnectFlashWithMessage:[err localizedDescription]];
    }
}

- (void)hipChatAppConnectSucceeded {
    [self hideFlashMessage];
}

- (void)onAppBecomeActive:(NSNotification *)notif {
    if ([NSApp keyWindow] == self.window || [NSApp keyWindow] == nil) {
        [HipChatOSXApp.instance focusCurrentChatInput];
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Helper methods
///////////////////////////////////////////////////////////////////////

- (void)hideFlashMessage {
    [[self.noticeBox animator] setAlphaValue:0.0];

    // Delay actually setting the hidden value of the box to allow the animation to complete
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self.noticeBox setHidden:YES];
    });
}

- (void)showFlashMessage:(NSString *)message {
    [self showFlashMessage:message duration:FLASH_DISPLAY_SECONDS];
}

- (void)showFlashMessage:(NSString *)message duration:(NSTimeInterval)duration {
    // Need to include style block so we don't show in Times New Roman (dumb)
    NSString *html = [NSString stringWithFormat:@"<html><head>%@</head><body>%@</body></html>",
                      htmlHead,
                      message];
    self.noticeWebView.policyDelegate = nil;
    [self.noticeWebView.mainFrame loadHTMLString:html baseURL:[NSURL URLWithString:@"http://www.hipchat.com"]];
    self.noticeWebView.policyDelegate = self;
    
    // Fade in animation
    if (self.noticeBox.isHidden) {
        [self.noticeBox setAlphaValue:0.0];
        [self.noticeBox setHidden:NO];
        [[self.noticeBox animator] setAlphaValue:1.0];
    }
    
    // Set a timer to hide the flash after hideDelay
    // Don't autohide if delay <= 0
    [flashTimer invalidate];
    if (duration > 0) {
        flashTimer = [NSTimer scheduledTimerWithTimeInterval:duration
                                                      target:self
                                                    selector:@selector(handleFlashTimer:)
                                                    userInfo:nil
                                                     repeats:NO];
    }
}

@end
