//
//  FileEntry.m
//  HipChat
//
//  Created by Christopher Rivers on 4/1/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "FileEntry.h"

#import "HipChatOSXApp.h"
#import "OSXHelpers.h"
#import "User.h"
#import "XMPP.h"

@implementation FileEntry

//    Response format for files:
//    <iq from="1_1@chat.hipchat.com" id="1" type="result">
//        <query xmlns="http://hipchat.com/protocol/files">
//            <item>
//                <id>2</id>
//                <name>10803/29833/randomchars/lolwut_thumb.png</name>
//                <thumbnail>10803/29833/rnd/lolwut_thumb.png</thumbnail>
//                <bucket>downloads.hipchat.com<bucket>
//                <size>1803</size>
//                <desc>OMG LOLWUT</desc>
//                <group_id>10803</group_id>
//                <user_id>29833</user_id>
//                <date>2013-05-31T21:08:38Z</date>
//            </item>
//            <item>...</item>
//            <item>...</item>
//            <item>...</item>
//        </query>
//    </iq>
- (id)initWithItem:(NSXMLElement *)item {
    self = [super init];
    if (self) {
        self.entryId = [[item elementForName:@"id"] stringValue];
        NSString *bucket = [[item elementForName:@"bucket"] stringValue];
        NSString *path = [[item elementForName:@"name"] stringValue];
        NSString *filename = [[path componentsSeparatedByString:@"/"] lastObject];
        NSString *url = [[item elementForName:@"url"] stringValue];
        self.url = (url && url.length > 0 ? [NSURL URLWithString:url] : [NSURL URLWithString:[OSXHelpers fileUrlWithBucket:bucket andPath:path]]);
        self.name = [OSXHelpers linkStyleWithText:filename font:nil alignment:NSLeftTextAlignment url:self.url];
        // Don't include the png in the file name (since we're using imageNamed with .tif files now)
        self.icon = [[OSXHelpers getFileTypeIcon:filename] stringByReplacingOccurrencesOfString:@".png" withString:@""];

        XMPPJID *userJid = [OSXHelpers jidFromUserId:[[item elementForName:@"user_id"] stringValue]];
        User *user = [HipChatOSXApp.instance getUserInfo:userJid];
        self.userName = (user ? user.name : @"Unknown");
        self.uploadDate = [OSXHelpers parseDate:[[item elementForName:@"date"] stringValue]];
    }
    return self;
}

@end
