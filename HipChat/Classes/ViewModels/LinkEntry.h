//
//  FileEntry.h
//  HipChat
//
//  Created by Christopher Rivers on 4/1/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "RightSidebarEntry.h"

@interface LinkEntry : RightSidebarEntry

- (id)initWithItem:(NSXMLElement *)item;

@property NSMutableAttributedString *urlString;
@property NSURL *url;
@property NSString *userName;

@end
