#!/bin/bash

# Script to create a template .dmg file using an existing .app package 
# HipChat.app must be located in this directory
# Script will output HipChat-Template.dmg
# To prepare the template, follow the manual steps below the hdiutil command

# HipChat-Template.dmg is used in the dmgcreate.sh script to create 
# version-specific .dmg package for a given .app file

SOURCE=HipChat.app
TITLE=HipChat
SIZE=`du -k -d 0 HipChat.app | cut -f -1`
SIZE=$(($SIZE + 3000))

hdiutil create -srcfolder "${SOURCE}" -volname "${TITLE}" -fs HFS+ \
      -fsargs "-c c=64,a=16,e=16" -format UDRW -size ${SIZE}k HipChat-Template.dmg

# Mount HipChat-Template.dmg

#mkdir /Volumes/HipChat/.background
#cp DMG_installer.tiff /Volumes/HipChat/.background/hc_dmg_background.tiff
#ln -s /Applications /Volumes/HipChat/

# Use Finder to manually set the icon size to 128x128
# Use Finder to hide the toolbar and remove the sidebar and resize the window to fix the background image

# Unmount the DMG

#tar -zcvf HipChat-Template.dmg.tgz HipChat-Template.dmg
