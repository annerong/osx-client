//
//  LobbyViewController.h
//  HipChat
//
//  Created by Christopher Rivers on 10/18/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class PrettyBorderBox;
@class WebView;
@class XMPPJID;

@interface LobbyViewController : NSViewController <NSTableViewDelegate> {
    NSMutableArray *entries;
    WebView *latestNewsWebView;
    NSTimer *rearrangeTimer;
    NSDictionary *alertArgs;
}

- (IBAction)lobbyEntryClickedForJid:(XMPPJID *)jid;
- (IBAction)handleArchiveRoom:(XMPPJID *)jid;
- (IBAction)handleChangePrivacy:(XMPPJID *)jid;
- (IBAction)handleDeleteRoom:(XMPPJID *)jid;
- (IBAction)handleInviteUsers:(XMPPJID *)jid;
- (IBAction)handleRemoveUsers:(XMPPJID *)jid;
- (IBAction)handleRenameRoom:(XMPPJID *)jid;
- (IBAction)handleUnarchiveRoom:(XMPPJID *)jid;

@property IBOutlet NSMutableArray *entries;
@property IBOutlet NSArrayController *lobbyArrayController;
@property IBOutlet PrettyBorderBox *bottomBox;
@property IBOutlet NSTableView *tableView;

@end
