//
//  JoinChatWindowController.m
//  HipChat
//
//  Created by Christopher Rivers on 1/4/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "JoinChatWindowController.h"

#import "RoomList.h"
#import "XMPPJID.h"

#import "ChatFilterArrayController.h"
#import "HipChatOSXApp.h"
#import "HipChatUser.h"
#import "OSXHelpers.h"

@interface JoinChatWindowController ()

@end

@implementation JoinChatWindowController

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Initialization
///////////////////////////////////////////////////////////////////////

- (void)windowDidLoad {
    [super windowDidLoad];

    HipChatOSXApp *app = HipChatOSXApp.instance;

    // Check if this group allows private chat
    // If not, don't add users to the search list
    BOOL canPrivateChat = [app.currentUser getPerm:PERM_PRIVATE_CHAT];

    NSDictionary *userList = (canPrivateChat ? [app getAllUsers] : [NSDictionary dictionary]);
    NSDictionary *roomList = [app getAllRooms];
    self.chatList = [NSMutableArray arrayWithCapacity:(userList.count + roomList.count)];
    for (id key in userList) {
        User *user = [userList objectForKey:key];
        [self.chatList addObject:user];
    }
    for (id key in roomList) {
        Room *room = [roomList objectForKey:key];
        [self.chatList addObject:room];
    }
    [self.chatList sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        BOOL isUser1 = [OSXHelpers isUserJid:[obj1 performSelector:@selector(jid)]];
        BOOL isUser2 = [OSXHelpers isUserJid:[obj2 performSelector:@selector(jid)]];
        if (isUser1 && !isUser2) {
            return NSOrderedAscending;
        } else if (isUser2 && !isUser1) {
            return NSOrderedDescending;
        }
        
        NSString *name1 = [obj1 performSelector:@selector(name)];
        NSString *name2 = [obj2 performSelector:@selector(name)];
        return [name1 compare:name2];
    }];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Helper functions
///////////////////////////////////////////////////////////////////////

- (void)closeSheet:(NSInteger)returnCode {
    // If we're not cancelling, make sure something was selected
    // Otherwise, beep
    if (returnCode == NSOKButton) {
        NSInteger selectedIndex = self.arrayController.selectionIndex;
        if (selectedIndex < 0 || selectedIndex > self.chatList.count) {
            NSBeep();
            return;
        } else {
            XMPPJID *jid = self.selectedJid;
            if (jid) {
                HipChatOSXApp *app = HipChatOSXApp.instance;
                [app joinChat:jid withFocus:YES];
            }
        }
    }

    self.searchField.stringValue = @"";
    [self hideAutoComplete];
    [super closeSheet:returnCode];
}

// This gets called by the parent class whenever someone hits return with an item selected from autocomplete
// Return: NO = continue on to default behavior, YES = don't perform default behavior
- (BOOL)handleAutocompleteReturn:(id)selectedItem {
    NSString *name = [selectedItem performSelector:@selector(name)];
    self.searchField.stringValue = name;
    [self hideAutoComplete];
    return NO;
}

// This gets called by the parent class whenever someone hits tab with an item selected from autocomplete
// Return: NO = continue on to default behavior, YES = don't perform default behavior
- (BOOL)handleAutocompleteTab:(id)selectedItem {
    [self handleAutocompleteReturn:selectedItem];
    return NO;
}

- (void)hideAutoComplete {
    [self.autoComplete close];
}

- (XMPPJID *)selectedJid {
    id selectedItem = [self.arrayController.arrangedObjects
                       objectAtIndex:self.arrayController.selectionIndex];
    if (selectedItem) {
        return [selectedItem performSelector:@selector(jid)];
    }

    return nil;
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark NSTextFieldDelegate functions
///////////////////////////////////////////////////////////////////////

- (void)controlTextDidChange:(NSNotification *)aNotification {
    NSString *searchStr = self.searchField.stringValue;
    if (!searchStr || searchStr.length == 0) {
        [self hideAutoComplete];
    } else {
        [self.arrayController search:searchStr];
        if (((NSArray *)self.arrayController.arrangedObjects).count == 0) {
            [self hideAutoComplete];
        } else {
            [self showAutoCompleteUnderInput:self.searchField];
        }
    }
}

@end
