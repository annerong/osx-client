//
//  RightSidebarViewController.h
//  HipChat
//
//  Created by Christopher Rivers on 4/1/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class FilesViewController;
@class LinksViewController;
@class ProfileViewController;
@class RosterViewController;

@interface RightSidebarViewController : NSViewController {
    FilesViewController *filesVC;
    LinksViewController *linksVC;
    ProfileViewController *profileVC;
    RosterViewController *rosterVC;
}

- (void)showPrivateChatSidebar;
- (void)showRoomSidebar;
- (void)showLobbySidebar;

- (IBAction)handleTabChange:(id)sender;

@property IBOutlet NSSegmentedControl *tabs;
@property IBOutlet NSView *displayedView;
@property NSInteger selectedTabIndex;

@end
