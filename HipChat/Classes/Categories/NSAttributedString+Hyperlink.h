//
//  NSAttributedString+Hyperlink.h
//  HipChat
//
//  Created by Christopher Rivers on 10/22/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSAttributedString (Hyperlink)

+(id)hyperlinkFromString:(NSString*)inString withURL:(NSURL*)aURL;

@end
