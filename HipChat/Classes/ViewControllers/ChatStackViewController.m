//
//  ChatStackViewController.m
//  HipChat
//
//  Created by Christopher Rivers on 10/23/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "ChatStackViewController.h"

#import <WebKit/WebKit.h>

#import "Linkify.h"
#import "Room.h"
#import "XMPPFramework.h"

#import "ChatViewController.h"
#import "HipChatOSXApp.h"
#import "HipChatTextView.h"
#import "OSXHelpers.h"
#import "PrettyBorderBox.h"
#import "FocusOnlyBox.h"

@interface ChatStackViewController ()

@end

@implementation ChatStackViewController

+ (BOOL)isSelectorExcludedFromWebScript:(SEL)aSelector {
    if (aSelector == @selector(topicDoubleClicked)) return NO;
    return YES;
}

- (void)loadView {
    [super loadView];

    self.topBox.cornerRadius = 0;
    self.topBox.bottomBorderColor = RGB(158.0, 158.0, 158.0);

    // This makes absolutely no sense, but if this webview is setup in the NIB,
    // sometimes it causes the chat views to either 1) not load or 2) not load images
    // Workaround: manually initialize the web view and arrange it here (dumb)
    // Same issue happened with the disconnection popup webview!
    NSSize boxSize = self.topBox.frame.size;
    NSRect topicFrame = NSMakeRect(10.0, 0, (boxSize.width-30.0-34.0), (boxSize.height));
    self.topicWebView = [[WebView alloc] initWithFrame:topicFrame];
    [self.topBox addSubview:self.topicWebView];
    self.topicWebView.drawsBackground = NO;
    self.topicWebView.autoresizingMask = (NSViewMinXMargin |
                                          NSViewWidthSizable |
                                          NSViewMaxXMargin |
                                          NSViewMinYMargin |
                                          NSViewHeightSizable |
                                          NSViewMaxYMargin);
    [self.topicWebView.mainFrame.frameView setAllowsScrolling:NO];
    [[self.topicWebView windowScriptObject] setValue:self forKey:@"osxConnector"];
    [self.topicWebView setToolTip:@"Double click to edit topic"];

    NSString *initialHtml = [NSString stringWithFormat:@"<html><head><style>"
        "* { font-family: Lucida Grande,arial,sans-serif; font-size: 12px; line-height: %ldpx; margin: 0; padding: 0; }"
        "p { font-family: Lucida Grande,arial,sans-serif; font-size: 12px; line-height: %ldpx; }"
        "</style></head>"
        "<body ondblclick='window.osxConnector.topicDoubleClicked();'></body>"
        "</html>", (long)floorf(boxSize.height), (long)floorf(boxSize.height)];
    [self.topicWebView.mainFrame loadHTMLString:initialHtml baseURL:nil];
    self.topicWebView.policyDelegate = self;

    // FocusOnlyBox uses a delegate to inform us when the box lost focus
    // It will hide itself automatically when this happens
    self.topicBox.delegate = self;
    self.searchBox.delegate = self;

    [self.searchToggle.cell addObserver:self forKeyPath:@"state" options:NSKeyValueObservingOptionNew context:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        HipChatOSXApp *app = HipChatOSXApp.instance;
        chatToFocus = nil;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFocusChat:) name:HipChatFocusChatNotification object:app];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFocusLobby:) name:HipChatFocusLobbyNotification object:app];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onJoinChat:) name:HipChatJoinChatNotification object:app];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onShowSearch:) name:HipChatShowSearchNotification object:app];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onChangeTopic:) name:HipChatShowChangeTopicNotification object:app];
    }

    return self;
}

- (void)dealloc {
    [self.searchToggle.cell removeObserver:self];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Notification handlers
///////////////////////////////////////////////////////////////////////

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"topic"]) {
        [self setTopic:[object topic]];
    } else if ([keyPath isEqualToString:@"state"]) {
        BOOL isDepressed = (self.searchToggle.state == NSOnState);
        [self.searchBox setHidden:!isDepressed];
        if (isDepressed) {
            [self.view.window makeFirstResponder:self.searchInput];
        }
    }
}

- (void)onChangeTopic:(NSNotification *)notif {
    [self showTopicEdit];
}

- (void)onFocusChat:(NSNotification *)notif {
    XMPPJID *jid = [[notif userInfo] objectForKey:@"jid"];
    if (jid) {
        // Focus chat may also be called with an index. If that's the case,
        // ignore this focus - the TabViewController will let us know which jid to focus
        [self focusChatWithJid:jid];
    }
}

- (void)onFocusLobby:(NSNotification *)notif {
    [currentChat performSelector:@selector(willHide)];
    [currentChat performSelector:@selector(didHide)];
    [currentChat.view removeFromSuperview];
    currentChat = nil;
    
    // Stop watching for topic changes on the current room
    [currentRoom removeObserver:self forKeyPath:@"topic"];
    currentRoom = nil;
}

- (void)onJoinChat:(NSNotification *)notif {
    BOOL doFocus = [[[notif userInfo] objectForKey:@"doFocus"] boolValue];
    if (!doFocus) {
        return;
    }

    XMPPJID *jid = [[notif userInfo] objectForKey:@"jid"];
    [self focusChatWithJid:jid];
}

- (void)onShowSearch:(NSNotification *)notif {
    [self.searchToggle setState:NSOnState];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark WebView Delegate functions
///////////////////////////////////////////////////////////////////////

- (void)webView:(WebView *)webView
decidePolicyForNavigationAction:(NSDictionary *)actionInformation
        request:(NSURLRequest *)request
          frame:(WebFrame *)frame
decisionListener:(id < WebPolicyDecisionListener >)listener {
    [[NSWorkspace sharedWorkspace] openURL:[request URL]];
}

- (void)topicDoubleClicked {
    [self showTopicEdit];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark FocusOnlyBox delegate functions
///////////////////////////////////////////////////////////////////////

- (void)focusBoxHidden:(id)sender {
    if (sender == self.topicBox) {
        [self.topicWebView setHidden:NO];
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark UI handlers
///////////////////////////////////////////////////////////////////////

- (IBAction)handleCancelTopic:(id)sender {
    [self.topicInput setStringValue:@""];
    [self hideTopicEdit];
}

- (IBAction)handleSearch:(id)sender {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    XMPPJID *currentJid = app.currentJid;
    if (currentJid == HipChatOSXApp.instance.lobbyJid) {
        DDLogError(@"Got search event when current jid is lobby - this shouldn't happen");
        return;
    }

    NSString *searchArg = self.searchInput.stringValue;
    if (searchArg.length == 0) {
        return;
    }

    NSString *idArg = @"";
    if ([OSXHelpers isRoomJid:currentJid]) {
        Room *room = [app getRoomInfo:currentJid];
        idArg = [NSString stringWithFormat:@"rid-%@", room.roomId];
    } else if ([OSXHelpers isUserJid:currentJid]) {
        idArg = [NSString stringWithFormat:@"uid-%@", [OSXHelpers getJidUserId:currentJid]];
    }

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/search?q=%@&t=%@",
                                       app.webHost,
                                       [searchArg stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                                       [idArg stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    [[NSWorkspace sharedWorkspace] openURL:url];

    [self.searchInput setStringValue:@""];
    [self hideSearch];
}

- (IBAction)handleSetTopic:(id)sender {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    Room *room = [app getRoomInfo:app.currentJid];
    if (!room) {
        return;
    }

    [room setNewTopic:self.topicInput.stringValue];
    [self setTopic:self.topicInput.stringValue];
    [self hideTopicEdit];
}

- (BOOL)control:(NSControl *)control textView:(NSTextView *)textView doCommandBySelector:(SEL)command {
    // Handle the ESC key being pressed while editing topic or searching
    if (command == @selector(cancelOperation:)) {
        [self hideTopicEdit];
        [self hideSearch];
        return YES;
    }
    return NO;
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Helper methods
///////////////////////////////////////////////////////////////////////

- (void)doFocusChatWithJid:(XMPPJID *)jid {
    if (!self.chatView) {
        chatToFocus = jid;
        return;
    }

    HipChatOSXApp *app = HipChatOSXApp.instance;
    ChatViewController *cvcToFocus = [app getActiveChat:jid];
    NSView *viewToFocus = cvcToFocus.view;
    if (!cvcToFocus || !viewToFocus) {
        DDLogError(@"Failed to get viewController (%@) or view (%@) when trying to focus chat for jid: %@",
                   cvcToFocus, viewToFocus, jid);
        return;
    }

    [viewToFocus setAutoresizingMask:NSViewMaxXMargin | NSViewMaxYMargin | NSViewMinXMargin | NSViewMinXMargin | NSViewWidthSizable | NSViewHeightSizable];
    [viewToFocus setFrame:self.chatView.frame];

    if (currentChat && [currentChat.jid.bare isEqualToString:jid.bare]) {
        DDLogInfo(@"Chat is already selected - no views to swap out");
        return;
    }

    [currentChat performSelector:@selector(willHide)];
    [cvcToFocus performSelector:@selector(willShow)];
    if (currentChat) {
        NSView *currentChatView = currentChat.view;
        [self.chatView replaceSubview:currentChatView with:viewToFocus];
    } else {
        [self.chatView addSubview:viewToFocus];
    }

    // Stop watching for topic changes on the current room
    [currentRoom removeObserver:self forKeyPath:@"topic"];

    if ([OSXHelpers isRoomJid:jid]) {
        // If we're switching to a room, switch the topic and watch for topic changes
        Room *room = [app getRoomInfo:jid];
        currentRoom = room;
        [room addObserver:self forKeyPath:@"topic" options:NSKeyValueObservingOptionNew context:nil];
        [self setTopic:room.topic];
    } else {
        // For non-rooms (1:1 chats), clear out the topic
        currentRoom = nil;
        [self setTopic:@""];
    }
    [self hideTopicEdit];
    [self hideSearch];
    
    //Update mute status
    [HipChatOSXApp.instance updateMuteMenuStatus:jid];

    [currentChat performSelector:@selector(didHide)];
    [cvcToFocus performSelector:@selector(didShow)];
    currentChat = cvcToFocus;
}

- (void)focusChatWithJid:(XMPPJID *)jid {
    // Make sure we do the focusing on the main thread. This might be called from a separate
    // thread like in the case of handling an new 1:1 message
    // When accessing CVC.view, it creates and initializes the WebView, which
    // must be done on the main thread.

    // Always use dispatch here - it fixes the issue with the first chat
    // not correctly resizing to fill the chat view area available
    // TODO: Figure out why this happens when calling focus immediately
    dispatch_async(dispatch_get_main_queue(), ^{
        [self doFocusChatWithJid:jid];
    });
}

- (void)hideSearch {
    [self.searchToggle setState:NSOffState];
    [HipChatOSXApp.instance focusCurrentChatInput];
}

- (void)hideTopicEdit {
    [self.topicBox setHidden:YES];
    [self.topicWebView setHidden:NO];
}

- (void)setTopic:(NSString *)topicString {
    dispatch_block_t block = ^{
        if (topicString && topicString.length > 0) {
            NSString *escapedString = [Helpers escape:topicString];
            NSString *linkifiedTopic = [Linkify linkify:escapedString withEmotions:YES andTruncate:NO matches:nil];
            self.topicWebView.mainFrame.DOMDocument.body.innerHTML = linkifiedTopic;
        } else {
            self.topicWebView.mainFrame.DOMDocument.body.innerHTML = @"";
        }
    };

    if (dispatch_get_current_queue() == dispatch_get_main_queue()) {
        block();
    } else {
        dispatch_async(dispatch_get_main_queue(), block);
    }
}

- (void)showTopicEdit {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    Room *room = [app getRoomInfo:app.currentJid];
    if (!room) {
        return;
    }

    [self.topicWebView setHidden:YES];
    [self.topicBox setHidden:NO];
    [self.topicInput setStringValue:room.topic];
    [self.view.window makeFirstResponder:self.topicInput];
    [self.topicInput selectText:self];
}

@end
