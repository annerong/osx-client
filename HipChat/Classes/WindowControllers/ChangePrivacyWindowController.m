//
//  ChangePrivacyWindowController.m
//  HipChat
//
//  Created by Christopher Rivers on 4/18/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "ChangePrivacyWindowController.h"

#import "HipChatOSXApp.h"
#import "Room.h"
#import "RoomList.h"
#import "XMPPIQ.h"

@implementation ChangePrivacyWindowController

- (void)windowDidLoad {
    if (!self.jid) {
        DDLogError(@"No jid was set when changing room privacy");
        [self closeSheet:NSCancelButton];
    }
    HipChatOSXApp *app = HipChatOSXApp.instance;
    Room *room = [app getRoomInfo:self.jid];
    if (!room) {
        DDLogError(@"No room data found when showing change privacy sheet. Aborting...");
        [app showFlash:@"Room data was not available. Cannot change privacy."];
        [self closeSheet:NSCancelButton];
    }

    // Select the proper radio button when the window opens
    initialPrivacy = room.privacy;
    if (room.isPublic) {
        [self.permText setStringValue:@"If you make this room private, "
            "the members currently chatting in the room "
            "will automatically receive access."];
        [self.radioPrivate deselectAllCells];
        [self.radioOpen selectCellAtRow:0 column:0];
    } else {
        [self.radioOpen deselectAllCells];
        [self.radioPrivate selectCellAtRow:0 column:0];
    }
}

- (void)closeSheet:(NSInteger)returnCode {
    // If we're not cancelling, make sure something was selected
    // Otherwise, beep
    if (returnCode == NSOKButton) {
        // If we're not cancelling, make sure we have all the correct data
        // Otherwise, beep
        if (!self.radioOpen.selectedCell && !self.radioPrivate.selectedCell) {
            NSBeep();
            return;
        }

        HipChatOSXApp *app = HipChatOSXApp.instance;
        Room *room = [app.roomList roomForJid:self.jid];
        if (!room) {
            [app showFlash:@"Rename failed. The room could not be found"];
        } else {
            NSString *privacy = (self.radioOpen.selectedCell ? @"public" : @"private");
            IQCallback callback = ^(XMPPIQ *iq) {
                if ([iq.type isEqualToString:@"error"]) {
                    [HipChatApp.instance showFlash:@"There was an error changing the room privacy setting."];
                } else {
                    [app showFlash:[NSString stringWithFormat:@"%@ is now %@", room.name, privacy]];
                }
            };
            [room changePrivacy:privacy callback:callback];
        }
    }

    [super closeSheet:returnCode];
}

- (IBAction)onSelectOpen:(id)sender {
    [self.radioPrivate deselectAllCells];
}

- (IBAction)onSelectPrivate:(id)sender {
    [self.radioOpen deselectAllCells];
}

@end
