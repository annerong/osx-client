//
//  FileEntry.h
//  HipChat
//
//  Created by Christopher Rivers on 4/1/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "RightSidebarEntry.h"

@interface FileEntry : RightSidebarEntry

- (id)initWithItem:(NSXMLElement *)item;

@property NSString *icon;
@property NSMutableAttributedString *name;
@property NSURL *url;
@property NSString *userName;

@end
