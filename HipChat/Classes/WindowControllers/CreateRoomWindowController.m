//
//  CreateRoomWindowController.m
//  HipChat
//
//  Created by Christopher Rivers on 1/4/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "CreateRoomWindowController.h"

#import "HipChatUser.h"
#import "ErrorMessageViewController.h"
#import "RoomList.h"
#import "Room.h"
#import "XMPPJID.h"

#import "HipChatOSXApp.h"
#import "ManagePermissionsWindowController.h"
#import "OSXHelpers.h"

@implementation CreateRoomWindowController

@dynamic perms;

- (void)windowDidLoad {
    [super windowDidLoad];

    previousPrivacy = @"open";
    if (self.jid) {
        Room *room = [HipChatOSXApp.instance getRoomInfo:self.jid];
        if (room) {
            [self.nameField setStringValue:room.name];
            if (room.topic) {
                [self.topicField setStringValue:room.topic];
            }
            if (room.isPrivate) {
                membersToAdd = room.membersToAdd;
                [self.radioOpen deselectAllCells];
                [self.radioPrivate selectCellAtRow:0 column:0];
                [self updatePermString];
            } else {
                membersToAdd = nil;
                [self.radioPrivate deselectAllCells];
                [self.radioOpen selectCellAtRow:0 column:0];
            }
        }
    }
}

- (IBAction)closeCreateRoom:(id)sender {
    NSInteger returnCode = NSOKButton;
    if (sender == self.cancelButton) {
        returnCode = NSCancelButton;
    } else {
        // If we're not cancelling, make sure we have all the correct data
        // Otherwise, beep
        if (![self validateFields]) {
            return;
        }
        [self createRoomWithCurrentFields];
    }

    self.nameField.stringValue = @"";
    [self.radioPrivate deselectAllCells];
    [self.radioOpen selectCellAtRow:0 column:0];

    [NSApp endSheet:self.window returnCode:returnCode];
    [self.window orderOut:self];
    [NSApp stopModal];
}

- (void)createRoomWithCurrentFields {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    XMPPJID *roomJid;

    // If the jid wasn't manually set already
    if (!self.jid) {
        roomJid = [OSXHelpers jidFromChatName:self.nameField.stringValue];
        // Make sure the jid is unique
        // NOTE: This isn't guaranteed to work - if the current user is trying to create a
        // room that results in the same jid as a private room they can't see, we won't
        // be able to detect the conflict (need to rely on the server error in this case)
        NSInteger count = 1;
        while ([app getRoomInfo:roomJid]) {
            roomJid = [OSXHelpers jidFromChatName:[NSString stringWithFormat:@"%@-%ld", self.nameField.stringValue, (long)count]];
            count++;
        }
    } else {
        roomJid = self.jid;
    }

    // If the jidFromChatName method can't create a jid - we can't go any further
    if (!roomJid) {
        return;
    }
    NSString *privacy = (self.radioOpen.selectedCell ? @"public" : @"private");
    RoomList *roomList = app.roomList;
    Room *newRoom = [roomList createNewRoomWithJid:roomJid
                                              name:self.nameField.stringValue
                                             topic:self.topicField.stringValue
                                           privacy:privacy];
    if ([privacy isEqualToString:@"private"]) {
        [newRoom setMembersToAdd:membersToAdd sendInvites:sendInvites];
    }
    [app joinChat:roomJid withFocus:YES];
    // TODO: Show invite UI after joining public rooms
}

- (void)handleManagePermsClose:(ManagePermissionsWindowController *)mpWc returnCode:(NSInteger)returnCode {
    if (returnCode == NSOKButton) {
        membersToAdd = mpWc.perms;
        [self updatePermString];
        sendInvites = (mpWc.invitesCheckbox.state == NSOnState);
    } else if (returnCode == NSCancelButton) {
        if ([previousPrivacy isEqualToString:@"open"]) {
            // Need to do this after the sheet is done - wait 0.2 seconds before switching radios
            [self.radioOpen selectCellAtRow:0 column:0];
            [self.radioPrivate deselectAllCells];
        }
    }
}

- (XMPPJID *)jid {
    return jid;
}

- (IBAction)onSelectOpen:(id)sender {
    [self.radioPrivate deselectAllCells];
    self.permissionsText.stringValue = @"Choose who has access...";
}

- (IBAction)onSelectPrivate:(id)sender {
    previousPrivacy = (self.radioOpen.selectedCell ? @"open" : @"private");
    [self.radioOpen deselectAllCells];
    [self.window orderOut:self];
    [NSApp stopModal];
    [NSApp endSheet:self.window returnCode:SHOW_PERMS_RETURN_CODE];
}

- (NSArray *)perms {
    if (!membersToAdd || membersToAdd.count == 0) {
        membersToAdd = [NSArray arrayWithObject:HipChatOSXApp.instance.currentUser.jid];
    }
    return membersToAdd;
}

- (void)setJid:(XMPPJID *)roomJid {
    jid = roomJid;
}

- (void)updatePermString {
    HipChatOSXApp *app = HipChatOSXApp.instance;
    XMPPJID *permJid = nil;
    NSMutableString *permString = [NSMutableString stringWithString:@"The following people will have access: "];
    NSUInteger numNames = (membersToAdd.count <= 9 ? membersToAdd.count : 8);
    for (NSUInteger i = 0; i < numNames; i++) {
        permJid = [membersToAdd objectAtIndex:i];
        if (i > 0) {
            [permString appendString:@", "];
        }
        [permString appendString:[app getDisplayName:permJid abbreviated:YES]];
    }
    
    NSInteger excessNames = membersToAdd.count - numNames;
    if (excessNames > 0) {
        [permString appendString:[NSString stringWithFormat:@", and %ld others", excessNames]];
    }
    self.permissionsText.stringValue = permString;
}

- (BOOL)validateFields {
    if (errorPopover) {
        [errorPopover close];
    }
    if (!self.nameField.stringValue || self.nameField.stringValue.length == 0) {
        NSBeep();
        return NO;
    }
    if (!self.radioOpen.selectedCell && !self.radioPrivate.selectedCell) {
        NSBeep();
        return NO;
    }

    if (![OSXHelpers validateChatName:self.nameField.stringValue]) {
        if (!errorPopover) {
            ErrorMessageViewController *evc = [[ErrorMessageViewController alloc] initWithNibName:@"ErrorMessage" bundle:nil];
            evc.message = @"That room name already exists. Please choose another.";
            errorPopover = [[NSPopover alloc] init];
            errorPopover.contentViewController = evc;
            errorPopover.animates = NO;
            errorPopover.behavior = NSPopoverBehaviorTransient;
        }
        [errorPopover showRelativeToRect:self.nameField.bounds ofView:self.nameField preferredEdge:NSMaxXEdge];
        [self.window makeFirstResponder:self.nameField];
        return NO;
    }

    return YES;
}

@end
