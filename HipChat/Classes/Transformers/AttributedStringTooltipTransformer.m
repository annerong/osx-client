//
//  AttributedStringTooltipTransformer.m
//  HipChat
//
//  Created by Christopher Rivers on 4/11/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "AttributedStringTooltipTransformer.h"

@implementation AttributedStringTooltipTransformer

+ (Class)transformedValueClass
{
    return [NSAttributedString self];
}

+ (BOOL)allowsReverseTransformation
{
    return NO;
}

// Return a path string for the given image name
- (id)transformedValue:(id)beforeObject
{
    if (beforeObject == nil) return nil;
    return ((NSAttributedString *)beforeObject).string;
}



@end
