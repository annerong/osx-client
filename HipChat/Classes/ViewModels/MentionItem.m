//
//  MentionItem.m
//  HipChat
//
//  Created by Christopher Rivers on 11/28/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "MentionItem.h"

#import "RegexKitLite.h"
#import "RoomOccupant.h"
#import "User.h"

#import "HipChatOSXApp.h"
#import "OSXHelpers.h"

@implementation MentionItem

@dynamic matchRating;

- (NSInteger)matchRating {
    NSInteger rating = (self.isFirstNameMatch ? 2 : 0) +
    (self.isInRoom ? 4 : 0) +
    (self.isMentionMatch ? 3 : 0) +
    (self.isSelf ? -5 : 0);
    return rating;
}

- (id)initWithUser:(User *)user {
    self = [super init];
    if (self) {
        self.label = [[NSMutableAttributedString alloc]
                      initWithString:[NSString stringWithFormat:@"%@ (@%@)", user.name, user.mentionName]];
        self.mention = user.mentionName;
        self.icon = [OSXHelpers getStatusIconNameForUser:user];
        self.isFirstNameMatch = NO;
        self.isInRoom = NO;
        self.isMentionMatch = NO;
        self.isSelf = NO;
        self.isMatch = YES;
    }
    return self;
}

- (id)initWithOccupant:(RoomOccupant *)occupant {
    self = [super init];
    if (self) {
        NSString *mentionName = occupant.mentionName;
        if (!mentionName) {
            User *user = [HipChatOSXApp.instance getUserInfo:occupant.jid];
            mentionName = user.mentionName;
        }
        self.label = [[NSMutableAttributedString alloc]
                      initWithString:[NSString stringWithFormat:@"%@ (@%@)", occupant.nickname, mentionName]];
        self.mention = mentionName;
        self.icon = [OSXHelpers getStatusIconNameForOccupant:occupant];
        self.isFirstNameMatch = NO;
        self.isInRoom = YES;
        self.isMentionMatch = NO;
        self.isSelf = NO;
        self.isMatch = YES;
    }
    return self;
}

- (void)checkMatchAndHighlight:(NSString *)searchStr {
    if (!searchStr || searchStr.length == 0) {
        self.isMentionMatch = NO;
        self.isFirstNameMatch = NO;
        self.isMatch = YES;
        return;
    }

    BOOL didHighlight = NO;
    NSString *mentionSearch = [NSString stringWithFormat:@"@%@", searchStr];
    NSRange mentionRange = [self.label.string rangeOfString:mentionSearch options:NSCaseInsensitiveSearch];
    if (mentionRange.location != NSNotFound) {
        self.isMentionMatch = YES;
        [self makeBoldInRange:mentionRange];
        didHighlight = YES;
    } else {
        self.isMentionMatch = NO;
    }

    NSString *regexStr = [NSString stringWithFormat:@"\\b%@", searchStr];
    NSRange matchRange = [self.label.string rangeOfRegex:regexStr
                                                 options:RKLCaseless
                                                 inRange:NSMakeRange(0, self.label.string.length)
                                                 capture:0
                                                   error:nil];
    if (matchRange.location == 0) {
        self.isFirstNameMatch = YES;
    } else {
        self.isFirstNameMatch = NO;
    }

    if (matchRange.location != NSNotFound) {
        self.isMatch = YES;
        if (!didHighlight) {
            [self makeBoldInRange:matchRange];
            didHighlight = YES;
        }
    } else {
        self.isMatch = NO;
    }

    if (!didHighlight) {
        NSDictionary *attrs =
        [NSDictionary dictionaryWithObject:[NSFont systemFontOfSize:12.0]
                                    forKey:NSFontAttributeName];
        [self.label setAttributes:attrs range:NSMakeRange(0, self.label.string.length)];
    }
}

- (void)makeBoldInRange:(NSRange)range {
    [self.label beginEditing];

    NSDictionary *unboldAttrs =
    [NSDictionary dictionaryWithObject:[NSFont systemFontOfSize:12.0]
                                forKey:NSFontAttributeName];
    [self.label setAttributes:unboldAttrs range:NSMakeRange(0, self.label.string.length)];
    NSDictionary *boldAttrs =
    [NSDictionary dictionaryWithObject:[NSFont boldSystemFontOfSize:12.0]
                                forKey:NSFontAttributeName];
    [self.label setAttributes:boldAttrs range:range];

    [self.label endEditing];
}

@end
