//
//  TableCellHighlightView.h
//  HipChat
//
//  Created by Chris Rivers on 11/25/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface TableCellHighlightView : NSTableCellView

@property BOOL isHiglighted;

@end
