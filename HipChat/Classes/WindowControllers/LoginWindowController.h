//
//  LoginWindowController.h
//  HipChat
//
//  Created by Christopher Rivers on 11/19/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>

@interface LoginWindowController : NSWindowController <NSTextFieldDelegate> {
    NSTimer *flashTimer;
}

- (void)checkForErrors;
- (void)updateCredentials;

- (IBAction)onHelpClicked:(id)sender;
- (IBAction)onRememberMeChange:(id)sender;
- (IBAction)onSignupClicked:(id)sender;
- (IBAction)submitLogin:(id)sender;

@property IBOutlet NSBox *logoBox;
@property IBOutlet NSImageView *appLogo;

@property IBOutlet NSBox *errorBox;
@property IBOutlet NSTextView *errorMessage;

@property IBOutlet NSBox *noticeBox;
@property IBOutlet WebView *noticeWebView;

@property IBOutlet NSBox *formBox;
@property IBOutlet NSTextField *emailField;
@property IBOutlet NSTextField *passwordField;
@property IBOutlet NSButton *rememberMeCheck;
@property IBOutlet NSButton *signInButton;
@property IBOutlet NSButton *signUpButton;
@property IBOutlet NSButton *helpButton;

@end
