//
//  LobbyRowView.m
//  HipChat
//
//  Created by Christopher Rivers on 10/23/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "LobbyRowView.h"

@implementation LobbyRowView

- (void)drawBackgroundInRect:(NSRect)dirtyRect {
    if (self.isHighlighted) {
        NSColor *bgColor = RGB(200, 200, 200);

        [bgColor set];
        [NSBezierPath fillRect:dirtyRect];
    }
    [super drawBackgroundInRect:dirtyRect];
}


@end
