//
//  RoomRemoveMembersWindowController.h
//  HipChat
//
//  Created by Christopher Rivers on 4/19/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "HipChatSheetController.h"

@class XMPPJID;

@interface RoomRemoveMembersWindowController : HipChatSheetController {
    NSMutableDictionary *userDict;
}

- (void)setDisabledUsers:(NSArray *)users;
- (NSArray *)selectedJids;

@property IBOutlet XMPPJID *jid;
@property IBOutlet NSTableView *permissionsTable;
@property IBOutlet NSArray *userList;

@end
