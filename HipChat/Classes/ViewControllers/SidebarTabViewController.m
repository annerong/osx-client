//
//  SidebarTabViewController.m
//  HipChat
//
//  Created by Christopher Rivers on 4/8/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "SidebarTabViewController.h"

#import "HipChatOSXApp.h"
#import "OSXHelpers.h"
#import "RightSidebarEntry.h"
#import "XMPP.h"

@implementation SidebarTabViewController

static const NSInteger BEFORE_DATE_TYPE = 0;
static const NSInteger AFTER_DATE_TYPE = 1;

- (void)awakeFromNib {
    [self.scrollView.contentView setPostsBoundsChangedNotifications:YES];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleTableBoundsChange:)
                                                 name:NSViewBoundsDidChangeNotification
                                               object:self.scrollView.contentView];

    self.entriesArrayController.automaticallyRearrangesObjects = NO;
    NSSortDescriptor *dateSort = [[NSSortDescriptor alloc] initWithKey:@"uploadDate"
                                                             ascending:NO
                                                              selector:@selector(compare:)];
    self.entriesArrayController.sortDescriptors = [NSArray arrayWithObject:dateSort];
}

- (void)clearEntries {
    allEntriesLoaded = NO;
    newestDate = nil;
    oldestDate = nil;
    NSRange range = NSMakeRange(0, [[self.entriesArrayController arrangedObjects] count]);
    [self.entriesArrayController removeObjectsAtArrangedObjectIndexes:[NSIndexSet indexSetWithIndexesInRange:range]];
}

- (void)handleTableBoundsChange:(NSNotification *)notif {
    NSRect visibleRect = self.scrollView.documentVisibleRect;
    NSSize contentSize = self.tableView.frame.size;
    NSInteger unitsToBottom = (contentSize.height - (visibleRect.origin.y + visibleRect.size.height));
    if (unitsToBottom < 10) {
        [self makeEntriesRequestWithDate:oldestDate type:BEFORE_DATE_TYPE];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        allEntriesLoaded = NO;
        requestInProgress = NO;

        self.entries = [NSMutableArray array];
    }
    return self;
}

- (RightSidebarEntry *)generateEntryWithItem:(NSXMLElement *)item {
    [NSException raise:@"MissingFunctionException"
                format:@"Entry controller must define a entry generation fucntion"];
    return nil;
}

- (NSInteger)getFetchCount {
    return 50;
}

- (NSString *)getRequestNamepsace {
    [NSException raise:@"MissingFunctionException"
                format:@"Entry controller must define a namespace for fetching"];
    return nil;
}

- (void)handleEntryClick:(id)entry {
    [[NSWorkspace sharedWorkspace] openURL:[entry url]];
}

- (void)handleFetchResponse:(XMPPIQ *)iq fetchType:(NSInteger)type {
    DDLogInfo(@"Handle IQ response for files/links");
    NSArray *items = [[iq elementForName:@"query" xmlns:[self getRequestNamepsace]] elementsForName:@"item"];
    if (!items || items.count == 0) {
        // If we go no data and there's nothing currently in the table,
        // hide the table and show the "No files/links shared yet" view
        if (self.entries.count == 0) {
            [self.noEntriesView setHidden:NO];
            [self.scrollView setHidden:YES];
        }

        if (![iq isErrorIQ] && type == BEFORE_DATE_TYPE) {
            // If we were fetching with a before date and got 0 entries,
            // mark this list as fully loaded
            allEntriesLoaded = YES;
        }
        requestInProgress = NO;
        return;
    }

    // Make sure we're showing the table of entries if we got data
    [self.noEntriesView setHidden:YES];
    [self.scrollView setHidden:NO];

    NSMutableArray *newItems = [NSMutableArray arrayWithCapacity:items.count];
    for (NSXMLElement *item in items) {
        NSString *itemId = [[item elementForName:@"id"] stringValue];
        NSInteger idx = [self.entries indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            if ([[obj entryId] isEqualToString:itemId]) {
                return YES;
                *stop = YES;
            }
            return NO;
        }];
        if (idx == NSNotFound) {
            RightSidebarEntry *entry = [self generateEntryWithItem:item];
            if (!newestDate) {
                newestDate = entry.uploadDate;
            }
            if (!oldestDate) {
                oldestDate = entry.uploadDate;
            }

            if ([oldestDate compare:entry.uploadDate] == NSOrderedDescending) {
                oldestDate = entry.uploadDate;
            } else if ([newestDate compare:entry.uploadDate] == NSOrderedAscending) {
                newestDate = entry.uploadDate;
            }

            [newItems addObject:entry];
        }
    }

    [self.entriesArrayController addObjects:newItems];
    [self.entriesArrayController rearrangeObjects];
    requestInProgress = NO;
}

- (void)makeEntriesRequestWithDate:(NSDate *)date {
    [self makeEntriesRequestWithDate:date type:-1];
}

- (void)makeEntriesRequestWithDate:(NSDate *)date type:(NSInteger)dateType {
    // Scrolling will call this function many times, but we only want one request in progress
    // Also prevent repeated requests after all items have been loaded
    if ((dateType == BEFORE_DATE_TYPE && allEntriesLoaded) || requestInProgress) {
        return;
    }
    DDLogInfo(@"Making request for sidebar entries with date: %@ (type = %ld)", date, (long)dateType);
    requestInProgress = YES;

    HipChatOSXApp *app = HipChatOSXApp.instance;
    NSString *uuid = [app.conn generateUUID];
	XMPPIQ *iq = [XMPPIQ iqWithType:@"get" to:app.currentJid elementID:uuid];
    
    NSXMLElement *queryNode = [NSXMLElement elementWithName:@"query" xmlns:[self getRequestNamepsace]];
    [queryNode addAttributeWithName:@"limit"
                        stringValue:[NSString stringWithFormat:@"%ld", [self getFetchCount]]];

    if (date) {
        if (dateType == BEFORE_DATE_TYPE) {
            [queryNode addAttributeWithName:@"before"
                                stringValue:[OSXHelpers formatXMPPDate:date]];
        } else if (dateType == AFTER_DATE_TYPE) {
            [queryNode addAttributeWithName:@"after"
                                stringValue:[OSXHelpers formatXMPPDate:date]];
        }
    }

    [iq addChild:queryNode];

    [app sendIQ:iq callback:^(XMPPIQ *iq) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self handleFetchResponse:iq fetchType:dateType];
        });
    }];
}

- (void)updateWithJid:(XMPPJID *)jid {
    allEntriesLoaded = NO;
    [self makeEntriesRequestWithDate:newestDate type:AFTER_DATE_TYPE];
}

@end
