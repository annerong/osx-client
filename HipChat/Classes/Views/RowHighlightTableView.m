//
//  RowHighlightTableView.m
//  HipChat
//
//  Created by Christopher Rivers on 10/23/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "RowHighlightTableView.h"

#import "TableRowHighlightView.h"
#import "TableCellHighlightView.h"

@implementation RowHighlightTableView

- (void)awakeFromNib {
    [[self window] setAcceptsMouseMovedEvents:YES];
    // Default to mouseOverView = YES since if the user starts with their cursor over the table
    // we won't get a mouseEntered event with which to set mouseOverView initially
    mouseOverView = YES;
    mouseOverRow = -1;
    lastOverRow = -1;
}

- (void)dealloc {
    if (trackingArea) {
        [self removeTrackingArea:trackingArea];
    }
}

- (void)updateTrackingAreas {
    if (trackingArea) {
        [self removeTrackingArea:trackingArea];
    }
    NSTrackingAreaOptions trackingOptions = (NSTrackingMouseEnteredAndExited | NSTrackingMouseMoved | NSTrackingActiveInKeyWindow | NSTrackingInVisibleRect);
    trackingArea = [[NSTrackingArea alloc] initWithRect:self.frame options:trackingOptions owner:self userInfo:nil];
    [self addTrackingArea:trackingArea];
}

- (void)mouseEntered:(NSEvent *)theEvent {
    mouseOverView = YES;
}

- (void)mouseMoved:(NSEvent *)theEvent {
    if (!mouseOverView) {
        return;
    }

    mouseOverRow = [self rowAtPoint:[self convertPoint:[theEvent locationInWindow] fromView:nil]];
    if (lastOverRow == mouseOverRow) {
        return;
    } else {
        NSTableRowView *rowView = (mouseOverRow > -1 ? [self rowViewAtRow:mouseOverRow makeIfNecessary:NO] : nil);
        if (rowView && [rowView isKindOfClass:[TableRowHighlightView class]]) {
            ((TableRowHighlightView *)rowView).isHighlighted = YES;
        }
        // lastOverRow might be > the number of rows if that row was removed before this even trigger
        // (e.g. when someone clicks the close button on the last tab in their list)
        NSTableRowView *prevRowView = (lastOverRow > -1 && lastOverRow < self.numberOfRows ? [self rowViewAtRow:lastOverRow makeIfNecessary:NO] : nil);
        if (prevRowView && [prevRowView isKindOfClass:[TableRowHighlightView class]]) {
            ((TableRowHighlightView *)prevRowView).isHighlighted = NO;
        }
        [self setNeedsDisplayInRect:[self rectOfRow:lastOverRow]];
        NSTableCellView *cellView = (mouseOverRow > -1 ? [self viewAtColumn:0 row:mouseOverRow makeIfNecessary:NO] : nil);
        if (cellView && [cellView isKindOfClass:[TableCellHighlightView class]]) {
            ((TableCellHighlightView *)cellView).isHiglighted = YES;
        }
        NSTableCellView *prevCellView = (lastOverRow > -1 && lastOverRow < self.numberOfRows ? [self viewAtColumn:0 row:lastOverRow makeIfNecessary:NO] : nil);
        if (prevCellView && [prevCellView isKindOfClass:[TableCellHighlightView class]]) {
            ((TableCellHighlightView *)prevCellView).isHiglighted = NO;
        }
        lastOverRow = mouseOverRow;
    }
    [self setNeedsDisplayInRect:[self rectOfRow:mouseOverRow]];
}

- (void)mouseExited:(NSEvent *)theEvent {
    mouseOverView = NO;
    [self setNeedsDisplayInRect:[self rectOfRow:mouseOverRow]];

    NSTableRowView *rowView = (mouseOverRow > -1 ? [self rowViewAtRow:mouseOverRow makeIfNecessary:NO] : nil);
    if (rowView && [rowView isKindOfClass:[TableRowHighlightView class]]) {
        ((TableRowHighlightView *)rowView).isHighlighted = NO;
    }
    NSTableCellView *cellView = (mouseOverRow > -1 ? [self viewAtColumn:0 row:mouseOverRow makeIfNecessary:NO] : nil);
    if (cellView && [cellView isKindOfClass:[TableCellHighlightView class]]) {
        ((TableCellHighlightView *)cellView).isHiglighted = NO;
    }

    mouseOverRow = -1;
    lastOverRow = -1;
}

- (NSInteger)mouseOverRow {
    return mouseOverRow;
}

@end
