//
//  LinksViewController.h
//  HipChat
//
//  Created by Christopher Rivers on 4/1/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "SidebarTabViewController.h"

@class LinkEntry;

@interface LinksViewController : SidebarTabViewController

@end
