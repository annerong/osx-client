//
//  ImagePreviewViewController.h
//  HipChat
//
//  Created by Christopher Rivers on 11/8/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ImagePreviewViewController : NSViewController

@property IBOutlet NSImage *image;
@property IBOutlet NSProgressIndicator *spinner;

@end
