//
//  RosterEntry.m
//  HipChat
//
//  Created by Christopher Rivers on 10/16/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "RosterEntry.h"

#import "RoomOccupant.h"
#import "User.h"
#import "XMPPPresence.h"
#import "XMPPResource.h"

#import "HipChatOSXApp.h"
#import "OSXHelpers.h"

@interface RosterEntry()

@property User *user;
@property RoomOccupant *occupant;

@end

@implementation RosterEntry

@synthesize icon;
@synthesize jid;
@synthesize mentionName;
@synthesize name;
@synthesize status;
@dynamic tooltip;

static NSTimeInterval IDLE_UPDATE_INTERVAL = 60.0;

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Init / Deinit methods
///////////////////////////////////////////////////////////////////////

- (RosterEntry *)initWithUser:(User *)initUser {
    self = [super init];
    if (self) {
        self.user = initUser;
        self.jid = self.user.jid;
        self.name = self.user.name;
        self.icon = [OSXHelpers getStatusIconNameForUser:self.user];
        self.isAdmin = NO;

        [self updateProperties];
        [self.user addObserver:self forKeyPath:@"lastPresence" options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}

- (RosterEntry *)initWithOccupant:(RoomOccupant *)initOccupant {
    self = [super init];
    if (self) {
        self.occupant = initOccupant;
        self.user = [HipChatApp.instance getUserInfo:self.occupant.jid];
        self.jid = self.occupant.jid;
        self.isAdmin = self.occupant.isAdmin;

        [self updateProperties];

        // Observe both the room presence (to tell when someone leaves the room)
        // and the user presence (for away/idle/dnd/active state)
        [self.occupant addObserver:self forKeyPath:@"lastPresence" options:NSKeyValueObservingOptionNew context:nil];
        [self.user addObserver:self forKeyPath:@"lastPresence" options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}

- (void)deinitialize {
    [self removeObservers];
}

- (void)dealloc {
    [self removeObservers];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    // Trigger property updates on the main queue, since they will trigger UI updates
    if (dispatch_get_current_queue() == dispatch_get_main_queue()) {
        [self updateProperties];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateProperties];
        });
    }
}

- (void)removeObservers {
    if (self.occupant) {
        [self.occupant removeObserver:self forKeyPath:@"lastPresence"];
        self.occupant = nil;
    }
    if (self.user) {
        [self.user removeObserver:self forKeyPath:@"lastPresence"];
        self.user = nil;
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Helper methods
///////////////////////////////////////////////////////////////////////

- (NSString *)generateStatusString:(XMPPPresence *)presence {
    NSInteger idleSeconds = [OSXHelpers getIdleSeconds:presence];
    NSString *idleString = [OSXHelpers getIdleString:idleSeconds];
    NSString *statusMessage = [presence status];
    NSString *separator = (idleString.length > 0 && statusMessage && statusMessage.length > 0 ? @" - " : @"");

    if (!idleUpdateTimer && idleSeconds > 0) {
        idleUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:IDLE_UPDATE_INTERVAL
                                                           target:self
                                                         selector:@selector(updateIdle)
                                                         userInfo:nil
                                                          repeats:YES];
    } else if (idleSeconds <= 0) {
        [idleUpdateTimer invalidate];
        idleUpdateTimer = nil;
    }

    return [NSString stringWithFormat:@"%@%@%@", idleString, separator, (statusMessage ? statusMessage : @"")];
}

// TODO: Remove this once we decide to not use a single-line display for status/idle
//- (NSAttributedString *)label {
////    NSString *html = [NSString stringWithFormat:@"%@ <font color='#999'>11m</font>", self.name];
////    return [[NSAttributedString alloc] initWithHTML:[html dataUsingEncoding:NSUnicodeStringEncoding] documentAttributes:nil];
//    NSMutableAttributedString *label = [[NSMutableAttributedString alloc] initWithString:
//                                        [NSString stringWithFormat:@"%@%@%@", self.name, @" 11m", (self.status.length == 0 ? @"\nstatus" : @"")]];
//    NSRange colorRange = NSMakeRange(self.name.length + 1, label.length - self.name.length - 1);
//    [label addAttribute:NSForegroundColorAttributeName value:[NSColor lightGrayColor] range:colorRange];
//    return label;
////    return [NSString stringWithFormat:@"%@ - %@", self.name, @"11m"];
//}

- (NSString *)tooltip {
    return [NSString stringWithFormat:@"%@\n@%@%@%@%@",
            self.name,
            self.mentionName,
            [NSString stringWithFormat:@"\nStatus: %@",
             (self.occupant ?
              [OSXHelpers getFriendlyStatusForPresence:self.occupant.lastPresence mobile:nil includeIdle:NO] :
              [OSXHelpers getFriendlyStatusForUser:self.user includeIdle:NO])],
            (self.user.lastPresence.status ? [NSString stringWithFormat:@"\nMessage: %@", self.user.lastPresence.status] : @""),
            (self.occupant && self.occupant.isAdmin ? @"\nAdmin" :
             (self.occupant && [self.occupant.role isEqualToString:VISITOR_ROLE] ? @"\nGuest" :
              @""))];
}

- (void)updateIdle {
    XMPPPresence *presence = (self.user ? self.user.lastPresence : [self.occupant lastPresence]);
    self.status = [self generateStatusString:presence];
    [self willChangeValueForKey:@"tooltip"];
    [self didChangeValueForKey:@"tooltip"];
}

- (void)updateProperties {
    [self willChangeValueForKey:@"intShow"];

    if (self.user) {
        self.mentionName = self.user.mentionName;
    }

    XMPPPresence *userPresence = self.user.lastPresence;
    if (self.occupant) {
        XMPPPresence *roomPresence = self.occupant.lastPresence;
        // Always use user.name as the source of truth for name
        // Fallback to occupant.nickname if no user is available
        // (e.g. for guest users, which won't show up in the overall roster)
        self.name = (self.user ? self.user.name : self.occupant.nickname);

        if ([roomPresence.type isEqualToString:@"available"]) {
            // User is in the room - generate presence based on user data if available
            if (self.user) {
                self.status = [self generateStatusString:userPresence];
                self.icon = [OSXHelpers getStatusIconNameForUser:self.user];
                intShow = userPresence.intShow;
            } else {
                // Guest user
                self.status = [self generateStatusString:roomPresence];
                self.icon = [OSXHelpers getStatusIconNameForOccupant:self.occupant];
                intShow = roomPresence.intShow;
            }
        } else {
            // User is not active in the room (may still be online in the group though)
            self.icon = [OSXHelpers getStatusIconNameForOccupant:self.occupant];
            self.status = @"";
            intShow = -1;
        }

    } else if (self.user) {
        // Lobby roster entry
        self.name = self.user.name;

        self.status = [self generateStatusString:userPresence];
        self.icon = [OSXHelpers getStatusIconNameForUser:self.user];
        intShow = ([userPresence.type isEqualToString:@"available"] ? userPresence.intShow : -1);
    }
    [self didChangeValueForKey:@"intShow"];
}

@end
