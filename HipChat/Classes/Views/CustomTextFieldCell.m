//
//  CustomTextFieldCell.m
//  HipChat
//
//  Created by Christopher Rivers on 5/14/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "CustomTextFieldCell.h"

@implementation CustomTextFieldCell

static CGFloat CORNER_RADIUS = 4.0;

- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView {
    NSBezierPath *betterBounds = [NSBezierPath bezierPathWithRoundedRect:cellFrame xRadius:CORNER_RADIUS yRadius:CORNER_RADIUS];
    [betterBounds addClip];
    [super drawWithFrame:cellFrame inView:controlView];
    if (self.isBezeled) {
        [betterBounds setLineWidth:2];
        [[NSColor blackColor] setStroke];
        [betterBounds stroke];
    }
}

- (NSRect)adjustedFrameToVerticallyCenterText:(NSRect)frame {
    // super would normally draw text at the top of the cell
    NSInteger offset = floor((NSHeight(frame) -
                              ([[self font] ascender] - [[self font] descender])) / 2);
    // It's not clear that the proper calculation is here.
    // Using these values shows text properly at Lucida Grande 11 on a 28px high text field
    return NSMakeRect(frame.origin.x, frame.origin.y + (offset/2)+1, NSWidth(frame), NSHeight(frame)-(offset/2));
}

- (void)editWithFrame:(NSRect)aRect inView:(NSView *)controlView
               editor:(NSText *)editor delegate:(id)delegate event:(NSEvent *)event {
    [super editWithFrame:[self adjustedFrameToVerticallyCenterText:aRect]
                  inView:controlView editor:editor delegate:delegate event:event];
}

- (void)selectWithFrame:(NSRect)aRect inView:(NSView *)controlView
                 editor:(NSText *)editor delegate:(id)delegate
                  start:(NSInteger)start length:(NSInteger)length {

    [super selectWithFrame:[self adjustedFrameToVerticallyCenterText:aRect]
                    inView:controlView editor:editor delegate:delegate
                     start:start length:length];
}

- (void)drawInteriorWithFrame:(NSRect)frame inView:(NSView *)view {
    [super drawInteriorWithFrame:
     [self adjustedFrameToVerticallyCenterText:frame] inView:view];
}

@end
