//
//  RosterViewController.h
//  HipChat
//
//  Created by Christopher Rivers on 10/17/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "OnOffSwitchControl.h"
#import "OnOffSwitchControlCell.h"

@class PrettyBorderBox;
@class Room;

@interface RosterViewController : NSViewController <NSTableViewDelegate, NSTextFieldDelegate> {
    Room *currentRoom;
    BOOL isPresenceLoaded;
    NSTimer *rearrangeTimer;
    NSMutableArray *entries;
}

//- (IBAction)onAddUsersClick:(id)sender;
- (IBAction)onGuestAccessToggle:(id)sender;
- (IBAction)onGuestAccessURLClick:(id)sender;
- (IBAction)onRosterDoubleClick;
- (IBAction)onSearchRoster:(id)sender;
- (IBAction)onSortByName:(id)sender;
- (IBAction)onSortByStatus:(id)sender;

@property IBOutlet NSArrayController *arrayController;
@property IBOutlet NSMutableArray *entries;
@property IBOutlet NSScrollView *scrollView;
@property IBOutlet PrettyBorderBox *searchBox;
@property IBOutlet NSSearchField *searchField;
@property IBOutlet PrettyBorderBox *sortBox;
@property IBOutlet NSPopUpButton *sortButton;
@property NSArray *sortDescriptors;
@property IBOutlet NSTableView *tableView;

// Guest Access
@property IBOutlet OnOffSwitchControl *guestAccessToggle;
@property (strong) IBOutlet OnOffSwitchControlCell *guestAccessToggleCell;
@property (strong) IBOutlet NSTextField *guestAccessToggleLabel;
@property IBOutlet PrettyBorderBox *guestAccessToggleBox;
@property IBOutlet NSButton *guestAccessUrl;
@property IBOutlet PrettyBorderBox *guestAccessURLBox;

@end
