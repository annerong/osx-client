//
//  AutoCompleteViewController.h
//  HipChat
//
//  Created by Christopher Rivers on 11/26/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AutoCompleteViewController : NSViewController <NSTableViewDataSource, NSTableViewDelegate> {
    NSMutableArray *mentionList;
}

- (NSInteger)getMatchCount;
- (IBAction)onAutocompleteClick:(id)sender;
- (void)updateMentionListWithSearch:(NSString *)searchStr;

@property NSObject *delegate;

@property IBOutlet NSPredicate *filterPredicate;
@property IBOutlet NSMutableArray *mentionList;
@property IBOutlet NSArrayController *mentionListArrayController;
@property IBOutlet NSArray *sortDescriptors;
@property IBOutlet NSTableView *tableView;

@end
