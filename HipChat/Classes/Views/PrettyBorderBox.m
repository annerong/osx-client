//
//  PrettyBorderBox.m
//  HipChat
//
//  Created by Christopher Rivers on 11/15/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "PrettyBorderBox.h"

@implementation PrettyBorderBox

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];

    [[NSGraphicsContext currentContext] setShouldAntialias:NO];

    // LEFT BORDER
    if (self.leftBorderColor) {
        [self.leftBorderColor set];
        NSBezierPath *borderPath = [NSBezierPath bezierPath];
        [borderPath moveToPoint:NSMakePoint(0, 0)];
        [borderPath lineToPoint:NSMakePoint(0, self.bounds.size.height)];
        [borderPath stroke];
    }
    if (self.leftBorderInsetColor) {
        [self.leftBorderInsetColor set];
        NSBezierPath *borderPath = [NSBezierPath bezierPath];
        [borderPath moveToPoint:NSMakePoint(1, 0)];
        [borderPath lineToPoint:NSMakePoint(1, self.bounds.size.height)];
        [borderPath stroke];
    }

    // RIGHT BORDER
    if (self.rightBorderColor) {
        [self.rightBorderColor set];
        NSBezierPath *borderPath = [NSBezierPath bezierPath];
        [borderPath moveToPoint:NSMakePoint(self.bounds.size.width, 0)];
        [borderPath lineToPoint:NSMakePoint(self.bounds.size.width, self.bounds.size.height)];
        [borderPath stroke];
    }
    if (self.rightBorderInsetColor) {
        [self.rightBorderInsetColor set];
        NSBezierPath *borderPath = [NSBezierPath bezierPath];
        [borderPath moveToPoint:NSMakePoint(self.bounds.size.width-1, 0)];
        [borderPath lineToPoint:NSMakePoint(self.bounds.size.width-1, self.bounds.size.height)];
        [borderPath stroke];
    }

    // TOP BORDER
    if (self.topBorderColor) {
        [self.topBorderColor set];
        NSBezierPath *borderPath = [NSBezierPath bezierPath];
        [borderPath moveToPoint:NSMakePoint(0, self.bounds.size.height-1)];
        [borderPath lineToPoint:NSMakePoint(self.bounds.size.width, self.bounds.size.height-1)];
        [borderPath stroke];
    }
    if (self.topBorderInsetColor) {
        [self.topBorderInsetColor set];
        NSBezierPath *borderPath = [NSBezierPath bezierPath];
        [borderPath moveToPoint:NSMakePoint(0, self.bounds.size.height-2)];
        [borderPath lineToPoint:NSMakePoint(self.bounds.size.width, self.bounds.size.height-2)];
        [borderPath stroke];
    }

    // BOTTOM BORDER
    if (self.bottomBorderColor) {
        [self.bottomBorderColor set];
        NSBezierPath *borderPath = [NSBezierPath bezierPath];
        [borderPath moveToPoint:NSMakePoint(0, 0)];
        [borderPath lineToPoint:NSMakePoint(self.bounds.size.width, 0)];
        [borderPath stroke];
    }
    if (self.bottomBorderInsetColor) {
        [self.bottomBorderInsetColor set];
        NSBezierPath *borderPath = [NSBezierPath bezierPath];
        [borderPath moveToPoint:NSMakePoint(0, 1)];
        [borderPath lineToPoint:NSMakePoint(self.bounds.size.width, 1)];
        [borderPath stroke];
    }
}

@end
