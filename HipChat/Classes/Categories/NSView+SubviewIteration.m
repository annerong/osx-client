//
//  NSView+SubviewIteration.m
//  HipChat
//
//  Created by Christopher Rivers on 2/13/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "NSView+SubviewIteration.h"

@implementation NSView (SubviewIteration)

- (NSArray *)allSubviews {
    NSMutableArray *combinedSubviews = [NSMutableArray array];
    for (NSView *subview in self.subviews) {
        [combinedSubviews addObjectsFromArray:[subview selfAndSubviews]];
    }
    return combinedSubviews;
}

- (NSArray *)selfAndSubviews {
    NSMutableArray *combinedSubviews = [NSMutableArray arrayWithObject:self];
    for (NSView *subview in self.subviews) {
        [combinedSubviews addObjectsFromArray:[subview selfAndSubviews]];
    }
    return combinedSubviews;
}

@end
